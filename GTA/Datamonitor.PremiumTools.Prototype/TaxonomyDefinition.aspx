<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaxonomyDefinition.aspx.cs" Inherits="Datamonitor.PremiumTools.Prototype.TaxonomyDefinition" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Taxonomy Definition</title>
         
    <link href="~/assets/css/treeview.css" rel="stylesheet" type="text/css"/>
    <link href="~/assets/css/gridStyle.css" rel="stylesheet" type="text/css"/>
     <link href="~/assets/css/datamonitor.css" rel="stylesheet" type="text/css"/>
</head>
<body style="background-color:White;background-image:none">
    <form id="form1" runat="server">
    <div style="font-size: 11px;">
    <p><b><asp:Label ID="Indicator" runat="server"></asp:Label></b></p>
    <p><asp:Label ID="Definition" runat="server"></asp:Label></p>
    
    </div>
    </form>
</body>
</html>
