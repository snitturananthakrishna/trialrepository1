<%@ Page Language="C#" AutoEventWireUp="true" %>
<%@ import Namespace="System.Threading" %>
<%@ import Namespace="System.IO" %>
<%@ import Namespace="ComponentArt.Web.UI" %>
<%@ import Namespace="System.Data" %>
<%@ import Namespace="System.Collections" %>
<%@ import Namespace="System.Collections.Generic" %>
<%@ import Namespace="System.Data.OleDb" %>
<%@ import Namespace="Datamonitor.PremiumTools.Prototype.Library.SqlDataAccess" %>
<%@ import Namespace="Datamonitor.PremiumTools.Prototype.Library" %>
<% Response.ContentType = "text/xml"; %>
<script language="C#" runat="server">
    void Page_Load(Object sender,EventArgs e)
    {
      ComponentArt.Web.UI.TreeView TreeView1 = new ComponentArt.Web.UI.TreeView();

      string expandedTaxonomy = Request.QueryString["id"];
      
      DataTable dtTaxonomy;
      dtTaxonomy = SqlDataService.GetTaxonomyForParent( Convert.ToInt32(expandedTaxonomy) );
      if(dtTaxonomy !=null && dtTaxonomy.Rows.Count>0)
      {
          string TaxonomyTypeID = dtTaxonomy.Rows[0]["taxonomytypeid"].ToString();
          //Get current selections from sessionstate
          Dictionary<int, string> CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>(TaxonomyTypeID.ToString());
        
          foreach (DataRow dbRow in dtTaxonomy.Rows)
          {
                  ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["taxonomyID"].ToString(), 
                      dbRow["name"].ToString(),
                      dbRow["DisplayName"].ToString(),
                      "", 
                      false, 
                      Convert.ToBoolean(dbRow["ShowCheckBox"].ToString()),
                      false) ;
                  
                  if (!Convert.ToBoolean(dbRow["isLeafNode"].ToString()))
                  {
                      newNode.ContentCallbackUrl = "TaxonomySubtreeXml.aspx?id=" + dbRow["taxonomyID"].ToString();
                  }
                  
                  if (CurrentSelections != null && CurrentSelections.ContainsKey(Convert.ToInt32(dbRow["taxonomyid"])))
                  {
                      newNode.Checked = true;
                  }
                  else
                  {
                      newNode.Checked = false;
                  }
                  TreeView1.Nodes.Add(newNode);
          }
      } 
      Response.Write(TreeView1.GetXml());  
    }
    private ComponentArt.Web.UI.TreeViewNode CreateNode(string nodeID,
        string nodeText,
        string nodeValue, 
        string imageurl, 
        bool expanded, 
        bool displayCheckBox, 
        bool IsChecked)
    {
        ComponentArt.Web.UI.TreeViewNode node = new ComponentArt.Web.UI.TreeViewNode();
        node.Text = nodeText;
        node.ID = nodeID;
        node.Value = nodeValue;
        if (displayCheckBox)
        {
            node.ShowCheckBox = true;
            if (IsChecked)
                node.Checked = true;
        }
        node.Expanded= expanded;
        return node;
    }



</script>
