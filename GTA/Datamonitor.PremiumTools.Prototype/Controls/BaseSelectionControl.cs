using System;
using Datamonitor.PremiumTools.Prototype.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Prototype.Library;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Web;

namespace Datamonitor.PremiumTools.Prototype.Controls
{
    public class BaseSelectionControl : System.Web.UI.UserControl
    {
        // PRIVATE FIELDS
        private string _taxonomyType;
        private int _taxonomyTypeID;
        private string _source;
        private string _fullTaxonomyTypeID;
        private string _parentIDs;
        private bool _showTaxonomyDefinition;
        private SiteSettings _currentSite;

        public SiteSettings CurrentSite
        {
            get { return _currentSite; }
            set { _currentSite = value; }
        }

        // PUBLIC PROPERTIES

        /// <summary>
        /// Gets or sets the type of the taxonomy.
        /// </summary>
        /// <value>The type of the taxonomy.</value>
        public string TaxonomyType
        {
            get { return _taxonomyType; }
            set { _taxonomyType = value; }
        }


        /// <summary>
        /// Gets or sets the full taxonomy ID.
        /// </summary>
        /// <value>The full taxonomy ID.</value>
        public string FullTaxonomyTypeID
        {
            get { return _fullTaxonomyTypeID; }
            set { _fullTaxonomyTypeID = value; }
        }

        /// <summary>
        /// Gets or sets the parent Ids.
        /// </summary>
        /// <value>The parent Ids.</value>
        public string ParentIDs
        {
            get { return _parentIDs; }
            set { _parentIDs = value; }
        }  

        /// <summary>
        /// Gets or sets the taxonomy type ID.
        /// </summary>
        /// <value>The taxonomy type ID.</value>
        public int TaxonomyTypeID
        {
            get { return _taxonomyTypeID; }
            set { _taxonomyTypeID = value; }
        }

        public virtual void LoadData()
        {

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (HttpContext.Current.Request.QueryString["site"] != null)
            {
                _currentSite = new SiteSettings(HttpContext.Current.Request.QueryString["site"].ToString());
            }
        }

        /// <summary>
        /// Gets or sets the source of event.
        /// </summary>
        /// <value>The source of event.</value>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        /// <summary>
        /// Gets or sets the value to show taxonomy definition or not.
        /// </summary>
        /// <value>The value to show taxonomy definition or not.</value>
        public bool ShowTaxonomyDefinition
        {
            get { return _showTaxonomyDefinition; }
            set { _showTaxonomyDefinition = value; }
        }

        /// <summary>
        /// Gets the filter criteria from session.
        /// </summary>
        /// <returns></returns>
        protected string GetFilterCriteriaFromSession()
        {
            string SelectionsXML = string.Empty;
            DataSet TaxonomyTypes = CurrentSite.GetTaxonomyTypes();
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
                {
                    Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                    if (selectedIDsList != null && selectedIDsList.Count > 0)
                    {
                        //Get all keys (ids) from dictionary
                        List<int> keys = new List<int>(selectedIDsList.Keys);
                        //convert int array to string array
                        string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                        //Build selections xml
                        SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                            taxonomyRow["ID"].ToString(),
                            string.Join(",", SelectedIDs),
                            taxonomyRow["ColumnRowID"].ToString());

                    }
                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }
            return SelectionsXML;
        }
    }
}

