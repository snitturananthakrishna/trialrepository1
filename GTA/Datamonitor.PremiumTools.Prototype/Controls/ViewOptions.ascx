<%@ Control Language="C#" 
    AutoEventWireup="true" 
    CodeBehind="ViewOptions.ascx.cs" 
    Inherits="Datamonitor.PremiumTools.Prototype.Controls.ViewOptions" %>

    <div id="database_tabs_bar">
        <ul>
            <li class="title">View Options</li>
            <li id="View1LI" runat="server">
                <asp:HyperLink ID="View1Link" runat="server" NavigateUrl="#">View 1</asp:HyperLink>
            </li>
            <li id="View2LI" runat="server">
                <asp:HyperLink ID="View2Link" runat="server" NavigateUrl="#">View 2</asp:HyperLink>
            </li>
            <li id="CountryComparisonLI" runat="server">
                <asp:HyperLink ID="CountryComparisonLink" runat="server" NavigateUrl="#">Country Comparison</asp:HyperLink>
            </li>
            <li id="CountryOverviewLI" runat="server">
                <asp:HyperLink ID="CountryOverviewLink" runat="server" NavigateUrl="#">Country Overview</asp:HyperLink>
            </li>
        </ul>
    </div>