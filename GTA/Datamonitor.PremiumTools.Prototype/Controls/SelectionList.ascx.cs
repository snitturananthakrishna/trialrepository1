using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Prototype.Library;

namespace Datamonitor.PremiumTools.Prototype.Controls
{
    public partial class SelectionList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {               
                //Load the selections from session
                LoadSelections();
            }
        }
        private void LoadSelections()
        {
            if (HttpContext.Current.Request.QueryString["site"] != null)
            {
                SiteSettings CurrentSite = new SiteSettings(HttpContext.Current.Request.QueryString["site"].ToString());

                //Get existing taxonomy types
                DataSet TaxonomyTypes = CurrentSite.GetTaxonomyLinks();
                StringBuilder Selections = new StringBuilder();
                string TaxonomyTypeID = "";
                string TaxonomyType = "";

                Selections.Append("<script language='javascript' type='text/javascript'>");
                foreach (DataRow Row in TaxonomyTypes.Tables[0].Rows)
                {
                    TaxonomyTypeID = Row["FullID"].ToString();
                    TaxonomyType = Row["Name"].ToString();
                    if (Row["type"].ToString() != "static")
                    {
                        //Get current selections from sessionstate
                        Dictionary<int, string> CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>(TaxonomyTypeID);
                        if (CurrentSelections != null && CurrentSelections.Count > 0)
                        {
                            // Loop over pairs with foreach
                            foreach (KeyValuePair<int, string> TaxonomySelection in CurrentSelections)
                            {
                                string script = string.Format("addToSelection('{0}','{1}','{2}','{3}','false');",
                                    TaxonomyTypeID,
                                     Microsoft.JScript.GlobalObject.escape(TaxonomyType),
                                    TaxonomySelection.Key,
                                    Microsoft.JScript.GlobalObject.escape(TaxonomySelection.Value));
                                Selections.Append(script);
                            }
                        }
                    }
                }

                Selections.Append("</script>");
                ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "loadSelections", Selections.ToString(), false);
            }
        }
    }
}