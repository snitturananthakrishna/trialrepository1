using System;
using System.Web;
using System.Web.Caching;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using Datamonitor.PremiumTools.Prototype.Library;

namespace Datamonitor.PremiumTools.Prototype.Library.SqlDataAccess
{
    public class SqlDataService : DataServiceBase
    {
        private const string GETTAXONOMY_PROC_NAME = "getTaxonomy";        
        private const string ADDUSERLOGGINGDETAILS_PROC_NAME = "AddUserLoggingDetails";
        private const string GETSUBTAXONOMYTYPEID_PROC_NAME = "USP_GetSubTaxonomyTypeID";        

        public SqlDataService() : base() { }

        /// <summary>
        /// Gets the taxonomy list.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="source">The source page.</param>
        /// <returns></returns>
        public static DataSet GetTaxonomyList(int taxonomyTypeID, string source, string path, string siteName)
        {
            DataSet retValue = null;
            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath(path));
            string xPathExpression = "//TaxonomyType[@ID='" + taxonomyTypeID + "']";
            XmlElement xaxisItem = (XmlElement)doc.SelectSingleNode(xPathExpression);
            string DependentTaxonomytype = xaxisItem.Attributes["DependentTaxonomytype"].Value;

            if (!string.IsNullOrEmpty(DependentTaxonomytype) && !DependentTaxonomytype.Equals("None", StringComparison.CurrentCultureIgnoreCase))
            {
                string taxonomyIDs = string.Empty;
                Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(DependentTaxonomytype);
                if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                {
                    //Get all keys (ids) from dictionary
                    List<int> keys = new List<int>(selectedIDsList.Keys);
                    //convert int array to string array
                    string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                    taxonomyIDs = string.Format(",{0},", string.Join(",", SelectedIDs));
                    retValue = GetDependentTaxonomyByTypeID(taxonomyTypeID, taxonomyIDs, source, siteName);
                }
                else
                {
                    retValue = GetOriginalTaxonomyByTypeID(taxonomyTypeID, source, siteName);
                }

            }
            else
            {
                retValue = GetOriginalTaxonomyByTypeID(taxonomyTypeID, source, siteName);
            }

            return retValue;

        }

        /// <summary>
        /// Gets the dependent taxonomy by type ID.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="taxonomyIDs">The taxonomy Ids.</param>
        /// <param name="source">The source page.</param>
        /// <returns></returns>
        private static DataSet GetDependentTaxonomyByTypeID(int taxonomyTypeID,
            string taxonomyIDs,
            string source,
            string siteName)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", taxonomyTypeID));
            sqlParams.Add(new SqlParameter("@selectedIDs", taxonomyIDs));
            sqlParams.Add(new SqlParameter("@source", source));
            sqlParams.Add(new SqlParameter("@siteName", siteName));
            object retValue = null;
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetTaxonomyByDependency", sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the original taxonomy by type ID.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="source">The source page.</param>
        /// <returns></returns>
        public static DataSet GetOriginalTaxonomyByTypeID(int taxonomyTypeID, string source,string siteName)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", taxonomyTypeID));
            sqlParams.Add(new SqlParameter("@source", source));
            sqlParams.Add(new SqlParameter("@siteName", siteName)); 
            object retValue = null;
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, GETTAXONOMY_PROC_NAME, sqlParams.ToArray());
            return ds;
        }

        /// <summary>
        /// Gets the taxonomy for selection.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="parentIDs">The parent Ids.</param>
        /// <returns></returns>
        public static DataSet GetTaxonomyForSelection(int taxonomyTypeID, string parentIDs, string siteName)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();
            sqlParams.Add(new SqlParameter("@taxonomyTypeID", taxonomyTypeID));
            sqlParams.Add(new SqlParameter("@taxonomyIds", parentIDs));
            sqlParams.Add(new SqlParameter("@siteName", siteName));
            object retValue = null;
            DataSet ds = SqlHelper.ExecuteDataset(ConnectionString, "GetTaxonomyForSelection", sqlParams.ToArray());
            return ds;
        }   
                
        /// <summary>
        /// 
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Site"></param>
        /// <param name="Event"></param>
        /// <param name="Product_ID"></param>
        /// <param name="AdditionalDetails"></param>
        /// <param name="IPAddress"></param>
        /// <param name="usrType"></param>
        public static void addUserLoggingDetails(string UserID, 
                 string SiteName, 
                 string Event, 
                 string DealID, 
                 string AdditionalDetails, 
                 string IPAddress, 
                 string userType)
        {
            List<SqlParameter> sqlParams = new List<SqlParameter>();           
            sqlParams.Add(new SqlParameter("@UserID", UserID));
            sqlParams.Add(new SqlParameter("@SiteName", SiteName));
            sqlParams.Add(new SqlParameter("@Event", Event));
            sqlParams.Add(new SqlParameter("@DealID", DealID));
            sqlParams.Add(new SqlParameter("@AdditionalDetails", AdditionalDetails));
            sqlParams.Add(new SqlParameter("@IPAddress", IPAddress));
            sqlParams.Add(new SqlParameter("@userType", userType));

            SqlHelper.ExecuteScalar(ConnectionString, CommandType.StoredProcedure, ADDUSERLOGGINGDETAILS_PROC_NAME,sqlParams.ToArray());
         }

         /// <summary>
         /// Gets the taxonomy definition.
         /// </summary>
         /// <param name="taxonomyID">The taxonomy ID.</param>
         /// <returns></returns>
         public static DataSet GetTaxonomyDefinition(int taxonomyID)
         {
             List<SqlParameter> sqlParams = new List<SqlParameter>();
             sqlParams.Add(new SqlParameter("@taxonomyID", taxonomyID));

             DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                 CommandType.StoredProcedure,
                 "GetTaxonomyDefinition",
                 sqlParams.ToArray());

             return ds;
         }

         /// <summary>
         /// 
         /// </summary>
         /// <param name="taxonomyID"></param>
         /// <returns></returns>
         public static DataSet GetSubTaxonomyTypeID(long taxonomyID)
         {
             List<SqlParameter> sqlParams = new List<SqlParameter>();
             sqlParams.Add(new SqlParameter("@taxonomyID", taxonomyID));

             DataSet ds = SqlHelper.ExecuteDataset(ConnectionString,
                 CommandType.StoredProcedure,
                 GETSUBTAXONOMYTYPEID_PROC_NAME,
                 sqlParams.ToArray());

             return ds;
         }
    }
}
