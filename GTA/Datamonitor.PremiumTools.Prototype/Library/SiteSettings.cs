using System;
using System.Data;
using System.Xml;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Configuration;

namespace Datamonitor.PremiumTools.Prototype.Library
{
    public class SiteSettings
    {
        private string _siteKey;
        private int _siteID;

        public SiteSettings(string siteKey)
        {
            _siteKey = siteKey;
        }

        public string SiteKey
        {
            get { return _siteKey; }
            set { _siteKey = value; }
        }        

        public int SiteID
        {
            get { return _siteID; }
            set { _siteID = value; }
        }

        public DataSet GetTaxonomyTypes()
        {
            DataSet TaxonomyTypes = new DataSet();

            string path = string.Format("~/Config/{0}/Taxonomy.xml", SiteKey);
            //Read valid taxonomy types information from config file
            TaxonomyTypes.ReadXml(HttpContext.Current.Server.MapPath(path));

            return TaxonomyTypes;
        }

        public DataSet GetTaxonomyLinks()
        {
            DataSet TaxonomyTypes = new DataSet();

            string path = string.Format("~/Config/{0}/TaxonomyLinks.xml", SiteKey);
            //Read valid taxonomy types information from config file
            TaxonomyTypes.ReadXml(HttpContext.Current.Server.MapPath(path));

            return TaxonomyTypes;
        }

        public DataSet GetTaxonomyDefaults()
        {
            DataSet TaxonomyTypes = new DataSet();

            string path = string.Format("~/Config/{0}/TaxonomyDefaults.xml", SiteKey);
            //Read valid taxonomy types information from config file
            TaxonomyTypes.ReadXml(HttpContext.Current.Server.MapPath(path));

            return TaxonomyTypes;
        }

        public string TaxonomyLinksConfigPath
        {
            get { return string.Format("~/Config/{0}/TaxonomyLinks.xml", SiteKey); }
        }
        public string TaxonomyDefaultsConfigPath
        {
            get { return string.Format("~/Config/{0}/TaxonomyDefaults.xml", SiteKey); }
        }

        public string[] ValidYears
        {
            get
            {
                List<string> Years = new List<string>();

                int StartYear = Convert.ToInt32(StartYearSelectValue);
                int EndYear = Convert.ToInt32(EndYearSelectValue);

                while (EndYear >= StartYear)
                {
                    Years.Add(StartYear.ToString());
                    StartYear++;
                }

                return Years.ToArray();
            }
        }

        public string SiteTitle
        {
            get { return GetSiteSetting("Title"); }
        }


        public string GetMandatoryTaxonomyInSearch
        {
            get { return GetSiteSetting("MandatoryTaxonomyInSearch"); }
        }


        public string NotesLink
        {
            get { return GetSiteSetting("NotesLink"); }
        }


        public bool ShowNotesLink
        {
            get { return false; }
        }


        public string StartYearSelectValue
        {
            get { return GetSiteSetting("StartYearSelectValue"); }
        }

        public string EndYearSelectValue
        {
            get { return GetSiteSetting("EndYearSelectValue"); }
        }

        public bool ShowTaxonomyDefinitionInSearch
        {
            get { return Convert.ToBoolean(GetSiteSetting("ShowTaxonomyDefinitionInSearch").ToLower()); }
        }

        /// <summary>
        /// Gets the KC redirect URL.
        /// </summary>
        /// <value>The KC redirect URL.</value>
        public string KCRedirectUrl
        {
            get { return WebConfigurationManager.AppSettings.Get("KCRedirectUrl"); }
        }

        /// <summary>
        /// Gets the ovum KC redirect URL.
        /// </summary>
        /// <value>The ovum KC redirect URL.</value>
        public string OvumKCRedirectUrl
        {
            get { return WebConfigurationManager.AppSettings.Get("OvumKCRedirectUrl"); }
        }

        public string MethodologyLink
        {
            get { return GetSiteSetting("MethodologyLink"); }
        } 

        public string GetSiteSetting(string name)
        {
            XmlDocument doc = new XmlDocument();
            string path = string.Format("~/Config/{0}/Settings.config", SiteKey);
            doc.Load(HttpContext.Current.Server.MapPath(path));
            string xPathExpression = "//appSettings/add[@key='" + name + "']";
            XmlElement viewItem = (XmlElement)doc.SelectSingleNode(xPathExpression);
            string Value;
            Value = viewItem != null ? viewItem.Attributes["value"].Value : string.Empty;

            return Value;

        }
       
    }
}
