using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Web.Configuration;

namespace Datamonitor.PremiumTools.Prototype.Library
{
    /// <summary>
    /// GlobalSettings class is used to "cache" settings that are used globally and frequently
    /// and are stored in the applications settings and connectionstrings section of the web.config file.
    /// This way the web.config file does not have to be read from disk each time a "Site Wide Setting' needs to be accessed. 
    /// When changes are made to the web.config file, the appdomain will be reset so the static constructor will be run when 
    /// the application restarts and the new values will be retrieved. 
    /// </summary>
    public static class GlobalSettings
    {
        ///// <summary>
        ///// Gets the taxonomy types.
        ///// </summary>
        ///// <returns></returns>
        //public static DataSet GetTaxonomyTypes()
        //{
        //    DataSet TaxonomyTypes = new DataSet();
        //    //Read valid taxonomy types information from config file
        //    TaxonomyTypes.ReadXml(HttpContext.Current.Server.MapPath(TaxonomyTypesConfigPath));
            
        //    return TaxonomyTypes;
        //}

        ///// <summary>
        ///// Gets the taxonomy links.
        ///// </summary>
        ///// <returns></returns>
        //public static DataSet GetTaxonomyLinks()
        //{
        //    DataSet TaxonomyTypes = new DataSet();            
        //    TaxonomyTypes.ReadXml(HttpContext.Current.Server.MapPath(TaxonomyLinksConfigPath));            
        //    return TaxonomyTypes;
        //}

        ///// <summary>
        ///// Gets the taxonomy defaults.
        ///// </summary>
        ///// <returns></returns>
        //public static DataSet GetTaxonomyDefaults()
        //{
        //    DataSet TaxonomyDefaults = new DataSet();
        //    //Read valid taxonomy types information from config file
        //    TaxonomyDefaults.ReadXml(HttpContext.Current.Server.MapPath(TaxonomyDefaultsConfigPath));

        //    return TaxonomyDefaults;
        //}

        ///// <summary>
        ///// Gets the taxonomy types config path.
        ///// </summary>
        ///// <value>The taxonomy types config path.</value>
        //public static string TaxonomyTypesConfigPath
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("TaxonomyTypesConfigPath").ToString(); }
        //}

        //public static string TaxonomyLinksConfigPath
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("TaxonomyLinksConfigPath").ToString(); }
        //}

        ///// <summary>
        ///// Gets the taxonomy defaults config path.
        ///// </summary>
        ///// <value>The taxonomy defaults config path.</value>
        //public static string TaxonomyDefaultsConfigPath
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("TaxonomyDefaultsConfigPath").ToString(); }
        //}  

        ///// <summary>
        ///// Gets the predefined views config path.
        ///// </summary>
        ///// <value>The predefined views config path.</value>
        //public static string PredefinedViewsConfigPath
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("PredefinedViewsConfigPath").ToString(); }
        //}
        
        ///// <summary>
        ///// Gets the valid years.
        ///// </summary>
        ///// <value>The valid years.</value>
        //public static string[] ValidYears
        //{
        //    get
        //    {
        //        List<string> Years = new List<string>();

        //        int StartYear = Convert.ToInt32(WebConfigurationManager.AppSettings.Get("StartYearDefault"));
        //        int EndYear = Convert.ToInt32(WebConfigurationManager.AppSettings.Get("EndYearDefault"));

        //        while (EndYear >= StartYear)
        //        {
        //            Years.Add(StartYear.ToString());
        //            StartYear++;
        //        }

        //        return Years.ToArray();
        //    }
        //}        

        ///// <summary>
        ///// Gets the site title.
        ///// </summary>
        ///// <value>The site title.</value>
        //public static string SiteTitle
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("Title").ToString(); }
        //}

        ///// <summary>
        ///// Gets the mandatory taxonomy types list.
        ///// </summary>
        ///// <returns></returns>        
        //public static string GetMandatoryTaxonomyInSearch
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("MandatoryTaxonomyInSearch").ToString(); }
        //}       

        ///// <summary>
        ///// Gets the notes link.
        ///// </summary>
        ///// <value>The notes link.</value>
        //public static string NotesLink
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("NotesLink").ToString(); }
        //}

        //public static bool ShowNotesLink
        //{
        //    get { return false; }
        //}

        ///// <summary>
        ///// Gets a value indicating whether this site is SSO enabled.
        ///// </summary>
        ///// <value>
        ///// 	<c>true</c> if this site is SSO enabled; otherwise, <c>false</c>.
        ///// </value>
        //public static bool IsSSOEnabled
        //{
        //    get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("SSOEnabled").ToString().ToLower()); }
        //}

        ////// <summary>
        ////// Gets the default results page.
        ////// </summary>
        ////// <returns></returns>        
        //////public static string DefaultResultsPage
        //////{
        //////    get { return WebConfigurationManager.AppSettings.Get("DefaultResultsPage").ToString(); }
        //////}

        ///// <summary>
        ///// Gets the start year select value
        ///// </summary>
        ///// <value>The start year select value.</value>
        //public static string StartYearSelectValue
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("StartYearSelectValue").ToString(); }
        //}

        ///// <summary>
        ///// Gets the end year select value
        ///// </summary>
        ///// <value>The end year select value.</value>
        //public static string EndYearSelectValue
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("EndYearSelectValue").ToString(); }
        //}       

        ///// <summary>
        ///// Gets a value indicating whether taxonomy definition is shown in search page or not.
        ///// </summary>
        ///// <value>
        ///// 	<c>true</c> if taxonomy definition is shown in search page; otherwise, <c>false</c>.
        ///// </value>
        //public static bool ShowTaxonomyDefinitionInSearch
        //{
        //    get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowTaxonomyDefinitionInSearch").ToString().ToLower()); }
        //}        

        ///// <summary>
        ///// Gets the KC redirect URL.
        ///// </summary>
        ///// <value>The KC redirect URL.</value>
        //public static string KCRedirectUrl
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("KCRedirectUrl"); }
        //}

        ///// <summary>
        ///// Gets the ovum KC redirect URL.
        ///// </summary>
        ///// <value>The ovum KC redirect URL.</value>
        //public static string OvumKCRedirectUrl
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("OvumKCRedirectUrl"); }
        //}       

        ///// <summary>
        ///// Gets the methodology link.
        ///// </summary>
        ///// <value>The methodology link.</value>
        //public static string MethodologyLink
        //{
        //    get { return WebConfigurationManager.AppSettings.Get("MethodologyLink").ToString(); }
        //}             
    }    
}
