using System;
using System.Web;
using System.Web.UI;
using Datamonitor.PremiumTools.Prototype.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Prototype.Library
{
    /// <summary>
    /// The base page for all pages that require the user to be autheticated successfully to access.
    /// </summary>
    public class AuthenticationBasePage : BasePage
    {       
        //PUBLIC PROPERTIES

        public string KCUserID
        {
            get { return Session["KCUserId"] == null ? null : Session["KCUserId"].ToString(); }
        }

        public string SourceKC
        {
            get { return Session["SourceKC"] == null ? null : Session["SourceKC"].ToString(); }
        }

        public bool IsSharedUser
        {
            get { return Session["IsSharedUser"] == null ? true : Convert.ToBoolean(Session["IsSharedUser"].ToString().ToLower()); }
        }

        // EVENT OVERRIDES

        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);

            if (Session["KCUserId"] == null)
            {
                Session["KCUserId"] = "user1";
                Session["SourceKC"] = "auto";
                Session["IsSharedUser"] = false;
                //Server.Transfer("~/Error/Default.aspx?Code=SessionExpired");
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

       
        //PUBLIC METHODS

        public void LogUsage(string Event, 
            string productID, 
            string additionalDetails)
        {
            if (!string.IsNullOrEmpty(KCUserID))
            {
                try
                {
                    SqlDataService.addUserLoggingDetails(KCUserID,
                        SourceKC,
                        Event,
                        productID,
                        additionalDetails,
                        HttpContext.Current.Request.UserHostAddress.ToString(),
                        "");
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}
