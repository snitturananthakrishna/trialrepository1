using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Datamonitor.PremiumTools.Prototype.Logging;
using Datamonitor.PremiumTools.Prototype.Library;
using System.Collections.Generic;

namespace Datamonitor.PremiumTools.Prototype
{
    public class Global : System.Web.HttpApplication
    {
        /// <summary>
        /// Handles the Error event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Application_Error(object sender, EventArgs e)
        {
            Logger.LogError(Server.GetLastError(), Logger.LoggingType.EventLog);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Session_Start(object sender, EventArgs e)
        {
            
                Session["SourceKC"] = "ps";
           

            Session["sessionkey"] = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_Start(object sender, EventArgs e)
        {
            
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}