    var map = null;
    var geocoder = null;

    function initialize(mapCanvasId) {
      if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById(mapCanvasId));
		//map.addControl(new GSmallZoomControl());
		map.addControl(new GSmallMapControl());
		map.addControl(new GMapTypeControl());
		map.addControl(new GScaleControl());
        geocoder = new GClientGeocoder();
      }
    }

    function showAddress(address, zoomlevel) {
      if (geocoder) {
        geocoder.getLatLng(
          address,
          function(point) {
            if (!point) {
              alert(address + " not found");
            } else {
              map.setCenter(point, zoomlevel);
              map.setMapType(G_HYBRID_MAP);
            }
          }
        );
      }
    }