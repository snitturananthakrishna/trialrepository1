using System;
using System.Xml.Linq;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

using System.Configuration;
using System.IO;
using System.Net;
using Ovum.Ictop.Sams;

namespace Datamonitor.PremiumTools.Logistics
{
    public partial class Remote_SSO : System.Web.UI.Page
    {
        private string REST_ENDPOINT = ConfigurationManager.AppSettings["REST_ENDPOINT"].ToString();

        // EVENT OVERRIDES

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!IsPostBack)
            {
                Session["KCUserId"] = null;
                Session["KCUserGUID"] = null;

                Session["SAMSUser"] = null;
                Session["SAMSUserId"] = null;
                Session["SAMSUserEmail"] = null;
                Session["SAMSParentAccountId"] = null;
            }

            if ((Request.QueryString["code"] != null) && (Request.QueryString["code"] != ""))
            {
                string Card = Request.QueryString["code"];
                if (SAMSAuthenticate(Card))
                {
                    
                    string sourceKCQueryString = "telecoms";
                    if ((Request.QueryString["kc"] != null) && (Request.QueryString["kc"] != ""))
                    {
                        sourceKCQueryString = Request.QueryString["kc"].ToString();
                    }

                    Session["SourceKC"] = sourceKCQueryString;

                    //Session["IsSharedUser"] = sharedUser;

                    // TODO: Remove telecoms
                    Response.Redirect("default.aspx?kc=" + sourceKCQueryString);
                }
                else
                {
                    if (ConfigurationManager.AppSettings["KCRedirectUrl_Ovum"] != null)
                    {
                        string strOvumUrl = ConfigurationManager.AppSettings["KCRedirectUrl_Ovum"].ToString();
                        Response.Redirect(strOvumUrl, true);
                    }
                }
            }
            else
            {

                if (ConfigurationManager.AppSettings["KCRedirectUrl_Ovum"] != null)
                {
                    string strOvumUrl = ConfigurationManager.AppSettings["KCRedirectUrl_Ovum"].ToString();
                    Response.Redirect(strOvumUrl, true);
                }
            }

        }


        // PUBLIC METHODS
        public void SetUserSession(string strUserData, string sharedUser)
        {
            Session["KCUserId"] = strUserData.Split('|')[2];
            Session["SourceKC"] = strUserData.Split('|')[0].ToLower();
            Session["IsSharedUser"] = sharedUser;

            //Log user login details
            SqlDataService.addUserLoggingDetails(strUserData.Split('|')[2],
                strUserData.Split('|')[0].ToLower(),
                "login",
                string.Empty,
                string.Empty,
                Request.UserHostAddress.ToString(),                
                sharedUser);

        }

        private bool SAMSAuthenticate(string card)
        {
           try
            {
                var endpoint = ConfigurationManager.AppSettings["SAMSAthenticateLink"] + card;

                //REST/subscription_token/689148190521
                var xDoc = HttpGet(endpoint);

                var name = xDoc.GetUserFirstName();
                var lastname = xDoc.GetUserLastName();
                var username = xDoc.GetUserName();
                var accountId = xDoc.GetAccountId();
                var email = xDoc.GetUserEmail();
                var parentAccountId = xDoc.GetParentAccountId();

                Session["KCUserId"] = username;
                Session["KCUserGUID"] = username;

                Session["SAMSUser"] = name + " " + lastname;
                Session["SAMSUserId"] = accountId;
                Session["SAMSUserEmail"] = email;
                Session["SAMSParentAccountId"] = parentAccountId;

                return true;

            }
            catch
            {
                Session["KCUserId"] = null;
                Session["KCUserGUID"] = null;

                Session["SAMSUser"] = null;
                Session["SAMSUserId"] = null;
                Session["SAMSUserEmail"] = null;
                Session["SAMSParentAccountId"] = null;
                return false;
            }
        }

        private XDocument HttpGet(string url)
        {
            XDocument result = null;

            try
            {

                HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;

                if (ConfigurationManager.AppSettings["X509CertificatePath"] != null)
                {
                    using (HttpWebResponse resp = req.GetResponse() as HttpWebResponse)
                    {
                        result = GetResponseAsXmlDocument(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }


        private XDocument GetResponseAsXmlDocument(HttpWebResponse response)
        {
            var reader = new StreamReader(response.GetResponseStream());
            var result = ClearResponseString(reader.ReadToEnd());
            var doc = XDocument.Parse(result);

            return doc;
        }

        private static string ClearResponseString(string result)
        {
            result = result.Replace(string.Format("xmlns=\"{0}\"", "http://semantico.com/xmlns/sams-db"), string.Empty);
            result = result.Replace(string.Format("xmlns:x=\"{0}\"", "http://www.w3.org/1999/xlink"), string.Empty);
            result = result.Replace(string.Format("xmlns:sams=\"{0}\"", "http://semantico.com/xmlns/sams"), string.Empty);
            result = result.Replace("sams:", string.Empty);
            result = result.Replace("x:", string.Empty);
            return result;
        }
    }
}
