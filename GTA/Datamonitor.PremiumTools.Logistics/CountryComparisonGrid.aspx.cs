using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using CArt = ComponentArt.Web.UI;

namespace Datamonitor.PremiumTools.Generic
{
    public partial class CountryComparisonGrid : AuthenticationBasePage
    {
        // EVENT OVERRIDES
        protected void Page_Load(object sender, EventArgs e)
        {   
            BindControls();
            if (!IsPostBack)
            {
                DataSet ResultsData = GetResults();
                BindResultsGrid(ResultsData, CountryCombo.Text);
            }
        }

        protected void ResultsLink_Click(object sender, EventArgs e)
        {
            DataSet ResultsData = GetResults();
            BindResultsGrid(ResultsData, CountryCombo.Text);
        }

        // PRIVATE METHODS (initial population)   
        private void BindResultsGrid(DataSet ResultsData,string selectedCountries)
        {
            //clear all existing columns
            ResultsGrid.Levels[0].Columns.Clear();

            CArt.GridColumn col1 = new CArt.GridColumn();
            //add indicator column
            col1.DataField="Indicator" ;
            col1.HeadingText="Indicator";
            col1.HeadingCellCssClass="HeadingCell_Freeze";
            col1.DataCellCssClass="HeadingCell_Freeze";
            col1.DataCellClientTemplateId="CommonDataTemplate";
            ResultsGrid.Levels[0].Columns.Add(col1);
            //add chart column
            col1 = new CArt.GridColumn();
            col1.HeadingCellCssClass="HeadingCell_Freeze";
            col1.DataCellCssClass="HeadingCell_Freeze";
            col1.DataCellClientTemplateId = "TrendChartTemplate";
            ResultsGrid.Levels[0].Columns.Add(col1);

            col1 = new CArt.GridColumn();
            col1.DataField = "rowID";
            col1.Visible = false;
            ResultsGrid.Levels[0].Columns.Add(col1);

            if (ResultsData != null && ResultsData.Tables.Count > 0)
            {
                foreach (DataColumn column in ResultsData.Tables[0].Columns)
                {
                    if (selectedCountries.Contains(column.ColumnName))
                    {                         
                        col1 = new CArt.GridColumn();
                        col1.DataField = column.ColumnName;
                        col1.HeadingText = column.ColumnName;
                        col1.DataCellClientTemplateId = "TooltipTemplate";                        
                        ResultsGrid.Levels[0].Columns.Add(col1);
                    }
                }
            }
            ResultsGrid.DataSource = ResultsData;
        }

        private DataSet GetResults()
        {
            DataSet ResultsData = null;
            if (CountryCombo.Text.Trim().Length > 0)
            {
                string SelectedCountries = "[" + CountryCombo.Text.Trim().Replace(";", "],[") + "]";

                ResultsData = SqlDataService.GetCountryComparisonGridData(GetSelectionsXML(),
                    SelectedCountries,
                    hdnYear.Value);
            }
            return ResultsData;
        }

        private string GetSelectionsXML()
        {
            string SelectionsXML = string.Empty;
            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {

                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {
                    string TaxonomySelectedValue = string.Empty;
                    if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE"))
                    {
                        Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                        if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                        {
                            //Get all keys (ids) from dictionary
                            List<int> keys = new List<int>(selectedIDsList.Keys);
                            //convert int array to string array
                            string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                            TaxonomySelectedValue = string.Join(",", SelectedIDs);
                        }

                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                               taxonomyRow["ID"].ToString(),
                               TaxonomySelectedValue,
                               taxonomyRow["ColumnRowID"].ToString());
                        }
                    }
                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }

            return SelectionsXML;
        }

        private void BindControls()
        {
            DataSet IndicatorsAndCountries = SqlDataService.GetIndicatorsAndCountries(GetSelectionsXML());
            //binding Country data
            foreach (DataRow dbRow in IndicatorsAndCountries.Tables[1].Rows)
            {
                ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["Country"].ToString(),
                      dbRow["CountryID"].ToString(),
                      true,
                      true,
                      false);

                CountryTree.Nodes.Add(newNode);
            }
            SelectDefaultsForCheckBoxCombo(CountryCombo, CountryTree, hdnCountry);

            //Get list of valid years from session
            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
            int EndYear = CurrentSession.GetFromSession<int>("EndYear");

            //Start year combo
            //End year combo
            for (int counter = StartYear; counter <= EndYear; counter++)
            {                
                ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(counter.ToString(),
                      counter.ToString(),
                      false,
                      false,
                      false);

                YearTree.Nodes.Add(newNode);
            }
            if (string.IsNullOrEmpty(hdnYear.Value))
            {
                YearTree.SelectedNode = YearTree.FindNodeById(StartYear.ToString());
                hdnYear.Value = YearTree.SelectedNode.Value;
                YearCombo.Text = YearTree.SelectedNode.Value;
            }
            else
            {
                YearTree.SelectedNode = YearTree.FindNodeById(hdnYear.Value);
            }
        }

        private CArt.TreeViewNode CreateNode(string text,
            string value,
            bool expanded,
            bool hasCheckbox,
            bool isChecked)
        {
            ComponentArt.Web.UI.TreeViewNode node = new CArt.TreeViewNode();
            node.Text = text;
            node.Value = value;
            node.ID = value;
            node.Expanded = expanded;
            node.ShowCheckBox = hasCheckbox;
            node.Checked = isChecked;
            return node;
        }

        private void SelectDefaultsForCheckBoxCombo(CArt.ComboBox combo,
            CArt.TreeView tree,
            HiddenField hdnField)
        {   
            
            if (hdnField.Value == "")
            {
                combo.Text = "";
                int DefaultSelectionsCount = 5;
                foreach (CArt.TreeViewNode tnode in tree.Nodes)
                {
                    if (DefaultSelectionsCount <= 0) break;
                    if (tnode.ShowCheckBox)
                    {
                        combo.Text += tnode.Text + ";";
                        hdnField.Value += tnode.Value + ",";
                        tnode.Checked = true;
                        DefaultSelectionsCount--;
                    }
                }
                combo.Text = combo.Text.TrimEnd(new char[] { ';' });
            }
            else
            {
                foreach (CArt.TreeViewNode tnode in tree.Nodes)
                {   
                    if (hdnField.Value.Contains(tnode.ID+","))
                    {                        
                        tnode.Checked = true;
                       
                    }
                }
            }
        }


    }
}
