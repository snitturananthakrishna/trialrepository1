using System;
using System.Web;
using System.Configuration;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Logistics
{
    public partial class UrlRef : System.Web.UI.Page
    {
         // EVENT OVERRIDES
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e); Session["noheader"] = "block";
            //CHECK IF URL REFERRER IS ENABLED
            if (ConfigurationManager.AppSettings.Get("URLReferer_Enabled") != null &&
                    Convert.ToBoolean(ConfigurationManager.AppSettings.Get("URLReferer_Enabled")))
            {
                string CurrentUrl = Request.ServerVariables["http_referer"].ToString().Substring(0, ConfigurationManager.AppSettings["URLReferer_URL"].Length);
                string _urlReferer = ConfigurationManager.AppSettings["URLReferer_URL"];

                if (CurrentUrl.Equals(_urlReferer, StringComparison.CurrentCultureIgnoreCase)
                    || CurrentUrl.ToLower().Contains("db1.datamonitor.com"))
                {
                    Session["HTTPRef_Authenticated"] = "true";
                    if (Request.QueryString["noheader"] != null && Request.QueryString["noheader"].ToString().Equals("true", StringComparison.CurrentCultureIgnoreCase))
                    {
                        Session["noheader"] = "none";
                    }
                    else
                    {
                        Session["noheader"] = "block";
                    }

                    Server.Transfer("default.aspx");
                }
                else
                {
                    Session["HTTPRef_Authenticated"] = "false";
                }
            }

        }
    }
}
