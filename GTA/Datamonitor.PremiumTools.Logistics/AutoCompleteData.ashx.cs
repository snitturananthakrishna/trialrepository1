using System;
using System.IO;
using System.Data;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.SessionState;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Library
{
    /// <summary>
    /// AutoCompleteData
    /// </summary>  
    public class AutoCompleteData : IHttpHandler
    {
        
        /// <summary>
        /// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler"/> interface.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpContext"/> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
        public void ProcessRequest(HttpContext context)
        {

            // Get querystring params               
            string SearchString = context.Request.QueryString["q"];

            string final = "";
            DataSet Taxonomy= SqlDataService.GetPredictiveSearchTaxonomy(2, SearchString);
            if (Taxonomy != null && Taxonomy.Tables.Count > 0)
            {
                foreach (DataRow TaxonomyRow in Taxonomy.Tables[0].Rows)
                {
                    final +=TaxonomyRow["taxonomyid"].ToString() + "####" + TaxonomyRow["displayname"].ToString() + "|";
                }
            }

           context.Response.Write(final.TrimEnd('|'));
        }

        public bool IsReusable { get { return false; } } 
    }
}
