using System;
using System.Text;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic
{
    public partial class Search1 : AuthenticationBasePage
    {
         // EVENT OVERRIDES

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            //Check quick search link can be visible or not
            SerachLinkTag.Visible = GlobalSettings.ShowQuickSearchPage;

            //Load user selections to hidden field from sessoin
            LoadUserSelectionsToHiddenField();

            if (!IsPostBack)
            {                
                //Load taxonomy selection links
                LoadTaxonomyTypes();

                //Bind years dropdowns
                BindYearsDropdown();

                //LoadSavedSearches
                ((SavedSearch)SavedSearch1).LoadSavedSearches(GlobalSettings.DefaultResultsPage);
            }

            //Check whether results found for the current selections or not.
            if (Request.QueryString["noresults"]!=null && 
                !string.IsNullOrEmpty(Request.QueryString["noresults"].ToString())&&
                Request.QueryString["noresults"].ToString()=="1")
            {
                ResultsFoundStatus.Visible = true;
            }
            else
            {
                ResultsFoundStatus.Visible = false;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Search";            
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!IsSharedUser &&
                IsWelcomePageVisible() &&
                ShowWelcomeMsg &&
                !ClientScript.IsClientScriptBlockRegistered("WelcomePage"))
            {
                ClientScript.RegisterStartupScript(this.GetType(),
                    "WelcomePage", "<script type='text/javascript'>hs.htmlExpand(null,{ contentId: 'highslide-Welcome' });</script>");
            }
            
        }

        // PRIVATE METHODS (initial population)
       
        private void BindYearsDropdown()
        {
            //Get list of valid years from config
            string[] ValidYears = GlobalSettings.ValidYears;

            //Bind startyear & endyear dropdowns
            StartYear.DataSource = ValidYears;
            StartYear.DataBind();
            EndYear.DataSource = ValidYears;
            EndYear.DataBind();

            #region Set Dropdown's selected value and Update session

            //Startyear's
            if (Session["StartYear"] != null)
            {
                //If startyear value found in session then made it as startyear dropdown selected value
                StartYear.SelectedValue = CurrentSession.GetFromSession<int>("StartYear").ToString();
            }
            else
            {
                //Otherwise
                try
                {
                    //Make default startyear from config as selected value
                    StartYear.SelectedValue = GlobalSettings.StartYearSelectValue; 

                    //Update startyear session with this value
                    CurrentSession.SetInSession<int>("StartYear", Convert.ToInt32(StartYear.SelectedValue));
                }
                catch (Exception ex) { }
            }

            //Endyear's
            if (Session["EndYear"] != null)
            {
                //If endyear value found in session then made it as endyear dropdown selected value
                EndYear.SelectedValue = CurrentSession.GetFromSession<int>("EndYear").ToString();
            }
            else
            {
                //Otherwise
                try
                {
                    //Make default endyear from config as selected value
                    EndYear.SelectedValue = GlobalSettings.EndYearSelectValue;

                    //Update endyear session with this value
                    CurrentSession.SetInSession<int>("EndYear", Convert.ToInt32(EndYear.SelectedValue));
                }
                catch (Exception ex) { }
            }

            #endregion
        }

        private bool IsWelcomePageVisible()
        {
            DataSet ds = SqlDataService.GetWelcomePageVisibility(KCUserID);

            if (ds.Tables[0].Rows[0][0].ToString().Equals("1"))
            {
                return true;
            }
            return false;
            
        }

        private void BindPredefinedViews()
        {
            //DataSet Views = GlobalSettings.GetPredefinedViews();
            //PredefinedViews.DataSource = Views;
            //PredefinedViews.DataTextField = "name";
            //PredefinedViews.DataValueField = "id";
            //PredefinedViews.DataBind();
            //PredefinedViews.Items.Insert(0, new ListItem("---Select---", string.Empty));

            //PredefinedViews.Attributes.Add("onchange", "javascript:PredefinedViews_onChange();");            
            //try
            //{
            //    object ViewID = CurrentSession.GetFromSession<string>("PredefinedViewID");
            //    PredefinedViews.SelectedValue = ViewID != null ? (string)ViewID : "0";
            //}
            //catch (Exception ex) { }

        }

        private void LoadDefaultTaxonomyTypeControl(int taxonomyTypeID,
            string taxonomyType,
            string controlType,
            string fullTaxonomyTypeID,
            string parentIds,
            string expandNodeID,
            string flagcheckboxText,
            string tempID)
        {
            SelectedTaxonomy.Text = taxonomyType;
            BaseSelectionControl SelectionControl;

            //Select the copntrol to be used
            switch (controlType)
            {
                case "CHKLIST":
                    SelectionControl = (CheckListControl)Page.LoadControl("Controls/CheckListControl.ascx");
                    break;
                case "TREE":
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");
                    break;
                default:
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");
                    break;
            }

            //Set the selection control attributes
            SelectionControl.ID = taxonomyTypeID.ToString();
            SelectionControl.TaxonomyTypeID = taxonomyTypeID;
            SelectionControl.TaxonomyType = taxonomyType;
            SelectionControl.FullTaxonomyTypeID = fullTaxonomyTypeID;
            SelectionControl.ParentIDs = parentIds;
            SelectionControl.ExpandNodeID = expandNodeID;
            SelectionControl.Source = "search1";
            SelectionControl.Visible = true;
            SelectionControl.ShowTaxonomyDefinition = GlobalSettings.ShowTaxonomyDefinitionInSearch;
            SelectionControl.FlagChecked = false;
            SelectionControl.FlagCheckboxText = flagcheckboxText;

            //Load Selection control data
            SelectionControl.LoadData();

            //Add the selection control to the control holder to display on page
            controlHolder.Controls.Add(SelectionControl);

            //Register startup script to load/render the default control 
            StringBuilder Selections = new StringBuilder();
            Selections.Append("<script language='javascript' type='text/javascript'>");
            Selections.Append(string.Format("renderControl('{0}','{1}','{2}','{3}','{4}','{5}',false,'{6}','{7}');",
                                taxonomyTypeID.ToString(),
                                taxonomyType,
                                controlType,
                                "false",
                                fullTaxonomyTypeID,
                                parentIds,
                                flagcheckboxText,
                                tempID));
            Selections.Append("</script>");
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "loadDefaultControl", Selections.ToString(), false);

            //Store current fulltaxonomytypeid in hidden field
            hdnFullTaxonomyTypeID.Value = fullTaxonomyTypeID;
        }

        public void LoadTaxonomyTypes()
        {
            //Get the taxonomy links list from config to bind the left side links
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyLinks();

            //Bind the taxonomy links to the repeater
            TaxonomyTypesList.DataSource = TaxonomyTypes;
            TaxonomyTypesList.DataBind();

            #region Load default taxonomy type control
            
            if (TaxonomyTypes != null &&
                TaxonomyTypes.Tables.Count > 0 &&
                TaxonomyTypes.Tables[0].Rows.Count > 0)
            {
                int DefaultRow = GlobalSettings.DefaultTaxonomyTypeRowNumber;
                string ExpandNodeID=string.Empty;
                
                //If 'sectionid' exists in query string...
                if (!string.IsNullOrEmpty(Request.QueryString["sectionid"]))
                {
                    //Get section details
                    DataSet SectionDetails = SqlDataService.GetCountryOverviewSectionDetails(Convert.ToInt32(Request.QueryString["sectionid"].ToString()));

                    //Change the values of DefaultRow & ExpandNodeID if section row found with current sectionid
                    if (SectionDetails != null &&
                        SectionDetails.Tables.Count > 0 &&
                        SectionDetails.Tables[0].Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(SectionDetails.Tables[0].Rows[0]["NavigationID"].ToString())&&
                            !string.IsNullOrEmpty(SectionDetails.Tables[0].Rows[0]["NodeID"].ToString()))
                        {
                            DefaultRow = Convert.ToInt32(SectionDetails.Tables[0].Rows[0]["NavigationID"].ToString());
                            ExpandNodeID = SectionDetails.Tables[0].Rows[0]["NodeID"].ToString();
                        }
                    }
                }

                //Load the control respect to default taxonomyTypeid
                LoadDefaultTaxonomyTypeControl(Convert.ToInt32(TaxonomyTypes.Tables[0].Rows[DefaultRow]["ID"].ToString()),
                                    TaxonomyTypes.Tables[0].Rows[DefaultRow]["Name"].ToString(),
                                    TaxonomyTypes.Tables[0].Rows[DefaultRow]["ControlType"].ToString(),
                                    TaxonomyTypes.Tables[0].Rows[DefaultRow]["FullID"].ToString(),
                                    TaxonomyTypes.Tables[0].Rows[DefaultRow]["ParentIds"].ToString(),
                                    ExpandNodeID,
                                    TaxonomyTypes.Tables[0].Rows[DefaultRow]["FlagcheckboxText"].ToString(),
                                    TaxonomyTypes.Tables[0].Rows[DefaultRow]["TempID"].ToString());
            }

            #endregion
        }

        private void LoadUserSelectionsToHiddenField()
        {
            //Get the taxonomy links list from config to bind the left side links
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyLinks();

            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow TaxonomyType in TaxonomyTypes.Tables[0].Rows)
                {                    
                    if (Request.QueryString["sectionid"] != null && !string.IsNullOrEmpty(Request.QueryString["sectionid"].ToString()) &&
                        Request.QueryString["cid"] != null && !string.IsNullOrEmpty(Request.QueryString["cid"].ToString()))
                    {
                        //If 'sectionid' exists in query string... remove the selections from session
                        if (TaxonomyType["ID"].ToString() != Request.QueryString["cid"].ToString())
                        {
                            CurrentSession.SetInSession<Dictionary<int, string>>(TaxonomyType["ID"].ToString(), null);
                            CurrentSession.SetInSession<Dictionary<int, string>>(TaxonomyType["FullID"].ToString(), null);
                        }
                    }
                    
                    //Add the selections to hidden field
                    Dictionary<int, string> CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>(TaxonomyType["FullID"].ToString());
                    if (CurrentSelections != null && CurrentSelections.Count > 0) //Check if the taxonomy data exists in session
                    {
                        foreach (KeyValuePair<int, string> TaxonomySelection in CurrentSelections)
                        {
                            if (TaxonomyType["FullID"].ToString() == TaxonomyType["ID"].ToString())
                            {
                                if (TaxonomyType["type"].ToString().ToLower() != "static")
                                {
                                    hdnUserSelections.Value += TaxonomyType["ID"].ToString() + "~" + TaxonomySelection.Key + "~" + TaxonomySelection.Value + "|";
                                }
                            }
                            else
                            {
                                hdnUserSelections.Value += TaxonomyType["ID"].ToString() + "~" + TaxonomySelection.Key + "~" + TaxonomySelection.Value + "|" +
                                    TaxonomyType["FullID"].ToString() + "~" + TaxonomySelection.Key + "~" + TaxonomySelection.Value + "|";
                            }
                        }
                    }
                    
                }
            }
        }
       
        //PUBLIC METHODS

        public string GetVisiblity(string id)
        {
            //Get current predefined id from session
            object ViewID = CurrentSession.GetFromSession<string>("PredefinedViewID");

            if (ViewID != null)
            {
                string SelectableTaxonomyTypes = GetPredefinedViewSelectableIDs((string)ViewID);
                SelectableTaxonomyTypes = "," + SelectableTaxonomyTypes + ",";

                if (SelectableTaxonomyTypes.Length > 0 && !SelectableTaxonomyTypes.Contains(","+id+","))
                {
                    return "disabled";
                }
            }

            return "";
        }

        public string GetMandatoryTaxonomy()
        {            
            return string.IsNullOrEmpty(GlobalSettings.GetMandatoryTaxonomyInSearch) ? 
                string.Empty : 
                GlobalSettings.GetMandatoryTaxonomyInSearch;
        }

        public string GetMandatoryTaxonomyTypeIDs()
        {
            return GlobalSettings.MandatoryTaxonomyTypeIDs;
        }

        // EVENT HANDLERS

        public void CallBack1_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {            
            //Load all the parameters data into local variables
            int taxonomyTypeID = Convert.ToInt32(e.Parameters[0]);
            string taxonomyType = e.Parameters[1].ToString();
            string ControlType = e.Parameters[2].ToString();
            string FullTaxonomyTypeID = e.Parameters[3].ToString();
            string ParentIds = e.Parameters[4].ToString();
            bool IsFlagCheckboxChecked =Convert.ToBoolean(e.Parameters[6].ToString());
            string strFlagCheckboxText = e.Parameters[7].ToString();
                        
            //Check any user selections exists
            if (e.Parameters[5].ToString().Trim().Length > 0)
            {
                //update current selections to session
                SessionHandler.UpdateSessionWithSelections("", "", e.Parameters[5].ToString(), "addGroupSelection", "");
            }

            //Assign current taxonomy type name to the heading label
            SelectedTaxonomy.Text = taxonomyType;

            //Render the SelectedTaxonomy label control
            SelectedTaxonomy.RenderControl(e.Output);

            BaseSelectionControl SelectionControl;

            //Check the selection control to be used
            switch (ControlType)
            {
                case "CHKLIST":
                    SelectionControl = (CheckListControl)Page.LoadControl("Controls/CheckListControl.ascx");                    
                    break;
                case "TREE":
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");                    
                    break;
                default:
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");
                    break;
            }
          
            //Add the control to the controlholder(to page)
            controlHolder.Controls.Add(SelectionControl);

            //Assign control attributes
            SelectionControl.ID = taxonomyTypeID.ToString();
            SelectionControl.TaxonomyTypeID = taxonomyTypeID;
            SelectionControl.TaxonomyType = taxonomyType;
            SelectionControl.FullTaxonomyTypeID = FullTaxonomyTypeID;
            SelectionControl.ParentIDs = ParentIds;
            SelectionControl.Source = "search1";
            SelectionControl.ShowTaxonomyDefinition = GlobalSettings.ShowTaxonomyDefinitionInSearch;
            SelectionControl.Visible = true;
            SelectionControl.FlagChecked = IsFlagCheckboxChecked;
            SelectionControl.FlagCheckboxText = strFlagCheckboxText;

            //Load the data into the control
            SelectionControl.LoadData();

            #region Display notes (if any) for the current taxonomytype
            
            //Get all taxonomytypes list
            string Path = Server.MapPath(GlobalSettings.TaxonomyTypesConfigPath);
            XmlDocument TaxonomyTypes = new XmlDocument();

            //Load the taxonomytypes to xml document
            TaxonomyTypes.Load(Path);

            //Get current taxonomytype's node
            XmlNode TaxonomyType = TaxonomyTypes.SelectSingleNode("//TaxonomyTypes/TaxonomyType[@ID='" + taxonomyTypeID.ToString() + "']");

            //check if any comments available for this taxonomy type
            if (TaxonomyType != null &&
                TaxonomyType.Attributes.GetNamedItem("Note") != null &&
                !string.IsNullOrEmpty(TaxonomyType.Attributes.GetNamedItem("Note").Value))
            {
                NoteLabel.Text = "Note: " + TaxonomyType.Attributes.GetNamedItem("Note").Value;
            }
            else
            {
                NoteLabel.Text = "";
            }

            #endregion

            //Render all the controls
            SelectionControl.RenderControl(e.Output);
            NoteLabel.RenderControl(e.Output);
        }
       
        protected void PredefinedViews_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Re bind (enable/disable) taxonomy links in left pane
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyTypes();
            TaxonomyTypesList.DataSource = TaxonomyTypes;
            TaxonomyTypesList.DataBind();
        }

        protected void ResultsLink_Click(object sender, EventArgs e)
        {
            //check the default taxonomytypeid is exits in selection string or not
            //if not add the default selection criteria to selection string
            if (!string.IsNullOrEmpty(GlobalSettings.DefaultSelectionTaxonomyType))
            {
                string[] DefaultTypes = GlobalSettings.DefaultSelectionTaxonomyType.Split(',');
                string[] DefaultSelections = GlobalSettings.DefaultSelection.Split('#');

                for (int counter = 0; counter < DefaultTypes.Length; counter++)
                {
                    if (!hdnUserSelections.Value.Contains("|" + DefaultTypes[counter] + "~"))
                    {
                        hdnUserSelections.Value += DefaultSelections[counter].Substring(1);
                    }
                }
            }

            //Update session with the current selections 
            SessionHandler.UpdateSessionWithSelections("", "", hdnUserSelections.Value, "addGroupSelection", "");

            //Redirect to default results page
            Response.Redirect(GlobalSettings.DefaultResultsPage+"?qsource=1");
        }

        protected void AnalysisLink_Click(object sender, EventArgs e)
        {
            //check the default taxonomytypeid is exits in selection string or not
            //if not add the default selection criteria to selection string
            if (!string.IsNullOrEmpty(GlobalSettings.DefaultSelectionTaxonomyType))
            {
                string[] DefaultTypes = GlobalSettings.DefaultSelectionTaxonomyType.Split(',');
                string[] DefaultSelections = GlobalSettings.DefaultSelection.Split('#');

                for (int counter = 0; counter < DefaultTypes.Length; counter++)
                {
                    if (!hdnUserSelections.Value.Contains("|" + DefaultTypes[counter] + "~"))
                    {
                        hdnUserSelections.Value += DefaultSelections[counter].Substring(1);
                    }
                }
            }

            //Update session with the current selections 
            SessionHandler.UpdateSessionWithSelections("", "", hdnUserSelections.Value, "addGroupSelection", "");

            //Redirect to analysis section
            Response.Redirect("analysis/countrycomparison.aspx");
        }

        public string GetResultsPage()
        {            
            return GlobalSettings.DefaultResultsPage;
        }
        
        protected void QuickSearch_Click(object sender, EventArgs e)
        {
            //Clear all selections made in normal search
            CurrentSession.ClearSelectionsFromSession();

            //Redirect to quick search page
            Response.Redirect("~/" + GlobalSettings.QuickSearchPage, true);
        }

    }
}
