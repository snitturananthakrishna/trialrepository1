using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public partial class SelectionList : System.Web.UI.UserControl
    {
        public string source;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {               
                //Load the selections from session
                LoadSelections();
            }
        }
        private void LoadSelections()
        {
            //Get existing taxonomy types
            DataSet TaxonomyTypes = null;
            if (!string.IsNullOrEmpty(source) && source == "quicksearch")
            {
                TaxonomyTypes = GlobalSettings.GetQuickSearchTaxonomyLinks();
            }
            else
            {
                TaxonomyTypes = GlobalSettings.GetTaxonomyLinks();
            }
            
            StringBuilder Selections = new StringBuilder();
            string TaxonomyTypeID = "";
            string TaxonomyType = "";
            string GroupIndicatorSelections = "|";
            Selections.Append("<script language='javascript' type='text/javascript'>");
            foreach (DataRow Row in TaxonomyTypes.Tables[0].Rows)
            {
                TaxonomyTypeID = Row["FullID"].ToString();
                TaxonomyType = Row["Name"].ToString();
                if (Row["type"].ToString() != "static")
                {
                    //Get current selections from sessionstate
                    Dictionary<int, string> CurrentSelections = null;
                    if (!string.IsNullOrEmpty(source) && source == "quicksearch" && TaxonomyTypeID=="2")
                    {
                        CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>("Groups");
                        if (CurrentSelections != null)
                        {
                            int[] SelectionsList = new int[CurrentSelections.Count];
                            string SelectedGroupIDs = "";
                            CurrentSelections.Keys.CopyTo(SelectionsList, 0);
                            foreach (int GroupID in SelectionsList)
                            {
                                SelectedGroupIDs += GroupID.ToString() + ",";
                            }

                            if (SelectedGroupIDs.Trim() != "")
                            {
                                DataSet GroupsData = SqlDataService.GetGroupTaxonomyIds(SelectedGroupIDs.TrimEnd(','));
                                if (GroupsData != null &&
                                    GroupsData.Tables.Count > 0 &&
                                    GroupsData.Tables[0].Rows.Count > 0)
                                {
                                    GroupIndicatorSelections += GroupsData.Tables[0].Rows[0][0].ToString();
                                }
                            }
                        }
                    }
                    else
                    {
                        CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>(TaxonomyTypeID);
                    }

                    if (CurrentSelections != null && CurrentSelections.Count > 0)
                    {
                        // Loop over pairs with foreach
                        foreach (KeyValuePair<int, string> TaxonomySelection in CurrentSelections)
                        {
                            string script = string.Format("addToSelection('{0}','{1}','{2}','{3}','false');",
                                TaxonomyTypeID,
                                 Microsoft.JScript.GlobalObject.escape(TaxonomyType),
                                TaxonomySelection.Key,
                                Microsoft.JScript.GlobalObject.escape(TaxonomySelection.Value));
                            Selections.Append(script);
                        }
                    }

                    if (!string.IsNullOrEmpty(source) && source == "quicksearch" && TaxonomyTypeID == "2")
                    {                        
                        CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>("AutoComplete");
                        if (CurrentSelections != null && CurrentSelections.Count > 0)
                        {
                            // Loop over pairs with foreach
                            foreach (KeyValuePair<int, string> TaxonomySelection in CurrentSelections)
                            {
                                if (!GroupIndicatorSelections.Contains("|" + TaxonomyTypeID + "~" + TaxonomySelection.Key + "~" + TaxonomySelection.Value + "|"))
                                {
                                    string script = string.Format("addToSelection('{0}','{1}','{2}','{3}','false');",
                                        "A" + TaxonomyTypeID,
                                         Microsoft.JScript.GlobalObject.escape("Search Indicators"),
                                        TaxonomySelection.Key,
                                        Microsoft.JScript.GlobalObject.escape(TaxonomySelection.Value));
                                    Selections.Append(script);
                                }
                            }
                        }
                    }

                    
                }
            }

            Selections.Append("</script>");
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "loadSelections", Selections.ToString(), false);
        }
    }
}