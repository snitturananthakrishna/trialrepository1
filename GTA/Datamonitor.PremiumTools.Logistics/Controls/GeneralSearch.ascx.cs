using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public partial class GeneralSearch : BaseSelectionControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        } 
     
        public override void LoadData()
        {           
            DataSet TaxonomyList;
            //Get taxonomy
            string FilterXml = GetFilterCriteriaFromSession();
            TaxonomyList = SqlDataService.GetUniqueTaxonomyByCriteria(FilterXml, TaxonomyTypeID);
            SearchCombo.DataSource = TaxonomyList;
            SearchCombo.DataBind();
        }
    }
}