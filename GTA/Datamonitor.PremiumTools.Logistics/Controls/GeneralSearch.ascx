<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralSearch.ascx.cs" Inherits="Datamonitor.PremiumTools.Generic.Controls.GeneralSearch" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<ComponentArt:ComboBox ID="SearchCombo" Width="192"
            runat="Server"           
            AutoHighlight="false"
            AutoComplete="true"
            AutoFilter="true">
          </ComponentArt:ComboBox>
          



