<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CountryFilter.ascx.cs" 
Inherits="Datamonitor.PremiumTools.Logistics.Controls.CountryFilter" %>
 <script type="text/javascript" language="javascript">
    function RefreshPage()
    {
        if (typeof this.__EVENTTARGET != "undefined")
        {
            __EVENTTARGET.value = "";
        }
        if (typeof this.__EVENTARGUMENT != "undefined")
        {
            __EVENTARGUMENT.value = "";
        }

        window.location.reload();
    }
 </script>
 <div style="display:<%= ShowCountryFilterLink%>">
    <a style="color:White;text-decoration: none;text-align: center;"    
        href="../results/<%= TaxonomyFilterPage%>?id=<%= TaxonomyType %>&uid=<%=DateTime.Now.ToString("HHmmss") %>" 
        onclick="sortFlag=false;return hs.htmlExpand(this, { contentId: 'ColumnFilterSelections', objectType: 'iframe', preserveContent: false} )">
        <img alt="Filter Country" 
            border="0" 
            src="../assets/images/filter.gif"/>  
    </a> 
</div>

<div class="highslide-html-content" id="ColumnFilterSelections" style="width:460px;height:470px;padding-bottom:35px">
    <div class="highslide-header">
        <ul>
            <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
            <li class="highslide-close">
                <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="hs.close(this);return false;"
                    Text="Close"></asp:LinkButton>
            </li>
        </ul>
        <h1>Filter Selections</h1>
    </div>
    <div class="highslide-body" style="overflow:hidden;margin-left:10px" >                 
    </div>
    <div class="button_right" style="width:100px;margin-top:0px;margin-right:50px">
        <asp:LinkButton ID="UpdateResults" runat="server" OnClientClick="hs.close(this);RefreshPage();return false;"
            Text="Update" Width="100px"></asp:LinkButton> 
    </div>     
</div>