<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Glossary.ascx.cs" Inherits="Datamonitor.PremiumTools.Logistics.Controls.Glossary" %>
     <!--<li class="other_option"> 
<asp:HyperLink ID="HyperLink1" runat="server" 
            class="help" 
            Target="_blank"
            Text="FAQ"
            NavigateUrl="~/config/cs/CountryStatisticsFAQ.PDF"
            ></asp:HyperLink>
            </li>-->
     <li class="other_option last last">
    <a href="#" class="ask" 
        style="font-size:10px; font-family:verdana; font-weight:lighter;" 
        onclick="return hs.htmlExpand(this, { contentId: 'highslide-ask' } )">Ask an analyst a question</a>
</li>
<li class="other_option">
   <div id="divMethodology" runat="server">                            
        <asp:HyperLink ID="MethodologyLink" runat="server" 
            class="help" 
            Target="_blank"
            Text="Methodology"
            ></asp:HyperLink>
    </div>
</li>
<li class="other_option">
   <div id="divMDA" runat="server" style="display:none;">                            
        <asp:LinkButton ID="MDALink" runat="server"
            Text="View retail sales data"            
            OnClick="MDALink_Click"></asp:LinkButton>
    </div>
</li>
 <li class="other_option">
    <div id="divNotes" runat="server">                            
        <asp:HyperLink ID="notesLink" runat="server" 
            class="help" 
            Target="_blank"
            Text="Glossary"
            ></asp:HyperLink>
    </div>
</li>
