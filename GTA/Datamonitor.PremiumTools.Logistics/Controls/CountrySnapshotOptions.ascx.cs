using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public partial class CountrySnapshotOptions : System.Web.UI.UserControl
    {
        string _source;

        /// <summary>
        /// Gets or sets the value to _source.
        /// </summary>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        // EVENT OVERRIDES

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Source.ToLower() == "browser")
                {
                    CountryBrowserLI.Attributes.Add("class", "lozenge");

                    CountryProfilerLI.Attributes.Add("class", "");
                }
                else if (Source.ToLower() == "profiler")
                {
                    CountryProfilerLI.Attributes.Add("class", "lozenge");

                    CountryBrowserLI.Attributes.Add("class", "");
                }
                //CountryBrowserLI.Visible = GlobalSettings.ShowQuickSearchPage;
                //SearchOtionsDiv.Style.Add("Display", GlobalSettings.ShowQuickSearchPage ? "Block" : "None");
            }
        }

        /// <summary>
        /// Handles the Click event of the Search control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CountryProfiler_Click(object sender, EventArgs e)
        {
            CurrentSession.ClearSelectionsFromSession();
            Response.Redirect("~/CountryProfiler/default.aspx", true);
        }

        /// <summary>
        /// Handles the Click event of the QuickSearch control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CountryBrowser_Click(object sender, EventArgs e)
        {
            CurrentSession.ClearSelectionsFromSession();
            Response.Redirect("~/results/CountryOverview.aspx", true);
        }
    }
}