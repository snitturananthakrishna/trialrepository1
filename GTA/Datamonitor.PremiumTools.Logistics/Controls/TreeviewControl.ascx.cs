using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public partial class TreeviewControl : BaseSelectionControl
    {  
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tvForAll.Nodes.Clear();
                if(ControlWidth!=0)
                {
                    tvForAll.Width = ControlWidth;
                }
                if (ControlHeight != 0)
                {
                    tvForAll.Height = ControlHeight;
                }
                LoadData();
            }
        }

        /// <summary>
        /// Gets the treeview data and binds.
        /// </summary>        
        /// <param name="taxonomyTypeID">The taxonomy type id.</param>
        public override void LoadData()
        {
            tvForAll.Nodes.Clear();
            DataTable TaxonomyTable;
            DataSet TaxonomyList;
            bool ShowFullTaxonomyInGridFilter = true;
            bool ShowTreeLines = true;
                        
            FlagCheckBox.Checked = FlagChecked;
            if (FlagCheckboxText.Trim() != "")
            {
                FlagCheckBox.Text = FlagCheckboxText;
                FlagCheckBox.Visible = true;
            }
            else
            {
                FlagCheckBox.Visible = false;
            }
            FlagCheckBox.Attributes.Add("onClick", string.Format("renderControl('{0}','{1}','{2}','{3}','{4}','{5}',this.checked,'{6}');",
                                TaxonomyTypeID.ToString(),
                                TaxonomyType,
                                "TREE",
                                "true",
                                FullTaxonomyTypeID,
                                ParentIDs,
                                FlagCheckboxText));

            if (Source == "results")
            {
                string Path = Server.MapPath(GlobalSettings.TaxonomyDefaultsConfigPath);
                XmlDocument TaxonomyTypes = new XmlDocument();
                TaxonomyTypes.Load(Path);
                XmlNode TaxonomyTypeNode = TaxonomyTypes.SelectSingleNode("//TaxonomyDefaults/TaxonomyType[@ID='" + TaxonomyTypeID + "']");               
                if (TaxonomyType != null)
                {
                    ShowFullTaxonomyInGridFilter = TaxonomyTypeNode.Attributes.GetNamedItem("ShowFullTaxonomyInGridFilter").Value.Equals("true") ? true : false;
                }
            }

            //Get taxonomy
            if (!ShowFullTaxonomyInGridFilter)
            {
                string FilterXml = GetFilterCriteriaFromSession();
                TaxonomyList = SqlDataService.GetUniqueTaxonomyByCriteria(FilterXml, TaxonomyTypeID);
            }
            else
            {
                if (Source == "quicksearch")
                {
                    TaxonomyList = SqlDataService.GetQuickSearchTaxonomyList(TaxonomyTypeID);
                    tvForAll.ShowLines = false;
                }
                else
                {
                    if (FlagChecked && TaxonomyTypeID==1)
                    {
                        TaxonomyList = SqlDataService.GetCountriesInOrder();
                        tvForAll.ShowLines = false;
                    }
                    else
                    {                        
                        string SelectedTaxonomyIDs=string.Empty;
                        //Check whether current taxonomytypeid is dependent or not
                        if (("," + GlobalSettings.DependentTaxonomyTypes + ",").Contains("," + TaxonomyTypeID.ToString() + ","))
                        {
                            SelectedTaxonomyIDs = GetDependentSelectedIDsFromSession();
                        }
                        TaxonomyList = SqlDataService.GetTaxonomyForSelection(TaxonomyTypeID,
                            ParentIDs, 
                            string.IsNullOrEmpty(SelectedTaxonomyIDs) ? null : SelectedTaxonomyIDs);

                        #region old code
                        //if (ParentIDs != null && ParentIDs.Length > 0)
                        //{
                        //    string SelectedTaxonomyIDs = string.Empty;
                        //    //Check whether current taxonomytypeid is dependent or not
                        //    if (("," + GlobalSettings.DependentTaxonomyTypes + ",").Contains("," + TaxonomyTypeID.ToString() + ","))
                        //    {
                        //        SelectedTaxonomyIDs = GetDependentSelectedIDsFromSession();
                        //    }
                        //    TaxonomyList = SqlDataService.GetTaxonomyForSelection(TaxonomyTypeID, ParentIDs, SelectedTaxonomyIDs);
                        //}
                        //else
                        //{
                        //    TaxonomyList = SqlDataService.GetTaxonomyList(TaxonomyTypeID, Source);
                        //}                
                        #endregion
                    }
                }

                if (TaxonomyList.Tables.Count > 0 && TaxonomyList.Tables[0].Rows.Count > 0
                    && ParentIDs!=null)
                {
                    string[] ids = ParentIDs.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string id in ids)
                    {
                        foreach (DataRow r in TaxonomyList.Tables[0].Rows)
                        {
                            if (r["ID"].ToString().Equals(id))
                            {
                                r["parentTaxonomyID"] = DBNull.Value;
                            }
                        }
                    }
                }
            }

            //check whether treelines can be shown or not
            string FilePath;
            if (Source == "quicksearch")
            {
                FilePath = Server.MapPath(GlobalSettings.QuickSearchTaxonomyLinksConfigPath);
            }
            else
            {
                FilePath = Server.MapPath(GlobalSettings.TaxonomyLinksConfigPath);
            }

            XmlDocument TaxonomyLinks = new XmlDocument();
            TaxonomyLinks.Load(FilePath);
            XmlNode TaxonomyLink = TaxonomyLinks.SelectSingleNode("//Taxonomies/Taxonomy[@ID='" + TaxonomyTypeID + "'][@FullID='" + 
                 (string.IsNullOrEmpty(FullTaxonomyTypeID)? TaxonomyTypeID.ToString():FullTaxonomyTypeID) + "']");
            if (TaxonomyLink != null)
            {
                ShowTreeLines = TaxonomyLink.Attributes.GetNamedItem("ShowTreeLines").Value.Equals("true") ? true : false;
            }
            tvForAll.ShowLines = ShowTreeLines;


            TaxonomyTable = TaxonomyList.Tables.Count > 0 ? TaxonomyList.Tables[0] : null;

            if (TaxonomyList.Tables[0].Select("parentTaxonomyID is not null").Length>0)
            {
                TaxonomyList.Relations.Add("NodeRelation", TaxonomyList.Tables[0].Columns["ID"], TaxonomyList.Tables[0].Columns["parentTaxonomyID"]);
            }


            //Get current selections from sessionstate
            Dictionary<int, string> TaxonomySelections ;
            if (TaxonomyTypeID == 2 && Source == "quicksearch")
            {
                TaxonomySelections = CurrentSession.GetFromSession<Dictionary<int, string>>("Groups");
            }
            else
            {

                TaxonomySelections = CurrentSession.GetFromSession<Dictionary<int, string>>(TaxonomyTypeID.ToString());
            }
            Dictionary<int, string> MacroEconomicSelections = null;
            Dictionary<int, string> CurrentSelections = null;

            if (Source == "results")
            {
                MacroEconomicSelections = CurrentSession.GetFromSession<Dictionary<int, string>>("11");
            }

            foreach (DataRow dbRow in TaxonomyTable.Rows)
            {
                if (dbRow.IsNull("parentTaxonomyID"))
                {   
                    ComponentArt.Web.UI.TreeViewNode ParentNode = CreateNode(dbRow["ID"].ToString(),
                        dbRow["Name"].ToString(),
                        dbRow["displayName"].ToString(),
                        Convert.ToBoolean(dbRow["isLeafNode"].ToString()),
                        Convert.ToBoolean(dbRow["expandNode"].ToString()),
                        "",
                        Convert.ToBoolean(dbRow["ShowCheckBox"].ToString()));
                                       

                    int NodeID = Convert.ToInt32(dbRow["ID"]);
                    
                    //If nodeID  is lessthan zero then the row data belongs to Macro economic data
                    //else other than macro economic data
                    if (NodeID < 0)
                    {
                        NodeID = NodeID * -1; 
                        CurrentSelections = MacroEconomicSelections;
                    }
                    else
                    {
                        CurrentSelections = TaxonomySelections;
                    }

                    bool HasDefinition = dbRow["definition"] != null && dbRow["definition"].ToString().Length > 0;                    

                    ParentNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DT" : "NT";
                    //if (Source == "search1")
                    //{
                    if (CurrentSelections != null && CurrentSelections.ContainsKey(NodeID))
                        {
                            if (CurrentSelections[NodeID].Replace("&lt;", "<").Equals(dbRow["displayName"].ToString() + "(All)") && 
                                (GlobalSettings.SearchPage.ToLower().Contains("search1")))
                            {
                                if (Convert.ToBoolean(dbRow["isDataAvailable"].ToString()) &&
                                     dbRow.GetChildRows("NodeRelation").Length > 0 &&
                                     (GlobalSettings.SearchPage.ToLower().Contains("search1")) &&
                                     GlobalSettings.AllowSummation)
                                {
                                    ParentNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DTPnSel" : "NTPnSel";
                                }
                                
                                ParentNode.Checked = true;
                                
                            }
                            else if (CurrentSelections[NodeID].Replace("&lt;", "<").Equals(dbRow["displayName"].ToString()))
                            {
                                if (Convert.ToBoolean(dbRow["isDataAvailable"].ToString()) && 
                                    dbRow.GetChildRows("NodeRelation").Length > 0 && 
                                    (GlobalSettings.SearchPage.ToLower().Contains("search1"))&&
                                    GlobalSettings.AllowSummation)
                                {
                                    ParentNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DTPn" : "NTPn";
                                }
                                else
                                {
                                    ParentNode.Checked = true;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToBoolean(dbRow["isDataAvailable"].ToString()) && 
                                dbRow.GetChildRows("NodeRelation").Length > 0 &&
                                (GlobalSettings.SearchPage.ToLower().Contains("search1")) &&
                                GlobalSettings.AllowSummation)
                            {
                                ParentNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DTPnSel" : "NTPnSel";
                            }
                        }
                    //}
                    //populate sub level tree if child nodes exists
                        if (dbRow.GetChildRows("NodeRelation").Length > 0)
                        {
                            bool ExpandStatus = PopulateSubTree(dbRow, ParentNode, CurrentSelections);
                            ParentNode.Expanded = ParentNode.Expanded ? ParentNode.Expanded : ExpandStatus;
                        }
                    
                    tvForAll.Nodes.Add(ParentNode);
                }
            }

            if (tvForAll.Nodes.Count == 1)
            {
                //Expand root level node
                tvForAll.Nodes[0].Expanded = true;
            }

            if (!string.IsNullOrEmpty(ExpandNodeID))
            {
                ComponentArt.Web.UI.TreeViewNode NodeToExpand =tvForAll.FindNodeById(ExpandNodeID);
                if (NodeToExpand != null)
                {
                    tvForAll.CollapseAll();
                    while (NodeToExpand != null)
                    {
                        NodeToExpand.Expanded = true;
                        NodeToExpand = NodeToExpand.ParentNode;
                    }
                }
            }
            //if (Source == "quicksearch")
            //{
            //    tvForAll.ShowLines = false;
            //}
        }

        /// <summary>
        /// Populates the sub tree.
        /// </summary>
        /// <param name="dbRow">The data row.</param>
        /// <param name="node">The node.</param>
        /// <param name="CurrentSelections">The current selections.</param>
        private bool PopulateSubTree(DataRow dbRow, 
            TreeViewNode node,
            Dictionary<int, string> currentSelections)
        {
            bool CanExpandParentnode = false;
            foreach (DataRow childRow in dbRow.GetChildRows("NodeRelation"))
            {
                //ComponentArt.Web.UI.TreeViewNode childNode = CreateNode(childRow["Text"].ToString(), childRow["ImageUrl"].ToString(), true);
                ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(childRow["ID"].ToString(),
                        childRow["Name"].ToString(),
                        childRow["displayName"].ToString(),
                        Convert.ToBoolean(childRow["isLeafNode"].ToString()),
                        Convert.ToBoolean(childRow["expandNode"].ToString()),
                        "",
                        Convert.ToBoolean(childRow["ShowCheckBox"].ToString()));

                bool HasDefinition = childRow["definition"] != null && childRow["definition"].ToString().Length > 0;

                int NodeID = Convert.ToInt32(childRow["ID"]);

                NodeID = (NodeID < 0) ? NodeID * -1 : NodeID;

                newNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DT" : "NT";
                newNode.Checked = node.Checked;
                //if (Source == "search1")
                //{
                if (currentSelections != null && currentSelections.ContainsKey(NodeID))
                    {
                        if (currentSelections[NodeID].Replace("&lt;", "<").Equals(childRow["displayName"].ToString() + "(All)") && 
                            (GlobalSettings.SearchPage.ToLower().Contains("search1")))
                        {
                            if (Convert.ToBoolean(childRow["isDataAvailable"].ToString()) &&
                                 childRow.GetChildRows("NodeRelation").Length > 0 &&
                                 (GlobalSettings.SearchPage.ToLower().Contains("search1")) &&
                                 GlobalSettings.AllowSummation)
                            {
                                newNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DTPnSel" : "NTPnSel";
                            }
                            
                            newNode.Checked = true;
                            CanExpandParentnode = true;
                            
                        }
                        else if (currentSelections[NodeID].Replace("&lt;","<").Equals(childRow["displayName"].ToString()))
                        {
                            if (Convert.ToBoolean(childRow["isDataAvailable"].ToString()) &&
                                childRow.GetChildRows("NodeRelation").Length > 0 &&
                                (GlobalSettings.SearchPage.ToLower().Contains("search1")) &&
                                GlobalSettings.AllowSummation)
                            {
                                newNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DTPn" : "NTPn";
                            }
                            else
                            {
                                newNode.Checked = true;
                                CanExpandParentnode = true;
                            }
                        }
                    }
                    else
                    {
                        if (Convert.ToBoolean(childRow["isDataAvailable"].ToString()) &&
                            childRow.GetChildRows("NodeRelation").Length > 0 &&
                            (GlobalSettings.SearchPage.ToLower().Contains("search1")) &&
                            GlobalSettings.AllowSummation)
                        {
                            newNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DTPnSel" : "NTPnSel";
                        }
                    }
                //}
                
                //populate sub level tree if child nodes exists
                if (childRow.GetChildRows("NodeRelation").Length > 0)
                {
                    bool ExpandStatus = PopulateSubTree(childRow, newNode, currentSelections);
                    newNode.Expanded=newNode.Expanded ? true : ExpandStatus; 
                    if (!CanExpandParentnode)
                    {
                        CanExpandParentnode = (newNode.Expanded) ? true : false;
                    }
                }
                //add current node to parent node
                node.Nodes.Add(newNode);
            }
            return CanExpandParentnode;
        }

        /// <summary>
        /// Creates the node.
        /// </summary>
        /// <param name="nodeID">The id.</param>
        /// <param name="nodeText">The text.</param>
        /// <param name="nodeValue">The value.</param>
        /// <param name="isLeafNode"></param>
        /// <param name="expanded">if set to <c>true</c> [expanded].</param>
        /// <param name="imageUrl">The imageurl.</param>
        /// <param name="showCheckBox"></param>
        /// <returns></returns>
        private ComponentArt.Web.UI.TreeViewNode CreateNode(string nodeID,
            string nodeText,
            string nodeValue,
            bool isLeafNode,
            bool expanded,
            string imageUrl,
            bool showCheckBox)
        {
            ComponentArt.Web.UI.TreeViewNode Node = new ComponentArt.Web.UI.TreeViewNode();
            Node.Text = nodeText;
            Node.ImageUrl = imageUrl;
            Node.Expanded = expanded;
            Node.Value = nodeValue;
            Node.ID = nodeID;
            Node.ShowCheckBox = showCheckBox;
            //if (!isLeafNode)
            //{
            //    Node.ContentCallbackUrl = "TaxonomySubtreeXml.aspx?id=" + nodeID;
            //}
            return Node;
        }

        private string GetDependentSelectedIDsFromSession()
        {
            string SelectedTaxonomyIDs = string.Empty;
            string DependentTaxonomySource = "," + GlobalSettings.DependentTaxonomySource + ",";

            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyTypes();
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                //Get Dependent taxonomy source SelectedIDs From Session
                foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
                {
                    if (DependentTaxonomySource.Contains(taxonomyRow["ID"].ToString()))
                    {
                        Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                        if (selectedIDsList != null && selectedIDsList.Count > 0)
                        {
                            //Get all keys (ids) from dictionary
                            List<int> keys = new List<int>(selectedIDsList.Keys);
                            //convert int array to string array
                            string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                            //Build selections xml
                            if(GlobalSettings.DependentTaxonomySource.Contains(","))
                            {
                                SelectedTaxonomyIDs += string.Format("{0}|{1};",
                                    taxonomyRow["ID"].ToString(),
                                    string.Join(",", SelectedIDs));
                            }
                            else
                            {
                                SelectedTaxonomyIDs += string.Join(",", SelectedIDs);
                                break;
                            }
                            
                        }
                    }
                }
                SelectedTaxonomyIDs.TrimEnd(';');
            }
            return SelectedTaxonomyIDs;
        }

    }
}