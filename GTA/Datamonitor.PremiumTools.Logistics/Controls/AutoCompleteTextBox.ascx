<%@ Control Language="C#" 
    AutoEventWireup="true" 
    CodeBehind="AutoCompleteTextBox.ascx.cs" 
    Inherits="Datamonitor.PremiumTools.Generic.Controls.AutoCompleteTextBox" %>

<link href="Assets/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="Assets/Scripts/jquery.min.js"></script>
<script type="text/javascript" src="Assets/Scripts/jquery-ui.min-1.8.1.js"></script>  

<script type="text/javascript">
	var jdata;
	
	    $(function() 
	    {
	        $(".tb").autocomplete(
	            {
	                source: function(request, response) 
	                {
	                    $.ajax({
	                        url: "AutoCompleteWebService.asmx/FetchTaxonomyResults",
	                        data: "{ 'taxonomyTypeID': " + $("#AC_TaxonomyTypeID").val() + " , 'searchString': '" + request.term + "'}",
	                        dataType: "json",
	                        type: "POST",
	                        contentType: "application/json; charset=utf-8",
	                        dataFilter: function(data) 
	                                    {
	                                        return data; 
	                                    },
	                        success: function(data) 
	                                    {
	                                        if(data.d.length<=0) alert("No results found");
	                                        $.map(data.d,function(item) 
	                                                    {
	                                                        if(jdata==null) {jdata="";}
	                                                        jdata+="|"+item.ID+"~"+item.Name;    	                                    
	                                                    }
	                                                );
	                                
	                                        response($.map(data.d, function(item) 
	                                                                {
	                                                                return { value: item.Name	}
	                                                                }
	                                                        )
	                                                )
	                                    },
	                        error: function(XMLHttpRequest, textStatus, errorThrown) 
	                                        {
	                                            alert(textStatus);
	                                        }
	                    });
	                },
	                minLength: 2
	            });
	    });
	    function getItemValue()
	    {	
	        if(jdata)
	        {
                var str= jdata.split("|");
                var txtval=$("#<%=SearchTextBox.ClientID%>").val();
                for(i=0;i<str.length;i++)
                {
                    if(str[i].indexOf(txtval)>-1)
                    {
                        return str[i].split("~")[0];
                    }
                }
            }
	        return false;
	    }
	    function getItemText()
	    {		
	        if(jdata)
	        {
	            var str= jdata.split("|");
	            var txtval=$("#<%=SearchTextBox.ClientID%>").val();
	            for(i=0;i<str.length;i++)
	            {
	                if(str[i].indexOf(txtval)>-1)
	                {
	                    return str[i].split("~")[1];
	                }
	            }
	        }
	        return false;
	    }
	    function getItem()
	    {		
	        if(jdata)
	        {
	            var str= jdata.split("|");
	            var txtval=$("#<%=SearchTextBox.ClientID%>").val();
	            for(i=0;i<str.length;i++)
	            {
	                if(str[i].indexOf(txtval)>-1)
	                {	                
	                    return str[i];
	                }
	            }
	        }
	        return false;
	    }
	    function clearSearchTextBox()
	    {
	        $("#<%=SearchTextBox.ClientID%>").val("");	
	    }
	</script>
	
<style type="text/css">
    .ui-autocomplete 
    { 
        text-align:left;
    }
</style>


<div class="ui-widget" style="padding-bottom:5px" >
     <asp:TextBox ID="SearchTextBox" CssClass="tb" runat="server" Width="100%" />
     <input id="AC_TaxonomyTypeID" type="hidden" />
</div>
