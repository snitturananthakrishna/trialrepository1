<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaxonomyDefinition.aspx.cs" Inherits="Datamonitor.PremiumTools.Logistics.TaxonomyDefinition" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Taxonomy Definition</title>
         
    <link href="~/assets/css/treeview.css" rel="stylesheet" type="text/css"/>
    <link href="~/assets/css/gridStyle.css" rel="stylesheet" type="text/css"/>
     <link href="~/assets/css/datamonitor.css" rel="stylesheet" type="text/css"/>
</head>
<body style="background-color:White;background-image:none;margin:0px;padding:0px 0px 0px 5px">
    <form id="form1" runat="server">
    <div style="font-size: 11px;margin:0px;overflow:auto;padding:0px">
    <div id="DefinitionDiv" runat="server">
    <p style="padding:0px;margin-top:0px;margin-bottom:2px;"><b><asp:Label ID="Indicator" runat="server"></asp:Label></b></p>
    <p style="padding:0px;margin:0px"><asp:Label ID="Definition" runat="server"></asp:Label></p><br /></div>
    <div id="CoverageRangeDiv" runat="server"><b>Data Coverage Years: </b><%=CoverageRange%></div>    
    </div>
    </form>
</body>
</html>
