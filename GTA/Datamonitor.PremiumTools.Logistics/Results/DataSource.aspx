<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataSource.aspx.cs" Inherits="Datamonitor.PremiumTools.Generic.Results.DataSource" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Source Info</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="SourceDiv" runat="server">
        <b>Source</b><br /><br />
        <%=SourceInfo %><br /><br /></div>
        <div id="DateDiv" runat="server"><b>Last Updated on :</b> <%=LastUpdatedDate %><br /></div>
        &nbsp;
        
    </div>
    </form>
</body>
</html>
