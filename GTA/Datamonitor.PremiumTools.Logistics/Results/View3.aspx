<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/ResultsMaster.Master" 
        AutoEventWireup="true" 
        CodeBehind="View3.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.View3" %>
        
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
        
<asp:Content ID="Content1" ContentPlaceHolderID="ResultsMasterHolder" runat="server">  
<link href="../assets/css/arcticwhite.css" rel="stylesheet" type="text/css" />
<link href="../assets/css/gridstyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../assets/Scripts/NumberFormat154.js"></script>
<style type="text/css">

.headerCss
{
    cursor:hand;
    cursor:pointer;
}

.ResultsPanel
{
    float:left;
    
}

</style>

<style type="text/css">
.headerCss
{
    cursor:hand;
    cursor:pointer;
}

.contentCss
{
  border-left:0px solid #C8C2A8;
  border-right:0px solid #C8C2A8;
  background-color:#ECECD9;
     width:205px;
     text-align:center;
     border-top:0px;
}

.iFrameCss
{
    overflow:auto;
    width:208px;
    height:302px;
    padding:0px;
    margins:0px;
}

</style>

 <script type="text/javascript">
    function formatN(value)
    {
        var decimalPlaces="<%= NoOfDecimalsToGridValues %>";
    
        //return value;
        if(value==null)
        return "";
        //for growth rows, data will come in the format xx.xx% - so format the number part
        var postfix='';
        if(String(value).indexOf('%')!=-1)
        {
            value=String(value).substring(0,value.length-1);
            postfix='%';
        }
        if(value == "")
    	{
       	 return "";
    	}
         var num = new NumberFormat();
        num.setInputDecimal('.');
        num.setNumber(value); // obj.value is '-1000.247'
        
        num.setCurrency(false);
        num.setNegativeFormat(num.LEFT_DASH);
        num.setNegativeRed(false);
        num.setSeparators(true, ',', ',');       
        
        if(Number(value) > 100)
        {
            num.setPlaces(0, false);    
        }
        else 
        {
            num.setPlaces(decimalPlaces, false);
        }
        return num.toFormatted()+postfix;

    }
    function AddPercentage(value)
    {
        if(value == "")
    	{
       	    return "";
    	}
    	return value + '%';
    }
    function GetCagrLabelCss(value)
    {
        if(value>0)
    	{
       	    return "green";
    	}
    	else if(value<0)
    	{
    	    return "red";
    	}   
    	else
    	{
    	    return "black";
    	} 	
    }
    function getValueType(dataitem, fieldName)
    {
        var fName;
        fName = fieldName.replace("Y", "");
        var ffy = dataitem.getMemberAt("FFY").get_value();
        if(ffy == "")
        {
            return "Actual";
        }
        else if (fName == ffy)
        {
            return "FFY";
        }
        else if (fName > ffy)
        {
            return "Forecast";
        }
        return "Actual";
    }

    function getColumnNumber(columnName)
    {
        var numColumns = ResultsGrid.get_table().get_columns().length;
        //alert(numColumns);
        for (i=0;i<numColumns;i++)
        {
            var col = ResultsGrid.get_table().get_columns()[i]
            if (col.get_dataField() == columnName) 
                return i;           
        }
    }

    
    var sortFlag = true; 

    function ResultsGrid_onSortChange(sender, e)
    {
        //alert("sort Change : " + flag);
        //	    sortDescending = e.get_descending();
        //	    var columnIndex = 0;
        //	    for (var i=0;i<sender.get_table().get_columns().length;i++)
        //	    {
        //		    if (sender.get_table().get_columns()[i].get_dataField() == e.get_column().get_dataField())
        //		    {
        //			    sortColumnIndex = i;
        //			    break;
        //		    }		    
        //	    }
	    if(!sortFlag)
	        e.set_cancel(true); 
	        
	    sortFlag=true;
    }
    
    function getTargetCurrentID()
    {
        var targetCurrentID=document.getElementById('<%=hdnConversionID.ClientID %>').value;
        if(targetCurrentID.length>0)
        {
            return targetCurrentID ;
        }
        else
        {
            return "0";
        }
    }    
    
    function ShowTaxonomyFilter(type)
    {        
        alert(type);
        sortFlag=false;         
         
    }
    
    function CheckChange(chkbox,colName)
    {    
        //ShowHideGridColumn(chkbox.value, chkbox.checked);        
        var hiddenColumns = document.getElementById('<%=HiddenColumns.ClientID%>');
        if(chkbox.checked)
        {            
            hiddenColumns.value = hiddenColumns.value.replace("|"+colName + "|", "|"); 
        }
        else
        {
            hiddenColumns.value = hiddenColumns.value + colName + '|';
        }
        
        ChangeDisplayOptionsChangedStatus();         
    }

    function ShowHideGridColumn(index, show)
    {
        var grd = <%=ResultsGrid.ClientObjectId %>;
        var col = grd.get_table().get_columns()[index];
        col.set_visible(show); 
        grd.render();  
    }
    function ResultsGrid_onLoad(sender, e)
    {       
        var columntoHide = document.getElementById('<%=ColumnToHide.ClientID%>').value;        
        if(columntoHide.length>0)
        {
            var grd = <%=ResultsGrid.ClientObjectId %>;       
            
            var columnstoHide = columntoHide.split(",");        
            var j = 0;        
            ResultsGrid.beginUpdate();
            for(j = 0; j < columnstoHide.length; j++)
            {            
                var col = grd.get_table().get_columns()[columnstoHide[j]];
                col.set_visible(false);         
            }         
            ResultsGrid.endUpdate();
        }
    }
    function ResultsGrid_onColumnResize(sender, eventArgs)
    {
        ResultsGrid.beginUpdate();
        
        
        setTimeout("ResultsGrid_onLoad(null,null)",100);    
        ResultsGrid.endUpdate();
    }
    

    function RefreshPage()
    {
        __EVENTTARGET.value = "";
        __EVENTARGUMENT.value = "";
        window.location.reload();
    }
   
    $(document).ready(function() { 
    //alert("inside ready");  
});


function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function ltrim(stringToTrim) {
	return stringToTrim.replace(/^\s+/,"");
}
function rtrim(stringToTrim) {
	return stringToTrim.replace(/\s+$/,"");
}

 function ChangeDisplayOptionsChangedStatus() 
 {
    var hdnStatus=document.getElementById('<%=isDisplayOptionsChanged.ClientID %>');
    hdnStatus.value="true";     
 }
 function CheckDisplayOptionsChanged()
 {
    var hdnStatus=document.getElementById('<%=isDisplayOptionsChanged.ClientID %>');
    return hdnStatus.value==""?false:true;
 }
 
 
 function getUniqueID()
 {
    var newDate = new Date;
    return newDate.getTime();  
 }
 
 function UnitSelections_onChange(dropDown)
{    
    ChangeDisplayOptionsChangedStatus(); 
    var selection = dropDown.value;
    var baseUnitID=selection.split("~")[0];
    var unitsConversions=document.getElementById('<%=hdnUnitsConversions.ClientID %>').value;    
    var unitsArray=unitsConversions.split("|");    
    
    if(unitsArray)
    {
        for(j = 0; j < unitsArray.length; j++)
        {            
            if(unitsArray[j].indexOf(baseUnitID)==0)
            {
                unitsConversions=unitsConversions.replace(unitsArray[j]+"|","");
                break;
            }
        }
    }
    unitsConversions=unitsConversions+selection+"|";
    document.getElementById('<%=hdnUnitsConversions.ClientID %>').value=unitsConversions;
    
}

 function getUnitsConversions()
 {
    var unitsConversions=document.getElementById('<%=hdnUnitsConversions.ClientID %>').value;
    var unitsArray=unitsConversions.split("|");    
    var result="";
    if(unitsArray)
    {
        for(j = 0; j < unitsArray.length; j++)
        {            
            if(unitsArray[j].indexOf("~")!=-1)
            {
                result=result+unitsArray[j].split("~")[1]+",";            
            }
        }
    }
    return result;
 }
 
 function Year_onChange(type,dropdown)
{
    ChangeDisplayOptionsChangedStatus(); 
    var selection = dropdown.value;
    
    if(type=="startyear")
    {
        document.getElementById('<%=hdnStartYear.ClientID %>').value =selection;   
    }
    else if(type=="endyear")
    {
        document.getElementById('<%=hdnEndYear.ClientID %>').value =selection;
    }    
}
 function getStartYear()
 {
    return  document.getElementById('<%=hdnStartYear.ClientID %>').value;
 }
 function getEndYear()
 {
    return  document.getElementById('<%=hdnEndYear.ClientID %>').value;
 }
 
 function TryRenderGrid()
 {
//    alert(IsIE8Browser());
    if(IsIE8Browser())
    {
        ResultsGrid.render();
    }
 }
 
 function IsIE8Browser() 
 {
     var rv = -1;
     var ua = navigator.userAgent;    
     var re = new RegExp("Trident\/([0-9]{1,}[\.0-9]{0,})");    
     if (re.exec(ua) != null)
      {        
        rv = parseFloat(RegExp.$1);    
      }    
    return (rv == 4);
}

 function currency_onChange(dropdown)
{   
    ChangeDisplayOptionsChangedStatus(); 
    var selection = dropdown.value;
    document.getElementById('<%=hdnConversionID.ClientID %>').value =selection;   
}

function AbsoluteOrIndex(type)
{
    ChangeDisplayOptionsChangedStatus(); 
    if(type=="index")
    {
        document.getElementById('<%=hdnConversionID.ClientID %>').value ="-1";   
    }
    else if(type=="percapita")
    {
        document.getElementById('<%=hdnConversionID.ClientID %>').value ="-2";   
    }
    else if(type=="growths")
    {
        document.getElementById('<%=hdnConversionID.ClientID %>').value ="-3";   
    }
    else
    {
        var dropdown=document.getElementById('<%=currency.ClientID %>');
        if(dropdown)
        {
            document.getElementById('<%=hdnConversionID.ClientID %>').value =dropdown.value;  
        }
        else
        {
            document.getElementById('<%=hdnConversionID.ClientID %>').value ="0";  
        }
    }
}
function getTaxonomyFilterPage()
{
    return "<%= getTaxonomyFilterPage()%>";
}
function getAnchorVisibility(val)
 {
     if(val.length>0)
     {     
        return "block";
     }
     else
     {
        return "none";
     }
 }
 
 function HasAdditionalDetails(val)
 {
     if(val)
     {     
        return "block";
     }
     else
     {
        return "none";
     }
 }
 </script>
 


    <div id="display_options">
        <div>
            <div class="display_options">
                <h3>                        
                    Display options 
                </h3>
            </div>
            <div>
              <table width="700">
                    <tr>
                        <td valign="top">  <b>Columns</b>
                            <div id="ColumnFilterDiv1" runat="server"></div>
                        </td>                        
                        <td valign="top">  <b>Years</b>
                            <table>
                                <tr>
                                    <td>Start Year&nbsp; </td>
                                    <td><asp:DropDownList ID="StartYear" runat="server"  Font-Size="11px" width="60px"/></td>
                                </tr>
                                <tr>
                                    <td>End Year&nbsp; </td>
                                    <td><asp:DropDownList ID="EndYear" runat="server"  Font-Size="11px" width="60px"/></td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="padding-left:50px;" runat="server" id="DataFormats">
                           <b>Show data in</b> 
                            <table><tr>
                            <td><asp:RadioButton ID="ShowAbsoluteValues" runat="server" Text="Absolute" onClick="AbsoluteOrIndex('absolute');" GroupName="Currency"/></td>
                            <td><asp:DropDownList ID="currency" Width="80px" runat="server" Font-Size="11px" /> </td></tr><tr><td colspan="2">
                            <asp:RadioButton ID="ShowIndexTo100" runat="server" Text="Index To 100" onClick="AbsoluteOrIndex('index');" GroupName="Currency"/>                            
                            </td></tr>
                            <tr><td colspan="2"><asp:RadioButton ID="ShowPerCapita" runat="server" Text="Per capita" onClick="AbsoluteOrIndex('percapita');" GroupName="Currency"/>
                            </td></tr><tr><td colspan="2">
                            <asp:RadioButton ID="ShowGrowths" runat="server" Text="Growth Rate (%)" onClick="AbsoluteOrIndex('growths');" GroupName="Currency"/>
                            </td></tr></table>
                       </td>
                       <td valign="top" style="padding-left:20px;">
                            <asp:Repeater ID="rptUnitSelections" runat="server"
                                OnItemDataBound="rptUnitSelections_ItemDataBound">
                                <HeaderTemplate>                               
                                <table cellpadding="2">
                                <tr>
                                    <td colspan="2"><b>Units Conversion</b></td>                                    
                                </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <asp:Label ID="BaseUnitsID" runat="server"
                                                Text='<%#Eval("baseunitsid") %>'
                                                Visible="false"></asp:Label>
                                           <%#Eval("basename") %> &nbsp; 
                                        </td>
                                        <td><asp:DropDownList ID="ddlOptions" runat="server"  
                                                Font-Size="11px"
                                                width="140px"/>
                                        </td>
                                    </tr>                                   
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table><br />
                                </FooterTemplate>        
                            </asp:Repeater>
                       </td>
                    </tr>
                </table>
            </div>
        </div>
           <div id="display_options_buttons">
                <a href="javascript:toggleLayer('display_options');TryRenderGrid();">Cancel</a>
                <asp:LinkButton ID="Update" runat="server" CssClass="save_button" OnClientClick="JavaScript:toggleLayer('display_options');return CheckDisplayOptionsChanged();"
                    OnClick="Update_Click">Update</asp:LinkButton>  
             </div>
                    </div>
   <div class="subtitle">
        <div id="display_options_tab" style="margin-top:1px;">
                <a href="javascript:toggleLayer('display_options');TryRenderGrid();">
                    <div>
                        Display options
                    </div>
                </a>
         </div>
          <div id="extractOpts" style="float:left;display: block;">
            <ul class="jumplinks">
                <li class="last"><span id="ctl00_ContentPlaceHolder1_lblExtract" class="lblExport">Extract data to </span></li>
                <li class="excel_icon"><asp:LinkButton ID="lnkExcel" Text="Excel" runat="server" OnClick="Excel_Click" ></asp:LinkButton> </li>
            </ul>
        </div>
    </div>
        
        <div style="float:left;margin-top:5px;width:100%;position:relative">
            <table width="100%">
                <tr>
                    <td width="500px"> 
                        <asp:Repeater ID="RemovedTaxonomies" runat="server">
                        <ItemTemplate>
                           <span class="RemovedTaxonomyHeading"> <%#Eval("key") %> </span>         
                            : <span class="RemovedTaxonomyData"> <%#Eval("value").ToString().Length > 0 ? Eval("value").ToString() : "[Blanks]" %></span><br />
                        </ItemTemplate>
                        <FooterTemplate>
                            <br />
                        </FooterTemplate>        
                    </asp:Repeater>
                    </td>
                </tr>            
            </table>  
          
        
    <ComponentArt:Menu ID="Menu1" runat="server"         
        CollapseDuration="0" 
        CssClass="mnu"
        DefaultGroupExpandOffsetX="4"
        DefaultGroupExpandOffsetY="-1" 
        DefaultItemLookId="ItemLook" 
        ExpandDuration="0"        
        Orientation="Horizontal" 
        ShadowEnabled="false">
        <ItemLooks>
            <ComponentArt:ItemLook ActiveCssClass="top-a" CssClass="top" ExpandedCssClass="top-a"
                HoverCssClass="top-h" LookId="TopItemLook" />
            <ComponentArt:ItemLook CssClass="itm" HoverCssClass="itm-h" LookId="ItemLook" />
            <ComponentArt:ItemLook CssClass="br" LookId="BreakItemLook" />
        </ItemLooks>
        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="ItemTemplate">
                <div>
                    <span class="ico ## DataItem.get_value(); ##"></span><span class="txt">## DataItem.get_text();
                        ##</span>
                </div>
            </ComponentArt:ClientTemplate>
            <ComponentArt:ClientTemplate ID="DisplayTextMenuTemplate">
                <div style="padding-left:5px;padding-right:5px; font-weight: bold; text-align: left; height:16px ">
                    <a href="## getTaxonomyFilterPage() ##?id=## DataItem.get_text();##" onclick="sortFlag=false;return hs.htmlExpand(this, { contentId: 'ColumnFilterSelections', objectType: 'iframe', preserveContent: true} )"
                        style="color: White; text-decoration: none; text-align: center;"><img alt="Filter ## DataItem.get_text();##" border="0" src="../assets/images/filter.gif" style="vertical-align:bottom;"/>
                        ## DataItem.get_text();## </a>
                </div>
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Menu>
   
    <asp:Label ID="GridStatusMessage" runat="server" ForeColor="red"></asp:Label>
      <asp:Panel ID="ResultsPanel" runat="server" Height="435px" Width="99%" CssClass="ResultsPanel" >
      <ComponentArt:Grid id="ResultsGrid"                                    
        CssClass="grid"
        RunningMode="client"                                                                             
        ShowHeader="false"    
        ShowFooter="false"
        AllowColumnResizing="true"
        AllowEditing="false"                                            
        HeaderCssClass="GridHeader"               
        FooterCssClass="GridFooter"                      
        SliderHeight="20"
        SliderWidth="150"
        SliderGripWidth="9"   
        SliderPopupOffsetX="20"                                                             
        IndentCellWidth="22"
        IndentCellCssClass="HeadingCell_Freeze"     
        LoadingPanelClientTemplateId="LoadingFeedbackTemplate"
        LoadingPanelPosition="MiddleCenter"
        LoadingPanelFadeDuration="10"
        LoadingPanelFadeMaximumOpacity="60"                                                                
        ScrollBar="auto"
        ImagesBaseUrl="../assets/images/"
        PagerStyle="numbered"
        PagerInfoPosition="BottomLeft"
        PagerPosition="BottomRight"
        PreExpandOnGroup="true"                                			        
        ScrollTopBottomImagesEnabled="true"
        ScrollTopBottomImageHeight="2"
        ScrollTopBottomImageWidth="16"
        ScrollImagesFolderUrl="../assets/images/scroller/"
        ScrollButtonWidth="16"
        ScrollButtonHeight="17"
        ScrollBarCssClass="ScrollBar"
        ManualPaging="false"
        ScrollGripCssClass="ScrollGrip"
        ScrollBarWidth="16"                  
        Height="410" 
        Width="910" runat="server" AllowHorizontalScrolling="True" EnableViewState="false" >
        <Levels>
          <ComponentArt:GridLevel                                      
                AllowReordering="false"
                AllowGrouping="false"      
                AllowSorting="false"                                                  
                HeadingCellCssClass="HeadingCell"                                                                                                
                HeadingRowCssClass="HeadingRow"
                HeadingTextCssClass="HeadingCellText"                                                    
                GroupHeadingCssClass="HeadingCell_Freeze"
                DataCellCssClass="DataCell"
                AlternatingRowCssClass="RowAlt"
                RowCssClass="Row"                                          
                SelectedRowCssClass="SelRow"   
                SortAscendingImageUrl="asc.gif"
                SortDescendingImageUrl="desc.gif" >                                                    
            <Columns>
                     
            </Columns>
          </ComponentArt:GridLevel>
          
        </Levels>
      <ClientEvents>
            <SortChange EventHandler="ResultsGrid_onSortChange" />          
            <Load EventHandler="ResultsGrid_onLoad" />
            <ColumnResize EventHandler="ResultsGrid_onColumnResize" />                 
      </ClientEvents>

        <ClientTemplates>                     
          <ComponentArt:ClientTemplate Id="CommonDataTemplate">                                                                           
            <div class="## getValueType(DataItem, DataItem.GetCurrentMember().Column.DataField) ##"> ## DataItem.GetCurrentMember().get_value() ##</div>
          </ComponentArt:ClientTemplate> 
          <ComponentArt:ClientTemplate Id="FormattedDataTemplate">                                                                           
            <div class="## getValueType(DataItem, DataItem.GetCurrentMember().Column.DataField) ##"> ## formatN(DataItem.GetCurrentMember().get_value()) ##</div>
          </ComponentArt:ClientTemplate> 
          <ComponentArt:ClientTemplate Id="TooltipTemplate">                                                                           
            <div title="## DataItem.GetCurrentMember().get_value() ##"> ## DataItem.GetCurrentMember().get_value() ##</div>
          </ComponentArt:ClientTemplate>  
          <ComponentArt:ClientTemplate Id="TooltipTemplateWithDefinition">                                                                           
            <div style="float:left;" title="## DataItem.GetCurrentMember().get_value() ##"> 
                <table cellpadding="0" cellspacing="0"><tr><td nowrap="nowrap">
                ## DataItem.GetCurrentMember().get_value() ##
                </td><td>
                
                 <a style="color: Red; text-decoration: none;display:## getAnchorVisibility(DataItem.GetMember('Definition').get_value())##" 
                    href="../TaxonomyDefinition.aspx?taxonomyid=## DataItem.GetMember('taxonomyid').Text ##" 
                    onclick="return hs.htmlExpand(this, { contentId: 'TaxonomyDefinition', objectType: 'ajax'} )">
                    &nbsp;<img alt="View Definition" border="0" src="Assets/Images/help2.gif"/>
                </a> 
                </td></tr></table>
            </div>
            <div style="float:right">
                <a style="cursor:hand;color:#1E3267; display:## HasAdditionalDetails(DataItem.GetMember('AdditionalDetailsAvailable').get_value())##" 
                    onclick="window.open('AdditionalDetails.aspx?rowid=## DataItem.GetMember('rowID').Text ##&startyear=## getStartYear() ##&endyear=## getEndYear() ##&targetCurrencyid=## getTargetCurrentID() ##','AdditionalDetails','toolbar=no,status=no,resizable=yes,scrollbars=yes,width=1000,left=50,top=100,height=580 ');">                                              
		            <img alt="View by field" border="0" width="60" height="13" src="../assets/images/Competitors.gif"/> 
                </a></div>
          </ComponentArt:ClientTemplate>  
           <ComponentArt:ClientTemplate Id="CAGRTemplate">                                                                           
            <div style="color:##GetCagrLabelCss(DataItem.GetCurrentMember().get_value())##"> ## AddPercentage(DataItem.GetCurrentMember().get_value()) ##</div>
          </ComponentArt:ClientTemplate>                 
          <ComponentArt:ClientTemplate Id="LoadingFeedbackTemplate">
              <table height="410" width="940" bgcolor="#f6f6f6" style="margin-left:0px;margin-top:0px;padding-top:0px"><tr><td valign="center">
                  <table cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td style="font-size:11pt;font-family:Arial;font-weight:bold;color:#0000a0;">Loading...&nbsp;</td>
                    <td><img src="../assets/images/spinner.gif" border="0"></td>
                  </tr>
                  </table>
          </td></tr></table>

          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate ID="TrendChartTemplate">
          <div style="padding: 0px; font-weight: bold; text-align: center;">
                <a style="color: Red; text-decoration: none;" 
                    href="TrendImage.aspx?rowid=## DataItem.GetMember('rowID').Text ##&conversionid=## DataItem.GetMember('ConversionID').Text ##&startyear=## getStartYear() ##&endyear=## getEndYear() ##&targetCurrencyid=## getTargetCurrentID() ##" 
                    onclick="return hs.htmlExpand(this, { contentId: 'PopupChart', objectType: 'iframe'} )">
                    <img alt="View Trend Chart" border="0" src="../assets/images/chart_icon.gif" />
                </a>                 
            </div>
          </ComponentArt:ClientTemplate>
           <ComponentArt:ClientTemplate ID="DataSourceClientTemplate">            
            <div style="text-align:center; padding-left:2px;">
                <a style="color:White;text-decoration: none;text-align: center;" 
                    href="DataSource.aspx?rowid=## DataItem.GetMember('rowID').Text ##"
                    onclick="return hs.htmlExpand(this, { contentId: 'DataSource', objectType: 'ajax'} )">
                    <img alt="" border="0" src="../assets/images/icon-info.gif" />
                </a>                 
            </div>
          </ComponentArt:ClientTemplate>
          <ComponentArt:ClientTemplate ID="ColumnFilterTemplate">            
            <div style="padding: 0px; font-weight: bold; text-align: left;overflow:hidden;">
            <table><tr><td>
                <a style="color: White; text-decoration: none;">
                    <span>## DataItem.DataField ##</span>
                </a> </td><td>
                <a style="color:White;text-decoration: none;text-align: center;" 
                    href="## getTaxonomyFilterPage() ##?id=## DataItem.DataField ##&uid=## getUniqueID() ##" 
                    onclick="sortFlag=false;return hs.htmlExpand(this, { contentId: 'ColumnFilterSelections', objectType: 'iframe', preserveContent: false} )">
                   <img alt="Filter ## DataItem.DataField ##" 
                        border="0" 
                        src="../assets/images/filter.gif"/>  
                </a>          </td></tr></table>       
                <%--<asp:ImageButton ID="filterData" runat="server"
                    ImageUrl="../assets/images/filter.gif"
                    OnClientClick="return hs.htmlExpand(this, { contentId: 'ColumnFilterSelections', objectType: 'ajax'} )"
                    PostBackUrl="DataSource.aspx" />--%>
            </div>
          </ComponentArt:ClientTemplate>
           <ComponentArt:ClientTemplate ID="ColumnNonFilterTemplate">            
            <div style="padding: 0px; font-weight: bold; text-align: left;">
                <a style="color: White; text-decoration: none;">
                    <span>## DataItem.DataField ##</span>
                </a>                 
            </div>
          </ComponentArt:ClientTemplate>
       <%-- <ComponentArt:ClientTemplate ID="ColumnFilterTemplate">            
            <div style="padding: 0px; font-weight: bold; text-align: left;">
                <a style="color: White; text-decoration: none;">
                    <span>## DataItem.DataField ##</span>
                </a> <img alt="pop" border="0" src="../assets/images/filter.gif" onclick="mypopup()" />                
            </div>
          </ComponentArt:ClientTemplate>--%>
        </ClientTemplates>
      </ComponentArt:Grid>
      </asp:Panel>
   <div style="width:100%;display:block;padding-top:5px;" align="right">Note: Forecast data in <i>italics</i><br />CAGR is calculated for the selected range of years
 </div>
       <div class="highslide-html-content" id="PopupChart" style="width:350px;height:325px">
                <div class="highslide-header">
                    <ul>
                        <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                        <li class="highslide-close">
                            <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="hs.close(this);return false;"
                                Text="Close"></asp:LinkButton>
                        </li>
                    </ul>
                    <h1>Trend Chart</h1>
                </div>
                <div class="highslide-body">               
                </div>
            </div>
      
        <div class="highslide-html-content" id="DataSource" style="width:280px;height:200px">
            <div class="highslide-header">
                <ul>
                    <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                    <li class="highslide-close">
                        <asp:LinkButton ID="LinkButton2" runat="server" OnClientClick="hs.close(this);return false;"
                            Text="Close"></asp:LinkButton>
                    </li>
                </ul>
                <h1>Data Source</h1>
            </div>
            <div class="highslide-body">
            </div>
        </div>
        <div class="highslide-html-content" id="ColumnFilterSelections" style="width:460px;height:470px;padding-bottom:35px">
            <div class="highslide-header">
                <ul>
                    <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                    <li class="highslide-close">
                        <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="hs.close(this);return false;"
                            Text="Close"></asp:LinkButton>
                    </li>
                </ul>
                <h1>Filter Selections</h1>
            </div>
            <div class="highslide-body" style="overflow:hidden;margin-left:10px" >                 
            </div>
            <div class="button_right" style="width:100px;margin-top:0px;margin-right:50px">
                <asp:LinkButton ID="UpdateResults" runat="server" OnClientClick="hs.close(this);RefreshPage();return false;"
                    Text="Update" Width="100px"></asp:LinkButton> 
            </div>     
        </div>
       
 
<div class="highslide-html-content" id="TaxonomyDefinition" style="width:280px;height:200px">
    <div class="highslide-header">
        <ul>
            <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
            <li class="highslide-close">
                <asp:LinkButton ID="LinkButton4" runat="server" OnClientClick="hs.close(this);return false;"
                    Text="Close"></asp:LinkButton>
            </li>
        </ul>
        <h1>Taxonomy Definition</h1>
    </div>
    <div class="highslide-body">
    </div>
    &nbsp;
</div>
 

<div id="Loadingimg" style="visibility: hidden;">
        <div style="left: 0px; top: 0px" id="progressBackgroundFilter_New" class="progressBackgroundFilter_New">
        </div>
        <div id="processMessage_New" class="processMessage_New">
            <b>Loading ...</b>
            <img alt="" id="spin" src="../Assets/Images/spinner.gif" />
        </div>
    </div>    
       
<asp:HiddenField ID="ColumnToHide" runat="server" Value="" />
<asp:HiddenField ID="hdnsavedsearch" runat="server" /> 
<asp:HiddenField ID="isDisplayOptionsChanged" runat="server" />
<asp:HiddenField ID="HiddenColumns" runat="server" />
<asp:HiddenField ID="hdnUnitsConversions" runat="server" value="1086~11|1087~6|1088~12|1107~13|1114~14|"/> 
<asp:HiddenField ID="hdnStartYear" runat="server" />
<asp:HiddenField ID="hdnEndYear" runat="server" />
<asp:HiddenField ID="hdnConversionID" runat="server" />
</asp:Content>


