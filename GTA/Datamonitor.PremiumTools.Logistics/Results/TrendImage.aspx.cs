using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using dotnetCHARTING;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using Aspose.Cells;
using Aspose.Words;
using System.IO;

namespace Datamonitor.PremiumTools.Generic.Results
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ImagePage : AuthenticationBasePage //System.Web.UI.Page
    {

        private static Color[] DMPalette = new Color[] { Color.FromArgb(0, 34, 82), Color.FromArgb(108, 128, 191), Color.FromArgb(165, 174, 204), Color.FromArgb(210, 217, 229), Color.FromArgb(216, 69, 25), Color.FromArgb(255, 102, 0), Color.FromArgb(255, 172, 112), Color.FromArgb(250, 222, 199), Color.FromArgb(255, 153, 0), Color.FromArgb(255, 204, 0) };
        bool isTrendChart = false;
        string trendChartType = string.Empty;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["rowid"] != null && !string.IsNullOrEmpty(Request.QueryString["rowid"].Trim()) &&
                Request.QueryString["conversionid"] != null && !string.IsNullOrEmpty(Request.QueryString["conversionid"].Trim()) &&
                Request.QueryString["targetcurrencyid"] != null && !string.IsNullOrEmpty(Request.QueryString["targetcurrencyid"].Trim()) &&
                Request.QueryString["startyear"] != null && !string.IsNullOrEmpty(Request.QueryString["startyear"].Trim()) &&
                Request.QueryString["endyear"] != null && !string.IsNullOrEmpty(Request.QueryString["endyear"].Trim()))
            {
                QuartileDiv.Visible = false;
                isTrendChart = true;

                //Trend chart for view results grid
                PlotViewResultsTrendChart();
                
                trendChartType = "view";
            }
            else if (Request.QueryString["rowid"] != null && !string.IsNullOrEmpty(Request.QueryString["rowid"].Trim()) &&
                Request.QueryString["type"] != null && !string.IsNullOrEmpty(Request.QueryString["type"].Trim()))
            {
                QuartileDiv.Visible = false;
                isTrendChart = true;

                //Trend chart for country overview page
                PlotCountryOverviewTrendChart();

                trendChartType = "countryoverview";

            }
            else if(Request.QueryString["rowid"] != null && !string.IsNullOrEmpty(Request.QueryString["rowid"].Trim()) &&
               Request.QueryString["year"] != null && !string.IsNullOrEmpty(Request.QueryString["year"].Trim()) &&
               Request.QueryString["countryid"] != null && !string.IsNullOrEmpty(Request.QueryString["countryid"].Trim()))
            {
                QuartileDiv.Visible = true;
                ChartTitle.Visible = false;
                SampleChart.Visible = false;
                trendChartType = "quartilestatus";

                //Quartile analysis status table
                ShowQuartileStatus();  
            }

            //Excel link can be displayed only when showing a trend chart
            if (isTrendChart == true)
            {
                lnkExcel.Visible = true;
            }
            else
            {
                lnkExcel.Visible = false;
            }
        }

        /// <summary>
        /// Shows the quartile status.
        /// </summary>
        private void ShowQuartileStatus()
        {
            DataSet QuartileCountriesData = new DataSet();

            //Assign quartile year value to year label
            QuartileYear.Text = Request.QueryString["year"].Trim();            

            //If 'IsCountryProfiler' exists in query string then quartile data is for CountryProfiler.. otherwise CountryOverview
            if (Request.QueryString["IsCountryProfiler"] != null && Convert.ToBoolean(Request.QueryString["IsCountryProfiler"]))
            {
                //Get country profiler quartile data
                QuartileCountriesData = SqlDataService.GetCountryProfileIndicatorQuartileData(Convert.ToInt32(Request.QueryString["rowid"].Trim()), Convert.ToInt32(Request.QueryString["year"].Trim()));
            }
            else
            {
                //Get country overview quartile data
                QuartileCountriesData = SqlDataService.GetCountryOverviewIndicatorQuartileData(Convert.ToInt32(Request.QueryString["rowid"].Trim()), Convert.ToInt32(Request.QueryString["year"].Trim()));
            }
            
            if (QuartileCountriesData != null && QuartileCountriesData.Tables.Count > 0)
            {
                //Assign the units value to the units label
                QuartileUnits.Text = QuartileCountriesData.Tables[0].Rows[0]["Units"].ToString() ;

                //Assign quartile year value to year label
                QuartileYear.Text = QuartileCountriesData.Tables.Count > 1 && QuartileCountriesData.Tables[1].Rows.Count>0?
                    QuartileCountriesData.Tables[1].Rows[0]["DataYear"].ToString():
                    QuartileCountriesData.Tables[0].Rows[0]["datayear"].ToString();

                //Assign indicator text to indicator label
                Indicatorlbl.Text = QuartileCountriesData.Tables[0].Rows[0]["Indicator"].ToString();

                //Made current country name as bold. add another column to decide the current company
                QuartileCountriesData.Tables[0].Columns.Add("font");
                foreach (DataRow dRow in QuartileCountriesData.Tables[0].Rows)
                {
                    dRow["font"] = "notBold";
                    if (dRow["countryID"].ToString() == Request.QueryString["countryid"].Trim())
                    {
                        dRow["font"] = "canBold";
                    }
                }

                //Bind the quartile data to repeater
                QuartileRepeater.DataSource = QuartileCountriesData;
                QuartileRepeater.DataBind();
            }
        }

        /// <summary>
        /// Plots the country overview trend chart.
        /// </summary>
        private void PlotCountryOverviewTrendChart()
        {
            string RowID = Request.QueryString["rowid"].Trim();
            DataSet ChartData = null;
            ChartData = SqlDataService.GetCountryOverviewTrendChartData(Convert.ToInt64(RowID));
            if (ChartData != null &&
                    ChartData.Tables.Count > 1 &&
                    ChartData.Tables[0].Rows.Count > 0 &&
                    ChartData.Tables[1].Rows.Count > 0)
            {
                PlotXYChart(ChartData,
                    SampleChart,
                    ChartData.Tables[1].Rows[0]["YaxisLabel"].ToString(),
                    325,
                    190);

                ChartTitle.Text = ChartData.Tables[1].Rows[0]["ChartTitle"].ToString();
            }
        }

        /// <summary>
        /// Plots the view results trend chart.
        /// </summary>
        private void PlotViewResultsTrendChart()
        {
            DataSet ChartData = GetViewResultsTrendChartData();
           
            if (ChartData != null &&
                ChartData.Tables.Count > 1 &&
                ChartData.Tables[0].Rows.Count > 0 &&
                ChartData.Tables[1].Rows.Count > 0)
            {
                string _y1AxisName = ChartData.Tables[1].Rows[0]["Y1AxisLabel"].ToString();
                string _y2AxisName = ChartData.Tables[1].Rows[0]["Y2AxisLabel"].ToString();
                ChartTitle.Text = ChartData.Tables[1].Rows[0]["ChartTitle"].ToString();

                PlotDualYAxisChart(ChartData, SampleChart, _y1AxisName, _y2AxisName, "");
            }   
        }

        /// <summary>
        /// Gets the view results trend chart data.
        /// </summary>
        /// <returns></returns>
        private DataSet GetViewResultsTrendChartData()
        {
            DataSet ChartData = null;
            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
            int EndYear = CurrentSession.GetFromSession<int>("EndYear");

            string RowID = Request.QueryString["rowid"].Trim();
            long ConversionID = Convert.ToInt64(Request.QueryString["conversionid"].Trim());
            int TargetCurrencyID = Convert.ToInt32(Request.QueryString["targetcurrencyid"].Trim());

            StartYear = (StartYear < Convert.ToInt32(Request.QueryString["startyear"].Trim())) ?
                Convert.ToInt32(Request.QueryString["startyear"].Trim()) :
                StartYear;

            EndYear = (EndYear > Convert.ToInt32(Request.QueryString["endyear"].Trim())) ?
                Convert.ToInt32(Request.QueryString["endyear"].Trim()) :
                EndYear;

            if (RowID.StartsWith("ME"))
            {
                ChartData = SqlDataService.GetMEPopupChartData(Convert.ToInt64(RowID.Substring(2)), StartYear, EndYear);
            }
            else
            {
                ChartData = SqlDataService.GetPopupChartData(Convert.ToInt64(RowID), StartYear, EndYear, ConversionID, TargetCurrencyID);
            }
            return ChartData;
        }

        /// <summary>
        /// Plots the Dual YAxis chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>
        private void PlotDualYAxisChart(DataSet chartDataset,
                                   dotnetCHARTING.Chart chart,
                                   string yAxis1Text,
                                   string yAxis2Text,
                                   string AxisCriteria)
        {
            chart.Use3D = false;
            chart.Visible = true;
            chart.Debug = false;
            //chart.FileManager.TempDirectory = HttpContext.Current.Server.MapPath("~/TempImages");
            chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;
            LogUsage("Results - Trend Chart", "trend chart", "Dual chart");
            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                //Set chart styles
                SampleChart.FileManager.ImageFormat = ImageFormat.Png;
                SampleChart.FileQuality = 100;
                //Set chart styles
                chart.Palette = DMPalette;
                chart.Debug = false;
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.XAxis.SpacingPercentage = 30;
                chart.ChartArea.ClearColors();
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.Width = 325;
                chart.Height = 210;
                chart.LegendBox.Visible = true;

                TwoDimensionalCharts.AddCopyrightText(chart, 325 - 190);

                //Set axis label texts and styles

                chart.XAxis.DefaultTick.Label.Color = Color.Black;
                chart.XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.YAxis.DefaultTick.Label.Color = Color.Black;
                chart.YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.XAxis.Line.Color = Color.DarkGray;
                chart.XAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.YAxis.Line.Color = Color.DarkGray;
                chart.YAxis.DefaultTick.Line.Color = Color.DarkGray;

                DataEngine de = new DataEngine();
                DataEngine de1 = new DataEngine();

                SeriesCollection sc;
                SeriesCollection sc1 = new SeriesCollection();
                chart.SeriesCollection.Clear();
                de.Data = chartDataset;
                de.DataFields = "XAxis=" +
                     chartDataset.Tables[0].Columns[0].ColumnName +
                     ",YAxis=" + chartDataset.Tables[0].Columns[1].ColumnName;
                string xAxisText = chartDataset.Tables[0].Columns[0].ColumnName;
                sc = de.GetSeries();

                sc[0].XAxis = new dotnetCHARTING.Axis();
                sc[0].YAxis = new dotnetCHARTING.Axis();
                sc[0].XAxis.Label.Text = xAxisText;
                sc[0].YAxis.Label.Text = yAxis1Text;
                sc[0].Name = yAxis1Text;

                de1.Data = chartDataset;
                de1.DataFields = "XAxis=" +
                     chartDataset.Tables[0].Columns[0].ColumnName +
                     ",YAxis=" + chartDataset.Tables[0].Columns[2].ColumnName;
               
                sc1 = de1.GetSeries();

                for (int i = 0; i < sc1.Count; i++)
                {
                    sc.Add(sc1[i]);
                }
                for (int i = 1; i < sc.Count - 1; i++)
                {
                    sc[i].XAxis = sc[0].XAxis;
                    sc[i].YAxis = sc[0].YAxis;
                }

                if (sc.Count == 2)
                {
                    sc[1].XAxis = sc[0].XAxis;
                }
                //chart.Type = dotnetCHARTING.ChartType.Combo;
                chart.SeriesCollection.Add(sc);

                sc[1].YAxis = new dotnetCHARTING.Axis();
                sc[1].YAxis.Orientation = dotnetCHARTING.Orientation.Right;
                sc[1].YAxis.LabelRotate = true;

                if (GlobalSettings.TrendChartAbsoluteDataChartType.ToLower() == "auto")
                {
                    sc[0].Type = chartDataset.Tables[0].Rows.Count > 15 ? SeriesType.AreaLine : SeriesType.Bar;
                }
                else
                {
                    sc[0].Type = GetSeriesType(GlobalSettings.TrendChartAbsoluteDataChartType);
                }
                if (GlobalSettings.TrendChartGrowthDataChartType.ToLower() == "auto")
                {
                    sc[1].Type = SeriesType.Line;
                }
                else
                {
                    sc[1].Type = GetSeriesType(GlobalSettings.TrendChartGrowthDataChartType);
                }
                switch (GlobalSettings.TrendChartType.ToLower())
                {
                    case "absoluteonly":
                        sc[0].Visible = true;
                        sc[1].Visible = false;
                        break;
                    case "growthonly":
                        sc[0].Visible = false;
                        sc[1].Visible = true;
                        break;
                    default:
                        sc[0].Visible = true;
                        sc[1].Visible = true;
                        break;
                }
                
                sc[1].Line.Width = 1;
                sc[1].Name = yAxis2Text;
                sc[1].YAxis.Label.Text = yAxis2Text;

                chart.DataBind();

                sc[0].XAxis.DefaultTick.Label.Color = Color.Black;
                sc[0].XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                sc[0].YAxis.DefaultTick.Label.Color = Color.Black;
                sc[0].YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                sc[0].XAxis.Line.Color = Color.DarkGray;
                sc[0].XAxis.DefaultTick.Line.Color = Color.DarkGray;
                sc[0].YAxis.Line.Color = Color.DarkGray;
                sc[0].YAxis.DefaultTick.Line.Color = Color.DarkGray;                
                sc[0].DefaultElement.Marker.Type = ElementMarkerType.None;
                sc[0].YAxis.ShowGrid = false;
                sc[0].XAxis.ShowGrid = false;
                sc[0].YAxis.AlternateGridBackground.Color = Color.White;
                if (GlobalSettings.AllowAutoYaxisScaleRange)
                {
                    sc[0].YAxis.Scale = Scale.Range;
                }

                sc[1].YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                sc[1].XAxis.Line.Color = Color.DarkGray;
                sc[1].XAxis.DefaultTick.Line.Color = Color.DarkGray;
                sc[1].YAxis.Line.Color = Color.DarkGray;
                sc[1].YAxis.DefaultTick.Line.Color = Color.DarkGray;
                sc[1].DefaultElement.Marker.Type = ElementMarkerType.None;
                sc[1].YAxis.ShowGrid = false;
                sc[1].XAxis.ShowGrid = false;
                sc[1].YAxis.AlternateGridBackground.Color = Color.White;

                chart.DefaultAxis.ShowGrid = false;
                chart.DefaultAxis.AlternateGridBackground.Color = Color.White;

                chart.LegendBox.Template = "%Icon%Name";
                SetStandardLegendStyles(chart);

                chart.BackColor = Color.White;
                if (!Directory.Exists(Server.MapPath("temp")))
                    Directory.CreateDirectory(Server.MapPath("temp"));

                chart.FileManager.TempDirectory = "temp";
                ChartImg.ImageUrl = chart.FileManager.SaveImage(chart.GetChartBitmap()).Replace("\\", "/");
                hdnImageUrl.Value = ChartImg.ImageUrl;
            }
        }

        /// <summary>
        /// Gets the type of the series.
        /// </summary>
        /// <param name="typeString">The type string.</param>
        /// <returns></returns>
        private SeriesType GetSeriesType(string typeString)
        {
            switch (typeString.ToLower())
            {
                case "bar": return SeriesType.Bar;
                case "line": return SeriesType.Line;
                case "area": return SeriesType.AreaLine;
                default: return SeriesType.Bar;
            }
        }

        /// <summary>
        /// Plots the XY chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart.</param>
        /// <param name="yAxisText">The y axis text.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns></returns>
        private dotnetCHARTING.Chart PlotXYChart(DataSet chartDataset,
                              dotnetCHARTING.Chart chart,
                              string yAxisText,
                              Unit width,
                              Unit height)
        {
            chart.Use3D = false;
            chart.Visible = true;

            LogUsage("Country Browser  - Trend chart", "trend chart", " ");
            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;
                chart.FileQuality = 100;
                DataEngine objDataEngine = new DataEngine();
                chart.SeriesCollection.Clear();

                objDataEngine.Data = chartDataset;
                dotnetCHARTING.SeriesCollection sc;

                //Set data fields
                objDataEngine.DataFields = "XAxis=" +
                    chartDataset.Tables[0].Columns[0].ColumnName +
                    ",YAxis=" + chartDataset.Tables[0].Columns[1].ColumnName +
                    ",splitBy=" + chartDataset.Tables[0].Columns[2].ColumnName;

                sc = objDataEngine.GetSeries();
                chart.SeriesCollection.Add(sc);
                chart.DataBind();
                TwoDimensionalCharts.AddCopyrightText(chart, Convert.ToInt32(width.Value) - 150);
                
                //chart.DefaultSeries.Type = chartDataset.Tables[0].Rows.Count > 15 ? SeriesType.AreaLine : SeriesType.Bar;
                chart.DefaultSeries.Type = SeriesType.Bar;

                if ((SeriesType)chart.DefaultSeries.Type == SeriesType.AreaLine)
                {
                    //chart.DefaultSeries.DefaultElement.Transparency = 20;
                    chart.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.None;
                }

                string xAxisText = chartDataset.Tables[0].Columns[0].ColumnName;

                //Set axis label texts and styles
                chart.XAxis.Label.Text = xAxisText;
                chart.YAxis.Label.Text = yAxisText;
                chart.XAxis.DefaultTick.Label.Color = Color.Black;
                chart.XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.YAxis.DefaultTick.Label.Color = Color.Black;
                chart.YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.XAxis.Line.Color = Color.DarkGray;
                chart.XAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.YAxis.Line.Color = Color.DarkGray;
                chart.YAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.DefaultSeries.DefaultElement.Marker.Size = 4;
                if (GlobalSettings.AllowAutoYaxisScaleRange)
                {
                    chart.YAxis.Scale = Scale.Range;
                }

                //Set chart styles
                chart.Palette = DMPalette;
                chart.Debug = false;
                chart.Height = height;
                chart.Width = width;
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.XAxis.SpacingPercentage = 30;
                chart.ChartArea.ClearColors();
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.ChartArea.Shadow.Color = Color.White;

                //Set legend properties and styles

                chart.LegendBox.Visible = false;
                if (!Directory.Exists(Server.MapPath("temp")))
                    Directory.CreateDirectory(Server.MapPath("temp"));
                
                chart.FileManager.TempDirectory = "temp";
                ChartImg.ImageUrl = chart.FileManager.SaveImage(chart.GetChartBitmap()).Replace("\\", "/");
                hdnImageUrl.Value = ChartImg.ImageUrl;
            }
            return chart;
        }

        /// <summary>
        /// Handles the Click event of the lnkExcel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            Workbook excel = new Workbook();
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string chartTitle = ChartTitle.Text;

            if (hdnImageUrl.Value.Length > 0)
            {
                chartImagePath = Server.MapPath(hdnImageUrl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            LogUsage("Results - Trend chart - Excel download", "trend image1", " ");
            string sheetName = "View";
            DataSet ChartData = null;

            //Get the chart data
            if (trendChartType == "view")
            {
                sheetName = "View";

                ChartData = GetViewResultsTrendChartData();
            }
            else if (trendChartType == "countryoverview")
            {
                sheetName = "Country Overview";

                string RowID = Request.QueryString["rowid"].Trim();
                ChartData = SqlDataService.GetCountryOverviewTrendChartData(Convert.ToInt64(RowID));

                //Remove extra columns
                if (ChartData != null)
                {
                    if (ChartData.Tables[0].Columns.Contains("Indicator"))
                    {
                        ChartData.Tables[0].Columns.Remove("Indicator");
                    }
                    if (ChartData.Tables[0].Columns.Contains("units"))
                    {
                        ChartData.Tables[0].Columns.Remove("units");
                    }
                    if (ChartData.Tables[0].Columns.Contains("spl"))
                    {
                        ChartData.Tables[0].Columns.Remove("spl");
                    }
                }
            }

            //Extract to excel
            excel = ExtractionsHelper.ExportToExcelTrendChart(ExcelTemplatePath,
                     chartTitle,
                     LogoPath,
                     chartBitmap,
                     sheetName,
                     ChartData != null ? ChartData.Tables[0] : null);

            //Save the extracted excel file
            excel.Save("Ovum" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);

        }

        /// <summary>
        /// Handles the Click event of the lnkExcel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkExcelQuartile_Click(object sender, EventArgs e)
        {
            Workbook excel = new Workbook();
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string chartTitle = ChartTitle.Text;
            string Indicator=string.Empty;
            string QuartileUnitsYear = QuartileYear.Text + "," + QuartileUnits.Text;
           
            LogUsage("Results - Trend chart - Excel download", "trend image1", " ");
            string sheetName = "Quartile";
            DataSet QuartileCountriesData = null;

            //Get the chart data
            QuartileYear.Text = Request.QueryString["year"].Trim();
            QuartileCountriesData = SqlDataService.GetCountryOverviewIndicatorQuartileData(Convert.ToInt32(Request.QueryString["rowid"].Trim()), Convert.ToInt32(Request.QueryString["year"].Trim()));

            //Remove extra columns
            if (QuartileCountriesData != null)
            {

                if (QuartileCountriesData.Tables.Count > 0 && QuartileCountriesData.Tables[0].Rows.Count > 0)
                {
                    Indicator=QuartileCountriesData.Tables[0].Rows[0]["Indicator"].ToString();
                }

                if (QuartileCountriesData.Tables[0].Columns.Contains("countryid"))
                {
                    QuartileCountriesData.Tables[0].Columns.Remove("countryid");
                }
                if (QuartileCountriesData.Tables[0].Columns.Contains("Units"))
                {
                    QuartileCountriesData.Tables[0].Columns.Remove("Units");
                }
                if (QuartileCountriesData.Tables[0].Columns.Contains("Rank"))
                {
                    QuartileCountriesData.Tables[0].Columns.Remove("Rank");
                }
                if (QuartileCountriesData.Tables[0].Columns.Contains("Indicator"))
                {
                    QuartileCountriesData.Tables[0].Columns.Remove("Indicator");
                }
                if (QuartileCountriesData.Tables[0].Columns.Contains("RankNumber"))
                {
                    QuartileCountriesData.Tables[0].Columns.Remove("RankNumber");
                }

               
            }
            
            //Extract to excel
            excel = ExtractionsHelper.ExportToExcelQuartileTrendChart(ExcelTemplatePath,
                     chartTitle,
                     LogoPath,                     
                     sheetName,
                     Indicator,
                     QuartileUnits.Text,
                     QuartileCountriesData != null ? QuartileCountriesData.Tables[0] : null);

            //Save the extracted excel file
            excel.Save("Ovum" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);

        }

        /// <summary>
        /// Sets the standard legend styles.
        /// </summary>
        /// <param name="objChart">The obj chart.</param>
        private static void SetStandardLegendStyles(dotnetCHARTING.Chart chart)
        {
            chart.LegendBox.Background = new dotnetCHARTING.Background(System.Drawing.Color.White);
            chart.LegendBox.CornerBottomLeft = BoxCorner.Square;
            chart.LegendBox.CornerBottomRight = BoxCorner.Square;
            chart.LegendBox.CornerTopLeft = BoxCorner.Square;
            chart.LegendBox.CornerTopRight = BoxCorner.Square;
            chart.LegendBox.LabelStyle = new dotnetCHARTING.Label("", new System.Drawing.Font("Verdana", 7), Color.Black);
            chart.LegendBox.Line = new dotnetCHARTING.Line(Color.White);
            chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
            chart.LegendBox.Line.Color = Color.White;
            chart.LegendBox.Shadow.Color = Color.White;
            chart.LegendBox.InteriorLine.Color = Color.White;
        }
    }
}
