using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Results
{
    public partial class ManageSavedSearches : AuthenticationBasePage // System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HomeLink.NavigateUrl = "~/" + GlobalSettings.SearchPage;

            if (!IsPostBack)
            {
                //ViewState["user"] =  "vnarapuram"; //GetKCUserId();
                ViewState["user"] = KCUserID; 
                ViewState["SortExpression"] = "searchName";
                ViewState["SortDirection"] = "Asc";
                BindSavedSearchesToGrid();
            }
        }

        protected void BindSavedSearchesToGrid()
        {
            DataSet savedSearchesDataset = new DataSet();
            savedSearchesDataset = SqlDataService.GetSavedSearchesOfUser(ViewState["user"].ToString(), ViewState["SortExpression"].ToString()+" "+ ViewState["SortDirection"].ToString());
            LogUsage("Saved Searches", "search1", " ");
            if (savedSearchesDataset.Tables.Count > 0 && savedSearchesDataset.Tables[0].Rows.Count > 0)
            {
                SavedSearchesGrid.Visible = true;
                SavedSearchesGrid.DataSource = savedSearchesDataset.Tables[0].DefaultView;
                SavedSearchesGrid.DataBind();
            }
            else
            {
                SavedSearchesGrid.Visible = false;
            }
        }

        protected void SavedSearchesGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteSavedSearch")
            {
                int rowNumber;
                rowNumber = Convert.ToInt32(e.CommandArgument);
                Label lblSearchId = new Label();
                lblSearchId.Text = ((Label)(SavedSearchesGrid.Rows[rowNumber].FindControl("lblSearchId"))).Text;

                SqlDataService.DeleteSavedSearch(Convert.ToInt32(lblSearchId.Text));
                BindSavedSearchesToGrid();
            }
        }

        protected void SavedSearchesGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["SortExpression"] = e.SortExpression;
            if (ViewState["SortExpression"].ToString() == "Asc")
                ViewState["SortExpression"] = "Desc";
            else
                ViewState["SortExpression"] = "Asc";
            BindSavedSearchesToGrid();            
        }

        protected void SavedSearchesGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView datarowView = e.Row.DataItem as DataRowView;

                HtmlAnchor aSearch = new HtmlAnchor();
                aSearch = ((HtmlAnchor)e.Row.FindControl("aSearch"));

                Label lblSearchId = new Label();
                lblSearchId = ((Label)e.Row.FindControl("lblSearchId"));

                Label lblDate = new Label();
                lblDate = ((Label)e.Row.FindControl("lblcreatedDate"));

                Label lblSearchName = new Label();
                lblSearchName = ((Label)e.Row.FindControl("lblSearchName"));

                ImageButton imageButtonDelete = new ImageButton();
                imageButtonDelete = (ImageButton)e.Row.FindControl("imageButtonDelete");

                imageButtonDelete.CommandArgument = e.Row.RowIndex.ToString();

                DateTime dt = Convert.ToDateTime(datarowView["createdDate"].ToString());
                lblDate.Text = dt.Day.ToString().PadLeft(2, '0') + "/" + dt.Month.ToString().PadLeft(2, '0') + "/" + dt.Year;
                
                string strRedirectPage = GlobalSettings.DefaultResultsPage.Substring(GlobalSettings.AlternateResultsPage.IndexOf("/") + 1);
                
                aSearch.HRef = strRedirectPage + "?SearchId=" + lblSearchId.Text;
                aSearch.InnerText = lblSearchName.Text;
            }

        }
    }
}
