<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/Logistics.Master" 
        AutoEventWireup="true" 
        CodeBehind="GroupResults.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Logistics.GroupResults" 
        Title="Datamonitor PremiumTools - Logistics, Group Results" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<%@ Register Src="~/Controls/SelectionList.ascx" TagName="Selections" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">
<link href="../assets/css/gridstyle2.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
function get_group_text(idx) 
{    
    return ResultsGrid.get_table().get_columns()[idx].get_headingText(); 
}
function GridRender()
{
    ResultsGrid.render();
}
function showHide(dialog)
{    
    Menu1.get_contextData().set_visible(false); 
    GridRender();    
}

function grd_onGroupingChange(sender, eventArgs)
{
  //alert("onGroupingChange");
  if (eventArgs.get_column())
  {
      
      col = eventArgs.get_column();
      if(col.get_visible()==true)
         col.set_visible(false); 
      else
         col.set_visible(true); 
      
      sender.render();  
      //alert(sender.get_id());
  }
  else
  {
    var cols = sender.get_table().get_columns();
    var i=0;   
    for (i=0;i<cols.length-1;i++)
    {        
        cols[i].set_visible(true);
    }
    sender.render();
  }
}

function RemoveGroups()
{
    var grd = <%=ResultsGrid.ClientObjectId %>;
    grd.unGroup();
}
</script>

<div class="statusbar">
    <div id="divCriteria" runat="server">
        <div id="breadcrumb_title" class="breadcrumb_line">
            <h1>
                MY CURRENT search</h1>
            <a href="#" id="lnkMysavedsearch" runat="server" onclick="return hs.htmlExpand(this, { contentId: 'highslide-save2' } )">
                <img src="../assets/images/transparent.gif" class="background_button_save" alt="Save this search"
                    width="103" height="15" border="0" /></a> <a id="lnkAlert" onclick="return hs.htmlExpand(this, { contentId: 'highslide-alert' } )"
                        href="#">
                        <%--<img class="background_button_alert" height="15" alt="Set as Research Alert" src="../assets/images/transparent.gif"
                            width="136" border="0"/>--%>
                    </a>
            <div class="highslide-html-content" id="highslide-save2">
                <div class="highslide-header">
                    <ul>
                        <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                        <li class="highslide-close">
                            <asp:LinkButton ID="lnkclose" runat="server" OnClientClick="hs.close(this);return false;"
                                Text="Close"></asp:LinkButton>
                        </li>
                    </ul>
                    <h1 class="popup_icon_save">
                        Save this Search
                    </h1>
                </div>
                <div class="highslide-body">
                    <p>
                        <b>Add your current search to the drop-down menu on the right so it'll only ever be
                            one click away.</b></p>
                    <p>
                        &nbsp;</p>
                    <p>
                        Please name your search</p>
                    <p>
                        <label>
                            <asp:TextBox ID="txtsavesearch" runat="server" CssClass="formstyle3" Width="220"></asp:TextBox>
                            <asp:HiddenField ID="hdnsavedsearchvalue" runat="server" />                                        
                        </label>
                    </p>
                    <p id="home" runat="server" visible="false">
                        <label>
                            <input type="checkbox" name="chkmakemyhome" id="chkmakemyhome" />
                        </label>
                        Make this My Home</p>
                    <p id="alert" runat="server" visible="false">
                        <label>
                            <input type="checkbox" name="chkresearchalert" id="chkresearchalert" />
                        </label>
                        Set as Research Alert</p>
                    <p>
                        &nbsp;</p>
                    <div class="button_right">                                    
                        <asp:LinkButton ID="lnksave" runat="server" Text="Save Search"></asp:LinkButton>                                    
                    </div>
                </div>
            </div>
            
            <div id="highslide-alert" class="highslide-html-content">
                <div class="highslide-header">
                    <ul>
                        <li class="highslide-move"><a onclick="return false" href="http://info.datamonitor.com/kcv2.1/productscan/viewresults1.htm#">
                            Move</a> </li>
                        <li class="highslide-close"><a onclick="return hs.close(this)" href="http://info.datamonitor.com/kcv2.1/productscan/viewresults1.htm#">
                            Close</a> </li>
                    </ul>
                    <h1 class="popup_icon_alert">
                        Set as Research Alert</h1>
                    <br />
                </div>
                <div class="highslide-body">
                    <p>
                        <b>Get regular email alerts on any new products related to your current search.</b></p>
                    <p>
                    </p>
                    <div class="button_right">
                        <a runat="server" id="lnkmakealert" href="#">Save as Alert</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
            
<div id="highslide-Groupalert" class="highslide-html-content">
    <div class="highslide-header">
        <ul>
            <li class="highslide-move"><a onclick="return false" href="http://info.datamonitor.com/kcv2.1/productscan/viewresults1.htm#">
                Move</a> </li>
            <li class="highslide-close"><a onclick="return hs.close(this)" href="http://info.datamonitor.com/kcv2.1/productscan/viewresults1.htm#">
                Close</a> </li>
        </ul>
        <h1 class="popup_icon_alert">
            Set as Group Research Alert</h1>
        <br />
    </div>
    <div class="highslide-body">
        <p>
            <b>Get regular email alerts on any new products related to your current search.</b></p>
        <p>
        </p>
        <div class="button_right">
            <a runat="server" id="lnkmakegrpalert" href="#">Save as Alert</a></div>
    </div>
</div>

<div id="SearchAndSelections" class="breadcrumb" runat="server">
    <ul>
        <li>Search Criteria</li>
        <li class="didyoumean"><a href="javascript:toggleLayer('maincontent_view');">[Show/Hide]</a></li>      
       
    </ul>
    <div id="maincontent_view" class="marketdata_search">
        <uc4:Selections ID="UcSelectionsTop" runat="server"/>
        <div class="button_right" style="width:150px"><a href="Default.aspx" style="width:150px">Update Results</a></div>
        
    </div>
</div>

<div id="maincontent_full">
    <div id="pagetitle">
       <table width="100%;" style="height:25">
            <tr>
                <td style="width:50%;"> 
                    <h1>Logistics & Express</h1>
                </td>
                <td align="right" style="width:50%;">
                    <a href="#" class="other_links" style="font-size:10px; font-family:verdana; font-weight:lighter;">Ask an analyst a question</a>
                </td>
            </tr>
      </table>
    </div>
    <ul id="productscan_tabs">
        <li><asp:HyperLink ID="SearchLink" runat="server" NavigateUrl="~/Search.aspx" Text="Search"></asp:HyperLink></li>
         <li><a class="selected">View Results</a></li>
        <li><asp:HyperLink ID="AnalyzeResultsLink" runat="server" NavigateUrl="~/Analysis/XYChart.aspx" Text="Analyze Results"></asp:HyperLink></li>   
    </ul>     
        <div id="display_options">
        <div>
            <div class="display_options">
                <h3>                        
                    Display options 
                </h3>
            </div>
            <div>
              <table width="400">
                    <tr>
                        <td>
                            <asp:CheckBox ID="CountryCheckbox" runat="server" Text="Country" /><br />
                            <asp:CheckBox ID="ModeCheckbox" runat="server" Text="Mode" /><br />
                            <asp:CheckBox ID="MarketCheckBox" runat="server" Text="Market" /><br />
                            <asp:CheckBox ID="DestinationCheckbox" runat="server" Text="Destination" /><br />
                            <asp:CheckBox ID="DatatypeCheckBox" runat="server" Text="Data Type" />   <br />                     
                            <asp:CheckBox ID="UnitCheckBox" runat="server" Text="Unit" />    <br />                    
                            <asp:CheckBox ID="CurrencyCheckBox" runat="server" Text="Currency" />  
                        </td>
                   
                        <td>                
                            <asp:CheckBox ID="Y2008Checkbox" runat="server" Text="2008" /><br />
                            <asp:CheckBox ID="Y2009CheckBox" runat="server" Text="2009" /><br />
                            <asp:CheckBox ID="Y2010CheckBox" runat="server" Text="2010" /><br />
                            <asp:CheckBox ID="Y2011CheckBox" runat="server" Text="2011" /><br />
                            <asp:CheckBox ID="Y2012CheckBox" runat="server" Text="2012" /><br />
                            <asp:CheckBox ID="Y2013CheckBox" runat="server" Text="2013" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
           <div id="display_options_buttons">
                        <a href="javascript:toggleLayer('display_options');">Cancel</a>
                        <a onclick="JavaScript:toggleLayer('display_options');" id="nkUpdate" class="save_button" href="#">Update</a>
                    </div>
                    </div>
    <div id="display_options_tab">
            <a href="javascript:toggleLayer('display_options');">
                <div>
                    Display options
                </div>
            </a>
     </div>
        <br />
        <a onclick="RemoveGroups()" >Remove Grouping</a>   
     <ComponentArt:Grid id="ResultsGrid" runat="server"
            CssClass="grid"
            AllowColumnResizing="true"
            ShowSearchBox="false"
            SearchBoxPosition="TopRight"
            SearchOnKeyPress="false"
            GroupBy=""
            GroupingMode="ConstantRows"
            GroupByCssClass="grp-hd-txt"
            GroupByTextCssClass="txt"
            GroupBySectionCssClass="grp"
            GroupBySectionSeparatorCssClass=""
            GroupingNotificationTextCssClass="txt"
            Sort="Country"
            GroupingNotificationText="Drag a column to this area to group by it."
            IndentCellCssClass="ind"
            DataAreaCssClass="GridData"
            EnableViewState="true"
            RunningMode="Client"
            ShowHeader="true"
            ShowFooter="false"
            HeaderCssClass="hdr"
            FooterCssClass="ftr"                       
            PreExpandOnGroup="true"           
            PagerStyle="Numbered"
            PagerTextCssClass="txt"
            PagerButtonWidth="44"
            PagerButtonHeight="27"
            PagerButtonHoverEnabled="true"
            ScrollBar="On" 
            ScrollTopBottomImagesEnabled="true"
            ScrollTopBottomImageHeight="2" 
            ScrollTopBottomImageWidth="16" 
            ScrollImagesFolderUrl="../assets/images/scroller/"
            ScrollButtonWidth="16" 
            ScrollButtonHeight="17" 
            ScrollBarWidth="16" 
               ScrollBarCssClass="ScrollBar"
        ScrollGripCssClass="ScrollGrip"
            ScrollPopupClientTemplateId="scrollerTemplate"
            ImagesBaseUrl="../assets/images/"
            PagerImagesFolderUrl="../assets/images/pager/"            
            Width="900" Height="400"            
            IndentCellWidth="16"
            TreeLineImagesFolderUrl="../assets/images/lines/"
            TreeLineImageWidth="11"
            TreeLineImageHeight="11"              
            AllowHorizontalScrolling="true">
            <Levels>
                <ComponentArt:GridLevel    
                    DataKeyField="rowID"                 
                    ShowTableHeading="false"
                    ShowSelectorCells="false"
                    RowCssClass="row"
                    HoverRowCssClass="row-h"
                    SelectedRowCssClass="row-s"
                    ColumnReorderIndicatorImageUrl="reorder.gif"
                    DataCellCssClass="dat"
                    HeadingCellCssClass="cell"
                    HeadingCellHoverCssClass="cell-h"
                    HeadingRowCssClass="row-hd"
                    HeadingTextCssClass="txt"
                    SortedHeadingCellCssClass="sort"
                    SortAscendingImageUrl="col-asc.png"
                    SortDescendingImageUrl="col-desc.png"
                    SortImageWidth="12"
                    SortImageHeight="22"
                    GroupHeadingCssClass="grp-hd"
                    GroupHeadingClientTemplateId="GroupHeadingTemplate">                   
                    <Columns>
                        <ComponentArt:GridColumn HeadingText="" Width="15" DataCellClientTemplateId="ChartTemplate" AllowGrouping="false" />             
                        <ComponentArt:GridColumn DataField="Country" Width="100" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" FixedWidth="False" Visible="true" />
                        <ComponentArt:GridColumn DataField="Mode" Width="100" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" FixedWidth="False"  Visible="true"/>
                        <ComponentArt:GridColumn DataField="Market" Width="250" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" FixedWidth="False"   Visible="true"/>
                        <ComponentArt:GridColumn DataField="Destination" Width="100" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" FixedWidth="False" />
                        <ComponentArt:GridColumn DataField="DataType" Width="100" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" FixedWidth="False" Visible="true" />
                        <ComponentArt:GridColumn DataField="Units" HeadingText="Units" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" Width="120" />
                        <ComponentArt:GridColumn DataField="Currency" Width="100"  ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" HeadingText="Currency" />
                        <ComponentArt:GridColumn DataField="Y2008"  Width="80" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" HeadingText="2008" FormatString="###,###,###,##0.00" Align="Right" />
                        <ComponentArt:GridColumn DataField="Y2009"  Width="80" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" HeadingText="2009" FormatString="###,###,###,##0.00" Align="Right"  />
                        <ComponentArt:GridColumn DataField="Y2010"  Width="80" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" HeadingText="2010" FormatString="###,###,###,##0.00" Align="Right"  />
                        <ComponentArt:GridColumn DataField="Y2011"  Width="80" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" HeadingText="2011" FormatString="###,###,###,##0.00" Align="Right"  />
                        <ComponentArt:GridColumn DataField="Y2012"  Width="80" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" HeadingText="2012" FormatString="###,###,###,##0.00" Align="Right"  />
                        <ComponentArt:GridColumn DataField="Y2013"  Width="80" ContextMenuId="Menu1" ContextMenuHotSpotCssClass="col-mnu" ContextMenuHotSpotHoverCssClass="col-mnu-h" ContextMenuHotSpotActiveCssClass="col-mnu-d" HeadingText="2013" FormatString="###,###,###,##0.00" Align="Right"  />                        
                        <ComponentArt:GridColumn DataField="rowID"  Visible="false" />
                    </Columns>
                </ComponentArt:GridLevel>
            </Levels>
              <ClientEvents>
                <GroupingChange EventHandler="grd_onGroupingChange" />
              </ClientEvents>

            <ClientTemplates>
                <ComponentArt:ClientTemplate Id="GroupHeadingTemplate">
                    <span >## get_group_text(DataItem.get_column()) ##: ## DataItem.get_columnValue() ##</span>
                </ComponentArt:ClientTemplate>
                <ComponentArt:ClientTemplate ID="ChartTemplate">
          <div style="padding: 0px; font-weight: bold; text-align: center;">
                <a style="color: Red; text-decoration: none;" 
                    href="TrendImage.aspx?rowid=## DataItem.GetMember('rowID').Text ##" 
                    onclick="return hs.htmlExpand(this, { contentId: 'PopupChart', objectType: 'ajax'} )">
                    <img alt="" border="0" src="../assets/images/chart_icon.gif" />
                </a>                 
            </div>
          </ComponentArt:ClientTemplate>
          
                <ComponentArt:ClientTemplate ID="scrollerTemplate">
                    <!-- Call Back Mode -->
<%--                    <div style="background-color: #404040; padding: 12px; color: #e0e0e0; font-family: arial, verdana; font-size: 12px;
                      font-weight: bold;"> Page <b>## DataItem.PageIndex + 1 ##</b> of <b>## Grid1.PageCount ##</b>
                    </div>--%>
                    <!-- Client Mode -->
                 <table class="SliderPopup" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                      <td valign="top" style="height:14px;background-color:#ffffff;padding:5px;border:solid 2px #0000A0;">
                      <table width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tr>
                        <td width="25" align="center" valign="top" style="padding-top:3px;"><img src="../assets/images/tool_extract.gif" width="14" height="14"></td>
                        <td>
                        <table cellspacing="0" cellpadding="1" border="0" style="width:255px;">
                        <tr>
                          <td colspan="2" style="font-family:verdana;font-size:11px;"><div style="overflow:hidden;width:250px;"><nobr>Market: ## DataItem.GetMember('Market').Value ##</nobr></div></td>
                        </tr>
                        <tr>
                          <td style="font-family:verdana;font-size:11px;"><div style="overflow:hidden;width:115px;"><nobr>Country: ## DataItem.GetMember('Country').Value ##</nobr></div></td>
                          <td style="font-family:verdana;font-size:11px;" align="right"><div style="overflow:hidden;width:135px;"><nobr>Mode:## DataItem.GetMember('Mode').Text ##</nobr></div></td>
                        </tr>
                        <tr>
                          <td style="font-family:verdana;font-size:11px;">Destination: ## DataItem.GetMember('Destination').Text ##</td>
                          <td style="font-family:verdana;font-size:11px;" align="right">Measure: ## DataItem.GetMember('DataType').Text ##</td>
                        </tr>
                        </table>
                        </td>
                      </tr>
                      </table>
                      </td>
                    </tr>
                    <!--<tr>
                      <td style="height:14px;background-color:#757598;">
                      <table width="100%" cellspacing="0" cellpadding="0" border="0">
                      <tr>
                        <td style="padding-left:5px;color:white;font-family:verdana;font-size:10px;">
                        Page <b>## DataItem.PageIndex + 1 ##</b> of <b>## Grid1.PageCount ##</b>
                        </td>
                        <td style="padding-right:5px;color:white;font-family:verdana;font-size:10px;" align="right">
                        Thread <b>## DataItem.Index + 1 ##</b> of <b>## Grid1.RecordCount ##</b>
                        </td>
                      </tr>
                      </table>
                      </td>
                    </tr>-->
                    </table>
                  </ComponentArt:ClientTemplate>
            </ClientTemplates>
        </ComponentArt:Grid>
    
    <ComponentArt:Menu
        Id="Menu1"
        RunAt="server"
        ContextMenu="Custom"
        SiteMapXmlFile="~/config/GroupGridMenu.xml"
        Orientation="Vertical"
        DefaultGroupExpandOffsetX="4"
        DefaultGroupExpandOffsetY="-1"
        ExpandDuration="0"
        CollapseDuration="0"
        DefaultItemLookId="ItemLook"
        CssClass="mnu"
        ShadowEnabled="true"
    >
        <ItemLooks>
            <ComponentArt:ItemLook LookId="ItemLook" CssClass="itm" HoverCssClass="itm-h" />
            <ComponentArt:ItemLook LookId="BreakItemLook" CssClass="br" />
        </ItemLooks>

        <ClientTemplates>
            <ComponentArt:ClientTemplate ID="ItemTemplate">
                <div>
                    <span class="ico ## DataItem.get_value(); ##"></span>
                    <span class="txt">## DataItem.get_text(); ##</span>
                </div>
            </ComponentArt:ClientTemplate>
        </ClientTemplates>
    </ComponentArt:Menu>

   
<div class="highslide-html-content" id="PopupChart">
                <div class="highslide-header">
                    <ul>
                        <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                        <li class="highslide-close">
                            <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="hs.close(this);return false;"
                                Text="Close"></asp:LinkButton>
                        </li>
                    </ul>
                    <h1>Trend Chart</h1>
                </div>
                <div class="highslide-body">
                </div>
            </div>
    
    </div>
</asp:Content>

