using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Aspose.Cells;
using System.Xml;

namespace Datamonitor.PremiumTools.Generic
{
    public partial class View2 : AuthenticationBasePage
    {
        //PRIVATE MEMBERS
        private string savedSearchXml;
        private DataSet _unitsDataSet;
        private string SearchSource;

        public int NoOfDecimalsToGridValues
        {
            get
            {
                return GlobalSettings.NoOfDecimalsToGridValues;
            }
        }


        public string CurrencyExchangeRateYear
        {
            get 
            { 
                //Used to decide which year's exchange rate is used in currency conversion.
                return GlobalSettings.CurrencyExchangeRateYear.ToString(); 
            }
        }

        // EVENT OVERRIDES

        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);

            //Check whether query string has 'qsource' key or not
            if (Request.QueryString["qsource"] != null &&
                !string.IsNullOrEmpty(Request.QueryString["qsource"].ToString()))
            {
                //Search source is to decide from which page this result page is redirected.
                SearchSource = Request.QueryString["qsource"].ToString();
            }

            //Update the session with latest selections string
            //AddDefaultSeletionsToSession();            
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Results";

            if (Request.Browser.Browser == "Firefox")
            {
                Response.Cache.SetNoStore();
            }
            
            ((ViewOptions)Master.FindControl("ViewOptionsControl")).Source = "view1";
            ((MasterPages.ResultsMaster)Master).AllowStatusBar = true;
 
            //Adding attribute to controls
            StartYear.Attributes.Add("onchange", "javascript:Year_onChange('startyear',this);");
            EndYear.Attributes.Add("onchange", "javascript:Year_onChange('endyear',this);");
            currency.Attributes.Add("onchange", "javascript:currency_onChange(this);");

            //Log
            LogUsage("Results - Grid View2", "view2", " ");

            if (!IsPostBack && !ResultsGrid.CausedCallback)
            {
                #region Initial page loading activities         
       
                //Set default values
                Session["HiddenColumns"] = "";
                Session["Units"] = hdnUnitsConversions.Value;
                HiddenColumns.Value = "|";                                
                hdnConversionID.Value = "0";
                Session["TargetCurrencyID"] = "0";
                Session["CurrencyConversionType"] = "false";
                hdnCurrencyConversionType.Value="false";
                FixedConversion.Checked = true;
                
                //Bind Currency list
                BindCurrencies();

                //Intialise default currency id hidden field
                if (currency.SelectedValue.Length > 0)
                {
                    hdnConversionID.Value = currency.SelectedValue;
                }

                #endregion
            }
            if (IsPostBack)
            {
                #region Maintain 'display options' selections on postback 

                Session["HiddenColumns"] = HiddenColumns.Value.StartsWith("|") ? HiddenColumns.Value : "|" + HiddenColumns.Value;
                Session["Units"] = hdnUnitsConversions.Value;
                Session["TargetCurrencyID"] = hdnConversionID.Value;
                Session["CurrencyConversionType"] = hdnCurrencyConversionType.Value;

                #endregion
            }

            #region Saved search code
            
            if (Request.QueryString["SearchId"] != null && Request.QueryString["SearchId"] != "")
            {
                //Initialising saved sear xml -- if 'searchid' is found in query string
                DataSet savedSearchesDataset = new DataSet();
                int searchId = Convert.ToInt32(Request.QueryString["SearchId"]);
                savedSearchesDataset = SqlDataService.GetSavedSearchSearchCriteria(searchId);
                if (savedSearchesDataset.Tables.Count > 0 && savedSearchesDataset.Tables[0].Rows.Count > 0)
                {
                    savedSearchXml = savedSearchesDataset.Tables[0].Rows[0]["searchXml"].ToString();
                }
            }

            #endregion

            #region Maintain currency conversion selections on postback

            if (hdnConversionID.Value != "-1" && 
                hdnConversionID.Value != "-2" &&
                hdnConversionID.Value != "-3" && 
                GlobalSettings.AllowAbsoluteValues)
            {
                ShowAbsoluteValues.Checked = true;
                currency.SelectedValue = hdnConversionID.Value;
            }
            else if (hdnConversionID.Value == "-1" && GlobalSettings.AllowIndexTo100)
            {
                ShowIndexTo100.Checked = true;         
            }
            else if (hdnConversionID.Value == "-2" && GlobalSettings.AllowPerCapita)
            {
                ShowPerCapita.Checked = true;
            }
            else if (hdnConversionID.Value == "-3" && GlobalSettings.AllowGrowthRates)
            {
                ShowGrowths.Checked = true;
            }

            #endregion
        }


        // PRIVATE METHODS (initial population)       
       

        private void GetMinMaxYears(DataTable Years, ref int minyear, ref int maxYear)
        {
            try
            {
                //Get list of valid years from session
                int StartYear = CurrentSession.GetFromSession<int>("StartYear");
                int EndYear = CurrentSession.GetFromSession<int>("EndYear");

                //get Min & Max years from the table                        
                if (Years != null &&
                    Years.Rows.Count > 0)
                {
                    int MinYear = Convert.ToInt32(Years.Rows[0]["MinYear"].ToString());
                    int MaxYear = Convert.ToInt32(Years.Rows[0]["MaxYear"].ToString());
                    //compare min & max year with start & end years and take the intersection of them
                    minyear = MinYear > StartYear ? MinYear : StartYear;
                    maxYear = MaxYear < EndYear ? MaxYear : EndYear;
                }
            }
            catch { }
        }

        private void BindDataToGrid()
        {
            //clears the grid columns & ColumnFilterDiv1
            ColumnFilterDiv1.Controls.Clear();
            ResultsGrid.Levels[0].Columns.Clear();

            //Get results from DB
            DataSet ResultsSet = GetDataToBindGrid(true);

            //get MinYear & MaxYear
            int MinYear = 0, MaxYear = 5000;

            if (ResultsSet != null)
            {
                //Get Min & Max years from the result set
                if (ResultsSet.Tables.Count > 3)
                {
                    GetMinMaxYears(ResultsSet.Tables[3], ref MinYear, ref MaxYear);
                }

                #region Binding the years to dropdown controls

                //Get list of valid years from session
                int SessionStartYear = CurrentSession.GetFromSession<int>("StartYear");
                int SessionEndYear = CurrentSession.GetFromSession<int>("EndYear");

                //Taking intersction of 'start & end' - 'min & max' years
                SessionStartYear = SessionStartYear > MinYear ? SessionStartYear : MinYear;
                SessionEndYear = SessionEndYear < MaxYear ? SessionEndYear : MaxYear;

                //Clear the dropdowns before ading tiesm
                StartYear.Items.Clear();
                EndYear.Items.Clear();

                //Assigning the years as items to dropdowns
                for (int counter = SessionStartYear; counter <= SessionEndYear; counter++)
                {
                    StartYear.Items.Add(new ListItem(counter.ToString()));
                    EndYear.Items.Add(new ListItem(counter.ToString()));
                }

                //Setting default values
                hdnStartYear.Value = (string.IsNullOrEmpty(hdnStartYear.Value) || !StartYear.Items.Contains(new ListItem(hdnStartYear.Value))) ?
                    SessionStartYear.ToString() :
                    hdnStartYear.Value;
                hdnEndYear.Value = (string.IsNullOrEmpty(hdnEndYear.Value) || !EndYear.Items.Contains(new ListItem(hdnEndYear.Value))) ?
                    SessionEndYear.ToString() :
                    hdnEndYear.Value;

                StartYear.SelectedValue = hdnStartYear.Value;
                EndYear.SelectedValue = hdnEndYear.Value;

                #endregion

                //Load data into grid
                string columnsToIgnore = GlobalSettings.ColumnsToIgnore;
                string columnsToHide = GlobalSettings.ColumnsToHide;
                string columnsToHideIntially = GlobalSettings.ColumnsToHideIntially;
                Dictionary<string, string> removedColumns = new Dictionary<string, string>();

                if (ResultsSet.Tables.Count > 1)
                {                   
                    #region Bind Units repeter

                    if (ResultsSet.Tables.Count > 5 && ResultsSet.Tables[4].Rows.Count > 0)
                    {
                        //Store the units data table
                        _unitsDataSet = ResultsSet;

                        rptUnitSelections.Visible = true;

                        //Bind the units data to repeater
                        rptUnitSelections.DataSource = ResultsSet.Tables[4];
                        rptUnitSelections.DataBind();
                    }
                    else
                    {
                        rptUnitSelections.Visible = false;

                        //Bind the units data to repeater
                        rptUnitSelections.DataSource = null;
                        rptUnitSelections.DataBind();
                    }

                    #endregion

                    bool IsTrendChartColumnAdded = false;
                    //Get predefined view selectable columns
                    object ViewID = CurrentSession.GetFromSession<string>("PredefinedViewID");
                    string PredefinedViewID;

                    PredefinedViewID = (ViewID != null) ? (string)ViewID : string.Empty;

                    string SelectableTaxonomyTypes = string.Empty;

                    SelectableTaxonomyTypes = PredefinedViewID.Length > 0 ?
                         GetPredefinedViewSelectableIDs(PredefinedViewID) :
                         ConfigurationManager.AppSettings.Get("SelectableTaxonomiesInGrid1");

                    //if (SelectableTaxonomyTypes.Length > 0)
                    //{
                    //    List<string> Ignorecolumns = GetColumnRowIdsbyTaxonomyIds(SelectableTaxonomyTypes);
                    //    foreach (string str in Ignorecolumns)
                    //    {
                    //        columnsToIgnore += string.Format("|{0}|", str);
                    //    }
                    //}
                    ColumnToHide.Value = "";

                    #region Adding columns to results grid

                    foreach (DataColumn column in ResultsSet.Tables[0].Columns)
                    {
                        bool AddColumnToGrid = Session["HiddenColumns"].ToString().Contains("|" + column.ColumnName + "|") ? false : true;

                        //Add data columns
                        if (column.ColumnName.StartsWith("Y19") ||
                            column.ColumnName.StartsWith("Y20") ||
                            column.ColumnName.StartsWith("Y21") ||
                            column.ColumnName.ToLower().Equals("cagr"))
                        {
                            if ((column.ColumnName.StartsWith("Y19") || column.ColumnName.StartsWith("Y20") || column.ColumnName.StartsWith("Y21")) &&
                                (Convert.ToInt32(column.ColumnName.Substring(1)) < Convert.ToInt32(hdnStartYear.Value) ||
                                Convert.ToInt32(column.ColumnName.Substring(1)) > Convert.ToInt32(hdnEndYear.Value)))
                                continue;

                            if (AddColumnToGrid)
                            {
                                if (!IsTrendChartColumnAdded)
                                {
                                    //Add source column
                                    if (!ConfigurationManager.AppSettings.Get("ShowSourceColumninGrid").Equals("false"))
                                    {
                                        GridColumn SourceColumn = new GridColumn();
                                        SourceColumn.HeadingCellCssClass = "HeadingCell_Freeze";
                                        SourceColumn.DataCellCssClass = "HeadingCell_Freeze";
                                        SourceColumn.DataCellClientTemplateId = "DataSourceClientTemplate";
                                        SourceColumn.Width = 40;
                                        SourceColumn.HeadingText = "Source";
                                        ResultsGrid.Levels[0].Columns.Add(SourceColumn);
                                    }

                                    //Add trend chart column
                                    GridColumn TrendChartColumn = new GridColumn();
                                    TrendChartColumn.HeadingCellCssClass = "HeadingCell_Freeze";
                                    TrendChartColumn.DataCellCssClass = "HeadingCell_Freeze";
                                    TrendChartColumn.DataCellClientTemplateId = "TrendChartTemplate";
                                    TrendChartColumn.Width = 15;
                                    TrendChartColumn.HeadingText = "";
                                    ResultsGrid.Levels[0].Columns.Add(TrendChartColumn);

                                    IsTrendChartColumnAdded = true;
                                }

                                string DataTemplate = GlobalSettings.FormatValuesInGrid ?
                                    "FormattedDataTemplate" :
                                    "CommonDataTemplate";

                                GridColumn col1 = new GridColumn();
                                col1.DataField = column.ColumnName;
                                col1.Width = GlobalSettings.GetDefaultGridColumnWidth;
                                col1.HeadingText = column.ColumnName.Replace("Y", "");
                                col1.Align = ComponentArt.Web.UI.TextAlign.Right;
                                col1.DataCellClientTemplateId = !column.ColumnName.ToLower().Equals("cagr") ?
                                    DataTemplate :
                                    "CAGRTemplate";
                                ResultsGrid.Levels[0].Columns.Add(col1);
                            }

                        }
                        else if (!columnsToIgnore.ToLower().Contains("|" + column.ColumnName.ToLower() + "|"))
                        {

                            bool IsHideColumn = columnsToHide.ToLower().Contains(("|" + column.ColumnName.ToLower() + "|"));
                            bool IsHideColumnIntially = columnsToHideIntially.ToLower().Contains(("|" + column.ColumnName.ToLower() + "|"));
                            bool IsColumnIDType = false;

                            int ColumnVisibility = GetColumnVisibility(ResultsSet.Tables[1], column.ColumnName);
                            if (ColumnVisibility == 1 && ResultsSet.Tables[0].Rows.Count > 0)
                            {
                                string ColumnValue = ResultsSet.Tables[0].Rows[0][column.ColumnName].ToString();
                                foreach (DataRow RemovedColumnRow in ResultsSet.Tables[0].Rows)
                                {
                                    if (!string.IsNullOrEmpty(RemovedColumnRow[column.ColumnName].ToString()))
                                    {
                                        ColumnValue = RemovedColumnRow[column.ColumnName].ToString();
                                        break;
                                    }
                                }
                                removedColumns.Add(column.ColumnName, ColumnValue);
                            }
                            //if (IsHideColumnIntially || ColumnVisibility == 1)
                            //{
                            //    if (AddColumnToGrid)
                            //    {
                            //        ColumnToHide.Value = ColumnToHide.Value.Length > 0 ?
                            //            ColumnToHide.Value + "," + (ResultsGrid.Levels[0].Columns.Count - 1).ToString() :
                            //            (ResultsGrid.Levels[0].Columns.Count - 1).ToString();
                            //    }
                            //}

                            if (AddColumnToGrid && !IsHideColumnIntially && ColumnVisibility != 1)
                            {
                                GridColumn col1 = new GridColumn();
                                col1.DataField = column.ColumnName;
                                col1.Width = GetColumnWidth(ResultsSet.Tables[2], column.ColumnName);
                                col1.HeadingText = column.ColumnName;
                                col1.HeadingCellCssClass = "HeadingCell_Freeze";

                                if (SelectableTaxonomyTypes.Length > 0)
                                {
                                    string currenctColumnID = GetColumnIDByName(column.ColumnName);
                                    if (("," + SelectableTaxonomyTypes + ",").Contains("," + currenctColumnID + ","))
                                    {
                                        col1.HeadingCellClientTemplateId = "ColumnFilterTemplate";
                                    }
                                    else
                                    {
                                        col1.HeadingCellClientTemplateId = "ColumnNonFilterTemplate";
                                    }
                                }
                                else
                                {
                                    col1.HeadingCellClientTemplateId = "ColumnFilterTemplate";
                                }

                                //Assign the data template id
                                if (GlobalSettings.ShowTaxonomyDefinitionInResultsGrid)
                                {
                                    switch (column.ColumnName.ToLower())
                                    {
                                        case "market":
                                        case "indicator":
                                            col1.DataCellClientTemplateId = "TooltipTemplateWithDefinition";
                                            break;
                                        case "measure":
                                            col1.DataCellClientTemplateId = "MeasureTooltipTemplateWithDefinition";
                                            break;
                                        default:
                                            col1.DataCellClientTemplateId = "TooltipTemplate";
                                            break;
                                    }
                                }
                                else
                                {
                                    col1.DataCellClientTemplateId = "TooltipTemplate";
                                }

                                col1.Visible = !IsHideColumn;

                                int nextIndex = ResultsSet.Tables[0].Columns[column.ColumnName].Ordinal + 1;
                                int result;
                                col1.DataCellCssClass = Int32.TryParse(ResultsSet.Tables[0].Columns[nextIndex].ColumnName.Replace("Y", ""), out result) ?
                                    "DataCell_FreezeLastCol" :
                                    "DataCell_Freeze";

                                ResultsGrid.Levels[0].Columns.Add(col1);
                            }


                            if (!IsHideColumn)
                            {
                                string isChecked = IsHideColumnIntially || ColumnVisibility == 1 ? "" : "CHECKED";
                                if (isChecked.Length > 0)
                                {
                                    isChecked = AddColumnToGrid ? "CHECKED" : "";
                                }
                                else
                                {
                                    Session["HiddenColumns"] = Session["HiddenColumns"].ToString().Trim().StartsWith("|") ?
                                       Session["HiddenColumns"].ToString() : "|" + Session["HiddenColumns"].ToString();

                                    Session["HiddenColumns"] = Session["HiddenColumns"].ToString() + column.ColumnName + '|';
                                }


                                Literal brk = new Literal();
                                brk.Text = string.Format("<INPUT TYPE='checkbox' NAME='chkCol{0}' ID='{0}' VALUE='{0}' {2} onClick='CheckChange(this,\"{1}\");' {3}>{1}<br />",
                                    (ResultsGrid.Levels[0].Columns.Count - 1).ToString(),
                                    column.ColumnName,
                                    isChecked,
                                    removedColumns.ContainsKey(column.ColumnName) ? "disabled" : "");
                                ColumnFilterDiv1.Controls.Add(brk);
                            }

                        }
                    }

                    #endregion

                    //Assigning data to Results grid
                    ResultsGrid.DataSource = ResultsSet;

                    GridStatusMessage.Text = ResultsSet.Tables[0].Rows.Count > 0 ? "" : "No results found for the current selection criteria.";

                    #region Assigning data to RemovedTaxonomies grid

                    //Prepare removed columns list/table
                    DataTable RemovedColumnsTable = new DataTable();
                    RemovedColumnsTable.Columns.Add("key");
                    RemovedColumnsTable.Columns.Add("value");
                    RemovedColumnsTable.Columns.Add("isvisible");
                    foreach (KeyValuePair<string, string> pair in removedColumns)
                    {
                        string currenctColumnID = GetColumnIDByName(pair.Key);
                        RemovedColumnsTable.Rows.Add(pair.Key,
                            pair.Value,
                            (("," + SelectableTaxonomyTypes + ",").Contains("," + currenctColumnID + ",")) ? "visible" : "hidden");
                    }
                    
                    //Bind removed column table to repeater
                    RemovedTaxonomies.DataSource = RemovedColumnsTable;
                    RemovedTaxonomies.DataBind();

                    #endregion
                }
            }
        }

        private DataSet GetDataToBindGrid(bool forGrid)
        {
            string SelectionsXML = string.Empty;
            DataSet ResultsSet = null;
            string conversionIDs = string.Empty;
            string PredefinedViewID=string.Empty;

            //Get the filter criteria from session 
            if (string.IsNullOrEmpty(savedSearchXml))
            {
                SelectionsXML = GetFilterCriteriaWithForcedTaxonomies();
            }
            else
            {
                SelectionsXML = savedSearchXml;
            }

            //Check whether any selections are found or not.. if not then redirect to search page
            if (!SelectionsXML.Trim().Replace(" ","").Equals("<selections></selections>"))
            {
                //Get ColumnIds
                StringBuilder sb = new StringBuilder();
                Dictionary<string, string> ColumnIDs = GlobalSettings.ColumnIds;
                //foreach (KeyValuePair<string, string> row in ColumnIDs)
                //{
                //    if (!row.Key.Equals("None", StringComparison.CurrentCultureIgnoreCase))
                //    {
                //        sb.Append(string.Format("COUNT(DISTINCT {0}) as [{1}] ,", row.Key, row.Value));
                //    }
                //}
                string ColumnNameIDs = sb.ToString();
                ColumnNameIDs = ColumnNameIDs.Length > 0 ? ColumnNameIDs.Substring(0, ColumnNameIDs.Length - 2) : ColumnNameIDs;

                //Get predefined view id
                object ViewID = CurrentSession.GetFromSession<string>("PredefinedViewID");
                PredefinedViewID = (ViewID != null) ? (string)ViewID : string.Empty;

                //Preparing unit's conversionIDs string
                foreach (string str in Session["Units"].ToString().Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    conversionIDs += str.Split(new char[] { '~' })[1] + ",";
                }
                conversionIDs = conversionIDs.TrimEnd(new char[] { ',' });

                //Get list of valid years from session
                int SessionStartYear = CurrentSession.GetFromSession<int>("StartYear");
                int SessionEndYear = CurrentSession.GetFromSession<int>("EndYear");

                //Get results
                ResultsSet = SqlDataService.GetGridDataView2(SelectionsXML,
                    false,
                    Convert.ToInt32(string.IsNullOrEmpty(Session["TargetCurrencyID"].ToString()) ? "-1" : Session["TargetCurrencyID"].ToString()),
                    GlobalSettings.IncludeMacroeconomicData,
                    PredefinedViewID,
                    conversionIDs,
                    forGrid ? ResultsGrid.CurrentPageIndex : 0,
                    forGrid ? -1 : GlobalSettings.ExcelDownloadMaxRows,
                    string.IsNullOrEmpty(hdnStartYear.Value) ? SessionStartYear.ToString() : hdnStartYear.Value,
                    string.IsNullOrEmpty(hdnEndYear.Value) ? SessionEndYear.ToString() : hdnEndYear.Value,
                    Convert.ToBoolean(Session["CurrencyConversionType"].ToString()));
            }

            //Redirect back to previous search page... if no results found for the current selections
            if (string.IsNullOrEmpty(SearchSource))
            {
                Response.Redirect("~/" + GlobalSettings.SearchPage);
            }
            else if (ResultsSet == null ||
                ResultsSet.Tables.Count == 0 ||
                ResultsSet.Tables[0].Rows.Count == 0)
            {
                if (SearchSource == "1")
                {
                    Response.Redirect("~/" + GlobalSettings.SearchPage + "?noresults=" + SearchSource);
                }
                else if (SearchSource == "2")
                {
                    Response.Redirect("~/" + GlobalSettings.QuickSearchPage + "?noresults=" + SearchSource);
                }
            }            

            return ResultsSet;
        }

        private int GetColumnWidth(DataTable widthsTable, string columnName)
        {
            //Get default grid column width from config
            int Width = GlobalSettings.GetDefaultGridColumnWidth;

            //Loop through all rows to get the corresponding column width
            foreach (DataRow row in widthsTable.Rows)
            {
                if (row["columnName"].ToString().Equals(columnName))
                {
                    Width = Int32.Parse(row["width"].ToString());
                    break;
                }
            }

            return Width;
        }

        private int GetColumnVisibility(DataTable visibilityTable, string columnName)
        {
            //Default visibility value
            int Visible = 2;
            try
            {
                //Get the visibility vlaue for the given column name
                Visible = visibilityTable.Rows[0][columnName].ToString().Equals("1") ? 1 : 0;
            }
            catch (Exception ex) { }

            return Visible;
        }

        private string GetYearsForMacroIndicators(string taxonomySelectedValue)
        {
            //Get default start & end years from config
            int DefaultStartYear = Convert.ToInt32(GlobalSettings.GetMacroEconomicDataMinYear);
            int DefaultEndYear = Convert.ToInt32(GlobalSettings.GetMacroEconomicDataMaxYear);

            //Get current start & end years from session
            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
            int EndYear = CurrentSession.GetFromSession<int>("EndYear");

            string IndicatorYears = "";

            //Loop through all years to prepare valid years list
            while (EndYear >= StartYear)
            {
                IndicatorYears += (StartYear >= DefaultStartYear && StartYear <= DefaultEndYear) ?
                    "cast([Y" + StartYear + "]as varchar(max)) as [Y" + StartYear + "], " :
                    "'0.0' AS [Y" + StartYear + "], ";

                StartYear++;
            }

            return IndicatorYears.Trim().TrimEnd(new char[] { ',' });
        }

        private void BindCurrencies()
        {
            //Set default settings
            ShowAbsoluteValues.Visible = false;
            currency.Visible = false;
            DataFormats.Visible = false;
            
            ShowIndexTo100.Visible = GlobalSettings.AllowIndexTo100;
            ShowGrowths.Visible = GlobalSettings.AllowGrowthRates;
            ShowPerCapita.Visible = GlobalSettings.AllowPerCapita;

            if (GlobalSettings.AllowAbsoluteValues || GlobalSettings.AllowIndexTo100 || GlobalSettings.AllowGrowthRates)
            {
                //If any one of above is true
                DataFormats.Visible = true;

                if (GlobalSettings.AllowAbsoluteValues)
                {
                    ShowAbsoluteValues.Visible = true;
                    currency.Visible = true;
                    ShowAbsoluteValues.Checked = true;

                    #region Bind Currency list to the dropdown list
                    //Get Currencies
                    int DefaultCurrencyID = Convert.ToInt32(GlobalSettings.DefaultCurrencyID);
                    DataSet Currencies = null;
                    Currencies = SqlDataService.GetCurrencyTypes(DefaultCurrencyID);

                    if (Currencies != null && Currencies.Tables.Count > 0 && Currencies.Tables[0].Rows.Count > 0)
                    {
                        //Bind Currency list to the dropdown list
                        currency.DataTextField = "Name";
                        currency.DataValueField = "ToID";
                        //currency.DataValueField = "ConversionID";                    
                        currency.DataSource = Currencies;
                        currency.DataBind();
                        //Default selection
                        if (currency.Items.Count > 0)
                        {
                            currency.SelectedValue = currency.Items[0].Value;
                            try
                            {
                                currency.SelectedValue = DefaultCurrencyID.ToString();
                                //currency.SelectedValue = Currencies.Tables[0].Select("ToID='" + DefaultCurrencyID.ToString() + "'")[0]["ConversionID"].ToString();
                            }
                            catch { }
                            Session["TargetCurrencyID"] = currency.SelectedValue;
                        }
                        
                        if (GlobalSettings.AllowLocalCurrencyConversion)
                        {
                            currency.Items.Add(new ListItem("Local currency","-4"));                         
                        }
                    }
                    else
                    {
                        currency.Visible = false;
                    }
                    
                    #endregion

                }
            }
        }

        private void InitializeComponent()
        {
            ResultsGrid.NeedRebind += new ComponentArt.Web.UI.Grid.NeedRebindEventHandler(OnNeedRebind);
            ResultsGrid.NeedDataSource += new ComponentArt.Web.UI.Grid.NeedDataSourceEventHandler(OnNeedDataSource);
            ResultsGrid.PageIndexChanged += new ComponentArt.Web.UI.Grid.PageIndexChangedEventHandler(OnPageChanged);
            ResultsGrid.SortCommand += new ComponentArt.Web.UI.Grid.SortCommandEventHandler(OnSort);
            ResultsGrid.FilterCommand += new ComponentArt.Web.UI.Grid.FilterCommandEventHandler(OnFilter);
        }

        public string StatusBarVisibility()
        {
            return IsSharedUser ? "none" : "block";
        }

        public string GetView2PageLink()
        {
            return GlobalSettings.AlternateResultsPage.Substring(GlobalSettings.AlternateResultsPage.IndexOf("/") + 1);
        }

        public string getTaxonomyFilterPage()
        {
            return GlobalSettings.SearchPage.ToLower().Contains("search1") ? "TaxonomyFilter1.aspx" : "TaxonomyFilter.aspx";
        }

        private void AddDefaultSeletionsToSession()
        {
            string UserSelections = "|";
            DataSet TaxonomyTypes = null;
            
            

            //Get the taxonomy type list
            if (!string.IsNullOrEmpty(SearchSource) && SearchSource == "2")
            {
                //Source is quick search page
                TaxonomyTypes = GlobalSettings.GetQuickSearchTaxonomyLinks();
            }
            else
            {
                //Source is normal search page
                TaxonomyTypes = GlobalSettings.GetTaxonomyLinks();
            }

            //Get all selections from session into a string
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow TaxonomyType in TaxonomyTypes.Tables[0].Rows)
                {
                    //Get selections for the current taxonomy type
                    Dictionary<int, string> CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>(TaxonomyType["FullID"].ToString());

                    //Check if the taxonomy data exists in session
                    if (CurrentSelections != null && CurrentSelections.Count > 0) 
                    {
                        //If selections found then loop through them and add to selections string
                        foreach (KeyValuePair<int, string> TaxonomySelection in CurrentSelections)
                        {
                            if (TaxonomyType["FullID"].ToString() == TaxonomyType["ID"].ToString())
                            {
                                if (TaxonomyType["type"].ToString().ToLower() != "static")
                                {
                                    UserSelections += TaxonomyType["ID"].ToString() + 
                                        "~" +
                                        TaxonomySelection.Key + 
                                        "~" + 
                                        TaxonomySelection.Value + 
                                        "|";
                                }
                            }
                            else
                            {
                                //If taxonomytypeid and fullid are different then build selection for both of them
                                UserSelections += TaxonomyType["ID"].ToString() + 
                                    "~" + 
                                    TaxonomySelection.Key + 
                                    "~" + 
                                    TaxonomySelection.Value + 
                                    "|" +
                                    TaxonomyType["FullID"].ToString() + 
                                    "~" + TaxonomySelection.Key + 
                                    "~" + TaxonomySelection.Value + 
                                    "|";
                            }
                        }
                    }
                }

                //check the default taxonomytypeid is exits in selection string or not
                //if not add the default selection criteria to selection string
                if (!string.IsNullOrEmpty(GlobalSettings.DefaultSelectionTaxonomyType))
                {
                    string[] DefaultTypes = GlobalSettings.DefaultSelectionTaxonomyType.Split(',');
                    string[] DefaultSelections = GlobalSettings.DefaultSelection.Split('#');

                    for (int counter = 0; counter < DefaultTypes.Length; counter++)
                    {
                        if (!UserSelections.Contains("|" + DefaultTypes[counter] + "~"))
                        {
                            UserSelections += DefaultSelections[counter].Substring(1);
                        }
                    }
                }
                                
                //update the session with latest selection string
                SessionHandler.UpdateSessionWithSelections("", "", UserSelections, "addGroupSelection", "");
            }

        }



        // EVENT HANDLERS       

        protected void Update_Click(object sender, EventArgs e)
        {
            //Rebind the grid with latest selections
            BindDataToGrid();
            isDisplayOptionsChanged.Value = "";
        }

        protected void rptUnitSelections_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //Check the item type
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                bool HideCurrentRow = true;
                DropDownList UnitsDropdown = (DropDownList)e.Item.FindControl("ddlOptions");
                Label BaseUnitsID = (Label)e.Item.FindControl("BaseUnitsID");

                if (UnitsDropdown != null)
                {
                    #region Bind units dropdownlist
                    
                    string ID = BaseUnitsID.Text;

                    //Get the 
                    DataTable UnitsTable = _unitsDataSet != null && _unitsDataSet.Tables.Count > 5 ? _unitsDataSet.Tables[5] : null;

                    if (UnitsTable != null && UnitsTable.Rows.Count > 0)
                    {
                        //Get the units with current baseunitid
                        DataRow[] results = UnitsTable.Select("baseunitsid=" + ID);
                        ListItem li;

                        //Check the results count
                        if (results.Length > 1)
                        {
                            //Don't hide the current repaeter row as results exists for this baseunitsid
                            HideCurrentRow = false;

                            //Bind the corresponding dropdownlist control
                            foreach (DataRow row in results)
                            {
                                li = new ListItem();
                                li.Text = row[4].ToString();
                                li.Value = row[1].ToString() + "~" + row[0].ToString();
                                UnitsDropdown.Items.Add(li);
                            }
                            UnitsDropdown.DataBind();
                        }
                    }

                    #endregion
                }

                if (!HideCurrentRow)
                {
                    //If current row is to be visible .. then..

                    //Get the current unit selections
                    string CurrentUnitSelections = hdnUnitsConversions.Value.EndsWith("|") ? hdnUnitsConversions.Value : hdnUnitsConversions.Value + "|";
                    string[] Unitselections = CurrentUnitSelections.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

                    //Set the dropdown's selected value by comparing current unit selections
                    for (int i = 0; i < Unitselections.Length; i++)
                    {
                        if (Unitselections[i].Contains(BaseUnitsID.Text + "~"))
                        {
                            UnitsDropdown.SelectedValue = Unitselections[i];
                            break;
                        }
                    }

                    //If dropdown selected value is not in current unit selections then add to the list
                    if (!CurrentUnitSelections.Contains(UnitsDropdown.SelectedValue))
                    {
                        CurrentUnitSelections += UnitsDropdown.SelectedValue + "|";
                    }

                    //Store the current selections in hdnUnitsConversions hidden field
                    hdnUnitsConversions.Value = CurrentUnitSelections;

                    //Add onchange attribute to the units dropdown
                    UnitsDropdown.Attributes.Add("onchange", "javascript:UnitSelections_onChange(this);");
                }

                //Decide the visibility of the current row/item
                e.Item.Visible = !HideCurrentRow;
            }
        }


        //CALLBACK EVENT HANDLERS

        public void OnNeedRebind(object sender, EventArgs oArgs)
        {
            System.Threading.Thread.Sleep(200);
            ResultsGrid.DataBind();
        }

        public void OnNeedDataSource(object sender, EventArgs oArgs)
        {
            BindDataToGrid();
        }

        public void OnPageChanged(object sender, ComponentArt.Web.UI.GridPageIndexChangedEventArgs oArgs)
        {
            ResultsGrid.CurrentPageIndex = oArgs.NewIndex;
        }

        public void OnFilter(object sender, ComponentArt.Web.UI.GridFilterCommandEventArgs oArgs)
        {
            ResultsGrid.Filter = oArgs.FilterExpression;
        }

        public void OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs oArgs)
        {
            ResultsGrid.Sort = oArgs.SortExpression;
        }


        //EXPORT EVENT HANDLERS

        protected void Excel_Click(object sender, EventArgs e)
        {
            string ExtractName = GlobalSettings.GetExtractName;
            string ExtractHeading = GlobalSettings.SiteTitle;

            //Get grid data            
            DataSet ResultsSet = GetDataToBindGrid(false);

            //Log
            LogUsage("Results - Grid View2 - Excel Download", "view2", " ");
            
            //Check the results count
            if (ResultsSet != null && ResultsSet.Tables.Count > 0)
            {
                //Remove columns
                string columnsToIgnore = GlobalSettings.ColumnsToIgnore;
                string columnsToHide = GlobalSettings.ColumnsToHide;
                
                if (GlobalSettings.ShowFfyColumnInExcel)
                {
                    columnsToHide = columnsToHide.Replace("|ffy|", "|");
                }

                columnsToHide += columnsToIgnore;

                #region Remove the columns which are not required from the results data table
                
                for (int count = ResultsSet.Tables[0].Columns.Count - 1; count >= 0; count--)
                {
                    DataColumn CurrentColumn = ResultsSet.Tables[0].Columns[count];
                    if (columnsToHide.Contains(("|" + CurrentColumn.ColumnName.ToLower() + "|")))
                    {
                        ResultsSet.Tables[0].Columns.RemoveAt(count);
                    }
                    else if (CurrentColumn.ColumnName.ToLower() == "ffy")
                    {
                        CurrentColumn.ColumnName = "First Forecast Year";
                    }
                    if (CurrentColumn.ColumnName.StartsWith("Y19") ||
                        CurrentColumn.ColumnName.StartsWith("Y20") ||
                        CurrentColumn.ColumnName.StartsWith("Y21"))
                    {
                        CurrentColumn.ColumnName = CurrentColumn.ColumnName.Replace("Y", "");
                        if (Convert.ToInt32(CurrentColumn.ColumnName) < Convert.ToInt32(hdnStartYear.Value) ||
                            Convert.ToInt32(CurrentColumn.ColumnName) > Convert.ToInt32(hdnEndYear.Value))
                        {
                            ResultsSet.Tables[0].Columns.RemoveAt(count);
                        }
                    }
                }

                #endregion

                //Prepare excel workbook                
                Workbook objExcel = ExtractionsHelper.ExportToExcel(ResultsSet.Tables[0],
                    ExcelTemplatePath,
                    LogoPath, 
                    ExtractHeading,
                    GlobalSettings.ExcelFreezedValuesForAllColumns,
                    GlobalSettings.ExcelAutoFilterRangeForAllColumns);

                //Remove the unused worksheet from the workbook
                objExcel.Worksheets.RemoveAt(objExcel.Worksheets.Count-1);  
         
                //Save the excel workbook
                objExcel.Save(ExtractName + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);
            }
        }
    }
}
