<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="getSelControl.aspx.cs" Inherits="Datamonitor.PremiumTools.Generic.Results.getSelControl" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../assets/css/treeview.css" rel="stylesheet" type="text/css"/>
</head>
<body style="margin:0px;">
    <form id="form1" runat="server">
    <div>
                             <ComponentArt:TreeView id="TreeView1" Height="293" Width="202"
                            DragAndDropEnabled="false"
                            NodeEditingEnabled="false"
                            KeyboardEnabled="true"
                            CssClass="TreeView"
                            NodeCssClass="TreeNode"                                                        
                            NodeEditCssClass="NodeEdit"
                            SelectedNodeCssClass="NodeSelected"
                            LineImageWidth="19"
                            LineImageHeight="20"
                            DefaultImageWidth="16"
                            DefaultImageHeight="16"
                            ItemSpacing="0"
                            NodeLabelPadding="3"
                            ImagesBaseUrl="../assets/tvlines/"
                            LineImagesFolderUrl="../assets/images/tvlines/"
                            ShowLines="true"
                            EnableViewState="true"
                            SiteMapXmlFile="~/Config/TaxonomyFilter.xml"
                            runat="server" >

                          </ComponentArt:TreeView>
    </div>
    </form>
</body>
</html>
