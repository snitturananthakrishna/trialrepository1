using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using CArt = ComponentArt.Web.UI;
using Aspose.Cells;
using Aspose.Words;

namespace Datamonitor.PremiumTools.Generic
{
    public partial class CountryOverview : AuthenticationBasePage
    {
        private static string _taxonomyTypeID;
        private string _sectionHeading;

        // EVENT OVERRIDES
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Country Overview";
            Notes.Text = GlobalSettings.CountryBrowserNotes;            
            if (!IsPostBack)
            {
                BindControls();
                BindOverviewData();
            }
        }

        protected void GoLink_Click(object sender, EventArgs e)
        {
            LogUsage("Country Browser - Results", "country1", " ");
            BindOverviewData();
        }

        // PRIVATE METHODS (initial population)   
        private void CheckYearSessions()
        {
            if (CurrentSession.GetFromSession<int>("StartYear").ToString() == "0")
            {
                CurrentSession.SetInSession<int>("StartYear", Convert.ToInt32(GlobalSettings.StartYearSelectValue));
            }
            if (CurrentSession.GetFromSession<int>("EndYear").ToString() == "0")
            {
                CurrentSession.SetInSession<int>("EndYear", Convert.ToInt32(GlobalSettings.EndYearSelectValue));
            }
        }

        private void BindIndicatorSections(DataSet IndicatorData)
        {
            if (IndicatorData != null && IndicatorData.Tables.Count > 1)
            {
                IndicatorData.Relations.Add("sections",
                    IndicatorData.Tables[0].Columns["SectionID"],
                    IndicatorData.Tables[1].Columns["SectionID"]);

                IndicatorGrid.DataSource = IndicatorData;
                IndicatorGrid.DataBind();
            }
        }

        private void BindOverviewData()
        {
            DataSet IndicatorData = GetAllSectionsData();
            BindIndicatorSections(IndicatorData);

            #region Binding Quartile Data

            //GetCountryOverviewQuartileData
            DataSet QuartileData = SqlDataService.GetCountryOverviewQuartileData(Convert.ToInt32(hdnCountry.Value));

            QuartileRepeater.DataSource = QuartileData;
            QuartileRepeater.DataBind();

            if (QuartileData != null)
            {
                if (QuartileData.Tables.Count > 0)
                {
                    Session["QuartileData"] = QuartileData.Tables[0];
                }
                if (QuartileData.Tables.Count > 1)
                {
                    //Assign the key statistcis label text.
                    if (QuartileData.Tables[1].Rows.Count > 0)
                    {
                        QuartileLabel.Text = string.Format("<h5>{0}</h5>", QuartileData.Tables[1].Rows[0]["name"].ToString());
                        QuartileLabel.Visible = true;
                    }
                    else
                    {
                        QuartileLabel.Text = "";
                        QuartileLabel.Visible = false;
                    }
                }
            }

            #endregion

            #region Binding Key Statistics Data

            //GetCountryOverviewKeyStatistics
            DataSet KeyStatisticsData = SqlDataService.GetCountryOverviewKeyStatistics(Convert.ToInt32(hdnCountry.Value));
            KeyStatisticsRepeater.DataSource = KeyStatisticsData;
            KeyStatisticsRepeater.DataBind();

            OverviewDescriptionDiv.Visible = false;

            if (KeyStatisticsData != null)
            {
                if (KeyStatisticsData.Tables.Count > 0)
                {
                    Session["KeyStatisticsData"] = KeyStatisticsData.Tables[0];
                }

                //Assign the map name and zoom level values to hidden fields
                if (KeyStatisticsData.Tables.Count > 1 && KeyStatisticsData.Tables[1].Rows.Count > 0)
                {
                    hdnCountryMapName.Value = KeyStatisticsData.Tables[1].Rows[0]["MapName"].ToString();
                    hdnZoomLevel.Value = KeyStatisticsData.Tables[1].Rows[0]["MapZoomLevel"].ToString();
                    if (KeyStatisticsData.Tables[1].Rows[0]["Description"] != null)
                    {
                        OverviewLabel.Text = KeyStatisticsData.Tables[1].Rows[0]["Description"].ToString().Replace("\n\n", "<br /><br />").Replace("\n", "<br /><br />");
                        if (!string.IsNullOrEmpty(OverviewLabel.Text.Trim()))
                        {
                            OverviewDescriptionDiv.Visible = true;
                        }
                    }
                    //assigning navigateurl to country profile pdf link
                    if (ShowIndepthCountryPDFdownload &&
                            KeyStatisticsData.Tables[1].Rows[0]["ProfileID"] != null &&
                            !string.IsNullOrEmpty(KeyStatisticsData.Tables[1].Rows[0]["ProfileID"].ToString()))
                    {
                        CountryProfilePDFLink.NavigateUrl = string.Format("{0}{1}/download.asp?R={2}&type=CountryProfile&itemHref={3}.pdf",
                        KCUrl.Replace("ovumkc.","www."),
                        SourceKC,
                        KeyStatisticsData.Tables[1].Rows[0]["ProfileID"].ToString(),
                        KeyStatisticsData.Tables[1].Rows[0]["ProfileID"].ToString());

                        ContryProfileSection.Visible = true;
                    }
                    else
                    {
                        ContryProfileSection.Visible = false;
                    }
                }
                else
                {
                    hdnCountryMapName.Value = hdnCountryName.Value;
                    hdnZoomLevel.Value = "3";
                }

                //Assign the key statistcis label text
                if (KeyStatisticsData.Tables.Count > 2 && KeyStatisticsData.Tables[2].Rows.Count > 0)
                {
                    KeyStatisticsLabel.Text = KeyStatisticsData.Tables[2].Rows[0]["name"].ToString();
                    KeyStatisticsLabel.Visible = true;
                }
                else
                {
                    KeyStatisticsLabel.Text = "";
                    KeyStatisticsLabel.Visible = false;
                }
            }

            #endregion

            #region Binding Flga Data

            DataSet FlgaData = SqlDataService.GetCountryOverviewFlagData(Convert.ToInt32(hdnCountry.Value));
            countrydata.DataSource = FlgaData.Tables[1];
            countrydata.DataBind();

            if (FlgaData.Tables[0].Rows.Count > 0)
            {
                flag.Visible = true;
                FlagLabel.Visible = true;
                flag.ImageUrl = flag.ResolveUrl("~/Assets/Images/flags/") + FlgaData.Tables[0].Rows[0][0].ToString() + ".bmp";
            }
            else
            {
                FlagLabel.Visible = false;
                flag.Visible = false;
            }

            #endregion

            CountryName.Text = hdnCountryName.Value;

            //Get the cookie name
            String CookieName = GlobalSettings.SiteTitle.Replace(" ", "").Replace("&", "") + KCUserID;

            //Create a new cookie, passing the name into the constructor
            HttpCookie cookie = new HttpCookie(CookieName);

            //Set the cookies value
            cookie.Value = hdnCountry.Value;

            //Set the cookie to expire in 3 months
            DateTime dtNow = DateTime.Now;
            cookie.Expires = dtNow.AddMonths(3);

            //Add the cookie
            Response.Cookies.Add(cookie);


        }

        private DataSet GetAllSectionsData()
        {
            DataSet OverviewData = null;
            if (!string.IsNullOrEmpty(hdnCountry.Value))
            {
                OverviewData = SqlDataService.GetCountryOverviewData(Convert.ToInt32(hdnCountry.Value));
            }
            return OverviewData;
        }

        private string GetSelectionsXML()
        {
            string SelectionsXML = string.Empty;
            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {

                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {
                    string TaxonomySelectedValue = string.Empty;
                    if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE"))
                    {
                        Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                        if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                        {
                            //Get all keys (ids) from dictionary
                            List<int> keys = new List<int>(selectedIDsList.Keys);
                            //convert int array to string array
                            string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                            TaxonomySelectedValue = string.Join(",", SelectedIDs);
                        }

                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                               taxonomyRow["ID"].ToString(),
                               TaxonomySelectedValue,
                               taxonomyRow["ColumnRowID"].ToString());
                        }
                    }
                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }

            return SelectionsXML;
        }

        private void BindControls()
        {
            DataSet IndicatorsAndCountries = SqlDataService.GetAllCountries();
            string LastViewedCountry = null;

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("PersistLastViewedCountry")) &&
                ConfigurationManager.AppSettings.Get("PersistLastViewedCountry").Equals("true", StringComparison.CurrentCultureIgnoreCase))
            {
                //Get the cookie name
                String CookieName = GlobalSettings.SiteTitle.Replace(" ", "").Replace("&", "") + KCUserID;

                //Grab the cookie
                HttpCookie cookie = Request.Cookies[CookieName];

                //Check to make sure the cookie exists
                if (cookie != null)
                {
                    LastViewedCountry = cookie.Value.ToString();
                }
            }

            if (IndicatorsAndCountries != null &&
                IndicatorsAndCountries.Tables.Count > 0 &&
                IndicatorsAndCountries.Tables[0].Rows.Count > 0)
            {
                _taxonomyTypeID = IndicatorsAndCountries.Tables[0].Rows[0]["taxonomyTypeID"].ToString();

                //binding Country data
                CountryCombo.DataSource = IndicatorsAndCountries.Tables[0];
                CountryCombo.DataBind();

                if (Request.QueryString["country"] != null && CountryCombo.Items.FindByText(Request.QueryString["country"].ToString()) != null)
                {
                    CountryCombo.SelectedItem = CountryCombo.Items.FindByText(Request.QueryString["country"].ToString());
                }
                else if (!string.IsNullOrEmpty(LastViewedCountry))
                {
                    CountryCombo.SelectedItem = CountryCombo.Items.FindByValue(LastViewedCountry);
                }
                else if (string.IsNullOrEmpty(hdnCountry.Value) && 
                        CountryCombo.Items.FindByValue(GlobalSettings.CountryBrowserDefaultCountryID) != null)
                {
                    CountryCombo.SelectedValue = GlobalSettings.CountryBrowserDefaultCountryID;
                }
                else
                {
                    CountryCombo.SelectedValue = hdnCountry.Value;
                }

                hdnCountry.Value = CountryCombo.SelectedItem.Value;
                hdnCountryName.Value = CountryCombo.SelectedItem.Text;
            }
        }

        protected void IndicatorGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                GridView SectionRepeater = (GridView)e.Row.FindControl("SectionRepeater");
                string sectionid = ((DataRowView)e.Row.DataItem).Row["SectionID"].ToString();
                _sectionHeading = ((DataRowView)e.Row.DataItem).Row["Name"].ToString();

                if (((DataRowView)e.Row.DataItem).Row.GetChildRows("sections").Length > 0)
                {
                    DataView SectionsDataView = ((DataRowView)e.Row.DataItem).Row.GetChildRows("sections")[0].Table.DefaultView;
                    SectionsDataView.RowFilter = "sectionid=" + sectionid;

                    DataTable SectionsDataTable = SectionsDataView.ToTable();
                    if (SectionsDataTable.Columns.Contains("SectionID"))
                    {
                        SectionsDataTable.Columns.Remove("SectionID");
                    }
                    if (!GlobalSettings.ShowRankColumnInSectionsGrid)
                    {
                        SectionRepeater.Columns.RemoveAt(SectionRepeater.Columns.Count - 1);
                    }
                    //binding columns to sections grid
                    BoundField col;
                    foreach (DataColumn dcol in SectionsDataTable.Columns)
                    {
                        col = new BoundField();
                        col.DataField = dcol.ColumnName;
                        col.HeaderText = dcol.ColumnName;
                        if (Regex.IsMatch(dcol.ColumnName, "^\\d+(\\.\\d+)?$"))
                        {
                            SectionRepeater.Columns.Add(col);
                        }
                    }
                    //assigning datasource to sections grid                

                    SectionRepeater.DataSource = SectionsDataTable;
                    SectionRepeater.DataBind();

                }

            }
        }

        protected void SectionRepeater_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            int ColumnNoToStartFormating = GlobalSettings.ColumnNoToStartFormating;

            if (!GlobalSettings.ShowRankColumnInSectionsGrid)
            {
                ColumnNoToStartFormating--;
            }

            if (e.Row.Cells.Count > 1)
            {
                e.Row.Cells[0].HorizontalAlign = HorizontalAlign.Left;
                //e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Left;
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                Label SectionHeading = (Label)e.Row.FindControl("SectionHeading");
                SectionHeading.Text = _sectionHeading;

                for (int i = ColumnNoToStartFormating; i < e.Row.Cells.Count; i++)
                {
                    e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //get a reference to the data used to databound the row    
                DataRowView drv = (DataRowView)e.Row.DataItem;
                string FormatString = "{0:#,0.###}";
                switch (Convert.ToInt32(drv["Rounding"]))
                {
                    case 0: FormatString = "{0:#,0}";
                        break;
                    case 1: FormatString = "{0:#,0.0}";
                        break;
                    case 2: FormatString = "{0:#,0.00}";
                        break;
                    case 3: FormatString = "{0:#,0.000}";
                        break;
                    case 4: FormatString = "{0:#,0.0000}";
                        break;
                }


                for (int i = ColumnNoToStartFormating; i < e.Row.Cells.Count; i++)
                {
                    //Add formatting to the value
                    e.Row.Cells[i].Text = e.Row.Cells[i].Text.Equals("&nbsp;") ? "" : string.Format(FormatString, System.Math.Round(Convert.ToDecimal(e.Row.Cells[i].Text), Convert.ToInt32(drv["Rounding"])));

                    //Add percentage symbol to the value-- when units is '%'
                    if (!string.IsNullOrEmpty(e.Row.Cells[i].Text.Trim()) && e.Row.Cells[1].Text.Trim().Equals("%"))
                    {
                        e.Row.Cells[i].Text = e.Row.Cells[i].Text + "%";
                    }
                }
            }
        }

        public int GetQuartileYear()
        {
            return GlobalSettings.CountryOverviewQuartlePopupYear;
        }

        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            Workbook excel = new Workbook();

            string header = "Country Overview - " + CountryName.Text;
            LogUsage("Country Browser  - Excel download", "country1", " ");

            DataSet IndicatorData = GetAllSectionsData();
            DataTable SectionIdsData = IndicatorData.Tables[0];
            DataTable SectionsDataForExcel = IndicatorData.Tables[1];
            //string excelTemplatePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("CountryOverviewExcelConfigPath"));

            excel = ExtractionsHelper.ExportToExcelCountryOverviewData(ExcelTemplatePath,
                         header,
                         LogoPath,
                         SectionIdsData,
                         SectionsDataForExcel);

            excel.Save("Ovum" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);

        }

        private void UpdateCountryDetailsToSeesion()
        {
            //Clear the country selections from session
            CurrentSession.SetInSession<Dictionary<int, string>>(_taxonomyTypeID == null ? "1" : _taxonomyTypeID, null);
            CurrentSession.SetInSession<Dictionary<int, string>>("1A", null);

            //Add selected country details to session against current taxonomytypeid
            SessionHandler.UpdateSessionWithSelections(_taxonomyTypeID,
                hdnCountry.Value,
                hdnCountryName.Value,
                "add",
                "");

            //Add selected country details to session against current taxonomytypeid's fullid
            SessionHandler.UpdateSessionWithSelections("1A",
                hdnCountry.Value,
                hdnCountryName.Value,
                "add",
                "");
        }

        protected void SearchLink_Click(object sender, EventArgs e)
        {
            //CurrentSession.SetInSession<Dictionary<int, string>>(_taxonomyTypeID, null);
            UpdateCountryDetailsToSeesion();
            Response.Redirect("~/" + GlobalSettings.DefaultSearchPage);
        }

        protected void ResultsLink_Click(object sender, EventArgs e)
        {
            //CurrentSession.SetInSession<Dictionary<int, string>>(_taxonomyTypeID, null);
            CheckYearSessions();
            UpdateCountryDetailsToSeesion();

            Response.Redirect(GlobalSettings.DefaultResultsPage.Substring(GlobalSettings.DefaultResultsPage.IndexOf("/") + 1));
        }

        protected void AnalysisLink_Click(object sender, EventArgs e)
        {
            //CurrentSession.SetInSession<Dictionary<int, string>>(_taxonomyTypeID, null);
            CheckYearSessions();
            UpdateCountryDetailsToSeesion();
            Response.Redirect("~/analysis/countrycomparison.aspx");
        }

        protected void lnkWord_Click(object sender, EventArgs e)
        {
            string header = "Country Overview - " + CountryName.Text;
            DataSet IndicatorData = GetAllSectionsData();
            DataTable SectionIdsData = IndicatorData.Tables[0];
            DataTable SectionsDataForExcel = IndicatorData.Tables[1];
            LogUsage("Country Browser  - Word download", "country1", " ");

            Document doc = ExtractionsHelper.ExportCountryOverviewDatatoWord(WordTemplatePath,
                                  header,
                                  LogoPath,
                                  SectionIdsData,
                                  SectionsDataForExcel);
            doc.Save("OvumExtract.doc", SaveFormat.FormatDocument, Aspose.Words.SaveType.OpenInWord, Response);
        }

        protected void MoreLink_Click(object sender, CommandEventArgs e)
        {
            CurrentSession.SetInSession<Dictionary<int, string>>(_taxonomyTypeID, null);

            UpdateCountryDetailsToSeesion();

            Response.Redirect("~/" + GlobalSettings.SearchPage + "?sectionid=" + e.CommandArgument + "&cid=" + _taxonomyTypeID);
        }

        public string DisplayAnchor(string value)
        {
            if (value.Length > 0)
            {
                return "block";
            }
            else
            {
                return "none";
            }
        }

        protected void BuidSearch_Click(object sender, EventArgs e)
        {
            CurrentSession.ClearSelectionsFromSession();
            Response.Redirect("~/" + GlobalSettings.SearchPage, true);
        }

    }
}

