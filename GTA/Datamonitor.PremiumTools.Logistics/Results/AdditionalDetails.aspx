<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdditionalDetails.aspx.cs" Inherits="Datamonitor.PremiumTools.Generic.Results.AdditionalDetails" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Additional Details</title>
        <link href="~/assets/css/datamonitor.css" rel="stylesheet" type="text/css"/>
    
    <link href="~/assets/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="../assets/css/arcticwhite.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/gridstyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../assets/Scripts/NumberFormat154.js"></script>
    <style type="text/css">

.headerCss
{
    cursor:hand;
    cursor:pointer;
}

.ResultsPanel
{
    float:left;
    
}
.headerCss
{
    cursor:hand;
    cursor:pointer;
}

.contentCss
{
  border-left:0px solid #C8C2A8;
  border-right:0px solid #C8C2A8;
  background-color:#ECECD9;
     width:205px;
     text-align:center;
     border-top:0px;
}


</style>

    <script type="text/javascript">
     function formatN(value)
    {
        var decimalPlaces="<%= NoOfDecimalsToGridValues %>";
        //return value;
        if(value==null)
        return "nk";
        //for growth rows, data will come in the format xx.xx% - so format the number part
        var postfix='';
        if(String(value).indexOf('%')!=-1)
        {
            value=String(value).substring(0,value.length-1);
            postfix='%';
        }
        if(value != "0" && value == "")
    	{
       	 return "nk";
    	}
         var num = new NumberFormat();
        num.setInputDecimal('.');
        num.setNumber(value); // obj.value is '-1000.247'
        
        num.setCurrency(false);
        num.setNegativeFormat(num.LEFT_DASH);
        num.setNegativeRed(false);
        num.setSeparators(true, ',', ',');       
        
        if(Number(value) > 100)
        {
            num.setPlaces(0, false);    
        }
        else 
        {
            num.setPlaces(decimalPlaces, false);
        }
        return num.toFormatted()+postfix;

    }
    function getValueType(dataitem, fieldName)
    {
//        var fName;
//        fName = fieldName.replace("Y", "");
//        var ffy = dataitem.getMemberAt("FFY").get_value();
//        if(ffy == "")
//        {
//            return "Actual";
//        }
//        else if (fName == ffy)
//        {
//            return "FFY";
//        }
//        else if (fName > ffy)
//        {
//            return "Forecast";
//        }
        return "Actual";
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">    
       <div id="wrapper" style=" padding-bottom:15px">
     <div id="maincontent_full">
        <div>
             <div style="float:right">
                <ul class="jumplinks" style="border:none;">                                
                    <li class="excel_icon"><asp:LinkButton ID="lnkExcel" Text="Excel" runat="server" OnClick="Excel_Click" ></asp:LinkButton> </li>
                </ul>
            </div>
            <div>
                <asp:Label ID="Heading" runat="server" ></asp:Label>
            </div>
        </div>
      <div>&nbsp;</div>
      
        <div>
           <ComponentArt:Grid id="AdditionalDetailsGrid"                                    
            CssClass="grid"
            RunningMode="client"                                                                             
            ShowHeader="false"    
            ShowFooter="false"
            AllowColumnResizing="false"
            AllowEditing="false"                                            
            HeaderCssClass="GridHeader"               
            FooterCssClass="GridFooter"                      
            SliderHeight="20"
            SliderWidth="150"
            SliderGripWidth="9"   
            SliderPopupOffsetX="20"                                                             
            IndentCellWidth="22"
            IndentCellCssClass="HeadingCell_Freeze"     
            LoadingPanelClientTemplateId="LoadingFeedbackTemplate"
            LoadingPanelPosition="MiddleCenter"
            LoadingPanelFadeDuration="10"
            LoadingPanelFadeMaximumOpacity="60"                                                                
            ScrollBar="Auto"
            ImagesBaseUrl="../assets/images/"
            PagerStyle="numbered"
            PagerInfoPosition="BottomLeft"
            PagerPosition="BottomRight"
            PreExpandOnGroup="true"                                			        
            ScrollTopBottomImagesEnabled="true"
            ScrollTopBottomImageHeight="2"
            ScrollTopBottomImageWidth="16"
            ScrollImagesFolderUrl="../assets/images/scroller/"
            ScrollButtonWidth="16"
            ScrollButtonHeight="17"
            ScrollBarCssClass="ScrollBar"
            ManualPaging="false"
            ScrollGripCssClass="ScrollGrip"
            ScrollBarWidth="16"    
            AllowPaging="false"
            FillContainer="true"
            Height="630" 
            Width="100%" runat="server" AllowHorizontalScrolling="True" EnableViewState="false" >
            <Levels>
              <ComponentArt:GridLevel                                      
                    AllowReordering="false"
                    AllowGrouping="false"      
                    AllowSorting="false"                                                  
                    HeadingCellCssClass="HeadingCell"                                                                                                
                    HeadingRowCssClass="HeadingRow"
                    HeadingTextCssClass="HeadingCellText"                                                    
                    GroupHeadingCssClass="HeadingCell_Freeze"
                    DataCellCssClass="DataCell"
                    AlternatingRowCssClass="RowAlt"
                    RowCssClass="Row"                                          
                    SelectedRowCssClass="SelRow"   
                    SortAscendingImageUrl="asc.gif"
                    SortDescendingImageUrl="desc.gif" >                                                    
                <Columns>
                         
                </Columns>
              </ComponentArt:GridLevel>
              
            </Levels>
            <ClientTemplates>                     
              <ComponentArt:ClientTemplate Id="CommonDataTemplate">                                                                           
                <div class="## getValueType(DataItem, DataItem.GetCurrentMember().Column.DataField) ##"> ## DataItem.GetCurrentMember().get_value() ##</div>
              </ComponentArt:ClientTemplate> 
              <ComponentArt:ClientTemplate Id="FormattedDataTemplate">                                                                           
                <div class="## getValueType(DataItem, DataItem.GetCurrentMember().Column.DataField) ##"> ## formatN(DataItem.GetCurrentMember().get_value()) ##</div>
              </ComponentArt:ClientTemplate>
              <ComponentArt:ClientTemplate Id="TooltipTemplate">                                                                           
                <div title="## DataItem.GetCurrentMember().get_value() ##"> ## DataItem.GetCurrentMember().get_value() ##</div>
              </ComponentArt:ClientTemplate> 
              <ComponentArt:ClientTemplate Id="LoadingFeedbackTemplate">
                  <table height="410" width="940" bgcolor="#f6f6f6" style="margin-left:0px;margin-top:0px;padding-top:0px"><tr><td valign="center">
                      <table cellspacing="0" cellpadding="0" border="0">
                      <tr>
                        <td style="font-size:11pt;font-family:Arial;font-weight:bold;color:#0000a0;">Loading...&nbsp;</td>
                        <td><img src="../assets/images/spinner.gif" border="0"></td>
                      </tr>
                      </table>
                </td></tr></table>
              </ComponentArt:ClientTemplate>
              <ComponentArt:ClientTemplate ID="ColumnNonFilterTemplate">            
                <div style="padding: 0px; font-weight: bold; text-align: left;">
                    <a style="color: White; text-decoration: none;">
                        <span>## DataItem.DataField ##</span>
                    </a>                 
                </div>
              </ComponentArt:ClientTemplate>
            </ClientTemplates>
          </ComponentArt:Grid>
        </div>
        <div>&nbsp;</div>
        </div>
        </div>
    </form>
</body>
</html>
