using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Aspose.Cells;
using System.Xml;

namespace Datamonitor.PremiumTools.Generic
{
    public partial class View3 : AuthenticationBasePage
    {
        //PRIVATE MEMBERS
        private string savedSearchXml;
        private DataSet _unitsDataSet;


        public int NoOfDecimalsToGridValues
        {
            get
            {
                return GlobalSettings.NoOfDecimalsToGridValues;
            }
        }

        // EVENT OVERRIDES

        protected override void OnInit(EventArgs e)
        {
            InitializeComponent();
            base.OnInit(e);

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Results";
            //lnksave.Attributes.Add("onclick", "return ValidSearchName();");
            StartYear.Attributes.Add("onchange", "javascript:Year_onChange('startyear',this);");
            EndYear.Attributes.Add("onchange", "javascript:Year_onChange('endyear',this);");
            currency.Attributes.Add("onchange", "javascript:currency_onChange(this);");
            ((ViewOptions)Master.FindControl("ViewOptionsControl")).Source = "view2";
            ((MasterPages.ResultsMaster)Master).AllowStatusBar = true;
            LogUsage("Results - Grid View3", "view3", " ");
            if (!IsPostBack && !ResultsGrid.CausedCallback)
            {
                Session["HiddenColumns"] = "";
                Session["Units"] = hdnUnitsConversions.Value;
                HiddenColumns.Value = "|";
                //LoadSavedSearches                
                //((SavedSearch)SavedSearch1).LoadSavedSearches(Context.Request.Url.Segments[Context.Request.Url.Segments.Length - 1].ToString());
                //lnksave.Attributes.Add("onclick", "return ValidSearchName();");
                hdnConversionID.Value = "0";
                Session["TargetCurrencyID"] = "0";
                BindCurrencies();
                if (currency.SelectedValue.Length > 0)
                {
                    hdnConversionID.Value = currency.SelectedValue;
                }
                string SelectionsXML = string.IsNullOrEmpty(savedSearchXml) ? GetFilterCriteriaWithForcedTaxonomies() : savedSearchXml;               
                Menu1.SiteMapXmlFile = GlobalSettings.DisplayTextFiltersConfigPath;

            }
            if (IsPostBack)
            {
                Session["HiddenColumns"] = HiddenColumns.Value.StartsWith("|") ? HiddenColumns.Value : "|" + HiddenColumns.Value;
                Session["Units"] = hdnUnitsConversions.Value;
                Session["TargetCurrencyID"] = hdnConversionID.Value;
            }
            if (Request.QueryString["SearchId"] != null && Request.QueryString["SearchId"] != "")
            {
                DataSet savedSearchesDataset = new DataSet();
                int searchId = Convert.ToInt32(Request.QueryString["SearchId"]);
                savedSearchesDataset = SqlDataService.GetSavedSearchSearchCriteria(searchId);
                if (savedSearchesDataset.Tables.Count > 0 && savedSearchesDataset.Tables[0].Rows.Count > 0)
                {
                    savedSearchXml = savedSearchesDataset.Tables[0].Rows[0]["searchXml"].ToString();
                }
            }
              
            if (hdnConversionID.Value != "-1" &&
                hdnConversionID.Value != "-2" &&
                hdnConversionID.Value != "-3" &&
                GlobalSettings.AllowAbsoluteValues)
            {
                ShowAbsoluteValues.Checked = true;
                currency.SelectedValue = hdnConversionID.Value;
            }
            else if (hdnConversionID.Value == "-1" && GlobalSettings.AllowIndexTo100)
            {
                ShowIndexTo100.Checked = true;
            }
            else if (hdnConversionID.Value == "-2" && GlobalSettings.AllowPerCapita)
            {
                ShowPerCapita.Checked = true;
            }
            else if (hdnConversionID.Value == "-3" && GlobalSettings.AllowGrowthRates)
            {
                ShowGrowths.Checked = true;
            }

        }


        // PRIVATE METHODS (initial population)

        protected void BindUnitConversions(DataSet unitsSet)
        {
            //DataSet UnitsSet = SqlDataService.GetUnitconversionsList();
            if (unitsSet.Tables.Count > 5)
            {
                _unitsDataSet = unitsSet;

                if (unitsSet.Tables[4].Rows.Count > 0)
                {
                    rptUnitSelections.DataSource = unitsSet.Tables[4];
                    rptUnitSelections.DataBind();
                }
                else
                {
                    rptUnitSelections.DataSource = null;
                    rptUnitSelections.DataBind();
                }
            }
        }

        private void GetMinMaxYears(DataTable Years, ref int minyear, ref int maxYear)
        {
            try
            {
                //Get list of valid years from session
                int StartYear = CurrentSession.GetFromSession<int>("StartYear");
                int EndYear = CurrentSession.GetFromSession<int>("EndYear");

                //get Min & Max years from the table                        
                if (Years != null &&
                    Years.Rows.Count > 0)
                {
                    int MinYear = Convert.ToInt32(Years.Rows[0]["MinYear"].ToString());
                    int MaxYear = Convert.ToInt32(Years.Rows[0]["MaxYear"].ToString());
                    //compare min & max year with start & end years and take the intersection of them
                    minyear = MinYear > StartYear ? MinYear : StartYear;
                    maxYear = MaxYear < EndYear ? MaxYear : EndYear;
                }
            }
            catch { }
        }

        private void BindDataToGrid()
        {
            //clears the grid columns & ColumnFilterDiv1
            ColumnFilterDiv1.Controls.Clear();
            ResultsGrid.Levels[0].Columns.Clear();

            //Get results from DB
            DataSet ResultsSet = GetDataToBindGrid(true);

            //get MinYear & MaxYear
            int MinYear = 0, MaxYear = 5000;

            //Get Min & Max years from the result set
            if (ResultsSet != null && ResultsSet.Tables.Count > 3)
            {
                GetMinMaxYears(ResultsSet.Tables[3], ref MinYear, ref MaxYear);
            }

            #region Binding the years to dropdown controls

            //Get list of valid years from session
            int SessionStartYear = CurrentSession.GetFromSession<int>("StartYear");
            int SessionEndYear = CurrentSession.GetFromSession<int>("EndYear");

            //Taking intersction of 'start & end' - 'min & max' years
            SessionStartYear = SessionStartYear > MinYear ? SessionStartYear : MinYear;
            SessionEndYear = SessionEndYear < MaxYear ? SessionEndYear : MaxYear;

            //Clear the dropdowns before ading tiesm
            StartYear.Items.Clear();
            EndYear.Items.Clear();

            //Assigning the years as items to dropdowns
            for (int counter = SessionStartYear; counter <= SessionEndYear; counter++)
            {
                StartYear.Items.Add(new ListItem(counter.ToString()));
                EndYear.Items.Add(new ListItem(counter.ToString()));
            }

            //Setting default values
            hdnStartYear.Value = (string.IsNullOrEmpty(hdnStartYear.Value) || !StartYear.Items.Contains(new ListItem(hdnStartYear.Value))) ?
                SessionStartYear.ToString() :
                hdnStartYear.Value;
            hdnEndYear.Value = (string.IsNullOrEmpty(hdnEndYear.Value) || !EndYear.Items.Contains(new ListItem(hdnEndYear.Value))) ?
                SessionEndYear.ToString() :
                hdnEndYear.Value;

            StartYear.SelectedValue = hdnStartYear.Value;
            EndYear.SelectedValue = hdnEndYear.Value; 

            #endregion

            //Load data into grid
            string columnsToIgnore = GlobalSettings.ColumnsToIgnore;
            string columnsToHide = GlobalSettings.ColumnsToHide;
            string columnsToHideIntially = GlobalSettings.ColumnsToHideIntially;
            Dictionary<string, string> removedColumns = new Dictionary<string, string>();

            if (ResultsSet.Tables.Count > 1)
            {
                BindUnitConversions(ResultsSet);

                bool IsTrendChartColumnAdded = false;
                //Get predefined view selectable columns
                object ViewID = CurrentSession.GetFromSession<string>("PredefinedViewID");
                string PredefinedViewID;

                PredefinedViewID = (ViewID != null) ? (string)ViewID : string.Empty;

                string SelectableTaxonomyTypes = string.Empty;

                SelectableTaxonomyTypes = PredefinedViewID.Length > 0 ?
                     GetPredefinedViewSelectableIDs(PredefinedViewID) :
                     ConfigurationManager.AppSettings.Get("SelectableTaxonomiesInGrid1");

                if (SelectableTaxonomyTypes.Length > 0)
                {
                    List<string> Ignorecolumns = GetColumnRowIdsbyTaxonomyIds(SelectableTaxonomyTypes);
                    foreach (string str in Ignorecolumns)
                    {
                        columnsToIgnore += string.Format("|{0}|", str);
                    }
                }
                ColumnToHide.Value = "";

                #region Adding columns to results grid

                foreach (DataColumn column in ResultsSet.Tables[0].Columns)
                {
                    bool AddColumnToGrid = Session["HiddenColumns"].ToString().Contains("|" + column.ColumnName + "|") ? false : true;

                    //Add data columns
                    if (column.ColumnName.StartsWith("Y19") ||
                        column.ColumnName.StartsWith("Y20") ||
                        column.ColumnName.StartsWith("Y21") ||
                        column.ColumnName.ToLower().Equals("cagr"))
                    {
                        if ((column.ColumnName.StartsWith("Y19") || column.ColumnName.StartsWith("Y20") || column.ColumnName.StartsWith("Y21")) &&
                            (Convert.ToInt32(column.ColumnName.Substring(1)) < Convert.ToInt32(hdnStartYear.Value) ||
                            Convert.ToInt32(column.ColumnName.Substring(1)) > Convert.ToInt32(hdnEndYear.Value)))
                            continue;

                        if (AddColumnToGrid)
                        {
                            if (!IsTrendChartColumnAdded)
                            {
                                //Add source column
                                if (!ConfigurationManager.AppSettings.Get("ShowSourceColumninGrid").Equals("false"))
                                {
                                    GridColumn SourceColumn = new GridColumn();
                                    SourceColumn.HeadingCellCssClass = "HeadingCell_Freeze";
                                    SourceColumn.DataCellCssClass = "HeadingCell_Freeze";
                                    SourceColumn.DataCellClientTemplateId = "DataSourceClientTemplate";
                                    SourceColumn.Width = 15;
                                    SourceColumn.HeadingText = "";
                                    ResultsGrid.Levels[0].Columns.Add(SourceColumn);
                                }

                                //Add trend chart column
                                GridColumn TrendChartColumn = new GridColumn();
                                TrendChartColumn.HeadingCellCssClass = "HeadingCell_Freeze";
                                TrendChartColumn.DataCellCssClass = "HeadingCell_Freeze";
                                TrendChartColumn.DataCellClientTemplateId = "TrendChartTemplate";
                                TrendChartColumn.Width = 15;
                                TrendChartColumn.HeadingText = "";
                                ResultsGrid.Levels[0].Columns.Add(TrendChartColumn);

                                IsTrendChartColumnAdded = true;
                            }

                            string DataTemplate = GlobalSettings.FormatValuesInGrid ?
                                "FormattedDataTemplate" :
                                "CommonDataTemplate";

                            GridColumn col1 = new GridColumn();
                            col1.DataField = column.ColumnName;
                            col1.Width = GlobalSettings.GetDefaultGridColumnWidth;
                            col1.HeadingText = column.ColumnName.Replace("Y", "");
                            col1.Align = ComponentArt.Web.UI.TextAlign.Right;
                            col1.DataCellClientTemplateId = !column.ColumnName.ToLower().Equals("cagr") ?
                                DataTemplate :
                                "CAGRTemplate";
                            ResultsGrid.Levels[0].Columns.Add(col1);
                        }

                    }
                    else if (!columnsToIgnore.ToLower().Contains("|" + column.ColumnName.ToLower() + "|"))
                    {

                        bool IsHideColumn = columnsToHide.ToLower().Contains(("|" + column.ColumnName.ToLower() + "|"));
                        bool IsHideColumnIntially = columnsToHideIntially.ToLower().Contains(("|" + column.ColumnName.ToLower() + "|"));
                        bool IsColumnIDType = false;

                        int ColumnVisibility = GetColumnVisibility(ResultsSet.Tables[1], column.ColumnName);
                        if (ColumnVisibility == 1 && ResultsSet.Tables[0].Rows.Count > 0)
                        {
                            removedColumns.Add(column.ColumnName, ResultsSet.Tables[0].Rows[0][column.ColumnName].ToString());
                        }
                        //if (IsHideColumnIntially || ColumnVisibility == 1)
                        //{
                        //    if (AddColumnToGrid)
                        //    {
                        //        ColumnToHide.Value = ColumnToHide.Value.Length > 0 ?
                        //            ColumnToHide.Value + "," + (ResultsGrid.Levels[0].Columns.Count - 1).ToString() :
                        //            (ResultsGrid.Levels[0].Columns.Count - 1).ToString();
                        //    }
                        //}

                        if (AddColumnToGrid && !IsHideColumnIntially && ColumnVisibility != 1)
                        {
                            GridColumn col1 = new GridColumn();
                            col1.DataField = column.ColumnName;
                            col1.Width = GetColumnWidth(ResultsSet.Tables[2], column.ColumnName);
                            col1.HeadingText = column.ColumnName;
                            col1.HeadingCellCssClass = "HeadingCell_Freeze";

                            if (SelectableTaxonomyTypes.Length > 0)
                            {
                                string currenctColumnID = GetColumnIDByName(column.ColumnName);
                                //if (("," + SelectableTaxonomyTypes + ",").Contains("," + currenctColumnID + ","))
                                //{
                                //    col1.HeadingCellClientTemplateId = "ColumnFilterTemplate";
                                //}
                                //else
                                //{
                                    col1.HeadingCellClientTemplateId = "ColumnNonFilterTemplate";
                                //}
                            }
                            else
                            {
                                col1.HeadingCellClientTemplateId = "ColumnFilterTemplate";
                            }

                            if (column.ColumnName.ToLower() == "indicator text" && GlobalSettings.ShowTaxonomyDefinitionInResultsGrid)
                            {
                                col1.DataCellClientTemplateId = "TooltipTemplateWithDefinition";
                            }
                            else
                            {
                                col1.DataCellClientTemplateId = "TooltipTemplate";
                            }
                            
                            col1.Visible = !IsHideColumn;

                            int nextIndex = ResultsSet.Tables[0].Columns[column.ColumnName].Ordinal + 1;
                            int result;
                            col1.DataCellCssClass = Int32.TryParse(ResultsSet.Tables[0].Columns[nextIndex].ColumnName.Replace("Y", ""), out result) ?
                                "DataCell_FreezeLastCol" :
                                "DataCell_Freeze";

                            ResultsGrid.Levels[0].Columns.Add(col1);
                        }


                        if (!IsHideColumn)
                        {
                            string isChecked = IsHideColumnIntially || ColumnVisibility == 1 ? "" : "CHECKED";
                            if (isChecked.Length > 0)
                            {
                                isChecked = AddColumnToGrid ? "CHECKED" : "";
                            }
                            else
                            {
                                Session["HiddenColumns"] = Session["HiddenColumns"].ToString().Trim().StartsWith("|") ?
                                   Session["HiddenColumns"].ToString() : "|" + Session["HiddenColumns"].ToString();

                                Session["HiddenColumns"] = Session["HiddenColumns"].ToString() + column.ColumnName + '|';
                            }


                            Literal brk = new Literal();
                            brk.Text = string.Format("<INPUT TYPE='checkbox' NAME='chkCol{0}' ID='{0}' VALUE='{0}' {2} onClick='CheckChange(this,\"{1}\");' {3}>{1}<br />",
                                (ResultsGrid.Levels[0].Columns.Count - 1).ToString(),
                                column.ColumnName,
                                isChecked,
                                removedColumns.ContainsKey(column.ColumnName) ? "disabled" : "");
                            ColumnFilterDiv1.Controls.Add(brk);
                        }

                    }
                }

                #endregion

                //Assigning data to Results grid
                ResultsGrid.DataSource = ResultsSet;

                GridStatusMessage.Text = ResultsSet.Tables[0].Rows.Count > 0 ? "" : "No results found for the current selection criteria.";

                //Assigning data to RemovedTaxonomies grid
                DataTable RemovedColumnsTable = new DataTable();
                RemovedColumnsTable.Columns.Add("key");
                RemovedColumnsTable.Columns.Add("value");
                RemovedColumnsTable.Columns.Add("isvisible");
                foreach (KeyValuePair<string, string> pair in removedColumns)
                {
                    string currenctColumnID = GetColumnIDByName(pair.Key);
                    RemovedColumnsTable.Rows.Add(pair.Key,
                        pair.Value,
                        (("," + SelectableTaxonomyTypes + ",").Contains("," + currenctColumnID + ",")) ? "visible" : "hidden");
                }
                //RemovedTaxonomies.DataSource = removedColumns;
                RemovedTaxonomies.DataSource = RemovedColumnsTable;
                RemovedTaxonomies.DataBind();
            }
        }

        private DataSet GetDataToBindGrid(bool forGrid)
        {
            string SelectionsXML = string.Empty;
            //Get the filter criteria from session 
            if (string.IsNullOrEmpty(savedSearchXml))
            {
                SelectionsXML = GetFilterCriteriaWithForcedTaxonomies();
            }
            else
            {
                SelectionsXML = savedSearchXml;
            }
            StringBuilder sb = new StringBuilder();
            Dictionary<string, string> ColumnIDs = GlobalSettings.ColumnIds;
            //foreach (KeyValuePair<string, string> row in ColumnIDs)
            //{
            //    if (!row.Key.Equals("None", StringComparison.CurrentCultureIgnoreCase))
            //    {
            //        sb.Append(string.Format("COUNT(DISTINCT {0}) as [{1}] ,", row.Key, row.Value));
            //    }
            //}
            string ColumnNameIDs = sb.ToString();
            ColumnNameIDs = ColumnNameIDs.Length > 0 ? ColumnNameIDs.Substring(0, ColumnNameIDs.Length - 2) : ColumnNameIDs;

            object ViewID = CurrentSession.GetFromSession<string>("PredefinedViewID");

            string PredefinedViewID;
            PredefinedViewID = ViewID != null ? (string)ViewID : string.Empty;

            //int StartYear = CurrentSession.GetFromSession<int>("StartYear");
            //int EndYear = CurrentSession.GetFromSession<int>("EndYear");

            string conversionIDs = string.Empty;

            foreach (string str in Session["Units"].ToString().Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries))
            {
                conversionIDs += str.Split(new char[] { '~' })[1] + ",";
            }
            conversionIDs = conversionIDs.TrimEnd(new char[] { ',' });

            //Get list of valid years from session
            int SessionStartYear = CurrentSession.GetFromSession<int>("StartYear");
            int SessionEndYear = CurrentSession.GetFromSession<int>("EndYear");

            //Get results
            DataSet ResultsSet = null;

            ResultsSet = SqlDataService.GetGridDataView3(SelectionsXML,
                false,
                Convert.ToInt32(string.IsNullOrEmpty(Session["TargetCurrencyID"].ToString()) ? "-1" : Session["TargetCurrencyID"].ToString()),
                GlobalSettings.IncludeMacroeconomicData,
                PredefinedViewID,
                conversionIDs,
                forGrid ? ResultsGrid.CurrentPageIndex : 0,
                forGrid ? -1 : GlobalSettings.ExcelDownloadMaxRows,
                string.IsNullOrEmpty(hdnStartYear.Value) ? SessionStartYear.ToString() : hdnStartYear.Value,
                string.IsNullOrEmpty(hdnEndYear.Value) ? SessionEndYear.ToString() : hdnEndYear.Value);

            return ResultsSet;
        }

        private int GetColumnWidth(DataTable widthsTable, string columnName)
        {
            int Width = GlobalSettings.GetDefaultGridColumnWidth;
            foreach (DataRow row in widthsTable.Rows)
            {
                if (row["columnName"].ToString().Equals(columnName))
                {
                    Width = Int32.Parse(row["width"].ToString());
                    break;
                }
            }

            return Width;
        }

        private int GetColumnVisibility(DataTable visibilityTable, string columnName)
        {
            int Visible = 2;
            try
            {
                Visible = visibilityTable.Rows[0][columnName].ToString().Equals("1") ? 1 : 0;
            }
            catch (Exception ex) { }

            return Visible;
        }

        private string GetYearsForMacroIndicators(string TaxonomySelectedValue)
        {
            int DefaultStartYear = Convert.ToInt32(GlobalSettings.GetMacroEconomicDataMinYear);
            int DefaultEndYear = Convert.ToInt32(GlobalSettings.GetMacroEconomicDataMaxYear);

            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
            int EndYear = CurrentSession.GetFromSession<int>("EndYear");

            string IndicatorYears = "";
            while (EndYear >= StartYear)
            {
                IndicatorYears += (StartYear >= DefaultStartYear && StartYear <= DefaultEndYear) ?
                    "cast([Y" + StartYear + "]as varchar(max)) as [Y" + StartYear + "], " :
                    "'0.0' AS [Y" + StartYear + "], ";

                StartYear++;
            }

            return IndicatorYears.Trim().TrimEnd(new char[] { ',' });
        }

        private void BindCurrencies()
        {
            ShowAbsoluteValues.Visible = false;
            currency.Visible = false;
            DataFormats.Visible = false;

            ShowIndexTo100.Visible = GlobalSettings.AllowIndexTo100;
            ShowGrowths.Visible = GlobalSettings.AllowGrowthRates;
            ShowPerCapita.Visible = GlobalSettings.AllowPerCapita;
            if (GlobalSettings.AllowAbsoluteValues || GlobalSettings.AllowIndexTo100 || GlobalSettings.AllowGrowthRates)
            {
                DataFormats.Visible = true;

                if (GlobalSettings.AllowAbsoluteValues)
                {
                    ShowAbsoluteValues.Visible = true;
                    currency.Visible = true;
                    ShowAbsoluteValues.Checked = true;

                    #region Bind Currency list to the dropdown list
                    //Get Currencies
                    int DefaultCurrencyID = Convert.ToInt32(GlobalSettings.DefaultCurrencyID);
                    DataSet Currencies = null;
                    Currencies = SqlDataService.GetCurrencyTypes(DefaultCurrencyID);
                    if (Currencies != null && Currencies.Tables.Count > 0 && Currencies.Tables[0].Rows.Count > 0)
                    {
                        //Bind Currency list to the dropdown list
                        currency.DataTextField = "Name";
                        currency.DataValueField = "ToID";
                        currency.DataSource = Currencies;
                        currency.DataBind();
                        //Default selection
                        if (currency.Items.Count > 0)
                        {
                            currency.SelectedValue = currency.Items[0].Value;
                            try
                            {
                                currency.SelectedValue = DefaultCurrencyID.ToString();
                                //currency.SelectedValue = Currencies.Tables[0].Select("ToID='" + DefaultCurrencyID.ToString() + "'")[0]["ConversionID"].ToString();
                            }
                            catch { }
                            Session["TargetCurrencyID"] = currency.SelectedValue;
                        }
                    }
                    else
                    {
                        currency.Visible = false;
                    }
                    #endregion

                }
            }
            ////add the option: 'IndexTo100' to currency radio list
            //currency.Items.Add(new ListItem("Absolute", "-1")); 
            //currency.Items.Add(new ListItem("Index To 100", "0"));        
        }

        private void InitializeComponent()
        {
            ResultsGrid.NeedRebind += new ComponentArt.Web.UI.Grid.NeedRebindEventHandler(OnNeedRebind);
            ResultsGrid.NeedDataSource += new ComponentArt.Web.UI.Grid.NeedDataSourceEventHandler(OnNeedDataSource);
            ResultsGrid.PageIndexChanged += new ComponentArt.Web.UI.Grid.PageIndexChangedEventHandler(OnPageChanged);
            ResultsGrid.SortCommand += new ComponentArt.Web.UI.Grid.SortCommandEventHandler(OnSort);
            ResultsGrid.FilterCommand += new ComponentArt.Web.UI.Grid.FilterCommandEventHandler(OnFilter);
        }

        public string StatusBarVisibility()
        {
            return IsSharedUser ? "none" : "block";
        }

        public string GetView1PageLink()
        {
            return GlobalSettings.DefaultResultsPage.Substring(GlobalSettings.DefaultResultsPage.IndexOf("/") + 1);
        }

        public string getTaxonomyFilterPage()
        {
            return GlobalSettings.SearchPage.ToLower().Contains("search1") ? "TaxonomyFilter1.aspx" : "TaxonomyFilter.aspx";
        }

        // EVENT HANDLERS

        //protected void lnkSave_Click(object sender, EventArgs e)
        //{
        //    //get username.
        //    //string userName = "vnarapuram";     
        //    string userName = KCUserID;
        //    //Get the filter criteria from session           
        //    string SelectionsXML = GetFilterCriteriaWithForcedTaxonomies();
        //    string SearchName = hdnsavedsearch.Value;
        //    StringBuilder strXML = new StringBuilder();
        //    string extractType = "save";

        //    if (!string.IsNullOrEmpty(SearchName.TrimEnd().TrimStart()))
        //    {
        //        int searchResult = SqlDataService.savedSearchDetails(extractType, userName, SearchName, SelectionsXML);
        //        if (searchResult > 0)
        //        {
        //            lblMessage.Text = "Search save successful.";
        //            lblMessage.ForeColor = System.Drawing.Color.Green;
        //            savedSearchXml = "";
        //            BindDataToGrid();
        //            //LoadSavedSearches                
        //            ((SavedSearch)SavedSearch1).LoadSavedSearches(Context.Request.Url.Segments[Context.Request.Url.Segments.Length - 1].ToString());                
        //        }
        //        else
        //        {
        //            lblMessage.Text = "Search save fail. A search already exists with this name.";
        //            lblMessage.ForeColor = System.Drawing.Color.Red;
        //        }
        //    }
        //}

        protected void Update_Click(object sender, EventArgs e)
        {
            BindDataToGrid();
            isDisplayOptionsChanged.Value = "";
        }

       

        protected void rptUnitSelections_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                bool HideCurrentRow = true;
                DropDownList dropdown = (DropDownList)e.Item.FindControl("ddlOptions");
                Label BaseUnitsID = (Label)e.Item.FindControl("BaseUnitsID");

                if (dropdown != null)
                {
                    string ID = BaseUnitsID.Text;
                    DataTable dt = _unitsDataSet != null && _unitsDataSet.Tables.Count > 5 ? _unitsDataSet.Tables[5] : null;

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DataRow[] results = dt.Select("baseunitsid=" + ID);
                        ListItem li;
                        if (results.Length > 1)
                        {
                            HideCurrentRow = false;
                            foreach (DataRow row in results)
                            {
                                li = new ListItem();
                                li.Text = row[4].ToString();
                                li.Value = row[1].ToString() + "~" + row[0].ToString();
                                dropdown.Items.Add(li);
                            }
                            dropdown.DataBind();
                        }
                    }
                }
                if (!HideCurrentRow)
                {
                    string CurrentUnitSelections = !hdnUnitsConversions.Value.EndsWith("|") ? hdnUnitsConversions.Value + "|" : hdnUnitsConversions.Value;
                    string[] unitselections = CurrentUnitSelections.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < unitselections.Length; i++)
                    {
                        if (unitselections[i].Contains(BaseUnitsID.Text + "~"))
                        {
                            dropdown.SelectedValue = unitselections[i];
                            break;
                        }
                    }
                    if (!CurrentUnitSelections.Contains(dropdown.SelectedValue))
                    {
                        CurrentUnitSelections += dropdown.SelectedValue + "|";
                    }


                    hdnUnitsConversions.Value = CurrentUnitSelections;
                    dropdown.Attributes.Add("onchange", "javascript:UnitSelections_onChange(this);");
                }
                e.Item.Visible = !HideCurrentRow;
            }
        }


        //CALLBACK EVENT HANDLERS

        public void OnNeedRebind(object sender, EventArgs oArgs)
        {
            System.Threading.Thread.Sleep(200);
            ResultsGrid.DataBind();
        }

        public void OnNeedDataSource(object sender, EventArgs oArgs)
        {
            BindDataToGrid();
        }

        public void OnPageChanged(object sender, ComponentArt.Web.UI.GridPageIndexChangedEventArgs oArgs)
        {
            ResultsGrid.CurrentPageIndex = oArgs.NewIndex;
        }

        public void OnFilter(object sender, ComponentArt.Web.UI.GridFilterCommandEventArgs oArgs)
        {
            ResultsGrid.Filter = oArgs.FilterExpression;
        }

        public void OnSort(object sender, ComponentArt.Web.UI.GridSortCommandEventArgs oArgs)
        {
            ResultsGrid.Sort = oArgs.SortExpression;
        }


        //EXPORT EVENT HANDLERS

        protected void Excel_Click(object sender, EventArgs e)
        {
            string ExtractName = GlobalSettings.GetExtractName;
            string ExtractHeading = GlobalSettings.SiteTitle;
            //Get grid data            
            DataSet ResultsSet = GetDataToBindGrid(false);
            LogUsage("Results - Grid View3 - Excel Download", "view3", " ");
            if (ResultsSet != null && ResultsSet.Tables.Count > 0)
            {
                //Remove columns
                string columnsToIgnore = GlobalSettings.ColumnsToIgnore;
                string columnsToHide = GlobalSettings.ColumnsToHide;
                
                //Check whether FFy column cna be shown in excel or not
                if (GlobalSettings.ShowFfyColumnInExcel)
                {
                    columnsToHide = columnsToHide.Replace("|ffy|", "|");
                }

                columnsToHide += columnsToIgnore;

                #region Remove the columns which are not required from the results data table

                for (int count = ResultsSet.Tables[0].Columns.Count - 1; count >= 0; count--)
                {
                    DataColumn CurrentColumn = ResultsSet.Tables[0].Columns[count];
                    if (columnsToHide.Contains(("|" + CurrentColumn.ColumnName.ToLower() + "|")))
                    {
                        ResultsSet.Tables[0].Columns.RemoveAt(count);
                    }
                    if (CurrentColumn.ColumnName.StartsWith("Y19") ||
                        CurrentColumn.ColumnName.StartsWith("Y20") ||
                        CurrentColumn.ColumnName.StartsWith("Y21"))
                    {
                        CurrentColumn.ColumnName = CurrentColumn.ColumnName.Replace("Y", "");
                        if (Convert.ToInt32(CurrentColumn.ColumnName) < Convert.ToInt32(hdnStartYear.Value) ||
                            Convert.ToInt32(CurrentColumn.ColumnName) > Convert.ToInt32(hdnEndYear.Value))
                        {
                            ResultsSet.Tables[0].Columns.RemoveAt(count);
                        }
                    }
                }

                #endregion

                //Prepare excel workbook
                Workbook objExcel = ExtractionsHelper.ExportToExcel(ResultsSet.Tables[0],
                    ExcelTemplatePath, 
                    LogoPath, 
                    ExtractHeading,
                    GlobalSettings.ExcelFreezedValuesForDisplayText,
                    GlobalSettings.ExcelAutoFilterRangeForDisplayText);

                //Remove the unused worksheet from the workbook
                objExcel.Worksheets.RemoveAt(objExcel.Worksheets.Count - 1);
               
                //Save the excel workbook
                objExcel.Save(ExtractName + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);
            }
        }
    }
}
