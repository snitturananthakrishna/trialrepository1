using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using dotnetCHARTING;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using ComponentArt.Web.UI;
using Aspose.Cells;


namespace Datamonitor.PremiumTools.Generic.Results
{
    public partial class AdditionalDetails : AuthenticationBasePage
    {

        public int NoOfDecimalsToGridValues
        {
            get
            {
                return GlobalSettings.NoOfDecimalsToGridValues;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.CustomPageTitle = "Additional Details";

            if (Request.QueryString["rowid"] != null && !string.IsNullOrEmpty(Request.QueryString["rowid"].Trim())&&
                Request.QueryString["startyear"] != null && !string.IsNullOrEmpty(Request.QueryString["startyear"].Trim())&&
                Request.QueryString["endyear"] != null && !string.IsNullOrEmpty(Request.QueryString["endyear"].Trim())&&
                Request.QueryString["targetCurrencyid"] != null && !string.IsNullOrEmpty(Request.QueryString["targetCurrencyid"].Trim()))
            {
                BindDataToGrid();
            }
        }

        protected void BindDataToGrid()
        {
            DataSet AdditionalDetailsData = GetAdditionalDetailsData(false);
            LogUsage("View Results - Additional Details", "details1", " ");
            if (AdditionalDetailsData != null &&
                AdditionalDetailsData.Tables.Count > 1 &&
                AdditionalDetailsData.Tables[1].Rows.Count > 0)
            {
                Heading.Text = AdditionalDetailsData.Tables[1].Rows[0][0].ToString();

            }

            int StartYear = Convert.ToInt32(Request.QueryString["startyear"].ToString());
            int EndYear = Convert.ToInt32(Request.QueryString["endyear"].ToString());

            #region Adding columns to grid

            //Load data into grid
            string columnsToIgnore = GlobalSettings.ColumnsToIgnore;
            string columnsToHide = GlobalSettings.ColumnsToHide;
            string columnsToHideIntially = GlobalSettings.ColumnsToHideIntially;

            foreach (DataColumn column in AdditionalDetailsData.Tables[0].Columns)
            {

                if (column.ColumnName.StartsWith("Y19") ||
                    column.ColumnName.StartsWith("Y20") ||
                    column.ColumnName.StartsWith("Y21"))
                {
                    if (Convert.ToInt32(column.ColumnName.Replace("Y", "")) >= StartYear &&
                        Convert.ToInt32(column.ColumnName.Replace("Y", "")) <= EndYear)
                    {
                        //Add data columns
                        GridColumn col1 = new GridColumn();
                        col1.DataField = column.ColumnName;
                        col1.Width = GlobalSettings.GetDefaultGridColumnWidth;
                        col1.HeadingText = column.ColumnName.Replace("Y", "");
                        col1.Align = ComponentArt.Web.UI.TextAlign.Right;
                        col1.DataCellClientTemplateId = GlobalSettings.FormatValuesInGrid ?
                                    "FormattedDataTemplate" :
                                    "CommonDataTemplate";
                        AdditionalDetailsGrid.Levels[0].Columns.Add(col1);
                    }
                }
                else if (!columnsToIgnore.ToLower().Contains("|" + column.ColumnName.ToLower() + "|"))
                {
                    //Add freeze columns
                    bool IsHideColumn = columnsToHide.ToLower().Contains(("|" + column.ColumnName.ToLower() + "|"));
                    bool IsHideColumnIntially = columnsToHideIntially.ToLower().Contains(("|" + column.ColumnName.ToLower() + "|"));

                    if (!IsHideColumnIntially)
                    {
                        GridColumn col1 = new GridColumn();
                        col1.DataField = column.ColumnName;
                        col1.Width = GlobalSettings.GetDefaultGridColumnWidth + GlobalSettings.GetDefaultGridColumnWidth;
                        col1.HeadingText = column.ColumnName;
                        col1.HeadingCellCssClass = "HeadingCell_Freeze";
                        col1.HeadingCellClientTemplateId = "ColumnNonFilterTemplate";
                        col1.DataCellClientTemplateId = "TooltipTemplate";

                        col1.Visible = !IsHideColumn;

                        col1.DataCellCssClass = "DataCell_Freeze";

                        AdditionalDetailsGrid.Levels[0].Columns.Add(col1);
                    }
                }
            }

            #endregion

            AdditionalDetailsGrid.DataSource = AdditionalDetailsData;
        }

        protected DataSet GetAdditionalDetailsData(bool isExcel)
        {
            return SqlDataService.GetAdditionalDetails(Convert.ToInt32(Request.QueryString["rowid"].Trim()), Convert.ToInt32(Request.QueryString["targetCurrencyid"].Trim()),isExcel);
        }

        //EXPORT EVENT HANDLERS

        protected void Excel_Click(object sender, EventArgs e)
        {
            string ExtractName = GlobalSettings.GetExtractName;
            string ExtractHeading = GlobalSettings.SiteTitle;
            //Get grid data            
            DataSet AdditionalDetailsData = GetAdditionalDetailsData(true);

            int StartYear = Convert.ToInt32(Request.QueryString["startyear"].ToString());
            int EndYear = Convert.ToInt32(Request.QueryString["endyear"].ToString());
            LogUsage("Additional Details - Excel Download", "details1", " ");
            if (AdditionalDetailsData != null && AdditionalDetailsData.Tables.Count > 0)
            {
                //Remove columns

                for (int count = AdditionalDetailsData.Tables[0].Columns.Count - 1; count >= 0; count--)
                {
                    DataColumn CurrentColumn = AdditionalDetailsData.Tables[0].Columns[count];
                    
                    if (CurrentColumn.ColumnName.StartsWith("Y19") ||
                        CurrentColumn.ColumnName.StartsWith("Y20") ||
                        CurrentColumn.ColumnName.StartsWith("Y21"))
                    {
                        CurrentColumn.ColumnName = CurrentColumn.ColumnName.Replace("Y", "");
                        if (Convert.ToInt32(CurrentColumn.ColumnName) < StartYear ||
                            Convert.ToInt32(CurrentColumn.ColumnName) > EndYear)
                        {
                            AdditionalDetailsData.Tables[0].Columns.RemoveAt(count);
                        }
                    }
                }
            }
            if (AdditionalDetailsData != null && AdditionalDetailsData.Tables.Count > 0)
            {   
                //Prepare excel workbook                
                Workbook objExcel = ExtractionsHelper.ExportToExcel(AdditionalDetailsData.Tables[0],
                    ExcelTemplatePath,
                    LogoPath,
                    ExtractHeading,
                    "10|8",
                    "");                

                LogUsage("Additional Details", "Excel download", "");

                //Save the excel workbook
                objExcel.Save(ExtractName + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);
            }
        }
                
    }
}
