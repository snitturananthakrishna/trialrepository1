using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using dotnetCHARTING;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using Aspose.Cells;
using System.IO;

namespace Datamonitor.PremiumTools.Generic.Results
{
    public partial class CountryComparisonImage : AuthenticationBasePage
    {

        private static Color[] DMPalette = new Color[] { Color.FromArgb(0, 34, 82), Color.FromArgb(108, 128, 191), Color.FromArgb(165, 174, 204), Color.FromArgb(210, 217, 229), Color.FromArgb(216, 69, 25), Color.FromArgb(255, 102, 0), Color.FromArgb(255, 172, 112), Color.FromArgb(250, 222, 199), Color.FromArgb(255, 153, 0), Color.FromArgb(255, 204, 0) };
        private static Color[] DMLinePalette = new Color[] { Color.FromArgb(0, 40, 95), Color.FromArgb(123, 139, 177), Color.FromArgb(230, 108, 30), Color.FromArgb(241, 186, 61), Color.FromArgb(4, 148, 173), Color.FromArgb(84, 197, 207)};
        string SelectedYear = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>D:\DMProjects\NDBSite\Code\trunk\source\Datamonitor.PremiumTools.Logistics\Results\CountryComparisonGrid.aspx
        protected void Page_Load(object sender, EventArgs e)
        {            
            string ChartType = string.Empty;
            
            //Get the chart type from query string
            if (Request.QueryString["charttype"] != null && !string.IsNullOrEmpty(Request.QueryString["charttype"].Trim()))
            {
                ChartType = Request.QueryString["charttype"].Trim();
            }

            Unit width = 325;
            Unit height = 200;
            SampleChart.Height = height;
            SampleChart.Width = width;

            //Get the chart data for the given query string parameters
            DataSet ChartData = GetChartData();

            if (ChartData != null &&
                ChartData.Tables.Count > 0 &&
                ChartData.Tables[0].Rows.Count > 0)
            {
                lnkExcel.Visible = true;
                dotnetCHARTING.Chart chart;

                //Assign the chart title to its label
                ChartTitle.Text = (ChartType == "bar" || ChartType == "pie") ?
                    ChartData.Tables[0].Rows[0]["Indicator"].ToString() + " (" + SelectedYear + ")" :
                    ChartData.Tables[0].Rows[0]["Indicator"].ToString();

                #region Plot Chart

                //Check the chart type and plot the chart
                switch (ChartType)
                {
                    case "bar":

                        //PLot bar chart for the data
                        PlotXYChart(ChartData,
                            SampleChart,
                            "7",
                            ChartData.Tables[0].Rows[0]["Units"].ToString(),
                            "",
                            width,
                            height);

                        //Hide the chart legend
                        SampleChart.LegendBox.Visible = false;

                        break;

                    case "pie":

                        //Remove the indicator column from the data table
                        ChartData.Tables[0].Columns.RemoveAt(2);

                        //PLot pie chart for the data
                        chart = TwoDimensionalCharts.PlotPieChart(ChartData,
                            SampleChart,
                            "",
                            "",
                            width,
                            height,
                            "Pie",
                            false);

                        //Add copyright info to chart.
                        TwoDimensionalCharts.AddCopyrightText(chart, Convert.ToInt32(width.Value) - 150);

                        //Check and assign the temp directory for the chart
                        if (!Directory.Exists(Server.MapPath("temp")))
                        {
                            Directory.CreateDirectory(Server.MapPath("temp"));
                        }
                        chart.FileManager.TempDirectory = "temp";

                        //Save the chart as image and assign the image url to chart
                        ChartImg.ImageUrl = chart.FileManager.SaveImage(chart.GetChartBitmap()).Replace("\\", "/");

                        //Store the image/chart url in hidden field
                        hdnImageUrl.Value = ChartImg.ImageUrl;

                        break;

                    case "line":

                        //PLot line chart for the data
                        chart = PlotLineChart(ChartData,
                            SampleChart,
                            ChartData.Tables[0].Columns[2].ColumnName,
                            width,
                            height);

                        //Add copyright info to chart.
                        TwoDimensionalCharts.AddCopyrightText(chart, Convert.ToInt32(width.Value) - 150);

                        //Set no marker to the data series
                        for (int i = 0; i < SampleChart.SeriesCollection.Count; i++)
                        {
                            SampleChart.SeriesCollection[i].DefaultElement.Marker.Type = ElementMarkerType.None;
                        }

                        //Assign Yaxis label text
                        SampleChart.YAxis.Label.Text = ChartData.Tables.Count > 0 && ChartData.Tables[0].Rows.Count > 0 ?
                           ChartData.Tables[0].Rows[0]["Units"].ToString() :
                           "";

                        //Check and assign chart temp directory
                        if (!Directory.Exists(Server.MapPath("temp")))
                        {
                            Directory.CreateDirectory(Server.MapPath("temp"));
                        }
                        chart.FileManager.TempDirectory = "temp";

                        //Save the chart and assign that url to chart image
                        ChartImg.ImageUrl = chart.FileManager.SaveImage(chart.GetChartBitmap()).Replace("\\", "/");

                        //Store the image url in hidden field
                        hdnImageUrl.Value = ChartImg.ImageUrl;

                        break;
                }

                #endregion
            }
            else
            {
                lnkExcel.Visible = false;
            }
        }

        /// <summary>
        /// Gets the chart data.
        /// </summary>
        /// <returns></returns>
        private DataSet GetChartData()
        {
            DataSet ChartData = null;

            //Check the query string parameters
            if (Request.QueryString["rowid"] != null && !string.IsNullOrEmpty(Request.QueryString["rowid"].Trim()) &&
                Request.QueryString["countries"] != null && !string.IsNullOrEmpty(Request.QueryString["countries"].Trim()) &&
                Request.QueryString["selectedyear"] != null && !string.IsNullOrEmpty(Request.QueryString["selectedyear"].Trim()) &&
                Request.QueryString["charttype"] != null && !string.IsNullOrEmpty(Request.QueryString["charttype"].Trim()))
            {
                //Get the parameter values from query string
                string RowID = Request.QueryString["rowid"].Trim();
                string SelectedCountries = Request.QueryString["countries"].TrimEnd(',');
                SelectedYear = Request.QueryString["selectedyear"].TrimEnd(',');
                string ChartType = Request.QueryString["charttype"].Trim();

                //If rowid is negative then get macro economic data .. otherwise normal data
                if (Convert.ToInt64(RowID) < 0)
                {
                    //Macro economic data
                    ChartData = SqlDataService.GetCountryComparisonImageMEData(-1 * Convert.ToInt64(RowID),
                        SelectedCountries,
                        SelectedYear,
                        ChartType);
                }
                else
                {
                    //Normal data
                    ChartData = SqlDataService.GetCountryComparisonImageData(Convert.ToInt64(RowID),
                        SelectedCountries,
                        SelectedYear,
                        ChartType);
                }
            }
            return ChartData;
        }

        /// <summary>
        /// Plots the XY chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart.</param>
        /// <param name="chartSeriesType">Type of the chart series.</param>
        /// <param name="yAxis1Text">The y axis1 text.</param>
        /// <param name="AxisCriteria">The axis criteria.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns></returns>
        internal dotnetCHARTING.Chart PlotXYChart(DataSet chartDataset,
                               dotnetCHARTING.Chart chart,
                               string chartSeriesType,
                               string yAxis1Text,
                               string AxisCriteria,
                               Unit width,
                               Unit height)
        {
            chart.Use3D = false;
            chart.Visible = true;

            //Add usage logging
            AuthenticationBasePage abPage = new AuthenticationBasePage();
            abPage.LogUsage("Results - Country Comparison - Trend chart", "xy chart", " ");

            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;
                chart.FileQuality = 100;
                DataEngine objDataEngine = new DataEngine();
                chart.SeriesCollection.Clear();

                objDataEngine.Data = chartDataset;
                dotnetCHARTING.SeriesCollection sc;                

                //Set data fields
                objDataEngine.DataFields = "XAxis=" +
                    chartDataset.Tables[0].Columns[0].ColumnName +
                    ",YAxis=" + chartDataset.Tables[0].Columns[2].ColumnName +
                    ",splitBy=" + chartDataset.Tables[0].Columns[1].ColumnName;

                sc = objDataEngine.GetSeries();
                chart.SeriesCollection.Add(sc);
                chart.DataBind();
                //to add copyright info to chart.
                TwoDimensionalCharts.AddCopyrightText(chart, Convert.ToInt32(width.Value) - 150);//150
                string xAxisText = chartDataset.Tables[0].Columns[0].ColumnName;
                string seriesText = chartDataset.Tables[0].Columns[1].ColumnName;
                //Set axis label texts and styles
                chart.XAxis.Label.Text = xAxisText;
                chart.YAxis.Label.Text = yAxis1Text;
                chart.XAxis.DefaultTick.Label.Color = Color.Black;
                chart.XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.YAxis.DefaultTick.Label.Color = Color.Black;
                chart.YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.XAxis.Line.Color = Color.DarkGray;
                chart.XAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.YAxis.Line.Color = Color.DarkGray;
                chart.YAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.DefaultSeries.DefaultElement.Marker.Size = 8;
                if (chartSeriesType.Equals("stacked"))
                {
                    chart.YAxis.Scale = Scale.Stacked;
                    chartSeriesType = "7";
                }

                //Set chart series types
                for (int i = 0; i < chart.SeriesCollection.Count; i++)
                {
                    chart.SeriesCollection[i].Type = (SeriesType)Convert.ToInt32(chartSeriesType);
                }

                if (chartSeriesType == "4")
                {
                    chart.DefaultSeries.DefaultElement.Transparency = 20;
                    chart.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.None;
                }
                if (chartSeriesType == "7")
                {
                    chart.DefaultSeries.DefaultElement.SmartLabel.Text = "%Name";
                }
                chart.XAxis.DefaultTick.Label.Text = "";
                chart.DefaultElement.ShowValue = true;                
                //Set chart styles
                chart.Palette = DMPalette;
                chart.Debug = false;
                chart.Height = height;
                chart.Width = width;
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.XAxis.SpacingPercentage = 30;
                chart.ChartArea.ClearColors();
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.ChartArea.Shadow.Color = Color.White;

                //Set legend properties and styles
                //chart.LegendBox.Template = "%Icon%Name";               

                chart.DefaultElement.ToolTip = "%SeriesName";
                chart.LegendBox.Visible = false;
                
                if (!Directory.Exists(Server.MapPath("temp")))
                    Directory.CreateDirectory(Server.MapPath("temp"));

                chart.FileManager.TempDirectory = "temp";
                ChartImg.ImageUrl = chart.FileManager.SaveImage(chart.GetChartBitmap()).Replace("\\", "/");
                hdnImageUrl.Value = ChartImg.ImageUrl;               
            }
            return chart;
        }


        /// <summary>
        /// Plots the line chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart.</param>
        /// <param name="yAxis1Text">The y axis1 text.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns></returns>
        internal static dotnetCHARTING.Chart PlotLineChart(DataSet chartDataset,
                                dotnetCHARTING.Chart chart,
                                string yAxis1Text,
                                Unit width,
                                Unit height)
        {
            chart.Use3D = false;
            chart.Visible = true;
            AuthenticationBasePage abPage = new AuthenticationBasePage();
            //Add usage logging
            abPage.LogUsage("Results - Country Comparison - Trend chart", "line chart", " ");
            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;
                chart.FileQuality = 100;
                DataEngine objDataEngine = new DataEngine();
                chart.SeriesCollection.Clear();

                objDataEngine.Data = chartDataset;
                dotnetCHARTING.SeriesCollection sc;

                //Set data fields
                objDataEngine.DataFields = "XAxis=" +
                    chartDataset.Tables[0].Columns[0].ColumnName +
                    ",YAxis=" + chartDataset.Tables[0].Columns[2].ColumnName +
                    ",splitBy=" + chartDataset.Tables[0].Columns[1].ColumnName;

                sc = objDataEngine.GetSeries();
                chart.SeriesCollection.Add(sc);
                chart.DataBind();

                string xAxisText = chartDataset.Tables[0].Columns[0].ColumnName;
                string seriesText = chartDataset.Tables[0].Columns[1].ColumnName;
                //Set axis label texts and styles
                chart.XAxis.Label.Text = xAxisText;
                chart.YAxis.Label.Text = yAxis1Text;
                chart.XAxis.DefaultTick.Label.Color = Color.Black;
                chart.XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.YAxis.DefaultTick.Label.Color = Color.Black;
                chart.YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.XAxis.Line.Color = Color.DarkGray;
                chart.XAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.YAxis.Line.Color = Color.DarkGray;
                chart.YAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.DefaultSeries.DefaultElement.Marker.Size = 4;
                

                //Set chart series types
                for (int i = 0; i < chart.SeriesCollection.Count; i++)
                {
                    chart.SeriesCollection[i].Type = SeriesType.Line;
                }
                chart.DefaultSeries.Line.Width = 3;
                

                //Set chart styles
                chart.Palette = DMLinePalette;
                chart.Debug = false;
                chart.Height = height;
                chart.Width = width;
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.XAxis.SpacingPercentage = 30;
                chart.ChartArea.ClearColors();
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.ChartArea.Shadow.Color = Color.White;

                //Set legend properties and styles
                chart.LegendBox.Template = "%Icon%Name";
                SetStandardLegendStyles(chart);

                chart.DefaultElement.ToolTip = "%SeriesName";
                
            }

            return chart;
        }

        /// <summary>
        /// Sets the standard legend styles.
        /// </summary>
        /// <param name="objChart">The obj chart.</param>
        private static void SetStandardLegendStyles(dotnetCHARTING.Chart chart)
        {
            chart.LegendBox.Background = new dotnetCHARTING.Background(System.Drawing.Color.White);
            chart.LegendBox.CornerBottomLeft = BoxCorner.Square;
            chart.LegendBox.CornerBottomRight = BoxCorner.Square;
            chart.LegendBox.CornerTopLeft = BoxCorner.Square;
            chart.LegendBox.CornerTopRight = BoxCorner.Square;
            chart.LegendBox.LabelStyle = new dotnetCHARTING.Label("", new System.Drawing.Font("Verdana", 7), Color.Black);
            chart.LegendBox.Line = new dotnetCHARTING.Line(Color.White);
            chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
            chart.LegendBox.Line.Color = Color.White;
            chart.LegendBox.Shadow.Color = Color.White;
            chart.LegendBox.InteriorLine.Color = Color.White;
        }


        /// <summary>
        /// Handles the Click event of the lnkExcel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            Workbook excel = new Workbook();
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string sheetName = "Country Comparison";
            string chartTitle = ChartTitle.Text;

            //Add Log
            AuthenticationBasePage abPage = new AuthenticationBasePage();
            abPage.LogUsage("Results - Country Comparison - Trend chart - Exel download", "chart1", " ");

            //Get the chart bitmap
            if (hdnImageUrl.Value.Length > 0)
            {
                chartImagePath = Server.MapPath(hdnImageUrl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }

            //Get the chart data
            DataSet ChartData = GetChartData();
            
            //Remove extra columns
            if (ChartData!=null && ChartData.Tables[0].Columns.Contains("spl"))
            {
                ChartData.Tables[0].Columns.Remove("spl");
            }

            //Extract to excel
            excel = ExtractionsHelper.ExportToExcelTrendChart(ExcelTemplatePath,
                     chartTitle,
                     LogoPath,
                     chartBitmap,                    
                     sheetName,
                     ChartData != null ? ChartData.Tables[0] : null);

            //Save the extracted excel file
            excel.Save("Datamonitor" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);

        }                   
    }
}
