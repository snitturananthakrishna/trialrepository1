using System;
using System.Data;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Generic;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic
{
    /// <summary>
    /// Summary description for AutoCompleteWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]
    public class AutoCompleteWebService : System.Web.Services.WebService
    {
        [WebMethod]
        public List<TaxonomyPair> FetchTaxonomyResults(int taxonomyTypeID,string searchString)
        {
            //Create an empty taxonomy results list
            List<TaxonomyPair> TaxonomyResults = new List<TaxonomyPair>();

            //Check the input parameters has values or not
            if (taxonomyTypeID != null && taxonomyTypeID != 0 && !string.IsNullOrEmpty(searchString))
            {
                try
                {
                    //Get the taxonomy display names by filtering with the searchString
                    DataSet TaxonomyData = SqlDataService.GetPredictiveSearchTaxonomy(taxonomyTypeID, searchString);

                    //Add all the resulted taxonomies to the list
                    if (TaxonomyData != null && TaxonomyData.Tables.Count > 0)
                    {
                        foreach (DataRow TaxonomyRow in TaxonomyData.Tables[0].Rows)
                        {
                            TaxonomyResults.Add(new TaxonomyPair(TaxonomyRow["ID"].ToString(), TaxonomyRow["displayName"].ToString()));
                        }
                    }
                }
                catch { }
            }
            //Return the taxonomy results list
            return TaxonomyResults;
        }
    }

    /// <summary>
    /// TaxonomyPair class
    /// </summary>
    public class TaxonomyPair
    {
        public string ID;
        public string Name;

        public TaxonomyPair()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public TaxonomyPair(string id, string name)
        {
            ID = id;
            Name = name;
        }
    }
}

