<%@ Page Language="C#" 
    MasterPageFile="~/MasterPages/Logistics.Master" 
    AutoEventWireup="true" 
    CodeBehind="default.aspx.cs" Inherits="Datamonitor.PremiumTools.Generic._default"   %>    
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<%@ Register Src="~/Controls/CountrySnapshotOptions.ascx" TagName="CountrySnapshotOptions" TagPrefix="GMPT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">    
    <script type="text/javascript" src="../Assets/Scripts/jquery-1.3.2.min.js"></script>
<style type="text/css">
input[type=checkbox] 
{
	width:5px;
	margin-left:-65px;
	margin-right:-65px;
	padding:0px;
	text-align:left;
}
</style>

    <script type="text/javascript">
    function CountryCombo_onChange(sender, eventArgs)
        {   
            var item = sender.getSelectedItem();
            
            document.getElementById('<%=hdnCountry.ClientID %>').value = item .get_value(); 
            document.getElementById('<%=hdnCountryName.ClientID %>').value = item .get_text();
        }
        function MultiSelectCountryTree_onNodeCheckChange(sender,eventArgs)
        {
           var hdnCountriesToCompare=document.getElementById('<%= hdnCountriesToCompare.ClientID %>');  
           var checkedNode = eventArgs.get_node();  
           var MultiSelectCountryComboText=MultiSelectCountryCombo.get_text();
           if (checkedNode.get_checked()) 
           {
               hdnCountriesToCompare.value = hdnCountriesToCompare.value  + checkedNode.get_id()+ ",";   
                    
               MultiSelectCountryCombo.set_text(MultiSelectCountryComboText+checkedNode.get_text()+";" );
           }
           else
           {
               hdnCountriesToCompare.value = hdnCountriesToCompare.value.replace(checkedNode.get_id()+",","");
               MultiSelectCountryComboText = MultiSelectCountryComboText.replace(checkedNode.get_text()+";","");
               MultiSelectCountryCombo.set_text(MultiSelectCountryComboText );
           }                        
        }
    </script>  
    
    
 <div id="maincontent_full">
 <GMPT:PageTitleBar ID="PageTitleBar1" runat="server"></GMPT:PageTitleBar>  
         <ul id="database_tabs">
            <%--<li><asp:HyperLink ID="SearchLink" runat="server" Text="Search"></asp:HyperLink></li>--%>
             <li><a class="selected">Country Snapshot</a></li>
             <li><asp:LinkButton ID="SearchLink" runat="server" OnClick="SearchLink_Click" Text="Search" ToolTip="click to modify search"></asp:LinkButton></li>
            <li><asp:LinkButton ID="ResultsLink" runat="server" OnClick="ResultsLink_Click" Text="View Results"  ToolTip="click to view results"></asp:LinkButton></li>        
            <li><asp:LinkButton ID="AnalysisLink" runat="server" OnClick="AnalysisLink_Click" Text="Chart Results" ToolTip="click to view chart results"></asp:LinkButton></li>
            <GMPT:Glossary ID="Links" runat="server" />
            <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;            </li>   
        </ul> 
        <GMPT:CountrySnapshotOptions id="CountrySnapshotOptions" runat="server" Source="profiler"></GMPT:CountrySnapshotOptions><br />
    <div>
            <div style="padding-bottom:8px; float: right;">
                <table><tr><td>
                    
                <div id="CountryDiv" runat="server" style="float: left; z-index:8;">
                    <table>
                        <tr>       <td></td>                                         
                            <td>
                 Select Country
                            </td>
                            <td>
                                 <ComponentArt:ComboBox
                                    Id="CountryCombo" CssClass="comboBox" HoverCssClass="comboBoxHover" FocusedCssClass="comboBoxHover"
                                    TextBoxCssClass="comboTextBox" DropDownCssClass="comboDropDown" ItemCssClass="TreeNode" 
                                    ItemHoverCssClass="TreeNode" SelectedItemCssClass="NodeSelected"
                                    DropHoverImageUrl="../assets/images/drop_hover.gif" DropImageUrl="../assets/images/drop.gif"
                                    Width="180" DropDownHeight="220" DropDownWidth="177"
                                    RunAt="Server" AutoTheming="true" AutoHighlight="false" AutoComplete="true" AutoFilter="true"
                                    DataTextField="Country" DataValueField="CountryID"
                                  >
                                  <ClientEvents>
                                  <Change EventHandler="CountryCombo_onChange" />
                                  </ClientEvents>
                                  </ComponentArt:ComboBox>
                            </td>
                            
                        </tr>
                        <tr>
                        <td>
                            <asp:RadioButton ID="CustomCountriesRadio" Checked="true" Visible="false" runat="Server" GroupName="ComparisonType" Text="Custom Countries"/>
                        </td>
                        <td>Compare with <asp:RadioButton ID="PeerCountriesRadio" Checked="true" Visible="false"  runat="Server" GroupName="ComparisonType" Text="Peer Countries "/>
                        </td>
                        
                        <td>
                            <ComponentArt:ComboBox id="MultiSelectCountryCombo" runat="server"
                                KeyboardEnabled="false"
                                AutoFilter="false"
                                AutoHighlight="false"
                                AutoComplete="false"
                                CssClass="comboBox"                                            
                                HoverCssClass="comboBoxHover"
                                FocusedCssClass="comboBoxHover"
                                TextBoxCssClass="comboTextBox"
                                DropDownCssClass="comboDropDown"
                                ItemCssClass="comboItem"
                                ItemHoverCssClass="comboItemHover"
                                SelectedItemCssClass="comboItemHover"
                                DropHoverImageUrl="../assets/images/drop_hover.gif"
                                DropImageUrl="../assets/images/drop.gif"
                                Width="180"
                                DropDownHeight="220"
                                DropDownWidth="177" EnableViewState="true" >
                              <DropdownContent>
                                  <ComponentArt:TreeView id="MultiSelectCountryTree" Height="220" Width="177"
                                    DragAndDropEnabled="false"
                                    NodeEditingEnabled="false"
                                    KeyboardEnabled="true"
                                    CssClass="TreeView"
                                    NodeCssClass="TreeNode"                                                        
                                    NodeEditCssClass="NodeEdit"
                                    SelectedNodeCssClass="NodeSelected"
                                    LineImageWidth="19"
                                    LineImageHeight="20"
                                    DefaultImageWidth="16"
                                    DefaultImageHeight="16"
                                    ItemSpacing="0"
                                    NodeLabelPadding="3"
                                    ImagesBaseUrl="../assets/images/tvlines/"
                                    LineImagesFolderUrl="../assets/images/tvlines/"
                                    ShowLines="false"
                                    EnableViewState="true"                           
                                    runat="server" >
                                  <ClientEvents>
                                    <NodeCheckChange EventHandler="MultiSelectCountryTree_onNodeCheckChange" />
                                  </ClientEvents>
                                  </ComponentArt:TreeView>
                              </DropdownContent>
                              
                            </ComponentArt:ComboBox>
                        </td>
                        </tr>
                    </table>                              
                </div>
                </td><td>
                <div class="button_right_chart" style="width: 120px;">
                    <asp:LinkButton ID="GoLink" runat="server" Text="go" OnClick="GoLink_Click" ToolTip="click to update results">
                    </asp:LinkButton>
                </div>
                <span>Extract To :<asp:LinkButton ID="lnkExcel" runat="server" OnClick="lnkExcel_Click" ToolTip="extract chart data to excel"><span style="background: url(../assets/images/tool_excel2.gif) no-repeat 0px 1px;padding-left:12px" >Excel</span></asp:LinkButton></span>
                
                </td></tr>
                
                </table>
                 
            </div>
            <div style="float: left; color:#1E3267; padding-top:1px;font-size:16px; font-weight:bold;">
            <asp:Label ID="CountryName" runat="server"></asp:Label>
            </div>
     </div>
 </div>
<%--<asp:Image ID="Chart1" runat="server" ImageUrl="GetProfilerChart.aspx?streamtype=img&chartID=16&countryID=345&countryName=China" />
--%>
<div id="divCharts" runat="server"></div>
<asp:HiddenField ID="hdnCountry" runat="server" />
<asp:HiddenField ID="hdnCountryName" runat="server" />
<asp:HiddenField ID="hdnCountriesToCompare" runat="server" />
</asp:Content>
