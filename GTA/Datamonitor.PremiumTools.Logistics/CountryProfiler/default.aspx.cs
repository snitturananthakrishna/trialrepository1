using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.IO;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CArt = ComponentArt.Web.UI;
using Aspose.Cells;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Controls;

namespace Datamonitor.PremiumTools.Generic
{
    public partial class _default : AuthenticationBasePage
    {
        // EVENT OVERRIDES
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Country Profiler";

            BindControls();
            
            if (!IsPostBack)
            {               
                BindProfilerData();
                PeerCountriesRadio.Checked = true;
            }                  
           
        }

        // EVENTY HANDLERS
        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            //LogUsage("Country Profiler", "country1", " ");

            string Title = hdnCountryName.Value + " - Country Profiler";
            Dictionary<string, Dictionary<string, Bitmap>> Charts = new Dictionary<string, Dictionary<string, Bitmap>>();

            DataSet ProfilerSections = SqlDataService.GETCountryProfilerSectionsNCharts(Convert.ToInt32(hdnCountry.Value));

            foreach (DataRow section in ProfilerSections.Tables[0].Rows)
            {
                string SectionHeading = section["Name"].ToString();
                Dictionary<string, Bitmap> SectionCharts = new Dictionary<string, Bitmap>();

                foreach (DataRow profilerChart in ProfilerSections.Tables[1].Rows)
                {
                    if (section["SectionID"].ToString().Equals(profilerChart["SectionID"].ToString()))
                    {
                        string ChartHeader = profilerChart["Name"].ToString();
                        Bitmap ChartBitmap = null;
                        string CountryIDsTOCompare = "";

                        if (CustomCountriesRadio.Checked)
                        {
                            //Get the selected countries list from combobox tree
                            CountryIDsTOCompare = hdnCountriesToCompare.Value;
                        }
                        else
                        {
                            //Get the selected countries list from DB(peer countries)
                            CountryIDsTOCompare = ProfilerSections.Tables[2].Select("IndicatorID=" + profilerChart["IndicatorForRanking"].ToString())[0]["PeerCountryIDs"].ToString();
                        }

                        //Get the chart bitmap from handler only when country data found to compare
                        if (!String.IsNullOrEmpty(CountryIDsTOCompare))
                        {
                            ChartBitmap = GetBitmap(profilerChart["ChartID"].ToString(),
                                                       hdnCountry.Value,
                                                       hdnCountryName.Value,
                                                       CountryIDsTOCompare.TrimEnd(','));
                        }
                        SectionCharts.Add(ChartHeader, ChartBitmap);
                    }
                }
                Charts.Add(SectionHeading, SectionCharts);
            }

            Workbook Excel;
            Excel = ExtractionsHelper.ExportCountryProfilerToExcel(Charts,
                Server.MapPath(GlobalSettings.ExcelTemplatePath),
                Server.MapPath(GlobalSettings.ExtractLogoPathForOvum),
                Title);
            Excel.Save("Ovum" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);

        }
        private Bitmap GetBitmap(string chartID,
                string countryID,
                string countryName,
                string countryIDsTOCompare)
        {
            WebResponse response = null;
            string PageUrl = Request.Url.AbsoluteUri;
            string imageurl = string.Format("GetProfilerChart.aspx?streamtype=bytes&chartID={0}&countryID={1}&countryName={2}&countryidstocompare={3}",
                chartID,
                countryID,
                countryName,
                countryIDsTOCompare);
            imageurl = PageUrl.ToLower().Replace("default.aspx", imageurl);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(imageurl);
            request.Method = "GET";
            response = request.GetResponse();

            Stream responseStream = response.GetResponseStream();
            return new Bitmap(responseStream);
        }
       
        protected void GoLink_Click(object sender, EventArgs e)
        {
            LogUsage("Country Profiler", "country1", " ");
            BindProfilerData();
        }
        
        // PRIVATE METHODS (initial population)  
        private void BindControls()
        {
            DataSet CountriesList = SqlDataService.GetAllCountries();

            if (CountriesList != null &&
                CountriesList.Tables.Count > 0 &&
                CountriesList.Tables[0].Rows.Count > 0)
            {

                #region Binding Country data - to single select combo

                CountryCombo.DataSource = CountriesList.Tables[0];
                CountryCombo.DataBind();

                if (Request.QueryString["country"] != null && CountryCombo.Items.FindByText(Request.QueryString["country"].ToString()) != null)
                {
                    CountryCombo.SelectedItem = CountryCombo.Items.FindByText(Request.QueryString["country"].ToString());
                }
                else if (string.IsNullOrEmpty(hdnCountry.Value) && CountryCombo.Items.FindByValue(GlobalSettings.CountryBrowserDefaultCountryID) != null)
                {
                    CountryCombo.SelectedValue = GlobalSettings.CountryBrowserDefaultCountryID;
                }
                else
                {
                    CountryCombo.SelectedValue = hdnCountry.Value;
                }

                hdnCountry.Value = CountryCombo.SelectedItem.Value;
                hdnCountryName.Value = CountryCombo.SelectedItem.Text;

                #endregion

                #region Binding Country data - to multi select combo

                //binding Country data
                foreach (DataRow dbRow in CountriesList.Tables[0].Rows)
                {
                    ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["Country"].ToString(),
                          dbRow["CountryID"].ToString(),
                          true,
                          true,
                          false);

                    if (("," + hdnCountriesToCompare.Value).Contains("," + dbRow["CountryID"].ToString() + ","))
                    {
                        newNode.Checked = true;
                    }
                    MultiSelectCountryTree.Nodes.Add(newNode);
                }

                if (string.IsNullOrEmpty(hdnCountriesToCompare.Value))
                {
                    MultiSelectCountryCombo.Text = "";
                    int Counter = 0;

                    foreach (ComponentArt.Web.UI.TreeViewNode CurrentNode in MultiSelectCountryTree.Nodes)
                    {
                        CurrentNode.Checked = true;
                        MultiSelectCountryCombo.Text += CurrentNode.Text + ";";
                        hdnCountriesToCompare.Value += CurrentNode.ID + ",";
                        Counter++;
                        if (Counter >= 2) break;
                    }
                }
                

                #endregion
            }
        }

        private void BindProfilerData()
        {
            CountryName.Text = hdnCountryName.Value + " - Country Profiler";

            DataSet ProfilerSections = SqlDataService.GETCountryProfilerSectionsNCharts(Convert.ToInt32(hdnCountry.Value));

            foreach (DataRow section in ProfilerSections.Tables[0].Rows)
            {
                // background-color:#e0e0e0; height:20px;width:900px;
                Literal sectionStart = new Literal();

                sectionStart.Text = string.Format("<div style='cursor:hand;margin-left:20px;width:250px;font-size:12px;font-weight:bold;' onclick=\"$('#div{0}').toggle('slow');\" >{1}</div><div id='div{0}'><table style='margin-left:20px;' cellpadding='10' width=100%'>", 
                        section["SectionID"].ToString(),
                        section["Name"].ToString());

                divCharts.Controls.Add(sectionStart);

                int chartcount = 1;

                foreach (DataRow profilerChart in ProfilerSections.Tables[1].Rows)
                {                    
                    if (section["SectionID"].ToString().Equals(profilerChart["SectionID"].ToString()))
                    {
                        Label ChartHeader = new Label();
                        ChartHeader.Text = profilerChart["Name"].ToString();
                        if (Convert.ToBoolean(profilerChart["ShowRank"]))
                        {
                            ChartHeader.Text = string.Format("{0} - Rank <a target='blank' href='../Results/TrendImage.aspx?rowid={1}&year=2009&countryid={2}&IsRank=true&IsCountryProfiler=true'><b>{3}</b></a>",
                                ChartHeader.Text,
                                profilerChart["rowid"].ToString(),
                                hdnCountry.Value,
                                profilerChart["QuartileRankNumber"].ToString());
                            
                        }
                        ChartHeader.Text = "<b>"+ChartHeader.Text + "</b><br />";                       

                        ProfilerChart Control1 = (ProfilerChart)Page.LoadControl("~/Controls/CountryProfiler/ProfilerChart.ascx");

                        Control1.ChartID = profilerChart["ChartID"].ToString();
                        Control1.CountryID = hdnCountry.Value;
                        Control1.CountryName = hdnCountryName.Value;

                        string CountryIDsTOCompare = "";

                        #region Get the CountryIDsTOCompare

                        if (CustomCountriesRadio.Checked)
                        {
                            //Get the selected countries list from combobox tree
                            CountryIDsTOCompare = hdnCountriesToCompare.Value;
                        }
                        else
                        {
                            //Get the selected countries list from DB(peer countries)
                            CountryIDsTOCompare = ProfilerSections.Tables[2].Select("IndicatorID=" + profilerChart["IndicatorForRanking"].ToString())[0]["PeerCountryIDs"].ToString();
                        }

                        #endregion

                        //Plot the chart only when country data found to compare
                        if (!String.IsNullOrEmpty(CountryIDsTOCompare))
                        {
                            Control1.CountryIDsTOCompare = CountryIDsTOCompare.TrimEnd(',');
                            Control1.BuildChart();

                            if (chartcount % 2 == 1)
                            {
                                Literal chartSectionStart = new Literal();
                                chartSectionStart.Text = "<tr><td>";
                                divCharts.Controls.Add(chartSectionStart);

                                divCharts.Controls.Add(ChartHeader);
                                divCharts.Controls.Add(Control1);

                                Literal chartSectionEnd = new Literal();
                                chartSectionEnd.Text = "</td>";
                                divCharts.Controls.Add(chartSectionEnd);
                            }
                            else if (chartcount % 2 == 0)
                            {
                                Literal chartSectionStart = new Literal();
                                chartSectionStart.Text = "<td>";
                                divCharts.Controls.Add(chartSectionStart);

                                divCharts.Controls.Add(ChartHeader);
                                divCharts.Controls.Add(Control1);

                                Literal chartSectionEnd = new Literal();
                                chartSectionEnd.Text = "</td></tr>";
                                divCharts.Controls.Add(chartSectionEnd);
                            }

                            chartcount++;
                        }
                    }
                }

                if (ProfilerSections.Tables[1].Rows.Count % 2 == 1)
                {
                    Literal ChartsectionEnd = new Literal();
                    ChartsectionEnd.Text = "</tr>";
                    divCharts.Controls.Add(ChartsectionEnd);

                }
                Literal sectionEnd = new Literal();
                sectionEnd.Text = "</table></div><br /><br />";
                divCharts.Controls.Add(sectionEnd);

            }

           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">The node text.</param>
        /// <param name="value">The node value.</param>
        /// <param name="expanded">Is node expanded</param>
        /// <param name="hassCheckbox">Has checkbox</param>
        /// <param name="isChecked">Is node checked</param>
        /// <returns></returns>
        private CArt.TreeViewNode CreateNode(string text,
            string value,
            bool expanded,
            bool hasCheckbox,
            bool isChecked)
        {
            ComponentArt.Web.UI.TreeViewNode node = new CArt.TreeViewNode();
            node.Text = text;
            node.Value = value;
            node.ID = value;
            node.Expanded = expanded;
            node.ShowCheckBox = hasCheckbox;
            node.Checked = isChecked;
            return node;
        }

        protected void SearchLink_Click(object sender, EventArgs e)
        {
            //CurrentSession.SetInSession<Dictionary<int, string>>(_taxonomyTypeID, null);
            //UpdateCountryDetailsToSeesion();
            Response.Redirect("~/" + GlobalSettings.DefaultSearchPage);
        }

        protected void ResultsLink_Click(object sender, EventArgs e)
        {
            //CurrentSession.SetInSession<Dictionary<int, string>>(_taxonomyTypeID, null);

            //UpdateCountryDetailsToSeesion();

            Response.Redirect(GlobalSettings.DefaultResultsPage.Substring(GlobalSettings.DefaultResultsPage.IndexOf("/") + 1));
        }

        protected void AnalysisLink_Click(object sender, EventArgs e)
        {
            //CurrentSession.SetInSession<Dictionary<int, string>>(_taxonomyTypeID, null);

            //UpdateCountryDetailsToSeesion();
            Response.Redirect("~/analysis/countrycomparison.aspx");
        }
    }
}

