<%@ Page Language="C#" MasterPageFile="MasterPages/Logistics.Master" AutoEventWireup="true"
    Codebehind="CountryComparisonGrid.aspx.cs" Inherits="Datamonitor.PremiumTools.Generic.CountryComparisonGrid"
    Theme="NormalTree" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">
    <link href="assets/css/arcticwhite.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/gridstyle.css" rel="stylesheet" type="text/css" />
    
    <style type="text/css">
        .rightdiv
        {
            float:right;
            margin-bottom:4px;
        }
        .leftdiv
        {
            float:left;
            width:60px;
        }
        input[type=checkbox] 
        {
	        width:5px;
	        margin-left:-65px;
	        margin-right:-65px;
	        padding:0px;
	        text-align:left;
        }
        
        .ResultsPanel
        {
            float:left;
            
        }
    </style>

    <script type="text/javascript">
        function getSelectedCountries()
        {
            return $("#<%= hdnCountry.ClientID%>").val();
        }
        function getSelectedYear()
        {
            return $("#<%= hdnYear.ClientID%>").val();
        }
        function getValueType(dataitem, fieldName)
        {        
            return "Actual";
        }

        function CountryTree_onNodeCheckChange(sender,eventArgs)
        {
            var hdnControl=document.getElementById('<%= hdnCountry.ClientID %>');    
            OnMultipleTreeCheckChange(eventArgs.get_node(),hdnControl,CountryCombo);
        }
        function YearTree_onNodeSelect(sender, eventArgs)
        {       
            YearCombo.set_text(eventArgs.get_node().get_text());
            YearCombo.collapse();
            document.getElementById('<%=hdnYear.ClientID %>').value = eventArgs.get_node().get_value();   
        }
        function OnMultipleTreeCheckChange(checkedNode,hdnControl,filterBox)
        {

            var comboText = filterBox.get_text();
            if (checkedNode.get_checked()) 
            {
                hdnControl.value = hdnControl.value  + checkedNode.get_id()+ ",";
                if(comboText.length>0)
                {
                    comboText +=";"+checkedNode.get_text();
                } 
                else
                {
                    comboText +=checkedNode.get_text();
                } 
                filterBox.set_text(comboText );
            }
            else
            {
                hdnControl.value = hdnControl.value.replace(checkedNode.get_id()+",","");
                if(comboText.length>0)
                {            
                    if(comboText.indexOf(checkedNode.get_text()+";")!=-1)
                    {
                        comboText=comboText.replace(checkedNode.get_text()+";","");
                    }
                    else if(comboText.indexOf(";"+checkedNode.get_text())!=-1)
                    {
                        comboText=comboText.replace(";"+checkedNode.get_text(),"");
                    }            
                    else
                    {
                        comboText=comboText.replace(checkedNode.get_text(),"");
                    }
                    filterBox.set_text(comboText );
                }
            }
        }

    </script>

    <div id="maincontent_full">
        <GMPT:PageTitleBar ID="PageTitleBar1" runat="server"></GMPT:PageTitleBar>
        <div style="padding-bottom: 20px; float: left;">
            <div id="CountryDiv" runat="server" style="float: left; padding-left: 20px;z-index:9999">
                <table>
                    <tr>
                        <td>
                            Country &nbsp;</td>
                        <td>
                            <ComponentArt:ComboBox ID="CountryCombo" runat="server" KeyboardEnabled="false" AutoFilter="false"
                                AutoHighlight="false" AutoComplete="false" CssClass="comboBox" HoverCssClass="comboBoxHover"
                                FocusedCssClass="comboBoxHover" TextBoxCssClass="comboTextBox" DropDownCssClass="comboDropDown"
                                ItemCssClass="comboItem" ItemHoverCssClass="comboItemHover" SelectedItemCssClass="comboItemHover"
                                DropHoverImageUrl="assets/images/drop_hover.gif" DropImageUrl="assets/images/drop.gif"
                                Width="150" DropDownHeight="220" DropDownWidth="147">
                                <DropDownContent>
                                    <ComponentArt:TreeView ID="CountryTree" Height="220" Width="147" DragAndDropEnabled="false"
                                        NodeEditingEnabled="false" KeyboardEnabled="true" CssClass="TreeView" NodeCssClass="TreeNode"
                                        NodeEditCssClass="NodeEdit" SelectedNodeCssClass="NodeSelected" LineImageWidth="19"
                                        LineImageHeight="20" DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0"
                                        NodeLabelPadding="3" ImagesBaseUrl="assets/images/tvlines/" LineImagesFolderUrl="assets/images/tvlines/"
                                        ShowLines="true" EnableViewState="false" runat="server">
                                        <ClientEvents>
                                            <NodeCheckChange EventHandler="CountryTree_onNodeCheckChange" />
                                        </ClientEvents>
                                    </ComponentArt:TreeView>
                                </DropDownContent>
                            </ComponentArt:ComboBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="YearDiv" runat="server" style="float: left; padding-left: 20px;z-index:9999">
                <table>
                    <tr>
                        <td>
                            Year &nbsp;
                        </td>
                        <td>
                            <ComponentArt:ComboBox ID="YearCombo" runat="server" KeyboardEnabled="false" AutoFilter="false"
                                AutoHighlight="false" AutoComplete="false" CssClass="comboBox" HoverCssClass="comboBoxHover"
                                FocusedCssClass="comboBoxHover" TextBoxCssClass="comboTextBox" DropDownCssClass="comboDropDown"
                                ItemCssClass="comboItem" ItemHoverCssClass="comboItemHover" SelectedItemCssClass="comboItemHover"
                                DropHoverImageUrl="assets/images/drop_hover.gif" DropImageUrl="assets/images/drop.gif"
                                Width="120" DropDownHeight="220" DropDownWidth="117">
                                <DropDownContent>
                                    <ComponentArt:TreeView ID="YearTree" Height="220" Width="117" DragAndDropEnabled="false"
                                        NodeEditingEnabled="false" KeyboardEnabled="true" CssClass="TreeView" NodeCssClass="TreeNode"
                                        NodeEditCssClass="NodeEdit" SelectedNodeCssClass="NodeSelected" LineImageWidth="19"
                                        LineImageHeight="20" DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0"
                                        NodeLabelPadding="3" ImagesBaseUrl="assets/images/tvlines/" LineImagesFolderUrl="assets/images/tvlines/"
                                        ShowLines="true" EnableViewState="false" runat="server">
                                        <ClientEvents>
                                            <NodeSelect EventHandler="YearTree_onNodeSelect" />
                                        </ClientEvents>
                                    </ComponentArt:TreeView>
                                </DropDownContent>
                            </ComponentArt:ComboBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="button_right_chart" style="width: 120px;">
                <asp:LinkButton ID="ResultsLink" runat="server" Text="Results" OnClick="ResultsLink_Click">
                </asp:LinkButton>
            </div>
            <br />
        </div>
                     
            <ComponentArt:Grid ID="ResultsGrid" CssClass="grid" RunningMode="client" ShowHeader="false"
                ShowFooter="false" AllowColumnResizing="false" AllowEditing="false" HeaderCssClass="GridHeader"
                FooterCssClass="GridFooter" SliderHeight="20" SliderWidth="150" SliderGripWidth="9"
                SliderPopupOffsetX="20" IndentCellWidth="22" IndentCellCssClass="HeadingCell_Freeze"
                LoadingPanelClientTemplateId="LoadingFeedbackTemplate" LoadingPanelPosition="MiddleCenter"
                LoadingPanelFadeDuration="10" LoadingPanelFadeMaximumOpacity="60" ScrollBar="Auto"
                ImagesBaseUrl="assets/images/" PagerStyle="numbered" PagerInfoPosition="BottomLeft"
                PagerPosition="BottomRight" PreExpandOnGroup="true" ScrollTopBottomImagesEnabled="true"
                ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="assets/images/scroller/"
                ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" 
                FillContainer="true" 
                AutoAdjustPageSize="false"
                ManualPaging="false" ScrollGripCssClass="ScrollGrip" ScrollBarWidth="16" Width="900" runat="server"
                AllowHorizontalScrolling="True" EnableViewState="false">
                <Levels>
                    <ComponentArt:GridLevel AllowReordering="false" AllowGrouping="false" AllowSorting="false"
                        HeadingCellCssClass="HeadingCell" HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText"
                        GroupHeadingCssClass="HeadingCell_Freeze" DataCellCssClass="DataCell" AlternatingRowCssClass="RowAlt"
                        RowCssClass="Row" SelectedRowCssClass="SelRow" SortAscendingImageUrl="asc.gif"
                        SortDescendingImageUrl="desc.gif">
                        <Columns>
                            
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="CommonDataTemplate">
                        <div class="## getValueType(DataItem, DataItem.GetCurrentMember().Column.DataField) ##">
                            ## DataItem.GetCurrentMember().get_value() ##</div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="TooltipTemplate">
                        <div title="## DataItem.GetCurrentMember().get_value() ##">
                            ## DataItem.GetCurrentMember().get_value() ##</div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="TrendChartTemplate">
                        <div style="padding: 0px; font-weight: bold; text-align: center;">
                            <a style="color: Red; text-decoration: none;" 
                                href="CountryComparisonImage.aspx?rowid=## DataItem.GetMember('rowID').Text ##&countries=## getSelectedCountries() ##&selectedyear=## getSelectedYear() ##" 
                                onclick="return hs.htmlExpand(this, { contentId: 'CountryComparisonImage', objectType: 'ajax'} )">
                                <img alt="" border="0" src="../assets/images/chart_icon.gif" />
                            </a>
                        </div>
                    </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
            
            <asp:HiddenField ID="hdnCountry" runat="server" />
            <asp:HiddenField ID="hdnYear" runat="server" />
       
    </div>
   <div class="highslide-html-content" id="CountryComparisonImage" style="width:350px;height:275px">
        <div class="highslide-header">
            <ul>
                <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                <li class="highslide-close">
                    <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="hs.close(this);return false;"
                        Text="Close"></asp:LinkButton>
                </li>
            </ul>
            <h1>Country Comparison Chart</h1>
        </div>
        <div class="highslide-body">               
        </div>
    </div>
    
</asp:Content>
