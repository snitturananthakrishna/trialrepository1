<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/AnalysisMaster.Master" 
        AutoEventWireup="true" 
        CodeBehind="IndicatorComparison.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.Analysis.IndicatorComparison"        
        Theme="NormalTree"%>
<%@ Register TagPrefix="ComponentArt" 
        Namespace="ComponentArt.Web.Visualization.Charting" 
        Assembly="ComponentArt.Web.Visualization.Charting" %>  
<%@ Register Assembly="dotnetCHARTING" 
        Namespace="dotnetCHARTING" 
        TagPrefix="dotnetCHARTING" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<%@ Register Src="../Controls/DynamicChartfilters.ascx" TagName="DynamicChartfilters" TagPrefix="GMPT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">

<style type="text/css">
.rightdiv
{
    float:right;
    margin-bottom:4px;
}
.leftdiv
{
    float:left;
    width:60px;
}
input[type=checkbox] 
{
	width:5px;
	margin-left:-65px;
	margin-right:-65px;
	padding:0px;
	text-align:left;
}
</style>
<script type="text/javascript">

function ChartTypes_onNodeSelect(sender, eventArgs)
{ 
    ChartTypesDropDown.set_text(eventArgs.get_node().get_text());
    ChartTypesDropDown.collapse();    
    document.getElementById('<%=hiddenChartType.ClientID %>').value = eventArgs.get_node().get_value();   
}

function PlotChart()
{  
    $("#PlotChartMsg").hide();
    var Indicator1= $("#<%= hdnIndicator1.ClientID%>").val();   
    var Indicator2= $("#<%= hdnIndicator2.ClientID%>").val();  
    var Indicators= $("#<%= hdnIndicators.ClientID%>").val();   
    var CountrySelected = $("#<%= hdnCountry.ClientID%>").val();
    var chartType = $("#<%= hiddenChartType.ClientID%>").val();
    var startYear=$("#<%= hdnStartYear.ClientID%>").val();
    var endYear=$("#<%= hdnEndYear.ClientID%>").val();
    var IsPredefined=$("#<%= hdnIsPredefined.ClientID%>").val();
    var Yaxis1SeriesTypes=$("#<%= hdnYaxis1SeriesTypes.ClientID%>").val();
    var Yaxis2SeriesTypes=$("#<%= hdnYaxis2SeriesTypes.ClientID%>").val();
   
    var IndicatorSelected;
    if(IsPredefined == "false")
    {
       IndicatorSelected =Indicator1+","+Indicator2;
    }
    else
    {
        IndicatorSelected = Indicators;
    }
    
    ChartsCallback.callback(IndicatorSelected,
        CountrySelected,
        chartType,
        startYear,
        endYear,
        CountryCombo.get_text(),
        IsPredefined,
        Yaxis1SeriesTypes,
        Yaxis2SeriesTypes);
  
    return false; 
}
function Indicator1Tree_onNodeSelect(sender, eventArgs)
{       
$("#PlotChartMsg").show();
    Indicator1Combo.set_text(eventArgs.get_node().get_text());
    Indicator1Combo.collapse();
    document.getElementById('<%=hdnIndicator1.ClientID %>').value = eventArgs.get_node().get_value();   
    document.getElementById('<%=hdnIndicatorText.ClientID %>').value = Indicator1Combo.get_text()+"; "+ Indicator2Combo.get_text();
}
function Indicator2Tree_onNodeSelect(sender, eventArgs)
{       
$("#PlotChartMsg").show();
    Indicator2Combo.set_text(eventArgs.get_node().get_text());
    Indicator2Combo.collapse();
    document.getElementById('<%=hdnIndicator2.ClientID %>').value = eventArgs.get_node().get_value();   
    document.getElementById('<%=hdnIndicatorText.ClientID %>').value = Indicator1Combo.get_text()+"; "+ Indicator2Combo.get_text();
}
function CountryTree_onNodeSelect(sender, eventArgs)
{       
$("#PlotChartMsg").show();
    CountryCombo.set_text(eventArgs.get_node().get_text());
    CountryCombo.collapse();
    document.getElementById('<%=hdnCountry.ClientID %>').value = eventArgs.get_node().get_value();   
    document.getElementById('<%=hdnCountryText.ClientID %>').value = eventArgs.get_node().get_text();   
}

function Years_onChange(type)
{    
    var StartYearDropdown = document.getElementById('<%=StartYearDropDown.ClientID %>');
    var EndYearDropdown = document.getElementById('<%=EndYearDropDown.ClientID %>');
    var StartYearHdn = document.getElementById('<%=hdnStartYear.ClientID %>');
    var EndYearHdn= document.getElementById('<%=hdnEndYear.ClientID %>');
    
    if(StartYearDropdown.value > EndYearDropdown.value)
    {
        StartYearDropdown.value= StartYearHdn.value;
        EndYearDropdown.value= EndYearHdn.value;
        alert('Start year cannot be greater than End year');
        return false;
    }
    
    if(type == 'startyear')
    {
        StartYearHdn.value = StartYearDropdown.value;
    }
    else if (type=='endyear')
    {
       EndYearHdn.value = EndYearDropdown.value;
    }
    
    $("#PlotChartMsg").show();
}
function OnChartTypeChange(type)
{
  var hdnControl=document.getElementById('<%= hiddenChartType.ClientID %>');   
  var StartYearDropdown = document.getElementById('<%=ChartTypeDropdown.ClientID %>'); 
  //hdnControl.value=type;
  hdnControl.value=StartYearDropdown.value;
  
  $("#PlotChartMsg").show();
}
</script>

   
   <div >
<h2><div id="PlotChartMsg" class="PlotChartMessage">*Please click on PLOT CHART to update</div><asp:Label ID="ChartTitle" runat="server" Text="Indicator Comparison"></asp:Label></h2>
</div><div class="analyzeresults_col1">
     
    <div style="height:80px;border-bottom:solid 1px lightgrey;"> 
    
       <div id="CountryDiv" runat="server" >
           <table><tr><td> Country &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
       <td>
       <ComponentArt:ComboBox id="CountryCombo" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="403"
            DropDownHeight="220"
            DropDownWidth="400" >
          <DropdownContent>
              <ComponentArt:TreeView id="CountryTree" Height="220" Width="400"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"
                ItemSpacing="0"
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"
                ShowLines="true"
                EnableViewState="false"                           
                runat="server" >
              <ClientEvents>                
                    <NodeSelect EventHandler="CountryTree_onNodeSelect" />
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
        </ComponentArt:ComboBox>
        </td>
        <td><GMPT:CountryFilter id="CountryFilter1" runat="server" TaxonomyType="Country"></GMPT:CountryFilter></td></tr></table>  
        </div>
        
        <div id="Indicator1Div" runat="server" style="float:left;">    
        <table><tr><td>Indicator 1 &nbsp;</td>
        <td>
        <ComponentArt:ComboBox id="Indicator1Combo" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="403"
            DropDownHeight="220"
            DropDownWidth="400" >
          <DropdownContent>
              <ComponentArt:TreeView id="Indicator1Tree" Height="220" Width="400"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"
                ItemSpacing="0"
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"
                ShowLines="true"
                EnableViewState="false"                           
                runat="server" >
              <ClientEvents>
               <NodeSelect EventHandler="Indicator1Tree_onNodeSelect" />
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
        </ComponentArt:ComboBox>
        </td>
        <td><GMPT:CountryFilter id="CountryFilter2" TaxonomyType="Indicator" runat="server"></GMPT:CountryFilter></td></tr></table>  
        </div>
        
        <div id="Indicator2Div" runat="server" style="float:left;">    
        <table><tr><td>Indicator 2 &nbsp;</td>
        <td>
        <ComponentArt:ComboBox id="Indicator2Combo" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="403"
            DropDownHeight="220"
            DropDownWidth="400" >
          <DropdownContent>
              <ComponentArt:TreeView id="Indicator2Tree" Height="220" Width="400"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"
                ItemSpacing="0"
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"
                ShowLines="true"
                EnableViewState="false"                           
                runat="server" >
              <ClientEvents>
               <NodeSelect EventHandler="Indicator2Tree_onNodeSelect" />
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
        </ComponentArt:ComboBox>
        </td></tr></table>  
        </div>
       
    </div>
    <div>
         <dotnetCHARTING:Chart ID="TwoDChart" runat="server" 
                Visible="false">                                           
            </dotnetCHARTING:Chart>
               
            <ComponentArt:CallBack ID="ChartsCallback" runat="server"
                PostState="true"
                OnCallback="Chart_Callback">
                <Content>                   
                    <asp:Image ID="ChartImg" runat="server" Visible="true" />
                    <asp:Label id="ImageMapLabel" runat="server"/>
                      <br />
                <div id="divChart" runat="server" 
            style=" overflow:auto; width:520px; height:150px; scrollbar-face-color: #BFC4D1;
                    scrollbar-shadow-color: #FFFFFF;
                    scrollbar-highlight-color: #FFFFFF;
                    scrollbar-3dlight-color: #FFFFFF;
                    scrollbar-darkshadow-color: #FFFFFF;
                    scrollbar-track-color: #FFFFFF;
                    scrollbar-arrow-color: #FFFFFF;"   >
                    
                    <asp:Image ID="LegendImg" runat="server" Visible="true" />
            
                    </div>
                     <asp:HiddenField ID="hdnImageurl" runat="server" Value="" /> 
                     <asp:HiddenField ID="hdnLegendUrl" runat="server" Value="" /> 
            </Content>
              <LoadingPanelClientTemplate>
            <table class="loadingpanel" width="100%" style="height:300" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td align="center">
                <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td colspan="2">
                  <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="../assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                  </td>
                </tr>
                </table>
                </td>
              </tr>
              </table>
        </LoadingPanelClientTemplate>
        </ComponentArt:CallBack>
    </div>
    
</div>
<div class="analyzeresults_col2" id="analyzeresults_filter">      
   
        <div id="filterContainer"> 

        <div id="StartYearDiv" runat="server">
            Start Year: &nbsp;<asp:DropDownList ID="StartYearDropDown" runat="server"                     
                    Width="70px" 
                    Font-Size="11px"></asp:DropDownList>            
            
        </div> <br />
        <div id="EndYearDiv2" runat="server" >
            End Year:&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="EndYearDropDown" runat="server"                     
                    Width="70px" 
                    Font-Size="11px"></asp:DropDownList>            
             
        </div> <br />
    </div>
        
        <div id="chartTypeDiv" runat="server" >
          
          <%--<b>Chart type</b>&nbsp;--%>
          <%--<input type="radio" name="radio" id="LineRadio" runat="server" onclick="OnChartTypeChange('3');" />
          Line&nbsp;&nbsp;
          <input type="radio" name="radio" id="BarRadio" runat="server" onclick="OnChartTypeChange('7');" />
          Bar&nbsp;&nbsp;--%>
          Chart&nbsp;type:&nbsp;
          <asp:DropDownList ID="ChartTypeDropdown" runat="server" Width="70px" Font-Size="11px">
          <asp:ListItem Text="Line" Value="3" />
          <asp:ListItem Text="Bar" Value="7" />
          <asp:ListItem Text="Area" Value="4" />         
          </asp:DropDownList>
         
        </div><br />
    
    <div class="button_right_chart" style="width:120px">
        <asp:LinkButton ID="PlotChart"  runat="server" 
            Text="Plot chart" 
            OnClientClick="javascript:return PlotChart(); " ToolTip="Update Chart">
        </asp:LinkButton>
    </div>
    
    <br /><br />     
       <h1>MY TOOLS</h1>
       <h2 class="tool_excel"><asp:LinkButton ID="lnkExcel" Text="Extract to Excel" runat="server" OnClick="lnkExcel_Click" ToolTip="extract chart data to excel"/></h2>
       <h2 class="tool_word"><asp:LinkButton ID="LinkButton1" Text="Extract to Word" runat="server" OnClick="lnkWord_Click" ToolTip="extract chart data to word"/></h2>
       <h2 class="tool_powerpoint"><asp:LinkButton ID="lnkPPT" Text="Extract to PowerPoint" runat="server" OnClick="lnkPPT_Click" ToolTip="extract chart data to power point"/></h2>               
<br /><br /><br /><br />

</div>   
  
<asp:HiddenField ID="hiddenChartType" runat="server" />
<asp:HiddenField ID="hdnIndicator1" runat="server" />
<asp:HiddenField ID="hdnIndicator2" runat="server" />
<asp:HiddenField ID="hdnCountry" runat="server" />
<asp:HiddenField ID="hdnIndicatorText" runat="server" />
<asp:HiddenField ID="hdnCountryText" runat="server" />
<asp:HiddenField ID="hdnIndicators" runat="server" />
<asp:HiddenField ID="hdnStartYear" runat="server" />
<asp:HiddenField ID="hdnEndYear" runat="server" />
<asp:HiddenField ID="hdnYaxis1SeriesTypes" runat="server" />
<asp:HiddenField ID="hdnYaxis2SeriesTypes" runat="server" />
<asp:HiddenField ID="hdnIsPredefined" runat="server" />

</asp:Content>

