using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using Datamonitor.PremiumTools.Logistics.Library;
using System.Data.SqlClient;
using ComponentArt.Web.UI;
using ComponentArt.Web.Visualization.Charting;
using Datamonitor.PremiumTools.Logistics.Library.SqlDataAccess;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

namespace Datamonitor.PremiumTools.Logistics.Analysis
{
    public partial class Default : AuthenticationBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            drpSeries.Attributes.Add("onchange", "loadDrps('" + drpRows.ClientID +"','" + drpSeries.ClientID + "'); return false;");            

            if (!IsPostBack)
            {   
                //Bind X-Axis data.
                DataSet dsXAxisData = SqlDataService.GetXaxisData();
                GetDataAndBindToXaxis(dsXAxisData);
                //Bind Y-Axis data.
                DataSet dsYAxisData = SqlDataService.GetYaxisData();
                GetDataAndBindToYaxis(dsYAxisData);
                //Bind Series data.
                DataSet dsDrpsData = SqlDataService.GetDrpsData();
                GetDataAndBindToDrps(dsDrpsData);                
            }
        }

        private void GetDataAndBindToXaxis(DataSet dsXAxisData)
        {
            try
            {
                if (dsXAxisData.Tables.Count > 0)
                {   
                    //Binding X-Axis
                    drpRows.DataSource = dsXAxisData;
                    drpRows.DataTextField = dsXAxisData.Tables[0].Columns["typeName"].ToString();
                    drpRows.DataValueField = dsXAxisData.Tables[0].Columns["taxonomyTypeID"].ToString();
                    drpRows.DataBind();
                    //Binding Series.
                    drpSeries.DataSource = dsXAxisData;
                    drpSeries.DataTextField = dsXAxisData.Tables[0].Columns["typeName"].ToString();
                    drpSeries.DataValueField = dsXAxisData.Tables[0].Columns["taxonomyTypeID"].ToString();
                    drpSeries.DataBind();                
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void GetDataAndBindToDrps(DataSet dsDrpsData)
        {
            try
            {
                DataTable dtCountry ;
                if (dsDrpsData.Tables.Count > 0)
                {
                    DataView dvCountry = dsDrpsData.Tables[0].DefaultView;
                    dvCountry.RowFilter = "taxonomyTypeId = 1"; // Country
                    dtCountry = dvCountry.ToTable();
                    drpCtry.DataSource = dtCountry;
                    drpCtry.DataTextField = dtCountry.Columns["name"].ToString();
                    drpCtry.DataValueField = dtCountry.Columns["taxonomyID"].ToString();
                    drpCtry.SelectedIndex = 0;
                    drpCtry.DataBind();

                    dvCountry.RowFilter = "taxonomyTypeId = 2"; //Mode
                    dtCountry  = dvCountry.ToTable();
                    drpMode.DataSource = dtCountry;
                    drpMode.DataTextField = dtCountry.Columns["name"].ToString();
                    drpMode.DataValueField = dtCountry.Columns["taxonomyID"].ToString();
                    drpMode.SelectedIndex = 5;
                    drpMode.DataBind();

                    dvCountry.RowFilter = "taxonomyTypeId = 3";
                    dtCountry = dvCountry.ToTable();
                    drpMarket.DataSource = dtCountry;
                    drpMarket.DataTextField = dtCountry.Columns["name"].ToString();
                    drpMarket.DataValueField = dtCountry.Columns["taxonomyID"].ToString();
                    //drpMarket.SelectedItem = "Total";
                    drpMarket.DataBind();

                    dvCountry.RowFilter = "taxonomyTypeId = 10";
                    dtCountry = dvCountry.ToTable();
                    drpDestn.DataSource = dtCountry;
                    drpDestn.DataTextField = dtCountry.Columns["name"].ToString();
                    drpDestn.DataValueField = dtCountry.Columns["taxonomyID"].ToString();
                    drpDestn.SelectedIndex = 2;
                    drpDestn.DataBind();

                    dvCountry.RowFilter = "taxonomyTypeId = 5";
                     dtCountry = dvCountry.ToTable();
                    drpProvider.DataSource = dtCountry;
                    drpProvider.DataTextField = dtCountry.Columns["name"].ToString();
                    drpProvider.DataValueField = dtCountry.Columns["taxonomyID"].ToString();
                    drpProvider.DataBind();
                    // No measure                     
                    dvCountry.RowFilter = "taxonomyTypeId = 8";
                     dtCountry = dvCountry.ToTable();
                    drpUnits.DataSource = dtCountry;
                    drpUnits.DataTextField = dtCountry.Columns["name"].ToString();
                    drpUnits.DataValueField = dtCountry.Columns["taxonomyID"].ToString();
                    drpUnits.DataBind();

                    dvCountry.RowFilter = "taxonomyTypeId = 9";
                    dtCountry = dvCountry.ToTable();
                    drpCurrency.DataSource = dtCountry;
                    drpCurrency.DataTextField = dtCountry.Columns["name"].ToString();
                    drpCurrency.DataValueField = dtCountry.Columns["taxonomyID"].ToString();
                    drpCurrency.DataBind();
                    
                    //No years
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void GetDataAndBindToYaxis(DataSet dsYAxisData)
        {
            try
            {
                if (dsYAxisData.Tables.Count > 0)
                {
                   //Binding Y-Axis
                    drpColumns.DataSource = dsYAxisData;
                    drpColumns.DataTextField = dsYAxisData.Tables[0].Columns["Name"].ToString();
                    drpColumns.DataValueField = dsYAxisData.Tables[0].Columns["Name"].ToString();
                    drpColumns.DataBind();
                }
            }
            catch (Exception ex)
            {
            }
        }

        protected void lnkBuildChart_Click(object sender, EventArgs e)
        {
            //Get selection criteria   
            DataSet TaxonomyTypes = LEGlobalSettings.GetTaxonomyTypes();
            BuildChartData(TaxonomyTypes);            
        }

        private void BuildChartData(DataSet TaxonomyTypes)
        {
            string SelectionsXML = string.Empty;
            //Build selection criteria xml
            foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
            {
                Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                if (selectedIDsList != null && selectedIDsList.Count > 0)
                {
                    //Get all keys (ids) from dictionary
                    List<int> keys = new List<int>(selectedIDsList.Keys);
                    //convert int array to string array
                    string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });

                    SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                        taxonomyRow["ID"].ToString(),
                        string.Join(",", SelectedIDs));                    
                }
            }            
            SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);

            //Get results
            DataSet ResultsSet = SqlDataService.GetChartData(SelectionsXML);            

            DataTable dt = new DataTable();
            dt = ResultsSet.Tables[0];

            DataView dvYaxis = ResultsSet.Tables[0].DefaultView;
            dvYaxis.RowFilter = "DataType= '" + drpColumns.SelectedItem.Value + "' and dataYear = " + drpYears.SelectedItem.Value;
            dt = dvYaxis.ToTable();     

            if (dt.Rows.Count > 0)
            {
                //Initialise WebChart to a variable i.e targetChart
                ComponentArt.Web.Visualization.Charting.Chart targetChart = Chart1;
                //Generate GUID
                string gId = System.Guid.NewGuid().ToString();
                //Place the charts in a specified path with Guid.
                //targetChart.ImageOutputDirectory = Server.MapPath("TempImgs");
                targetChart.CustomImageFileName = "Chart_" + gId + ".png";

                //Clear the series of a chart.
                targetChart.Series.Clear();

                //Create Series.
                Series s = new Series("S0");
                s.RemoveParentNameFromLabel = true;
                targetChart.Series.Add(s);
                s.Depth = 4;

                //Define X-Axis.
                if (drpRows.SelectedItem.Text == "Country")
                    targetChart.DefineValue("x", dt.Columns["Country"]);
                if (drpRows.SelectedItem.Text == "Mode")
                    targetChart.DefineValue("x", dt.Columns["Mode"]);
                if (drpRows.SelectedItem.Text == "Market")
                   targetChart.DefineValue("x",  dt.Columns["Market"]);
                if (drpRows.SelectedItem.Text == "Vertical")
                    targetChart.DefineValue("x",  dt.Columns["Vertical"]);
                if (drpRows.SelectedItem.Text == "Provider")
                   targetChart.DefineValue("x", dt.Columns["ByProvider"]);
                if (drpRows.SelectedItem.Text == "Route")
                    targetChart.DefineValue("x", dt.Columns["Route"]);                
                if (drpRows.SelectedItem.Text == "Units")
                   targetChart.DefineValue("x",  dt.Columns["Units"]);
                if (drpRows.SelectedItem.Text == "Currency")
                  targetChart.DefineValue("x",  dt.Columns["Currency"]);
                if (drpRows.SelectedItem.Text == "Destination")
                  targetChart.DefineValue("x", dt.Columns["Destination"]);

                //Define Y-Axis.          
                targetChart.DefineValue("y", dt.Columns["value"]);               
                
                //Define Series
                if (drpSeries.SelectedItem.Text == "Country")
                   targetChart.DefineValue("series", dt.Columns["Country"]);
                if (drpSeries.SelectedItem.Text == "Mode")
                    targetChart.DefineValue("series",dt.Columns["Mode"]);
                if (drpSeries.SelectedItem.Text == "Market")
                   targetChart.DefineValue("series",dt.Columns["Market"]);
                if (drpSeries.SelectedItem.Text == "Vertical")
                   targetChart.DefineValue("series", dt.Columns["Vertical"]);
                if (drpSeries.SelectedItem.Text == "Provider")
                   targetChart.DefineValue("series", dt.Columns["ByProvider"]);
                if (drpSeries.SelectedItem.Text == "Route")
                   targetChart.DefineValue("series", dt.Columns["Route"]);
                if (drpSeries.SelectedItem.Text == "Units")
                   targetChart.DefineValue("series",  dt.Columns["Units"]);
                if (drpSeries.SelectedItem.Text == "Currency")
                   targetChart.DefineValue("series", dt.Columns["Currency"]);

               targetChart.ResizeMarginsToFitLabels = false;

               targetChart.Width = Unit.Pixel(800);
               targetChart.Height = Unit.Pixel(450);
               targetChart.View.NativeSize = new System.Drawing.Size(Convert.ToInt32(targetChart.Width.Value), Convert.ToInt32(targetChart.Height.Value));

               targetChart.MainStyle = "Block";// "Area2D";Block
               targetChart.CompositionKind = CompositionKind.Merged;
               
               //Show the chart in 2D.                                                              
               targetChart.View.Kind = ProjectionKind.TwoDimensional;
               targetChart.View.LightsOffOn2D = true;

               //Setting chart label's font.                        
               targetChart.LabelStyles["DefaultAxisLabels"].Font = new System.Drawing.Font("Arial", 10);
               targetChart.LabelStyles["DefaultAxisLabels"].ForeColor = System.Drawing.Color.Black;

               //Create Annotations Font.
               LabelStyle ls = targetChart.LabelStyles.CreateFrom("AnnFont", LabelStyleKind.Default);
               ls.Font = new System.Drawing.Font("Arial", 9);
               ls.ForeColor = Color.Black;

               targetChart.Legend.Visible = true;

               targetChart.GeometricEngineKind = GeometricEngineKind.HighSpeedRendering;

               //Setting chart label's font.                        
               targetChart.LabelStyles["DefaultAxisLabels"].Font = new System.Drawing.Font("Arial", 10);
               targetChart.LabelStyles["DefaultAxisLabels"].ForeColor = System.Drawing.Color.Black;

               //Databinding to chart.
               targetChart.DataBind();

               //To clear Gridlines,Stripsets.
               targetChart.CoordinateSystem.PlaneXY.StripSets.Clear();
               targetChart.CoordinateSystem.PlaneXY.Grids.Clear();

               //Setting backcolor, backGradientEndingColor of a chart.
               targetChart.BackColor = Color.White;
               targetChart.BackGradientEndingColor = System.Drawing.Color.White;
               //Setting chart margins.
               targetChart.View.Margins.Left = 18;
               targetChart.View.Margins.Right = 10;
               targetChart.View.Margins.Top = 1;
               targetChart.View.Margins.Bottom = 24;

               //Define Palette
               ComponentArt.Web.Visualization.Charting.Palette CorpColorPalette = buildPalette();
               targetChart.Palettes.Add(CorpColorPalette);
               targetChart.SelectedPaletteName = "CorpPalette";

               //Setting LEGEND styles
               SetStandardLegendStyles(targetChart);

               //Create xAnnotation i.e x-Axis.
               AxisAnnotation xAnnotation = targetChart.CoordinateSystem.XAxis.AxisAnnotations["X@Ymin"];
               xAnnotation.LabelStyleName = "AnnFont";
               xAnnotation.RotationAngle = 25.0;
               xAnnotation.AxisTitleOffsetPts = 60.0;
               //xAnnotation.AxisTitlePositionKind = AxisTitlePositionKind.AtMiddlePoint;
               xAnnotation.AxisTitle = drpRows.SelectedItem.Text;

               //Create yAnnotation i.e Y-Axis
               AxisAnnotation yAnnotation = targetChart.CoordinateSystem.YAxis.AxisAnnotations["Y@Xmin"];
               yAnnotation.LabelStyleName = "AnnFont";
               yAnnotation.AxisTitleOffsetPts = 60.0;
               yAnnotation.AxisTitle = drpColumns.SelectedItem.Text;              
            }
        }

        /// <summary>
        /// Build Color Palette.
        /// </summary>
        /// <returns></returns>
        public Palette buildPalette()
        {
            ComponentArt.Web.Visualization.Charting.Palette CorpColorPalette = new ComponentArt.Web.Visualization.Charting.Palette();
            CorpColorPalette.Name = "CorpPalette";
            CorpColorPalette.PrimaryColors = ComponentArt.Web.Visualization.Charting.ChartColorCollection.FromString("002252,6c80bf,a5aecc,d2d9e5,d84519,ff6600,ffac70,fadec7,ff9900,ffcc00");
            return CorpColorPalette;
        }

        /// <summary>
        /// Method for legend styles
        /// </summary>
        /// <param name="objChart">chart object</param>
        public void SetStandardLegendStyles(ComponentArt.Web.Visualization.Charting.Chart objChart)
        {
            System.Drawing.Font f = new System.Drawing.Font("Arial", 8);           
            //Set TitleBox
            objChart.Legend.BackColor = Color.White;
            //To set Border color of legend.
            objChart.Legend.BorderColor = Color.White;
            objChart.Legend.BorderWidth = 0;
            objChart.Legend.BorderShadeWidth = 0;
            //To set font for legend.
            objChart.Legend.Font = f;
            objChart.Legend.FontColor = Color.Black;
            //To set layout,position
            objChart.Legend.LegendLayout = LegendKind.Column;
            objChart.Legend.LegendPosition = LegendPositionKind.CenterRight;
            objChart.Legend.SharesChartArea = false;
            objChart.Legend.ItemBorderShadeWidth = 0;         
        }
    }
}
