<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/Logistics.Master" 
        AutoEventWireup="true" 
        CodeBehind="Filter.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Logistics.Analysis.Filter" 
        Title="Datamonitor PremiumTools - Logistics, Analyse results"%>
<%@ Register TagPrefix="ComponentArt" 
        Namespace="ComponentArt.Web.Visualization.Charting" 
        Assembly="ComponentArt.Web.Visualization.Charting" %>  
<%@ Register Assembly="dotnetCHARTING" 
        Namespace="dotnetCHARTING" 
        TagPrefix="dotnetCHARTING" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
        
<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">
<script type="text/javascript">
function TreeView1_onNodeSelect(sender, eventArgs)
    {
        FilterBox1.set_text(eventArgs.get_node().get_text());
        FilterBox1.collapse();
    }

</script>
<div id="maincontent_full">
<div id="pagetitle">
       <table width="100%;" style="height:25">
            <tr>
                <td style="width:50%;"> 
                    <h1>Logistics & Express</h1>
                </td>
                <td align="right" style="width:50%;">
                    <a href="#" class="other_links" style="font-size:10px; font-family:verdana; font-weight:lighter;">Ask an analyst a question</a>
                </td>
            </tr>
      </table>
    </div>
    <ul id="productscan_tabs">
        <li><asp:HyperLink ID="SearchLink" runat="server" NavigateUrl="~/Search.aspx" Text="Search"></asp:HyperLink></li>
        <li><asp:HyperLink ID="ViewResultsLink" runat="server" NavigateUrl="~/Results/Default.aspx" Text="View Results"></asp:HyperLink></li>
        <li><a class="selected">Analyze Results</a></li>
    </ul> 
    <div id="column_other_chartnav">
        <div id="navSection" class="refinesearch">           
            <h2>Analytical options </h2>
            <ul>
              <li class="selected"><a href="#"> XY Chart</a></li>
              <li><a href="#">Dual Y-Axis Chart</a></li>
              <li><a href="#">Pie Chart</a></li>
              <li><a href="#">Bars(time Series)+Pie</a></li>              
              <li><a href="#">Bubble Chart</a></li>
              <li><a href="#">Growth Comparison</a></li>
              <li><a href="#">Heat Map</a></li>
              <li><a href="#">Build Custom Chart</a></li>
            </ul>  
        </div>
    </div>
    <div id="maincontent_chart" class="other_styles">
            <h2>X-Y Chart Title</h2>
          <div class="analyzeresults_col1">
               <div style="margin-bottom:10px;"> X Axis 
                    <asp:DropDownList ID="xAxisDropdown" runat="server" Width="110px" Font-Size="11px">
                    </asp:DropDownList>
                    &nbsp;&nbsp;&nbsp;&nbsp;Y Axis 
                    <asp:DropDownList ID="YAxisDropDown" runat="server" Width="110px" Font-Size="11px">
                    </asp:DropDownList>            
                   &nbsp;&nbsp;&nbsp;&nbsp; Series type
                     <asp:DropDownList ID="SeriesDropDown" runat="server" Width="110px" Font-Size="11px">
                    </asp:DropDownList>
                </div>
                   <div class="iReport_figure">

                    <ul class="iReport_extract">
<%--                    <li class="iReport_extract_nolink">
						                    Figure 1</li>--%>
                    <li class="iReport_extract_nolink">Extract to...</li>
                    <li><a href="http://www.datamonitor.com/kc/officeExport/OfficeExport.aspx?pid=CSCM0284&amp;sid=d0e32&amp;iid=d0e130&amp;type=CompanyReport&amp;ExtractType=FigureExtract&amp;app=word&amp;site=consumer "><img width="12" height="10" border="0" src="../assets/images/tool_word.gif">Word
						                    </a></li>
                    <li><a href="http://www.datamonitor.com/kc/officeExport/OfficeExport.aspx?pid=CSCM0284&amp;sid=d0e32&amp;iid=d0e130&amp;type=CompanyReport&amp;ExtractType=FigureExtract&amp;app=ppt&amp;site=consumer"><img width="12" height="10" border="0" src="../assets/images/tool_excel.gif">Excel
						                    </a></li>
                    <li><a href="http://www.datamonitor.com/kc/officeExport/OfficeExport.aspx?pid=CSCM0284&amp;sid=d0e32&amp;iid=d0e130&amp;type=CompanyReport&amp;ExtractType=FigureExtract&amp;app=ppt&amp;site=consumer"><img width="12" height="10" border="0" src="../assets/images/tool_powerpoint.gif">PowerPoint
						                    </a></li>						                    
                    </ul>
                </div>
                <ComponentArt:ComboBox id="FilterBox1" runat="server"
                      KeyboardEnabled="false"
                      AutoFilter="false"
                      AutoHighlight="false"
                      AutoComplete="false"
                      CssClass="comboBox"                                            
                      TextBoxCssClass="comboTextBox"
                      DropDownCssClass="comboDropDown"
                      ItemCssClass="comboItem"                      
                      SelectedItemCssClass="comboItemHover"
                      DropHoverImageUrl="../assets/images/drop_hover.gif"
                      DropImageUrl="../assets/images/drop.gif"
                      Width="100"
                      DropDownHeight="297"
                      DropDownWidth="250"
                      Visible="false"
                      >
                      <DropdownContent>
                          <ComponentArt:TreeView id="TreeView1" Height="297" Width="250"
                            DragAndDropEnabled="false"
                            NodeEditingEnabled="false"
                            KeyboardEnabled="true"
                            CssClass="TreeView"
                            NodeCssClass="TreeNode"                                                        
                            NodeEditCssClass="NodeEdit"
                            SelectedNodeCssClass="NodeSelected"
                            LineImageWidth="19"
                            LineImageHeight="20"
                            DefaultImageWidth="16"
                            DefaultImageHeight="16"
                            ItemSpacing="0"
                            NodeLabelPadding="3"
                            ImagesBaseUrl="../assets/tvlines/"
                            LineImagesFolderUrl="../assets/images/tvlines/"
                            ShowLines="true"
                            EnableViewState="false"                           
                            runat="server" >
                          <ClientEvents>
                            <NodeSelect EventHandler="TreeView1_onNodeSelect" />
                          </ClientEvents>
                          </ComponentArt:TreeView>
                      </DropdownContent>
            </ComponentArt:ComboBox>
        <ComponentArt:Chart id="AnalysisChart" runat="server" 
            RenderingPrecision="0.1"            
            SaveImageOnDisk="False" BackColor="White" >
        </ComponentArt:Chart>
        
        <dotnetCHARTING:Chart ID="TargetChart" runat="server" Visible="false">                    
             
        </dotnetCHARTING:Chart>
    
    </div>
            
            <div class="analyzeresults_col2" id="analyzeresults_filter">
            <h1>FILTER OPTIONS</h1>
            <div id="FilterType1"> 
                <div style="float:right;margin-bottom:4px;"><asp:DropDownList ID="CountryDropDown" runat="server"
                    width="120px" Font-Size="11px" ></asp:DropDownList></div>
                <div style="width:60px;">Country:  </div>
              

           </div>
            <div id="FilterType2">
                 <div style="float:right;margin-bottom:4px;">
                <asp:DropDownList ID="ModeDropDown" runat="server" Width="120px" Font-Size="11px">
                </asp:DropDownList></div>
                <div style="width:60px;">Mode: </div>
            </div>
            <div id="FilterType3">
                <div style="float:right;margin-bottom:4px;">
                    <asp:DropDownList ID="MarketDropdown" runat="server" Width="120px" Font-Size="11px">
                    </asp:DropDownList>             
                 </div>
                <div style="width:60px;">Market:</div> 
            </div>
            <div id="FilterType8">
                <div style="float:right;margin-bottom:4px;">
                    <asp:DropDownList ID="UnitsDropDown" runat="server" Width="120px" Font-Size="11px"></asp:DropDownList>
                </div>
                <div style="width:60px;">Units: </div>
            </div>  
           
          
            <div id="FilterType10">
                <div style="float:right;margin-bottom:4px;">                    
              <asp:DropDownList ID="DestinationDropDown" runat="server" Width="120px" Font-Size="11px" >
                </asp:DropDownList></div>
                 <div style="width:60px;">Destination </div>
            </div>
            <div id="FilterTypeYear">
                <div style="float:right;margin-bottom:4px;">                    
                    <asp:DropDownList ID="YearDropDown" runat="server" Width="120px" Font-Size="11px">
                    </asp:DropDownList></div>
                 <div style="width:60px;">Year</div>
            </div>
            <div id="FilterChartType">
                <div style="float:right;margin-bottom:4px;">                    
                <asp:DropDownList ID="ChartTypeDropDown" runat="server" Width="120px" Font-Size="11px">
                     <asp:ListItem Text="2D" Value="2D" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="3D" Value="3D"></asp:ListItem>
                </asp:DropDownList></div>
                <div style="width:60px;">Chart&nbsp;Type: </div>
            </div>
            
            <div class="button_right_chart" style="width:120px">
                <asp:LinkButton ID="PlotChart"  runat="server" Text="Plot chart" OnClick="PlotChart_Click">
                </asp:LinkButton>
                </div>
                <br /><br /><br /><br />
                <h1>MY TOOLS</h1>
                <h2 class="tool_excel"><a href="#">Extract to Excel</a></h2>
                <h2 class="tool_word"><a href="#">Extract to Word</a></h2>
                <h2 class="tool_powerpoint"><a href="#">Extract to Powerpoint</a></h2>
    </div>
    </div>
</div>
</asp:Content>
