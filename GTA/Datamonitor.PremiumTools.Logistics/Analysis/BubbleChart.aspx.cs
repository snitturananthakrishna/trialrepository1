using System;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using System.Drawing;
using System.IO;
using Aspose.Cells;
using Aspose.Slides;
using Aspose.Words;
using CArt = ComponentArt.Web.UI;

namespace Datamonitor.PremiumTools.Generic.Analysis
{
    public partial class BubbleChart : AuthenticationBasePage
    {
        //PRIVATE MEMBERS
        private string AxisCriteria = string.Empty;

        // EVENT OVERRIDES
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack &&
                 !ChartFilterCallback.IsCallback &&
                 !ChartsCallback.IsCallback)
            {
                //Bind the axis selection dropdowns.
                BindAxisDropdownsData();
                //Bind the chart type dropdown.
                BindChartTypeDropdownData();
                SelectDefaultchartType(); 

                seriesDropdown.Attributes.Add("onchange", "javascript:Series_onChange();");
                xAxisDropdown.Attributes.Add("onchange", "javascript:XAxis_onChange(true);");
                YAxisDropDown.Attributes.Add("onchange", "javascript:YAxis_onChange(true);");
                BubbleDropDown.Attributes.Add("onchange", "javascript:Bubble_onChange(true);");
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Analysis";

            if (!ChartFilterCallback.IsCallback &&
                !ChartsCallback.IsCallback &&
                !IsPostBack)
            {
                ChartFilters1.YAxis = Convert.ToString(System.Math.Abs(Convert.ToInt32(hdnXAxis.Value))) +
                        "," +
                        Convert.ToString(System.Math.Abs(Convert.ToInt32(hdnYAxis.Value))) +
                        "," +
                        Convert.ToString(System.Math.Abs(Convert.ToInt32(hdnBubble.Value)));                      

                ChartFilters1.LoadData();
            }                                 
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!IsPostBack && !ChartsCallback.IsCallback)
            {
                PlotChart_Click(null, null);
            }
        }

        // PRIVATE METHODS (initial population)

        private void BindChartTypeDropdownData()
        {
            CArt.TreeViewNode newNode;

            newNode = new CArt.TreeViewNode();
            newNode.Text = "2D";
            newNode.Value = "2D";
            ChartTypesTreeView.Nodes.Add(newNode);

            //newNode = new CArt.TreeViewNode();
            //newNode.Text = "3D";
            //newNode.Value = "3D";
            //ChartTypesTreeView.Nodes.Add(newNode);
        }
        
        private void SelectDefaultchartType()
        {
            CArt.TreeViewNode LeafNode = null;
            foreach (CArt.TreeViewNode tnode in ChartTypesTreeView.Nodes)
            {
                if (tnode.Nodes.Count > 0)
                {
                    LeafNode = GetFirstLeafNode(tnode);
                    break;
                }
                else
                {
                    LeafNode = tnode;
                    break;
                }
            }
            if (LeafNode != null)
            {
                ChartTypesDropDown.Text = LeafNode.Text;
                hiddenChartType.Value = LeafNode.Value;
                ChartTypesTreeView.SelectedNode = LeafNode;
            }
        }

        private CArt.TreeViewNode GetFirstLeafNode(CArt.TreeViewNode parentNode)
        {
            foreach (CArt.TreeViewNode tnode in parentNode.Nodes)
            {
                if (tnode.Nodes.Count > 0)
                {
                    return GetFirstLeafNode(tnode);
                }
                else
                {
                    return tnode;
                }
            }
            return parentNode;
        }
        
        private void BindAxisDropdownsData()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(Server.MapPath(GlobalSettings.ChartFilterOptionsConfigPath));
            string xPathExpression = "//Taxonomy[@type='Series']";
            XmlElement SeriesItem = (XmlElement)doc.SelectSingleNode(xPathExpression);         
            //Get and bind data to series drop down
            string taxonomtypes = SeriesItem.Attributes["taxonomtypes"].Value;
            string from = SeriesItem.Attributes["from"].Value;
            string includeYears = SeriesItem.Attributes["includeYears"].Value;                     
            DataSet SeriesData = new DataSet();

            if (from.Equals("taxonomtypes"))
            {
                SeriesData = AnalaysisData.GetAxisData(taxonomtypes, includeYears.Equals("true"));
            }
            else if (from.Equals("dataset"))
            {
                SeriesData = SqlDataService.GetTaxonomyData(Convert.ToInt32(taxonomtypes));
            }

            seriesDropdown.DataSource = SeriesData;
            seriesDropdown.DataTextField = SeriesData.Tables[0].Columns["Name"].ToString();
            seriesDropdown.DataValueField = SeriesData.Tables[0].Columns["ID"].ToString();
            seriesDropdown.DataBind();

            try
            {               
                seriesDropdown.SelectedValue = ConfigurationManager.AppSettings.Get("PieChartDefaultXAxisType");
                hdnSeries.Value = ConfigurationManager.AppSettings.Get("PieChartDefaultXAxisType");
                hdnSeriesText.Value = seriesDropdown.SelectedItem.Text.ToString();
            }
            catch (Exception ex) { }          

            //Get All Axis data.

            string XAxisSelectionsXML = GetFilterCriteriaFromSession();

            DataSet XAxisDataSet = SqlDataService.GetYAxisTaxonomyByCriteria(XAxisSelectionsXML, true);
            if (XAxisDataSet.Tables.Count > 0)
            {
                //Binding Y-Axis
                xAxisDropdown.DataSource = XAxisDataSet;
                xAxisDropdown.DataTextField = XAxisDataSet.Tables[0].Columns["displayName"].ToString();
                xAxisDropdown.DataValueField = XAxisDataSet.Tables[0].Columns["ID"].ToString();
                xAxisDropdown.DataBind();

                YAxisDropDown.DataSource = XAxisDataSet;
                YAxisDropDown.DataTextField = XAxisDataSet.Tables[0].Columns["displayName"].ToString();
                YAxisDropDown.DataValueField = XAxisDataSet.Tables[0].Columns["ID"].ToString();
                YAxisDropDown.DataBind();

                BubbleDropDown.DataSource = XAxisDataSet;
                BubbleDropDown.DataTextField = XAxisDataSet.Tables[0].Columns["displayName"].ToString();
                BubbleDropDown.DataValueField = XAxisDataSet.Tables[0].Columns["ID"].ToString();
                BubbleDropDown.DataBind();

                try
                {
                    xAxisDropdown.SelectedValue = GlobalSettings.DefaultYAxisType;
                    YAxisDropDown.SelectedIndex = YAxisDropDown.Items.Count > 1 ? 1 : 0;
                    BubbleDropDown.SelectedIndex = BubbleDropDown.Items.Count > 2 ? 2 : 0;

                    hdnXAxis.Value = xAxisDropdown.SelectedValue;
                    hdnYAxis.Value = YAxisDropDown.Items[YAxisDropDown.SelectedIndex].Value;
                    hdnBubble.Value = BubbleDropDown.Items[BubbleDropDown.SelectedIndex].Value;
                    hdnXAxisText.Value = xAxisDropdown.SelectedItem.Text.ToString();
                    hdnYAxisText.Value = YAxisDropDown.SelectedItem.Text.ToString();
                    hdnBubbleText.Value = BubbleDropDown.SelectedItem.Text.ToString();

                } 
                catch (Exception ex) { }
            }
        }

        
        // METHODS (Build selection criteria into XML)

        private string GetCallbackDataFilterXML(string series, string filterValues,string filterValuesText)
        {
            string SelectionsXML = string.Empty;
            string SelectionsTextXML = string.Empty;

            string[] taxonomyFilters = filterValues.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            string[] taxonomyFiltersText = filterValuesText.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < taxonomyFilters.Length; i++)
            {
                string taxonomyID = taxonomyFilters[i].Split('|')[0];
                string taxonomyName = taxonomyFiltersText[i].Split('|')[0];
                string taxonomyRowID = GetColumnRowIDById(taxonomyID);
                string taxonomyValues = taxonomyID.Equals(series) ?
                    taxonomyFilters[i].Split('|')[2] :
                    taxonomyFilters[i].Split('|')[1];

                string taxonomyValuesText = taxonomyID.Equals(series) ?
                 taxonomyFiltersText[i].Split('|')[2] :
                 taxonomyFiltersText[i].Split('|')[1];

                SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                                    taxonomyID,
                                    taxonomyValues.TrimEnd(new char[] { ',' }),
                                    taxonomyRowID);

                if (taxonomyID == "999")
                {
                    SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                                    taxonomyName,
                                    taxonomyValues.TrimEnd(new char[] { ',' }));
                }
                else
                {
                    SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}'/>",
                         taxonomyName,
                         taxonomyValuesText.TrimEnd(new char[] { ',' }));
                }
            }
          

            SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);
            Session["SelectionsTextXML"] = SelectionsTextXML.ToString();
            return SelectionsXML;
        }

        private string GetDataFilterXML()
        {
            string SelectionsXML = string.Empty;
            string SelectionsTextXML = string.Empty;
            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {
                    string TaxonomySelectedValue = string.Empty;
                    string TaxonomySelectedText = string.Empty;

                    if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE") && !taxonomyRow["ID"].ToString().Equals("999"))
                    {
                        //Get the x axis, series taxonomies filter data from session
                        if (seriesDropdown.SelectedValue.Equals(taxonomyRow["ID"].ToString()))
                        {
                            Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                            if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                            {
                                //Get all keys (ids) from dictionary
                                List<int> keys = new List<int>(selectedIDsList.Keys);
                                //convert int array to string array
                                string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                                TaxonomySelectedValue = string.Join(",", SelectedIDs);
                            }
                            //Get selected taxonomy id from user control's hidden field
                            if (ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection") != null)
                            {
                                TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection")).Value;
                                TaxonomySelectedValue = TaxonomySelectedValue.TrimEnd(new char[] { ',' });
                            }
                            if (ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText") != null)
                            {
                                TaxonomySelectedText = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText")).Value;
                                TaxonomySelectedText = TaxonomySelectedText.TrimEnd(new char[] { ',' });
                            }
                        }
                        else
                        {
                            //Get selected taxonomy id from user control's hidden field
                            if (ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection") != null)
                            {
                                TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection")).Value;
                            }
                            if (ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText") != null)
                            {
                                TaxonomySelectedText = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText")).Value;
                            }
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                              taxonomyRow["ID"].ToString(),
                              TaxonomySelectedValue,
                              taxonomyRow["ColumnRowID"].ToString());
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedText))
                        {
                            if (TaxonomySelectedText.Contains("'"))
                            {
                                TaxonomySelectedText = TaxonomySelectedText.Replace("'", "&apos;");
                            }
                            SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                               taxonomyRow["Name"].ToString(),
                               TaxonomySelectedText);
                        }
                    }
                    else if (taxonomyRow["ID"].ToString().Equals("999"))
                    {
                        if (seriesDropdown.SelectedValue.Equals(taxonomyRow["ID"].ToString()))
                        {
                            //Year is selected as x-axis or series
                            //Get the years filter data from session
                            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
                            int EndYear = CurrentSession.GetFromSession<int>("EndYear");

                            TaxonomySelectedValue = StartYear.ToString();
                            while (EndYear > StartYear)
                            {
                                StartYear++;
                                TaxonomySelectedValue += "," + StartYear;
                            }
                        }
                        else
                        {
                            //Get selected year value from user control's hidden field
                            TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy999Selection")).Value;

                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                               taxonomyRow["ID"].ToString(),
                               TaxonomySelectedValue,
                               taxonomyRow["ColumnRowID"].ToString());
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}'/>",
                               taxonomyRow["Name"].ToString(),
                               TaxonomySelectedValue);
                        }
                    }
                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
                SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);
                Session["SelectionsTextXML"] = SelectionsTextXML.ToString();
            }
            return SelectionsXML;
        }

        
        // EVENT HANDLERS      

        protected void PlotChart_Click(object sender, EventArgs e)
        {
            AxisCriteria = "Series: "+ seriesDropdown.SelectedItem.Text.ToString() +
                           "; X Axis: " + xAxisDropdown.SelectedItem.Text.ToString() +
                           "; Y Axis: " + YAxisDropDown.SelectedItem.Text.ToString() +
                           "; Bubble: " + BubbleDropDown.SelectedItem.Text.ToString();
            //Get selection criteria   
            string SelectionsXML = GetDataFilterXML();
            
            DataSet ChartDataset = new DataSet();

            LogUsage("Chart Results - Bubble Chart", "Bubble Chart1", SelectionsXML);

            //Get chart data 
            ChartDataset = SqlDataService.GetBubbleChartData(Int32.Parse(seriesDropdown.SelectedValue),
               Int32.Parse(xAxisDropdown.SelectedValue),
               Int32.Parse(YAxisDropDown.SelectedValue),
               Int32.Parse(BubbleDropDown.SelectedValue),
               SelectionsXML);


            ViewState["ChartDataset"] = ChartDataset;

            //Plot chart
            if (ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                string ChartOptions = hiddenChartType.Value;
                string ChartType = "2D";
                string ChartSeriesType = "Bar";

                if (!string.IsNullOrEmpty(ChartOptions))
                {
                    ChartType = ChartOptions.Split('|')[0];
                }

                if (ChartType == "3D")
                {
                    ComponentArt.Web.Visualization.Charting.Chart targetChart;
                    targetChart = ThreeDimensionalCharts.PlotBubbleChart(ChartDataset, ThreeDChart, ChartSeriesType, seriesDropdown.SelectedItem.Text, YAxisDropDown.SelectedItem.Text);
                    if (targetChart != null)
                    {
                        TwoDChart.Visible = false;
                        targetChart.Visible = true;
                        //Save the chart in viewstate, used in extractions.
                        ViewState["3DChartImage"] = targetChart.ImageOutputDirectory.ToString();
                        //Save the Chart GUID in viewstate, used in extractions.
                        ViewState["ChartID"] = targetChart.CustomImageFileName.ToString();
                        ViewState["ChartType"] = "3D";

                        hdnImageurl.Value = targetChart.ImageOutputDirectory.ToString();
                        hdnChartID.Value = targetChart.CustomImageFileName.ToString();
                        hdnImageType.Value = "3D";
                    }
                }
                else
                {
                    dotnetCHARTING.Chart targetChart;
                    targetChart = TwoDimensionalCharts.PlotBubbleChart(ChartDataset, 
                                  TwoDChart, 
                                  ChartSeriesType, 
                                  xAxisDropdown.SelectedItem.Text, 
                                  YAxisDropDown.SelectedItem.Text, 
                                  BubbleDropDown.SelectedItem.Text,
                                  AxisCriteria);
                    targetChart.Visible = false;
                    ThreeDChart.Visible = false;
                    TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                    if (!Directory.Exists(Server.MapPath("temp")))
                        Directory.CreateDirectory(Server.MapPath("temp"));

                    targetChart.FileManager.TempDirectory = "temp";
                    // Save the legend as a separate image file
                    targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.BottomMiddle;
                    if (targetChart.SeriesCollection.Count > 3)
                    {
                        targetChart.Height = Unit.Point(150 + ((targetChart.SeriesCollection.Count - 3) * 30));
                    }
                    LegendImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");
                    LegendImg.Visible = string.IsNullOrEmpty(LegendImg.ImageUrl.Trim()) ? false : true;
                    targetChart.Width = Unit.Point(520);
                    targetChart.Height = Unit.Point(300);
                    // Remove the legend from the chart and save the chart as a separate image file
                    targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.None;
                    ChartImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");
                    ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                    ImageMapLabel.Text = targetChart.ImageMapText;

                    //Save the chart in viewstate, used in extractions.
                    ViewState["2DChartImage"] = ChartImg.ImageUrl;
                    ViewState["ChartType"] = "2D";

                    hdnImageurl.Value = ChartImg.ImageUrl;
                    hdnImageType.Value = "2D";
                    hdnLegendUrl.Value = LegendImg.ImageUrl;
                }
            }
        }
        
        // CALLBACK EVENT HANDLERS

        protected void Chart_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            AxisCriteria = "Series: " + e.Parameters[6].ToString() +
                           "; X Axis: " + e.Parameters[7].ToString().ToString() +
                           "; Y Axis: " + e.Parameters[8].ToString().ToString() +
                           "; Bubble: " + e.Parameters[9].ToString().ToString();

            string FilterXML = GetCallbackDataFilterXML(e.Parameters[0].ToString(), e.Parameters[4].ToString(),e.Parameters[10].ToString());

            //Get chart data
            DataSet ChartDataset = SqlDataService.GetBubbleChartData(Int32.Parse(e.Parameters[0].ToString()),
               Int32.Parse(e.Parameters[1].ToString()),
               Int32.Parse(e.Parameters[2].ToString()),
               Int32.Parse(e.Parameters[3].ToString()),
               FilterXML);

            //Plot chart
            if (ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                string ChartOptions = e.Parameters[5].ToString();
                string ChartType = "2D";
                string ChartSeriesType = "Bar";

                string XAxisLabel = ChartDataset.Tables[0].Rows[0]["XUnit"].ToString();
                string YAxisLabel = ChartDataset.Tables[0].Rows[0]["YUnit"].ToString();
                string BubbleLabel = ChartDataset.Tables[0].Rows[0]["BubbleUnit"].ToString();

                if (!string.IsNullOrEmpty(ChartOptions))
                {
                    ChartType = ChartOptions.Split('|')[0];
                }

                if (ChartType == "3D")
                {
                    ComponentArt.Web.Visualization.Charting.Chart targetChart;

                    targetChart = ThreeDimensionalCharts.PlotBubbleChart(ChartDataset,
                        ThreeDChart,
                        ChartSeriesType,
                        XAxisLabel,
                        YAxisLabel);

                    TwoDChart.Visible = false;
                    targetChart.Visible = true;
                    targetChart.RenderControl(e.Output);
                }
                else
                {
                    dotnetCHARTING.Chart targetChart;
                    targetChart = TwoDimensionalCharts.PlotBubbleChart(ChartDataset,
                        TwoDChart,
                        ChartSeriesType,
                        XAxisLabel,
                        YAxisLabel,
                        BubbleLabel,
                        AxisCriteria);

                    targetChart.Visible = false;
                    ThreeDChart.Visible = false;
                    TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                    if (!Directory.Exists(Server.MapPath("temp")))
                    {
                        Directory.CreateDirectory(Server.MapPath("temp"));
                    }

                    targetChart.FileManager.TempDirectory = "temp";
                    string strLegendPath = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");

                    targetChart.Width = Unit.Point(520);
                    targetChart.Height = Unit.Point(300);
                    // Remove the legend from the chart and save the chart as a separate image file
                    targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.None;
                    string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");

                    ChartImg.ImageUrl = ChartImg.ResolveUrl(strChartPath);
                    ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                    ImageMapLabel.Text = targetChart.ImageMapText;
                    ChartImg.RenderControl(e.Output);
                    hdnImageurl.Value = strChartPath;
                    hdnImageurl.RenderControl(e.Output);
                    hdnImageType.Value = ChartType;
                    hdnImageType.RenderControl(e.Output);
                    ImageMapLabel.RenderControl(e.Output);

                    LegendImg.ImageUrl = ChartImg.ResolveUrl(strLegendPath);
                    LegendImg.Visible = string.IsNullOrEmpty(LegendImg.ImageUrl.Trim()) ? false : true;
                    divChart.RenderControl(e.Output);
                    hdnLegendUrl.Value = strLegendPath;
                    hdnLegendUrl.RenderControl(e.Output);     
                }
            }
            else
            {
                ErrorMessagelabel.Text = "Data not available";
            }
            ErrorMessagelabel.RenderControl(e.Output);
        }

        public void UpdateChartfilters_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            string FilterData = e.Parameters[3];
            string FilterDataText = e.Parameters[4];

            ChartFilters1.YAxis = Convert.ToString(System.Math.Abs(Convert.ToInt32(e.Parameters[0]))) +
                        "," +
                        Convert.ToString(System.Math.Abs(Convert.ToInt32(e.Parameters[1]))) +
                        "," +
                        Convert.ToString(System.Math.Abs(Convert.ToInt32(e.Parameters[2])));

            ChartFilters1.LoadDataInCallback(FilterData, FilterDataText);
            ChartFilters1.RenderControl(e.Output);
        }
       
        
        // EXPORT EVENT HANDLERS      
       
        protected void lnkWord_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string chartTitle = GlobalSettings.SiteTitle;

            string SelectionsXML = GetDataFilterXML();

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetBubbleChartData(Int32.Parse(hdnSeries.Value),
                Int32.Parse(hdnXAxis.Value),
                Int32.Parse(hdnYAxis.Value),
                Int32.Parse(hdnBubble.Value),
                SelectionsXML);

            LogUsage("Bubble Chart - Word Download ", "Bubbble Chart1"," ");

            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("2D"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }                 

            if (chartBitmap != null)
            {
                Document doc = ExtractionsHelper.ExportToWord(ChartDataset.Tables[0],
                            chartBitmap,
                            "BubbleChart",
                            WordTemplatePath, 
                            ChartTitle.Text);
                doc.Save("OvumExtract.doc", SaveFormat.FormatDocument, Aspose.Words.SaveType.OpenInWord, Response);
            }
        }
        
        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;           
            Bitmap chartBitmap=null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;

            string chartTitle = GlobalSettings.SiteTitle;
            string chartType = "BubbleChart";
            string SelectionsXML = GetDataFilterXML();

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetBubbleChartData(Int32.Parse(hdnSeries.Value),
                Int32.Parse(hdnXAxis.Value),
                Int32.Parse(hdnYAxis.Value),
                Int32.Parse(hdnBubble.Value),
                SelectionsXML);
            LogUsage("Bubble Chart - Excel Download ", "Bubbble Chart1"," ");

            string ChartType = string.Empty;
            
            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("2D"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }   

            Workbook Excel;
            if (chartBitmap != null)
            {                
                Excel = ExtractionsHelper.ExportToExcel(ChartDataset.Tables[0], 
                                chartBitmap, 
                                legendBitmap,
                                chartType,
                                ExcelTemplatePath, 
                                LogoPath,
                                ChartTitle.Text);
                Excel.Save("Ovum" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);
            }
        }
        
        protected void lnkPPT_Click(object sender, EventArgs e)
        {
            string strExtractName = "OvumExtract";
            string chartImagePath = string.Empty;        
            Bitmap chartBitmap = null;
            string chartTitle = GlobalSettings.SiteTitle;

            string SelectionsXML = GetDataFilterXML();
            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetBubbleChartData(Int32.Parse(hdnSeries.Value),
                Int32.Parse(hdnXAxis.Value),
                Int32.Parse(hdnYAxis.Value),
                Int32.Parse(hdnBubble.Value),
                SelectionsXML);
            LogUsage("Bubble Chart - PPT Download ", "Bubbble Chart1"," ");

            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("2D"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }                 

            if (chartBitmap != null)
            {
                Presentation presentation = ExtractionsHelper.ExportToPowerpoint(ChartDataset.Tables[0],
                    chartBitmap,
                    "BubbleChart",  
                    PowerpointTempaltePath,
                    ChartTitle.Text);
                Response.ContentType = "application/vnd.ms-powerpoint";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + strExtractName + ".ppt");
                presentation.Write(Response.OutputStream);
                Response.Flush();
            }
        }        
    }
}
