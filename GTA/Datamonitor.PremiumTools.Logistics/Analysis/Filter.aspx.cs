using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ComponentArt.Web.Visualization.Charting;
using Datamonitor.PremiumTools.Logistics.Library;
using Datamonitor.PremiumTools.Logistics.Library.SqlDataAccess;
using dotnetCHARTING;
using dotnetCHARTING.Mapping;
using dotnetCHARTING.Libraries;
using dotnetCHARTING.Designer;
using System.Drawing;

namespace Datamonitor.PremiumTools.Logistics.Analysis
{
    public partial class Filter : System.Web.UI.Page
    {
        static Color[] DMPalette = new Color[] { Color.FromArgb(0, 34, 82), 
            Color.FromArgb(108, 128, 191), 
            Color.FromArgb(165, 174, 204), 
            Color.FromArgb(210, 217, 229), 
            Color.FromArgb(216, 69, 25), 
            Color.FromArgb(255, 102, 0), 
            Color.FromArgb(255, 172, 112), 
            Color.FromArgb(250, 222, 199),
            Color.FromArgb(255, 153, 0), 
            Color.FromArgb(255, 204, 0) };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Bind the axis selection dropdowns.
                BindAxisDropdownsData();
                //Bind the taxonomy selection dropdowns.
                BindTaxonomySelectionDropdowns();

                buildTree();
            }
        }
       
        /// <summary>
        /// Binds the axis dropdowns data.
        /// </summary>
        private void BindAxisDropdownsData()
        {
            //Get X-Axis data
            DataSet XAxisDataSet = SqlDataService.GetXaxisData();
            if (XAxisDataSet.Tables.Count > 0)
            {
                //Binding X-Axis
                xAxisDropdown.DataSource = XAxisDataSet;
                xAxisDropdown.DataTextField = XAxisDataSet.Tables[0].Columns["typeName"].ToString();
                xAxisDropdown.DataValueField = XAxisDataSet.Tables[0].Columns["taxonomyTypeID"].ToString();
                xAxisDropdown.DataBind();
                //Bind Series data.
                SeriesDropDown.DataSource = XAxisDataSet;
                SeriesDropDown.DataTextField = XAxisDataSet.Tables[0].Columns["typeName"].ToString();
                SeriesDropDown.DataValueField = XAxisDataSet.Tables[0].Columns["taxonomyTypeID"].ToString();
                SeriesDropDown.DataBind();

                try
                {
                    xAxisDropdown.SelectedValue = LEGlobalSettings.DefaultXAxisType;
                    SeriesDropDown.SelectedValue = LEGlobalSettings.DefaultSeriesType;
                }
                catch(Exception ex){}

            }
            //Get Y-Axis data.
            DataSet YAxisDataSet = SqlDataService.GetYaxisData();
            if (YAxisDataSet.Tables.Count > 0)
            {
                //Binding Y-Axis
                YAxisDropDown.DataSource = YAxisDataSet;
                YAxisDropDown.DataTextField = YAxisDataSet.Tables[0].Columns["Name"].ToString();
                YAxisDropDown.DataValueField = YAxisDataSet.Tables[0].Columns["taxonomyID"].ToString();
                YAxisDropDown.DataBind();

                try
                {
                    YAxisDropDown.SelectedValue = LEGlobalSettings.DefaultYAxisType;                    
                }
                catch (Exception ex) { }
            }
        }

        /// <summary>
        /// Binds the taxonomy selection dropdowns.
        /// </summary>
        private void BindTaxonomySelectionDropdowns()
        {
            DataSet TaxonomyDataset = SqlDataService.GetDrpsData();
            if (TaxonomyDataset.Tables.Count > 0)
            {
                DataView TaxonomyView = TaxonomyDataset.Tables[0].DefaultView;                
                //Get list of taxonomies from config
                DataSet TaxonomyDefaults = LEGlobalSettings.GetTaxonomyDefaults();
                if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
                {                   
                    foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                    {
                        DropDownList CurrentDropdown = GetDropDownByTaxonomyType(Convert.ToInt32(taxonomyRow["ID"]));
                        if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE"))
                        {
                            string defaultValue = taxonomyRow["TaxonomyID"].ToString().Equals("FIRST")? null:taxonomyRow["TaxonomyID"].ToString();                           
                            //Load taxonomiesdata into dropdown list from session(filtered in search page) 
                            bool LoadedFromSession = BindTaxonomyDropDownFromSession(Convert.ToInt32(taxonomyRow["ID"]),
                                CurrentDropdown,
                                defaultValue);
                            //If a taxonomy data does not exist in session then load from the database
                            if (!LoadedFromSession)
                            {
                                BindTaxonomyDropDownFromDatabase(TaxonomyView,
                                    CurrentDropdown,
                                    Convert.ToInt32(taxonomyRow["ID"]),
                                    "taxonomyID",
                                    "displayName",
                                    taxonomyRow["TaxonomyID"].ToString());
                            }

                        }
                    }
                }

               
            }
        }

        /// <summary>
        /// Gets the drop down by taxonomy type.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>       
        private DropDownList GetDropDownByTaxonomyType(int id)
        {
            DropDownList CurrentDropdown = new DropDownList();

            switch (id)
            {
                case 1:
                    CurrentDropdown = CountryDropDown;
                    break;
                case 2:
                    CurrentDropdown = ModeDropDown;
                    break;
                case 3:
                    CurrentDropdown = MarketDropdown;
                    break;
                case 8:
                    CurrentDropdown = UnitsDropDown;
                    break;
                case 10:
                    CurrentDropdown = DestinationDropDown;
                    break;

            }
            return CurrentDropdown;
        }

        /// <summary>
        /// Binds the taxonomy drop down from session.
        /// </summary>
        /// <param name="TaxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="dropdown">The dropdown object.</param>
        /// <param name="selectedValue">The selected value.</param>
        /// <returns></returns>
        private bool BindTaxonomyDropDownFromSession(int TaxonomyTypeID,
            DropDownList dropdown,                        
            string selectedValue)
        {
            Dictionary<int, string> SessionData = CurrentSession.GetFromSession<Dictionary<int, string>>(TaxonomyTypeID.ToString());
            if (SessionData != null && SessionData.Count > 0)
            {
                dropdown.DataSource = SessionData;
                dropdown.DataTextField = "Value";
                dropdown.DataValueField = "Key";
                dropdown.DataBind();
                if (!string.IsNullOrEmpty(selectedValue) && SessionData.ContainsKey(Convert.ToInt32(selectedValue)))
                {
                    dropdown.SelectedValue = selectedValue;
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Binds the taxonomy drop down values with data from database.
        /// </summary>
        /// <param name="TaxonomyView">The taxonomy view.</param>
        /// <param name="dropdown">The dropdown.</param>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="valueColumn">The value column.</param>
        /// <param name="textColumn">The text column.</param>
        /// <param name="selectedValue">The selected value.</param>
        private void BindTaxonomyDropDownFromDatabase(DataView TaxonomyView, 
            DropDownList dropdown,
            int taxonomyTypeID, 
            string valueColumn,
            string textColumn,
            string selectedValue)
        {
            TaxonomyView.RowFilter = "taxonomyTypeId = " + taxonomyTypeID; 
            DataTable TaxonomyTable = TaxonomyView.ToTable();
            dropdown.DataSource = TaxonomyTable;
            dropdown.DataTextField = textColumn;
            dropdown.DataValueField = valueColumn;            
            dropdown.DataBind();
            if (!string.IsNullOrEmpty(selectedValue))
            {
                try
                {
                    dropdown.SelectedValue = selectedValue;
                }
                catch (Exception ex) {}
            }
        }

        /// <summary>
        /// Handles the Click event of the PlotChart control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PlotChart_Click(object sender, EventArgs e)
        {
            //Get selection criteria   
            string SelectionsXML = GetDataFilterXML();
            bool PlotTypeIs3D = false;
            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetAnalysisData(Int32.Parse(xAxisDropdown.SelectedValue),
                Int32.Parse(YAxisDropDown.SelectedValue),
                Int32.Parse(SeriesDropDown.SelectedValue),
                SelectionsXML,
                "2008");

            string ChartType = ChartTypeDropDown.SelectedItem.Text.ToString();
            
            if (ChartType == "3D")
               PlotTypeIs3D = true;            
            else
                PlotTypeIs3D = false;
           
            //Plot chart
            if ( ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                if (PlotTypeIs3D)
                {
                    Plot3DChart(ChartDataset); //Component Art Charts
                }
                else
                {
                    Plot2DChart(ChartDataset); //Dotnet Charts
                }

            }

        }

        /// <summary>
        /// Plots the 2 Dimensional chart using dotnet charting.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        private void Plot2DChart(DataSet chartDataset)
        {
            AnalysisChart.Visible = false;
            TargetChart.Visible = true;
            TargetChart.Use3D = false;

            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                TargetChart.FileManager.ImageFormat = ImageFormat.Png;
                TargetChart.FileQuality = 100;
                DataEngine objDataEngine = new DataEngine();
                TargetChart.SeriesCollection.Clear();

                objDataEngine.Data = chartDataset;
                dotnetCHARTING.SeriesCollection sc;

                //Set data fields
                objDataEngine.DataFields = "XAxis=" + 
                    chartDataset.Tables[0].Columns[0].ColumnName + 
                    ",YAxis=" + chartDataset.Tables[0].Columns[2].ColumnName + 
                    ",splitBy=" + chartDataset.Tables[0].Columns[1].ColumnName;

                sc = objDataEngine.GetSeries();
                TargetChart.SeriesCollection.Add(sc);
                TargetChart.DataBind();                
                
                //Set axis label texts and styles
                TargetChart.XAxis.Label.Text = chartDataset.Tables[0].Columns[0].ColumnName;
                TargetChart.YAxis.Label.Text = chartDataset.Tables[0].Columns[2].ColumnName;
                TargetChart.XAxis.DefaultTick.Label.Color = Color.Black;
                TargetChart.XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                TargetChart.YAxis.DefaultTick.Label.Color = Color.Black;
                TargetChart.YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                TargetChart.XAxis.Line.Color = Color.DarkGray;
                TargetChart.XAxis.DefaultTick.Line.Color = Color.DarkGray;
                TargetChart.YAxis.Line.Color = Color.DarkGray;
                TargetChart.YAxis.DefaultTick.Line.Color = Color.DarkGray;

                //Set chart series types
                for (int i = 0; i<TargetChart.SeriesCollection.Count; i++)
                {
                    TargetChart.SeriesCollection[i].Type = SeriesType.Bar;
                }  
                
                //Set chart styles
                TargetChart.Palette = DMPalette;
                TargetChart.Debug = false;                    
                TargetChart.Height = 300;
                TargetChart.Width = 530;
                TargetChart.ChartArea.Line.Color = Color.White;
                TargetChart.ChartArea.Shadow.Color = Color.White;
                TargetChart.XAxis.SpacingPercentage = 30;
                TargetChart.ChartArea.ClearColors();
                TargetChart.DefaultChartArea.Background.Color = Color.White;
                TargetChart.ChartArea.Line.Color = Color.LightGray;
                TargetChart.ChartArea.Shadow.Color = Color.White;               

                //Set legend properties and styles
                TargetChart.LegendBox.Template = "%Icon%Name";
                SetStandardLegendStyles(TargetChart);
            }
            
        }

        /// <summary>
        /// Plots the 3 Dimensional chart using Component Art.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        private void Plot3DChart(DataSet chartDataset)
        {
            AnalysisChart.Visible = true;
            TargetChart.Visible = false;

            AnalysisChart.Series.Clear();

            ComponentArt.Web.Visualization.Charting.Series s = new ComponentArt.Web.Visualization.Charting.Series("S0");
            s.RemoveParentNameFromLabel = true;
            AnalysisChart.Series.Add(s);
            //s.Depth = 4;

            ComponentArt.Web.Visualization.Charting.Series s1 = new ComponentArt.Web.Visualization.Charting.Series("PreSeries");
            //AnalysisChart.DefineAsExpression("PreSeries" + ":transparency", "1"); 
            //AnalysisChart.Series.Add(s1);

            AnalysisChart.DefineValue("x", chartDataset.Tables[0].Columns[0]);
            AnalysisChart.DefineValue("y", chartDataset.Tables[0].Columns[2]);
            AnalysisChart.DefineValue("series", chartDataset.Tables[0].Columns[1]);

            AnalysisChart.ResizeMarginsToFitLabels = false;

            AnalysisChart.Width = Unit.Pixel(530);
            AnalysisChart.Height = Unit.Pixel(300);
            AnalysisChart.View.NativeSize = new System.Drawing.Size(Convert.ToInt32(AnalysisChart.Width.Value), Convert.ToInt32(AnalysisChart.Height.Value));


            //Setting chart label's font.                        
            AnalysisChart.LabelStyles["DefaultAxisLabels"].Font = new System.Drawing.Font("Arial", 8);
            AnalysisChart.LabelStyles["DefaultAxisLabels"].ForeColor = System.Drawing.Color.Black;

            //Create Annotations Font.
            //LabelStyle ls = AnalysisChart.LabelStyles.CreateFrom("AnnFont", LabelStyleKind.Default);
            //ls.Font = new System.Drawing.Font("Arial", 9);
           // ls.ForeColor = Color.Black;

            //AnalysisChart.MainStyle = "Block";
            //AnalysisChart.MainStyleKind = SeriesStyleKind.Rectangle;
            //AnalysisChart.View.Kind = ProjectionKind.TwoDimensional;
            //AnalysisChart.View.LightsOffOn2D = true;
            // AnalysisChart.CompositionKind = CompositionKind.Merged;
            //AnalysisChart.SeriesStyles[AnalysisChart.MainStyle].RelativeLeftSpace = 0.01;
            //AnalysisChart.SeriesStyles[AnalysisChart.MainStyle].RelativeRightSpace = 0.01;
            //AnalysisChart.SeriesStyles[AnalysisChart.MainStyle].BorderLineStyleKind = LineStyle2DKind.AxisLine;

            AnalysisChart.DataBind();

            //Series s2 = new Series("PostSeries");
            //AnalysisChart.DefineAsExpression("PostSeries" + ":transparency", "1"); 
            //AnalysisChart.Series.Add(s2);

            //AnalysisChart.Legend.Visible = true;
            SetStandardLegendStyles(AnalysisChart);

            //Create xAnnotation i.e x-Axis.
            AxisAnnotation xAnnotation = AnalysisChart.CoordinateSystem.XAxis.AxisAnnotations["X@Zmax"];
            //xAnnotation.LabelStyleName = "AnnFont";
            //xAnnotation.RotationAngle = 30.0;
            //xAnnotation.AxisTitleOffsetPts = 20.0;
            //xAnnotation.AxisTitlePositionKind = AxisTitlePositionKind.AtMiddlePoint;
            xAnnotation.AxisTitle = xAxisDropdown.SelectedItem.Text;

            //Create yAnnotation i.e Y-Axis
            AxisAnnotation yAnnotation = AnalysisChart.CoordinateSystem.YAxis.AxisAnnotations["Y@Zmax"];
            //yAnnotation.LabelStyleName = "AnnFont";
            //yAnnotation.AxisTitleOffsetPts = 20.0;
            yAnnotation.AxisTitle = YAxisDropDown.SelectedItem.Text;

            ComponentArt.Web.Visualization.Charting.Palette CorpColorPalette = buildPalette();
            AnalysisChart.Palettes.Add(CorpColorPalette);
            AnalysisChart.SelectedPaletteName = "CorpPalette";
        }

        /// <summary>
        /// Build Color Palette.
        /// </summary>
        /// <returns></returns>
        public  ComponentArt.Web.Visualization.Charting.Palette buildPalette()
        {
            ComponentArt.Web.Visualization.Charting.Palette CorpColorPalette = new ComponentArt.Web.Visualization.Charting.Palette();
            CorpColorPalette.Name = "CorpPalette";
            CorpColorPalette.PrimaryColors = ComponentArt.Web.Visualization.Charting.ChartColorCollection.FromString("002252,6c80bf,a5aecc,d2d9e5,d84519,ff6600,ffac70,fadec7,ff9900,ffcc00");
            return CorpColorPalette;
        }

        /// <summary>
        /// Gets the data filter XML.
        /// </summary>
        /// <returns></returns>
        private string GetDataFilterXML()
        {
            string SelectionsXML = string.Empty;
            DataSet TaxonomyDefaults = LEGlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {
                
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {
                    if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE"))
                    {
                        //Get the x axis, series taxonomies filter data from session
                        if (xAxisDropdown.SelectedValue.Equals(taxonomyRow["ID"].ToString()) || SeriesDropDown.SelectedValue.Equals(taxonomyRow["ID"].ToString()))
                        {
                            Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                            if (selectedIDsList != null && selectedIDsList.Count > 0)
                            {
                               //Get all keys (ids) from dictionary
                               List<int> keys = new List<int>(selectedIDsList.Keys);
                               //convert int array to string array
                               string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });                              

                                SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                                    taxonomyRow["ID"].ToString(),
                                    string.Join(",", SelectedIDs)); 
                            }
                        }
                        else //get other taxonomy selection values from dropdowns
                        {
                            string DefaultTaxonomyID = DefaultTaxonomyValue(taxonomyRow["ID"].ToString());
                            if (!string.IsNullOrEmpty(DefaultTaxonomyID))
                            {
                                SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                                   taxonomyRow["ID"].ToString(),
                                   DefaultTaxonomyID);                                        
                            }
                        }
                    }
                    else //other other taxonomy selection from dropdowns
                    {
                        string DefaultTaxonomyID = DefaultTaxonomyValue(taxonomyRow["ID"].ToString());
                        if (!string.IsNullOrEmpty(DefaultTaxonomyID))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                                    taxonomyRow["ID"].ToString(),
                                    DefaultTaxonomyID); 
                        }
                    }
                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }

            return SelectionsXML;
        }

        /// <summary>
        /// Defaults the taxonomy value.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <returns></returns>
        private string DefaultTaxonomyValue(string taxonomyTypeID)
        {
            //Get other taxonomies from drop downs
            string DefaultTaxonomyID = string.Empty;
            switch (taxonomyTypeID)
            {
                case "1":
                    DefaultTaxonomyID = CountryDropDown.SelectedValue;
                    break;
                case "2":
                    DefaultTaxonomyID = ModeDropDown.SelectedValue;
                    break;
                case "3":
                    DefaultTaxonomyID = MarketDropdown.SelectedValue;
                    break;
                case "10":
                    DefaultTaxonomyID = DestinationDropDown.SelectedValue;
                    break;

            }
            return DefaultTaxonomyID;
        }

        /// <summary>
        /// Method for legend styles
        /// </summary>
        /// <param name="objChart">chart object</param>
        public static void SetStandardLegendStyles(dotnetCHARTING.Chart objChart)
        {
            //Set TitleBox        
            objChart.LegendBox.Background = new dotnetCHARTING.Background(System.Drawing.Color.White);
            objChart.LegendBox.CornerBottomLeft = BoxCorner.Square;
            objChart.LegendBox.CornerBottomRight = BoxCorner.Square;
            objChart.LegendBox.CornerTopLeft = BoxCorner.Square;
            objChart.LegendBox.CornerTopRight = BoxCorner.Square;
            objChart.LegendBox.LabelStyle = new dotnetCHARTING.Label("", new System.Drawing.Font("Verdana", 7), Color.Black);
            objChart.LegendBox.Line = new dotnetCHARTING.Line(Color.White);
            objChart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
            objChart.LegendBox.Line.Color = Color.White;
            objChart.LegendBox.Shadow.Color = Color.White;
            objChart.LegendBox.InteriorLine.Color = Color.White;          
        }

        public static void SetStandardLegendStyles(ComponentArt.Web.Visualization.Charting.Chart objChart)
        {
            System.Drawing.Font f = new System.Drawing.Font("Arial", 8, FontStyle.Regular);
            //Set TitleBox
            objChart.Legend.BackColor = Color.White;
            //To set Border color of legend.
            objChart.Legend.DrawBackgroundRectangle = true;
            objChart.Legend.BorderColor = Color.White;
            //To set font for legend.
            objChart.Legend.Font = f;
            objChart.Legend.FontColor = Color.Black;
            //To set layout,position
            objChart.Legend.LegendLayout = LegendKind.Column;
            objChart.Legend.LegendPosition = LegendPositionKind.CenterRight;
            objChart.Legend.DrawBackgroundRectangle = false;
            objChart.Legend.SharesChartArea = false;
        }

        private void buildTree()
        {
            int taxonomyID = 1;
            //Get taxonomy data
            DataSet ds = SqlDataService.GetTaxonomyData(taxonomyID);
            ds.Relations.Add("NodeRelation", ds.Tables[0].Columns["ID"], ds.Tables[0].Columns["parentTaxonomyID"]);

            foreach (DataRow dbRow in ds.Tables[0].Rows)
            {
                if (dbRow.IsNull("parentTaxonomyID"))
                {
                    ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["Name"].ToString(), dbRow["ID"].ToString(), true);
                    TreeView1.Nodes.Add(newNode);
                    PopulateSubTree(dbRow, newNode);
                }
            }
        }

        private void PopulateSubTree(DataRow dbRow, ComponentArt.Web.UI.TreeViewNode node)
        {
            foreach (DataRow childRow in dbRow.GetChildRows("NodeRelation"))
            {
                ComponentArt.Web.UI.TreeViewNode childNode = CreateNode(childRow["Name"].ToString(), childRow["ID"].ToString(), true);
                node.Nodes.Add(childNode);
                PopulateSubTree(childRow, childNode);
            }
        }

        private ComponentArt.Web.UI.TreeViewNode CreateNode(string text, string value,  bool expanded)
        {
            ComponentArt.Web.UI.TreeViewNode node = new ComponentArt.Web.UI.TreeViewNode();
            node.Text = text;
            node.Value = value;
            node.Expanded = expanded;
            return node;
        }



       
    }
}
