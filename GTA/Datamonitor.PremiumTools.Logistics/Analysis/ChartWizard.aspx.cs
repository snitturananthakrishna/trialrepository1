using System;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using System.Drawing;
using System.IO;
using Aspose.Cells;
using Aspose.Slides;
using Aspose.Words;
using dotnetCHARTING;
using CArt = ComponentArt.Web.UI;

namespace Datamonitor.PremiumTools.Generic.Analysis
{
    public partial class ChartWizard : AuthenticationBasePage
    {
        #region PAGE EVENTS 

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            //Clear the session for measures
            Session["SelectedMeasures"] = null;

            //maintain selected indicators in multiple page visits
            if (Session["SelectedIndicators"] != null && Session["SelectedIndicators"].ToString().Trim() != "")
            {
                hdnSelectedIndicators.Value = Session["SelectedIndicators"].ToString().EndsWith(",") ? Session["SelectedIndicators"].ToString() : Session["SelectedIndicators"].ToString()+",";
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Analysis";

            XYChartLink.Visible = false;
            //DualYaxisChartLink.Visible = false;
            BubbleChartLink.Visible = false;
            PieChartLink.Visible = false;
            HeatMapLink.Visible = false;

            //Load the indicators dropdown
            LoadIndicators();

            if(!IsPostBack)
            {
                if (!string.IsNullOrEmpty(hdnSelectedIndicators.Value.TrimEnd(',')))
                {
                    ShowValidCharts(hdnSelectedIndicators.Value);
                }
            }
        }

        #endregion

        #region  PRIVATE METHODS (initial population)

        /// <summary>
        /// 
        /// </summary>
        public void LoadIndicators()
        {
            string FilterXml = GetDataFilterXML();

            DataSet TaxonomyDatSet = SqlDataService.GetUniqueTaxonomyByCriteria(FilterXml, string.Empty, null);

            TaxonomyDatSet.Relations.Add("NodeRelation", TaxonomyDatSet.Tables[0].Columns["ID"], TaxonomyDatSet.Tables[0].Columns["parentTaxonomyID"]);

            BuildTree(2,
                   IndicatorTreeview,
                   TaxonomyDatSet);
        }

        /// <summary>
        /// Gets the data filter XML.
        /// </summary>
        /// <returns></returns>
        private string GetDataFilterXML()
        {
            string SelectionsXML = string.Empty;
            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {

                    Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                    if (selectedIDsList != null && selectedIDsList.Count > 0)
                    {
                        //convert int array to string array
                        string SelectedTaxonomyIDs = "";
                        string SelectedParentTaxonomyIDs = "";
                        foreach (KeyValuePair<int, string> selectedID in selectedIDsList)
                        {
                            if (selectedID.Value.Contains("(All)"))
                            {
                                SelectedParentTaxonomyIDs += selectedID.Key.ToString() + ",";
                            }
                            else
                            {
                                SelectedTaxonomyIDs += selectedID.Key.ToString() + ",";
                            }
                        }
                        //Build selections xml
                        SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' parentSelectionIDs='{2}' columnID='{3}' />",
                            taxonomyRow["ID"].ToString(),
                            SelectedTaxonomyIDs.TrimEnd(','),
                            SelectedParentTaxonomyIDs.TrimEnd(','),
                            taxonomyRow["ColumnRowID"].ToString());
                    }

                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }

            return SelectionsXML;
        }

        /// <summary>
        /// Builds the tree.
        /// </summary>
        /// <param name="taxonomyID">The taxonomy ID.</param>
        /// <param name="treeView">The tree view.</param>
        /// <param name="taxonomyDatSet">The taxonomy dat set.</param>
        private void BuildTree(int taxonomyID,
            CArt.TreeView treeView,
            DataSet taxonomyDatSet)
        {
            if (taxonomyDatSet.Tables.Count > 0 )
            {
                string TaxonomySelectedValue = hdnSelectedIndicators.Value;
                //Filter rows of the given taxonomy type
                DataRow[] rows = taxonomyDatSet.Tables[0].Select("taxonomyTypeID=" + taxonomyID);
                foreach (DataRow dbRow in rows)
                {
                    if (dbRow.IsNull("parentTaxonomyID"))
                    {
                        TaxonomySelectedValue = TaxonomySelectedValue.StartsWith(",") ? TaxonomySelectedValue : "," + TaxonomySelectedValue;
                        ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["Name"].ToString(),
                            dbRow["ID"].ToString(),
                            dbRow["displayName"].ToString(),
                            true,
                            Convert.ToBoolean(dbRow["isLeafNode"].ToString()),
                            TaxonomySelectedValue.Contains("," + dbRow["ID"].ToString() + ","));

                        treeView.Nodes.Add(newNode);

                        PopulateSubTree(dbRow,
                            newNode,
                            TaxonomySelectedValue);

                    }
                }
            }
        }

        /// <summary>
        /// Populates the sub tree.
        /// </summary>
        /// <param name="dbRow">The db row.</param>
        /// <param name="node">The node.</param>
        /// <param name="taxonomySelectedValue">The taxonomy selected value.</param>
        private bool PopulateSubTree(DataRow dbRow,
            CArt.TreeViewNode node,
            string taxonomySelectedValue)
        {
            bool CanExpandParentnode = false;
            taxonomySelectedValue = taxonomySelectedValue.StartsWith(",") ? taxonomySelectedValue : "," + taxonomySelectedValue;
            foreach (DataRow childRow in dbRow.GetChildRows("NodeRelation"))
            {
                ComponentArt.Web.UI.TreeViewNode childNode = CreateNode(childRow["Name"].ToString(),
                    childRow["ID"].ToString(),
                    childRow["displayName"].ToString(),
                    false,
                    Convert.ToBoolean(childRow["isLeafNode"].ToString()),
                    taxonomySelectedValue.Contains("," + childRow["ID"].ToString() + ","));

                node.Nodes.Add(childNode);

                bool CanExpandNode =PopulateSubTree(childRow,
                    childNode,
                    taxonomySelectedValue);
                
                if (!childNode.Expanded)
                {
                    childNode.Expanded = CanExpandNode;
                }
                if (!CanExpandParentnode && (childNode.Checked || childNode.Expanded))
                {
                    CanExpandParentnode = true;
                }
            }
            return CanExpandParentnode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">The node text.</param>
        /// <param name="value">The node value.</param>
        /// <param name="expanded">Is node expanded</param>
        /// <param name="hassCheckbox">Has checkbox</param>
        /// <param name="isChecked">Is node checked</param>
        /// <returns></returns>
        private CArt.TreeViewNode CreateNode(string text,
            string value,
            string displayText,
            bool expanded,
            bool hasCheckbox,
            bool isChecked)
        {
            ComponentArt.Web.UI.TreeViewNode node = new CArt.TreeViewNode();
            node.Text = text;
            //node.Value = value;
            node.ID = value;
            node.Value = displayText;
            node.Expanded = expanded;
            node.ShowCheckBox = hasCheckbox;
            node.Checked = isChecked;
            return node;
        }

        /// <summary>
        /// Shows the valid charts.
        /// </summary>
        /// <param name="selectedIndicators">The selected indicators.</param>
        private void ShowValidCharts(string selectedIndicators)
        {
            //Get the distinct measures for the selected indicators
            DataSet MeasuresList = SqlDataService.GetMeasuresForSelectedIndicators(selectedIndicators);

            //Hide all chart links
            XYChartLink.Visible = false;
            //DualYaxisChartLink.Visible = false;
            BubbleChartLink.Visible = false;
            PieChartLink.Visible = false;
            HeatMapLink.Visible = false;

            //check for the valid chart links to show
            if (MeasuresList != null &&
                MeasuresList.Tables.Count > 0 &&
                MeasuresList.Tables[0].Rows.Count > 0)
            {
                //If atleast single measure found...
                XYChartLink.Visible = true;
                //DualYaxisChartLink.Visible = true;
                PieChartLink.Visible = true;
                HeatMapLink.Visible = true;

                //Bubble chart link will be shown only when the distict measures are more than 3
                if (MeasuresList.Tables[0].Rows.Count > 3)
                {
                    BubbleChartLink.Visible = true;
                }

                //Concatinating the distinct measureids as a string
                string MeasuresString = "";
                foreach (DataRow MeasureRow in MeasuresList.Tables[0].Rows)
                {
                    MeasuresString += MeasureRow[0].ToString() + ",";
                }
                MeasuresString = MeasuresString.EndsWith(",") ? MeasuresString.TrimEnd(',') : MeasuresString;

                //Maintain selected measures in session to be used in next chart page to bind the measures.
                Session["SelectedMeasures"] = MeasuresString;
                Session["SelectedIndicators"] = selectedIndicators.EndsWith(",") ? selectedIndicators.TrimEnd(',') : selectedIndicators;
            }
            
            //show the chartlinks panel
            ChartLinksPanel.Visible = true;
        }

        #endregion

        #region EVENT HANDLERS

        /// <summary>
        /// Handles the Callback event of the ShowValidCharts control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ComponentArt.Web.UI.CallBackEventArgs"/> instance containing the event data.</param>
        public void ShowValidCharts_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            //Call the private function to check the valid charts
            ShowValidCharts(e.Parameters[0]);

            //Render the chartlinks panel
            ChartLinksPanel.Visible = true;
            ChartLinksPanel.RenderControl(e.Output);
        }

        #endregion
     
        
    }
}
