using System;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using System.Drawing;
using System.IO;
using Aspose.Cells; 
using Aspose.Slides;
using Aspose.Words;
using dotnetCHARTING;
using CArt = ComponentArt.Web.UI;

namespace Datamonitor.PremiumTools.Logistics.Analysis
{
    public partial class BubbleChart1 : AuthenticationBasePage
    {
        //PRIVATE MEMBERS
        private string AxisCriteria = string.Empty;
        CArt.TreeViewNode FirstLeafNode = null;
        CArt.TreeViewNode SecondLeafNode = null;
        CArt.TreeViewNode ThirdLeafNode = null;
        int counter = 0;

        //EVENT OVERRIDES

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            
            if (!IsPostBack &&
                !ChartsCallback.IsCallback)
            {      
                BindControls();
                StartYearDropDown.Attributes.Add("onchange", "javascript:Years_onChange();");       
            }

            ////Clear the session for measures
            //Session["SelectedMeasures"] = null;

            ////maintain selected indicators in multiple page visits
            //if (Session["SelectedIndicators"] != null && Session["SelectedIndicators"].ToString().Trim() != "")
            //{
            //    hdnSelectedIndicators.Value = Session["SelectedIndicators"].ToString().EndsWith(",") ? Session["SelectedIndicators"].ToString() : Session["SelectedIndicators"].ToString() + ",";
            //}
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Analysis";
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!IsPostBack &&
                !ChartsCallback.IsCallback)
            {
                PlotChart_Click(null, null);
            }
        }

        //PRIVATE METHODS
        
        private void BindControls()
        {
            string SelectionsXML = GetFilterCriteriaFromSession();
            DataSet IndicatorsAndCountries = SqlDataService.GetIndicatorsForBCBubbleChart(SelectionsXML);
            XmlDocument document = new XmlDocument();
            //hdnIsPredefined.Value = "false";
            string startYear = string.Empty;
            ChartTitle.Text = "Basic Bubble Chart";   

            DataSet TaxonomyDatSet = SqlDataService.GetUniqueTaxonomyByCriteria(SelectionsXML, string.Empty, null);                   
            TaxonomyDatSet.Relations.Add("NodeRelation", TaxonomyDatSet.Tables[0].Columns["ID"],
                                     TaxonomyDatSet.Tables[0].Columns["parentTaxonomyID"]);

            BuildTree(2, XAxisTree, TaxonomyDatSet);
            BuildTree(2, YAxisTree, TaxonomyDatSet);
            BuildTree(2, BubbleTree, TaxonomyDatSet);           
            
            SelectDefaultsForCombos(XAxisTree);
          
            hdnIndicatorText.Value = hdnXAxisText.Value + ";" + hdnYAxisText.Value + ";" + hdnBubbleText.Value;
            //binding Country Data
            foreach (DataRow dbRow in IndicatorsAndCountries.Tables[1].Rows)
            {
                ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["Country"].ToString(),
                     dbRow["CountryID"].ToString(),
                     dbRow["CountryID"].ToString(),
                     true,
                     true,
                     false);

                CountryTree.Nodes.Add(newNode);
            }
            SelectDefaultsForCheckBoxCombo(CountryCombo, CountryTree, hdnCountry);
            hdnCountryText.Value = CountryCombo.Text;

            //Get list of valid years from session
            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
            int EndYear = 2015;
            //year combo            
            for (int counter = StartYear; counter <= EndYear; counter++)
            {
                StartYearDropDown.Items.Add(new ListItem(counter.ToString()));                
            }

            string SelectedYear = "2009";
            StartYearDropDown.SelectedValue = SelectedYear;
            //"2010";  //string.IsNullOrEmpty(startYear) ? StartYearDropDown.Items[0].Value : startYear;
            hdnStartYear.Value = StartYearDropDown.SelectedValue;                
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="singleFilterDropdown"></param>
        /// <param name="singleFilterTree"></param>
        /// <param name="hdnSingle"></param>
        /// <param name="hdnSingleText"></param>
        private void SelectDefaultsForCombos(CArt.TreeView singleFilterTree)
        {
            foreach (CArt.TreeViewNode tnode in singleFilterTree.Nodes)
            {
                if (tnode.Nodes.Count > 0)
                {
                    GetLeafNodes(tnode);
                }
            }

            if (FirstLeafNode == null && singleFilterTree.Nodes.Count > 0)
            {
                FirstLeafNode = singleFilterTree.Nodes[0];
                if (SecondLeafNode == null)
                {
                    SecondLeafNode = singleFilterTree.Nodes.Count > 1 ? singleFilterTree.Nodes[1] : FirstLeafNode;

                    if (ThirdLeafNode == null)
                    {
                        ThirdLeafNode = singleFilterTree.Nodes.Count > 2 ? singleFilterTree.Nodes[2] : SecondLeafNode;
                    }
                }
            }

            if (FirstLeafNode != null)
            {
                XAxisCombo.Text = FirstLeafNode.Value;
                hdnXAxis.Value = FirstLeafNode.ID;
                hdnXAxisText.Value = FirstLeafNode.Value;
                XAxisTree.SelectedNode = FirstLeafNode;
            }
            if (SecondLeafNode != null)
            {
                YAxisCombo.Text = SecondLeafNode.Value;
                hdnYAxis.Value = SecondLeafNode.ID;
                hdnYAxisText.Value = SecondLeafNode.Value;
                YAxisTree.SelectedNode = SecondLeafNode;
            }
            else if (FirstLeafNode != null)
            {
                YAxisCombo.Text = FirstLeafNode.Value;
                hdnYAxis.Value = FirstLeafNode.ID;
                hdnYAxisText.Value = FirstLeafNode.Value;
                YAxisTree.SelectedNode = FirstLeafNode;
            }
            if (ThirdLeafNode != null)
            {
                BubbleCombo.Text = ThirdLeafNode.Value;
                hdnBubble.Value = ThirdLeafNode.ID;
                hdnBubbleText.Value = ThirdLeafNode.Value;
                BubbleTree.SelectedNode = ThirdLeafNode;
            }
            else if (SecondLeafNode != null)
            {
                BubbleCombo.Text = SecondLeafNode.Value;
                hdnBubble.Value = SecondLeafNode.ID;
                hdnBubbleText.Value = SecondLeafNode.Value;
                BubbleTree.SelectedNode = SecondLeafNode;
            }
            else if (FirstLeafNode != null)
            {
                BubbleCombo.Text = FirstLeafNode.Value;
                hdnBubble.Value = FirstLeafNode.ID;
                hdnBubbleText.Value = FirstLeafNode.Value;
                BubbleTree.SelectedNode = FirstLeafNode;
            }
        }
        
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNode"></param>
        /// <returns></returns>
        private void GetLeafNodes(CArt.TreeViewNode parentNode)
        {            
            foreach (CArt.TreeViewNode tnode in parentNode.Nodes)
            {
                if (tnode.Nodes.Count > 0)
                {
                     GetLeafNodes(tnode);
                }
                else
                {
                    counter++;
                    if (counter == 1)
                        FirstLeafNode = tnode;
                    if (counter == 2)
                        SecondLeafNode = tnode;
                    if (counter == 3)
                    {
                        ThirdLeafNode = tnode;
                        break;
                    }
                }
            }      
        }

        /// <summary>
        /// Creates the node.
        /// </summary>
        /// <param name="text">The node text.</param>
        /// <param name="value">The node value.</param>
        /// <param name="id">The id.</param>
        /// <param name="expanded">Is node expanded</param>
        /// <param name="hasCheckbox">if set to <c>true</c> [has checkbox].</param>
        /// <param name="isChecked">Is node checked</param>
        /// <returns></returns>
        private CArt.TreeViewNode CreateNode(string text,
            string value,
            string id,
            bool expanded,
            bool hasCheckbox,
            bool isChecked)
        {
            ComponentArt.Web.UI.TreeViewNode node = new CArt.TreeViewNode();
            node.Text = text;
            node.Value = value;
            node.ID = id;
            node.Expanded = expanded;
            node.ShowCheckBox = hasCheckbox;
            node.Checked = isChecked;            
            return node;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="multipleFilterDropdown"></param>
        /// <param name="multipleFilterTree"></param>
        /// <param name="hdnMultiple"></param>
        private void SelectDefaultsForCheckBoxCombo(CArt.ComboBox combo,
            CArt.TreeView tree,
            HiddenField hdnField)
        {
            int DefaultSelectionsCount = 5;
            combo.Text = "";
            hdnField.Value = "";
            foreach (CArt.TreeViewNode tnode in tree.Nodes)
            {
                if (DefaultSelectionsCount <= 0) break;
                if (tnode.ShowCheckBox)
                {
                    combo.Text += tnode.Text + ";";
                    hdnField.Value += tnode.ID + ",";
                    tnode.Checked = true;
                    DefaultSelectionsCount--;
                }
            }
            combo.Text = combo.Text.TrimEnd(new char[] { ';' });
        }

        /// <summary>
        /// Builds the tree.
        /// </summary>
        /// <param name="taxonomyID">The taxonomy ID.</param>
        /// <param name="treeView">The tree view.</param>
        /// <param name="taxonomyDatSet">The taxonomy dat set.</param>
        private void BuildTree(int taxonomyID,
            CArt.TreeView treeView,
            DataSet taxonomyDatSet)
        {
            if (taxonomyDatSet.Tables.Count > 0)
            {
                //Filter rows of the given taxonomy type
                DataRow[] rows = taxonomyDatSet.Tables[0].Select("taxonomyTypeID=" + taxonomyID);
                foreach (DataRow dbRow in rows)
                {
                    if (dbRow.IsNull("parentTaxonomyID"))
                    {
                        ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["Name"].ToString(),
                            dbRow["displayName"].ToString(), 
                            dbRow["ID"].ToString(),                           
                            true,
                            false,
                            false); 

                        treeView.Nodes.Add(newNode);
                        PopulateSubTree(dbRow,newNode);                            
                    }
                }
            }
        }

        /// <summary>
        /// Populates the sub tree.
        /// </summary>
        /// <param name="dbRow">The db row.</param>
        /// <param name="node">The node.</param>
        /// <param name="taxonomySelectedValue">The taxonomy selected value.</param>
        private void PopulateSubTree(DataRow dbRow,
            CArt.TreeViewNode node)           
        {            
            foreach (DataRow childRow in dbRow.GetChildRows("NodeRelation"))
            {
                ComponentArt.Web.UI.TreeViewNode childNode = CreateNode(childRow["Name"].ToString(),
                    childRow["displayName"].ToString(), 
                    childRow["ID"].ToString(),
                    false,
                    false,
                    false);

                node.Nodes.Add(childNode);
                PopulateSubTree(childRow,childNode);                    
            }
        }

    
        // EVENT HANDLERS
        protected void PlotChart_Click(object sender, EventArgs e)
        {
            string xAxisText = hdnXAxisText.Value;
            string yAxisText = hdnYAxisText.Value;
            string bubbleText = hdnBubbleText.Value;
            AxisCriteria = "X Axis: " + xAxisText
                         + "; Y Axis: " + yAxisText
                         + "; Series: " + bubbleText;

            int year = Convert.ToInt16(StartYearDropDown.SelectedValue);

            //Get Chart Data          
            DataSet ChartDataset = SqlDataService.GetBasicBubbleChartData(hdnCountry.Value.TrimEnd(new char[] { ',' }),
                Convert.ToInt32(hdnXAxis.Value.TrimEnd(new char[] { ',' })),
                Convert.ToInt32(hdnYAxis.Value.TrimEnd(new char[] { ',' })),
                Convert.ToInt32(hdnBubble.Value.TrimEnd(new char[] { ',' })),
                year);

            if (ChartDataset.Tables[0].Rows.Count > 0)
            {
                string xAxisUnits = string.Empty;
                string yAxisUnits = string.Empty;

                foreach (DataRow drow in ChartDataset.Tables[0].Rows)
                {
                    xAxisUnits = drow["XaxisUnits"].ToString();
                    yAxisUnits = drow["YAxisUnits"].ToString();

                    if ((xAxisUnits != null && xAxisUnits != "") && (yAxisUnits != null && yAxisUnits != ""))
                        break;
                }
                
                xAxisText = xAxisText + "\n(" + xAxisUnits + ")";
                yAxisText = yAxisText + "\n(" + yAxisUnits + ")";
            }

            dotnetCHARTING.Chart targetChart;

            //Plot Chart 
            if ((ChartDataset.Tables.Count > 0) && (ChartDataset.Tables[0].Rows.Count > 0))
            {
                targetChart = TwoDimensionalCharts.PlotBubbleChart(ChartDataset,
                                                   TwoDChart,
                                                   "Bubble",
                                                   xAxisText,
                                                   yAxisText,
                                                   bubbleText,
                                                   AxisCriteria);

                //TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                targetChart.Visible = false;
                if (!Directory.Exists(Server.MapPath("temp")))
                {
                    Directory.CreateDirectory(Server.MapPath("temp"));
                }
                targetChart.Width = Unit.Point(520);
                targetChart.Height = Unit.Point(300);

                targetChart.FileManager.TempDirectory = "temp";
                // Remove the legend from the chart and save the chart as a separate image file
                targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.None;
                //string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap());
                ChartImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");
                ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                ImageMapLabel.Text = targetChart.ImageMapText;
                hdnImageurl.Value = ChartImg.ImageUrl;
                LegendImg.Visible = false;
                //Save the chart in viewstate, used in extractions.
                ViewState["2DChartImage"] = ChartImg.ImageUrl;
                ViewState["ChartType"] = "2D"; 
            }          
        }

        //CALLBACK EVENT HANDLERS
        protected void Chart_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            string xAxisText = e.Parameters[5].ToString();
            string yAxisText = e.Parameters[6].ToString();
            string bubbleText = e.Parameters[7].ToString();
            AxisCriteria = "X Axis: " + xAxisText
                         + "; Y Axis: " + yAxisText
                         + "; Series: " + bubbleText;

            //GET Chart Data
            DataSet chartDataset = SqlDataService.GetBasicBubbleChartData(e.Parameters[0].ToString().TrimEnd(new char[] { ',' }),
                Convert.ToInt32(e.Parameters[1].ToString()),
                Int32.Parse(e.Parameters[2].ToString()),
                Int32.Parse(e.Parameters[3].ToString()),
                Int32.Parse(e.Parameters[4].ToString()))
                ;

            if (chartDataset.Tables.Count > 0 && chartDataset.Tables[0].Rows.Count > 0)
            {
                string xAxisUnits = string.Empty;
                string yAxisUnits = string.Empty;

                foreach (DataRow drow in chartDataset.Tables[0].Rows)
                {
                    xAxisUnits = drow["XaxisUnits"].ToString();
                    yAxisUnits = drow["YAxisUnits"].ToString();

                    if ((xAxisUnits != null && xAxisUnits!="") && (yAxisUnits != null && yAxisUnits != ""))
                        break;
                }
              
                xAxisText = xAxisText + "\n(" + xAxisUnits + ")";
                yAxisText = yAxisText + "\n(" + yAxisUnits + ")";
            }

            //Plot Chart
            if (chartDataset.Tables.Count > 0 && chartDataset.Tables[0].Rows.Count > 0)
            {
                dotnetCHARTING.Chart targetChart;
                targetChart = TwoDimensionalCharts.PlotBubbleChart(chartDataset,
                                                   TwoDChart,
                                                   "Bubble",
                                                   xAxisText,
                                                   yAxisText,
                                                   bubbleText,
                                                   AxisCriteria);
                //TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                targetChart.Visible = false;
                if (!Directory.Exists(Server.MapPath("temp")))
                {
                    Directory.CreateDirectory(Server.MapPath("temp"));
                }
                targetChart.FileManager.TempDirectory = "temp";
                targetChart.Width = Unit.Point(520);
                targetChart.Height = Unit.Point(300);
                // Remove the legend from the chart and save the chart as a separate image file
                targetChart.LegendBox.Position = LegendBoxPosition.None;
                string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap());

                ChartImg.ImageUrl = ChartImg.ResolveUrl(strChartPath);
                ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                ImageMapLabel.Text = targetChart.ImageMapText;
                ChartImg.RenderControl(e.Output);
                hdnImageurl.Value = strChartPath;
                hdnImageurl.RenderControl(e.Output);
                ImageMapLabel.RenderControl(e.Output);
                LegendImg.Visible = false;
            }
        }

        //EXPORT EVENT HANDLERS
        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;
            int year = Convert.ToInt16(StartYearDropDown.SelectedValue);
            Workbook Excel;

            string SelectionsXML = GetFilterCriteriaFromSession();
            DataSet ChartDataset = SqlDataService.GetBasicBubbleChartData(hdnCountry.Value.TrimEnd(new char[] { ',' }),
                 Convert.ToInt32(hdnXAxis.Value.TrimEnd(new char[] { ',' })),
                 Convert.ToInt32(hdnYAxis.Value.TrimEnd(new char[] { ',' })),
                 Convert.ToInt32(hdnBubble.Value.TrimEnd(new char[] { ',' })),
                 year);

            LogUsage("Charts - Bubble - Word download", "Basic Bubble Chart", " ");
            if (hdnImageurl.Value.Length > 0)
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            #region build Filter criteria xml to show in extracts

            string SelectionsTextXML = string.Format("<taxonomy id='Indicator' selectIDs='{0}'/>", Microsoft.JScript.GlobalObject.escape(hdnIndicatorText.Value));
            SelectionsTextXML += string.Format("<taxonomy id='Country' selectIDs='{0}'/>", hdnCountryText.Value.Replace(';', ','));
            SelectionsTextXML += string.Format("<taxonomy id='Year' selectIDs='{0}'/>", year);
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);

            Session["SelectionsTextXML"] = SelectionsTextXML;

            #endregion

            if (chartBitmap != null)
            {
                Excel = ExtractionsHelper.ExportToExcel(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "BasicBubbleChart",
                    ExcelTemplatePath,
                    LogoPath,
                    ChartTitle.Text);

                Excel.Save("Ovum" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);
            }
        }

        protected void lnkWord_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;
            int year = Convert.ToInt16(StartYearDropDown.SelectedValue);

            string SelectionsXML = GetFilterCriteriaFromSession();
            DataSet ChartDataset = SqlDataService.GetBasicBubbleChartData(hdnCountry.Value.TrimEnd(new char[] { ',' }),
                 Convert.ToInt32(hdnXAxis.Value.TrimEnd(new char[] { ',' })),
                 Convert.ToInt32(hdnYAxis.Value.TrimEnd(new char[] { ',' })),
                 Convert.ToInt32(hdnBubble.Value.TrimEnd(new char[] { ',' })),
                 year);

            LogUsage("Charts - Bubble - Word download", "Basic Bubble Chart", " ");
            if (hdnImageurl.Value.Length > 0)
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            #region build Filter criteria xml to show in extracts
            
            string SelectionsTextXML = string.Format("<taxonomy id='Indicator' selectIDs='{0}'/>", Microsoft.JScript.GlobalObject.escape(hdnIndicatorText.Value));
            SelectionsTextXML += string.Format("<taxonomy id='Country' selectIDs='{0}'/>", hdnCountryText.Value.Replace(';', ','));
            SelectionsTextXML += string.Format("<taxonomy id='Year' selectIDs='{0}'/>", year);
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);

            Session["SelectionsTextXML"] = SelectionsTextXML;

            #endregion

            if (chartBitmap != null)
            {
                Document doc = ExtractionsHelper.ExportToWord(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "BasicBubbleChart",
                    WordTemplatePath,
                    ChartTitle.Text);
                doc.Save("OvumExtract.doc", SaveFormat.FormatDocument, Aspose.Words.SaveType.OpenInWord, Response);
            }

        }        

        protected void lnkPPT_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;
            string strExtractName = "OvumExtract";
            int year = Convert.ToInt16(StartYearDropDown.SelectedValue);

            string SelectionsXML = GetFilterCriteriaFromSession();
            DataSet ChartDataset = SqlDataService.GetBasicBubbleChartData(hdnCountry.Value.TrimEnd(new char[] { ',' }),
                 Convert.ToInt32(hdnXAxis.Value.TrimEnd(new char[] { ',' })),
                 Convert.ToInt32(hdnYAxis.Value.TrimEnd(new char[] { ',' })),
                 Convert.ToInt32(hdnBubble.Value.TrimEnd(new char[] { ',' })),
                 year);

            LogUsage("Charts - Bubble - Word download", "Basic Bubble Chart", " ");
            if (hdnImageurl.Value.Length > 0)
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            #region build Filter criteria xml to show in extracts
            
            string SelectionsTextXML = string.Format("<taxonomy id='Indicator' selectIDs='{0}'/>", Microsoft.JScript.GlobalObject.escape(hdnIndicatorText.Value));
            SelectionsTextXML += string.Format("<taxonomy id='Country' selectIDs='{0}'/>", hdnCountryText.Value.Replace(';', ','));
            SelectionsTextXML += string.Format("<taxonomy id='Year' selectIDs='{0}'/>", year);
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);

            Session["SelectionsTextXML"] = SelectionsTextXML;

            #endregion

            if (chartBitmap != null)
            {
                Presentation presentation = ExtractionsHelper.ExportToPowerpoint(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "BasicBubbleChart",
                    PowerpointTempaltePath,
                    ChartTitle.Text);

                Response.ContentType = "application/vnd.ms-powerpoint";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + strExtractName + ".ppt");
                presentation.Write(Response.OutputStream);
                Response.Flush();
            }
        }    
    }
}
