using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using Datamonitor.PremiumTools.Generic.Library;
using System.IO;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using System.Drawing;
using System.Drawing.Imaging;
using Aspose.Slides;
using Aspose.Cells;
using Aspose.Words.Reporting;
using Aspose.Words.Viewer;
using System.Net;

namespace Datamonitor.PremiumTools.Generic.Analysis
{
   public partial class HeatMap : AuthenticationBasePage
    {  
       //PRIVATE MEMBERS
       private double interval = 0;      
       private double minValue = 0;
       private double maxValue = 0;       

       //PAGE EVENT
       protected void Page_Load(object sender, EventArgs e)
       {
           if (!IsPostBack)
           {
               //Bind the axis selection dropdowns.
               BindAxisDropdownsData();
               //MeasureDropDown.Attributes.Add("onchange", "javascript:YAxis_onChange(true);");
               //MeasureDropDown.Attributes.Add("onchange", "javascript:ShowHideFilterOptions();"); 
               bool IncludeMeasureInHeatmap = GlobalSettings.IncludeMeasureInHeatMap;

               //Hide the 'back to chart wizard' link when loading predefined chart
               BackToChartWizardDiv.Visible = (Request.QueryString["id"] == null);

               if (IncludeMeasureInHeatmap)
               {
                   Measurelbl.Visible = true;
                   MeasureDropDown.Visible = true;
               }
               else
               {
                   Measurelbl.Visible = false;
                   MeasureDropDown.Visible = false;
               }
           }

           //If measure is allowed, then get the selected measureid
           if (GlobalSettings.IncludeMeasureInHeatMap)
           {
               hdnMeasure.Value = MeasureDropDown.SelectedValue;

               ChartFilters1.YAxis = hdnMeasure.Value.Length > 0 ?
                          Convert.ToString(System.Math.Abs(Convert.ToInt32(hdnMeasure.Value))) :
                          "";
           }

           //Load filters
           ChartFilters1.LoadData();
       }

       // EVENT OVERRIDES
       protected override void OnPreRender(EventArgs e)
       {
           if (!IsPostBack) // && !ChartFilterCallback.IsCallback
           {
               PlotChart_Click(null, null);
           }
       }
 
       // PRIVATE METHODS (initial population) 
       private void BindAxisDropdownsData()
       {
           XmlDocument document = new XmlDocument();
           XmlDocument doc = new XmlDocument();

           //Get Measure Dropdown data.
           string SelectionsXML = GetFilterCriteriaFromSession();

           DataSet YAxisDataSet = SqlDataService.GetYAxisTaxonomyByCriteria(SelectionsXML, false);
           if (YAxisDataSet.Tables.Count > 0)
           {
               //Binding Y-Axis

               if (GlobalSettings.ShowChartWizard &&
                        Request.QueryString["source"] != null &&
                        Request.QueryString["source"].ToString().Equals("wizard") &&
                        Session["SelectedMeasures"] != null &&
                        Session["SelectedMeasures"].ToString() != "")
               {
                   //Filter measures if chart wizard selections are found
                   DataView SelectedMeasures = YAxisDataSet.Tables[0].DefaultView;
                   SelectedMeasures.RowFilter = "id in (" + Session["SelectedMeasures"].ToString() + ")";
                   DataTable SelectedMeasuresTable = SelectedMeasures.ToTable();

                   //Binding Y-Axis
                   MeasureDropDown.DataSource = SelectedMeasuresTable;
                   MeasureDropDown.DataTextField = SelectedMeasuresTable.Columns["displayName"].ToString();
                   MeasureDropDown.DataValueField = SelectedMeasuresTable.Columns["ID"].ToString();
                   MeasureDropDown.DataBind();
               }
               else
               {
                   //Binding Y-Axis
                   MeasureDropDown.DataSource = YAxisDataSet;
                   MeasureDropDown.DataTextField = YAxisDataSet.Tables[0].Columns["displayName"].ToString();
                   MeasureDropDown.DataValueField = YAxisDataSet.Tables[0].Columns["ID"].ToString();
                   MeasureDropDown.DataBind();
               }

               try
               {
                   MeasureDropDown.SelectedValue = GlobalSettings.DefaultYAxisType;
                   hdnMeasure.Value = GlobalSettings.DefaultYAxisType;
               }
               catch (Exception ex) { }
           }

           if (!string.IsNullOrEmpty(Request.QueryString["id"]))
           {
               DataSet ds = SqlDataService.GetPredefinedChartMetaData(Convert.ToInt32(Request.QueryString["id"]));
               
               if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
               {
                   string filterXml = ds.Tables[0].Rows[0]["Selections"].ToString();
                   document.LoadXml(filterXml);
                   ChartTitle.Text = document.SelectSingleNode("//Title").InnerText;
               }
           }

       }

       // METHODS (Build selection criteria into XML)  
       private string GetDataFilterXML()
       {
           string SelectionsXML = string.Empty;
           string SelectionsTextXML = string.Empty;

           DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
           if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
           {
               //string TaxonomySelectedValue = string.Empty;
               //string TaxonomySelectedText = string.Empty;

               //Build selection criteria xml
               foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
               {
                   string TaxonomySelectedValue = string.Empty;
                   string TaxonomySelectedText = string.Empty;

                   if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE") && !taxonomyRow["ID"].ToString().Equals("999"))
                   {
                       //Get the x axis, series taxonomies filter data from session
                       if (taxonomyRow["ID"].ToString().Equals("1"))
                       {
                           Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                           if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                           {
                               //Get all keys (ids) from dictionary
                               List<int> keys = new List<int>(selectedIDsList.Keys);
                               //convert int array to string array
                               string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                               TaxonomySelectedValue = string.Join(",", SelectedIDs);
                           }
                           //Get selected taxonomy id from user control's hidden field
                           if (ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection") != null)
                           {
                               TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection")).Value;
                               TaxonomySelectedValue = TaxonomySelectedValue.TrimEnd(new char[] { ',' });
                           }
                           if (ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText") != null)
                           {
                               TaxonomySelectedText = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText")).Value;
                               TaxonomySelectedText = TaxonomySelectedText.TrimEnd(new char[] { ',' });
                           }
                       }
                       else
                       {
                           //Get selected taxonomy id from user control's hidden field
                           if (ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection") != null)
                           {
                               TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection")).Value;
                           }
                           if (ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText") != null)
                           {
                               TaxonomySelectedText = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText")).Value;
                           }
                       }
                       if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                       {
                           SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                               taxonomyRow["ID"].ToString(),
                               TaxonomySelectedValue,
                               taxonomyRow["ColumnRowID"].ToString());
                       }
                       if (!string.IsNullOrEmpty(TaxonomySelectedText))
                       {
                           if (TaxonomySelectedText.Contains("'"))
                           {
                               TaxonomySelectedText = TaxonomySelectedText.Replace("'", "&apos;");
                           }

                           SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                              taxonomyRow["Name"].ToString(),
                              TaxonomySelectedText);

                           if (taxonomyRow["Name"].ToString().Equals("Indicator") ||
                               taxonomyRow["Name"].ToString().Equals("Market"))
                           {
                               Session["IndicatorText"] = TaxonomySelectedText;
                           }
                       }
                   }
                   else if (taxonomyRow["ID"].ToString().Equals("999"))
                   {
                       //Get selected year value from user control's hidden field
                       TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy999Selection")).Value;
                      
                       if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                       {
                           SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                            taxonomyRow["ID"].ToString(),
                            TaxonomySelectedValue,
                            taxonomyRow["ColumnRowID"].ToString());
                       }
                       if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                       {
                           SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}'/>",
                              taxonomyRow["Name"].ToString(),
                              TaxonomySelectedValue);
                       }
                   }

               }
               SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
               SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);
               Session["SelectionsTextXML"] = SelectionsTextXML.ToString();
           }

           return SelectionsXML;
       }      

       // EVENT HANDLERS
       protected void PlotChart_Click(object sender, EventArgs e)
       {
           DataSet heatMapDataset = null;
           if (!string.IsNullOrEmpty(MeasureDropDown.SelectedValue))
           {
               string SelectionsXML = GetDataFilterXML();

               string predefinedID = string.IsNullOrEmpty(Request.QueryString["id"]) ?
                    "" :
                    "Predefined Chart:" + Request.QueryString["id"].ToString();

               LogUsage("Charts - Heat Map", predefinedID, SelectionsXML);

               //Get chart data 
               heatMapDataset = SqlDataService.GetHeatMapData(Int32.Parse(MeasureDropDown.SelectedValue),
                   MapTypeDropdown.SelectedValue.ToString(),
                   SelectionsXML);

               Session["HeatMapData"] = (heatMapDataset.Tables.Count > 0) ?
                              heatMapDataset.Tables[0] :
                              null;
           }
           if (heatMapDataset != null && (heatMapDataset.Tables.Count > 0) && (heatMapDataset.Tables[0].Rows.Count > 0))
           {
               BuildMap(heatMapDataset);
           }
           else
           {
               WorldPopulationMap.Text = "No Data for your Selection Criteria";
           }
       }

       // METHODS
       private void BuildMap(DataSet heatMapDataset)
       {
            int showDecimals = 1;
            string displayValueFormat = string.Empty;
            StringBuilder heatMapXML = new StringBuilder();           

            DataTable rankDatatable = heatMapDataset.Tables[1];
            //Set Minimum and Maximum values to define color range.
            if (!rbPlotByRank.Checked)
            {
                SetMinMaxValues(heatMapDataset);

                if (interval > 0 && interval < 1)
                {
                    showDecimals = 3;
                }
                else if (interval > 1 && interval < 100)
                {
                    showDecimals = 2;
                }               
                displayValueFormat = (interval > 0 && interval < 1) ?
                    "{0:0.###}" :
                    "{0:0}";
            }
    
            //Read Map colors.
            string strColors = ConfigurationManager.AppSettings["heatMapColors"].ToString();
            string[] heatMapColors = strColors.Replace("{", "").Replace("}", "").Split(',');
            ViewState["GUID"] = Guid.NewGuid().ToString();
            hdnMap.Value = "excel";

            string UnitsValue = heatMapDataset.Tables[0].Rows[0]["Units"].ToString();
            if (UnitsValue == "%")
            {
                UnitsValue = "(" + UnitsValue + " %25)";
            }
            else
            {
                UnitsValue = "(" + UnitsValue + ")";
            }
           
            string fusionMapURL = "FusionMapsSave.aspx?userId=" + ViewState["GUID"].ToString() + "|" + hdnMap.Value; ;

            heatMapXML.Append("<map imageSave='1' exportEnabled='1' showLegend='1' imageSaveURL='" + 
                fusionMapURL + 
                "' legendPosition='BOTTOM' legendBorderColor='FFFFFF' legendShadow='0' legendBorderAlpha='0' borderColor='666666' fillColor='FFFFFF' hoverColor='666666' showBevel='0' showShadow='0'  formatnumber='1' defaultAnimation='0' showLabels='0' hoverOnEmpty='0' showCanvasBorder='0' numberSuffix='" + 
                UnitsValue + 
                "' decimals='" + 
                showDecimals + 
                "' showToolTip='1' toolTipBgColor ='FFFFFF' toolTipSepChar=' : ' >");
            heatMapXML.Append("<colorRange>");
                       
            double maxValue = minValue + interval;
            double DisplayMinValue = 0;
            double DisplayMaxValue = 0;
            //Replace ' with &apos;
            for (int i = 0; i < heatMapDataset.Tables[0].Rows.Count; i++)
            {
                if (heatMapDataset.Tables[0].Rows[i]["Name"].ToString() == "C�te d'Ivoire")
                {
                    heatMapDataset.Tables[0].Rows[i]["Name"] = heatMapDataset.Tables[0].Rows[i]["Name"].ToString().Replace("C�te d'Ivoire", "Cote d Ivoire"); //"C�te d'Ivoire", "Cote d Ivoire"
                    break;
                }
            }
            if (!rbPlotByRank.Checked) 
            { 
                for (int i = 0; i < heatMapColors.Length; i++)
                {
                    DisplayMinValue = Convert.ToDouble(string.Format(displayValueFormat, minValue));
                    DisplayMaxValue = Convert.ToDouble(string.Format(displayValueFormat, maxValue));

                    string str = string.Format("<color minValue='{0}' maxValue='{1}' displayValue='{2}' color='{3}' />",
                            minValue,
                            maxValue,
                            DisplayMinValue + " to " + DisplayMaxValue + UnitsValue ,
                            heatMapColors[i]);

                    heatMapXML.Append(str);

                    minValue = maxValue;
                    maxValue = maxValue + interval;
                }
            }
            else
            {
                for (int i = 0; i < rankDatatable.Rows.Count; i++)
                {
                    minValue = Convert.ToDouble(rankDatatable.Rows[i]["minValue"].ToString());
                    maxValue = Convert.ToDouble(rankDatatable.Rows[i]["maxValue"].ToString()) + 0.5;

                    DisplayMinValue = Convert.ToDouble(string.Format("{0:0}", minValue));
                    DisplayMaxValue = Convert.ToDouble(string.Format("{0:0}", maxValue));

                    string labeltext = string.Empty;
                    string color = string.Empty;
                    switch (i)
                    {
                        case 0:
                            labeltext = "Group 1(highest or 5th quintile)";
                            color = heatMapColors[4];
                            break;
                        case 1:
                            labeltext = "Group 2 (4th quintile)";
                            color = heatMapColors[3];
                            break;
                        case 2:
                            labeltext = "Group 3 (3rd quintile)";
                            color = heatMapColors[2];
                            break;
                        case 3:
                            labeltext = "Group 4 (2nd quintile)";
                            color = heatMapColors[1];
                            break;
                        case 4:
                            labeltext = "Group 5 (1st quintile) ";
                            color = heatMapColors[0];
                            break;

                    }

                    string str = string.Format("<color minValue='{0}' maxValue='{1}' displayValue='{2}' color='{3}' />",
                               minValue,
                               maxValue,
                               labeltext,
                               color);

                    heatMapXML.Append(str);
                }
            }
           
            heatMapXML.Append("</colorRange>");
            //Append data tag.
            heatMapXML.Append("<data>");
            for (int i = 0; i < heatMapDataset.Tables[0].Rows.Count; i++)
            {
                string value = heatMapDataset.Tables[0].Rows[i]["Value"] != null && heatMapDataset.Tables[0].Rows[i]["Value"].ToString().Length > 0 ?
                    heatMapDataset.Tables[0].Rows[i]["Value"].ToString() :
                    string.Empty;
                
                string str = string.Format("<entity ID='{0}' value='{1}' CountryName='{2}' />",
                        heatMapDataset.Tables[0].Rows[i]["FusionID"].ToString().TrimStart().TrimEnd(),
                        value,
                        heatMapDataset.Tables[0].Rows[i]["Name"].ToString());
                heatMapXML.Append(str);         
            }
            heatMapXML.Append("</data>");
            //close Map element
            heatMapXML.Append("</map>");  

            ViewState["heatMapXml"] = heatMapXML;
            hdnXmlData.Value = Microsoft.JScript.GlobalObject.escape(heatMapXML);
            //Load Map based on Selection                
            string mapHTML = getHTML(MapTypeDropdown.SelectedValue.Trim(), true);                
            //Embed the chart rendered as HTML into Literal - WorldPopulationMap        
            WorldPopulationMap.Text = mapHTML;          
        }
        
       private string getHTML(string country, bool flag)
        {
            string returnStr = FusionMaps.RenderMap(ConfigurationManager.AppSettings[country], "", 
                ViewState["heatMapXml"].ToString(), 
                "mapid", 
                "530", 
                "320", 
                false,
                true, 
                false); 
            return returnStr;
        }
      
       private void SetMinMaxValues(DataSet heatMapDataset)
       {           
           minValue = 0;
           if (heatMapDataset.Tables[0].Rows.Count == 1)
           {              
               maxValue = heatMapDataset.Tables[0].Rows[0]["Value"] != null && heatMapDataset.Tables[0].Rows[0]["Value"].ToString().Length > 0 ?
                   Convert.ToDouble(heatMapDataset.Tables[0].Rows[0]["Value"]) : 0;
           }
           else
           {
               for (int i = 0; i < heatMapDataset.Tables[0].Rows.Count; i++)
               {
                   if (heatMapDataset.Tables[0].Rows[i]["Value"] != null && heatMapDataset.Tables[0].Rows[i]["Value"].ToString().Length > 0)
                   {
                       double currentValue = Convert.ToDouble(heatMapDataset.Tables[0].Rows[i]["Value"]);
                       
                       if (currentValue < minValue)
                       {
                           minValue = currentValue;
                       }
                       if (currentValue > maxValue)
                       {
                           maxValue = currentValue;
                       }
                   }
               }
           }
           interval = (double)((maxValue - minValue) / 5.0) * 1.01;          
        }       
    }
}
