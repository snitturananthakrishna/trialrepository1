<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/AnalysisMaster.Master" 
        AutoEventWireup="true" 
        CodeBehind="DualYAxisChart.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.Analysis.DualYAxisChart"        
        Theme="NormalTree"
        EnableEventValidation="false"%>       
<%@ Register TagPrefix="ComponentArt" 
        Namespace="ComponentArt.Web.Visualization.Charting" 
        Assembly="ComponentArt.Web.Visualization.Charting" %>  
<%@ Register Assembly="dotnetCHARTING" 
        Namespace="dotnetCHARTING" 
        TagPrefix="dotnetCHARTING" %>
<%@ Register Assembly="ComponentArt.Web.UI" 
        Namespace="ComponentArt.Web.UI" 
        TagPrefix="ComponentArt" %>
<%@ Register Src="../Controls/ChartFilters.ascx" 
        TagName="ChartFilters" 
        TagPrefix="GPMT" %>
<%@ Register Src="../Controls/DynamicChartfilters.ascx" 
        TagName="DynamicChartFilters" 
        TagPrefix="GPMT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">

<style type="text/css">
.rightdiv
{
    float:right;
    margin-bottom:4px;
}
.leftdiv
{
    float:left;
    width:60px;
}
input[type=checkbox] 
{
	width:5px;
	margin-left:-65px;
	margin-right:-65px;
	padding:0px;
	text-align:left;
}
</style>

<script type="text/javascript">
function ShowHideFilterOptions()
{   
  var XAxis= "#<%= xAxisDropdown.ClientID%>";    
  var selectedXAxis = $(XAxis).val();  
  
  $("#filterContainer > div").show();  
  $("#filterContainer  div  div").show();  
  
  $("div[id*='SingleFilterDiv']").show(); 
  $("div[id*='MultipleFilterDiv']").hide();     
      
  $("div[id$='SingleFilterDiv"+selectedXAxis+"']").hide(); 
  $("div[id$='MultipleFilterDiv"+selectedXAxis+"']").show(); 
  
  $("#divIndicators").hide();
  
  AddMacroeconomicToDropdown();     
  document.getElementById('<%= YAxis2DropDown.ClientID%>').value = $("#<%= hdnYAxis2.ClientID%>").val();
  ToggleIndicatorSelection();
  
}
$(document).ready(function() {
   setTimeout("ShowHideFilterOptions()",500);
});

function Xaxis_onChange()
{
    ShowHideFilterOptions();
    $("#PlotChartMsg").show();
    var Xaxis= document.getElementById('<%=hdnXAxis.ClientID%>');
    Xaxis.value = $("#<%= xAxisDropdown.ClientID%>").val();
    //To get AxisCriteria
    var XaxisText= document.getElementById('<%=hdnXAxisText.ClientID%>');    
    XaxisText.value = $("#<%= xAxisDropdown.ClientID%> option:selected").text();
    
}
function ChartFilterCallback_onCallbackComplete(sender, eventArgs)
{
   ShowHideFilterOptions();   
}
function AddMacroeconomicToDropdown()
{
    if('<%=ConfigurationManager.AppSettings["IncludeMacroeconomicData"].ToString() %>'=='false')
    {       
        return;
    }
    var Yaxis2 =  document.getElementById('<%= YAxis2DropDown.ClientID%>');
    removeMacroeconomicSelection(Yaxis2);
    
    if($("#<%= xAxisDropdown.ClientID%>").val()=="1" || 
        $("#<%= xAxisDropdown.ClientID%>").val()=="999")
    {
        // Create an Option object                
        var opt = document.createElement("option");       
        // Assign text and value to Option object
        opt.text = "Macroeconomic data";
        opt.value = "11";
        // Add an Option object to Yaxis2 Drop Down
        Yaxis2.options.add(opt);   
    }
}

function ChartTypes_onNodeSelect(sender, eventArgs)
{ 
    ChartTypesDropDown.set_text(eventArgs.get_node().get_text());
    ChartTypesDropDown.collapse();    
    document.getElementById('<%=hiddenChartType.ClientID %>').value = eventArgs.get_node().get_value();   
}

function TaxonomyView11_onNodeSelect(sender, eventArgs)
{ 
    IndicatorFilterBox.set_text(eventArgs.get_node().get_text());
    IndicatorFilterBox.collapse();
    document.getElementById('<%= hdnTaxonomy11Selection.ClientID %>').value = eventArgs.get_node().get_value();
}

function YAxis1_onChange(requireCallback)
{  
    var YAxis1= document.getElementById('<%=hdnYAxis1.ClientID%>');
    YAxis1.value = $("#<%= YAxis1DropDown.ClientID%>").val();
    //To get AxisCriteria
    var Yaxis1Text= document.getElementById('<%=hdnYAxis1Text.ClientID%>');    
    Yaxis1Text.value = $("#<%= YAxis1DropDown.ClientID%> option:selected").text();    
    $("#PlotChartMsg").show();
    if(requireCallback)
    {    
        var list=$("#hdnControlsList").val();
    
        var stringSplitArray = list.split(";");
        var filterData = "";
        var filterDataText = "";
        var j = 0;
        for(j = 0; j < stringSplitArray.length-1; j++)
        {   
            var taxonomyhiddenFields = stringSplitArray[j].split("|");
            
            var taxonomyHiddenFieldsSingleText = taxonomyhiddenFields[1].split("@");
            
            var taxonomyHiddenFieldsMultipleText = taxonomyhiddenFields[2].split("@");            
         
            filterData = filterData + 
                taxonomyhiddenFields[0] + 
                "|" + 
                $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
                "|" + 
                $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
                ";";
            if(taxonomyhiddenFields[0] == "999")
            {
                filterDataText =  filterDataText+
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
                        ";";               
            }
            else
            {
                  filterDataText = filterDataText +
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[1]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[1]).val()+
                        ";";
            }          
        }   
        
        ChartFilterCallback.callback(YAxis1.value, 
                $("#<%= YAxis2DropDown.ClientID%>").val(),
                filterData,
                filterDataText); 
    }
}

function YAxis2_onChange(requireCallback)
{    
    ToggleIndicatorSelection();
    $("#PlotChartMsg").show();
    var YAxis2= document.getElementById('<%=hdnYAxis2.ClientID%>');
    YAxis2.value = $("#<%= YAxis2DropDown.ClientID%>").val();    
    //To get AxisCriteria
    var Yaxis2Text= document.getElementById('<%=hdnYAxis2Text.ClientID%>');    
    Yaxis2Text.value = $("#<%= YAxis2DropDown.ClientID%> option:selected").text();
    
    if(requireCallback)
    {      
        var list=$("#hdnControlsList").val();
    
        var stringSplitArray = list.split(";");
        var filterData = "";
        var filterDataText = "";
        var j = 0;
        for(j = 0; j < stringSplitArray.length-1; j++)
        {   
            var taxonomyhiddenFields = stringSplitArray[j].split("|");
            
            var taxonomyHiddenFieldsSingleText = taxonomyhiddenFields[1].split("@");
            
            var taxonomyHiddenFieldsMultipleText = taxonomyhiddenFields[2].split("@");            
         
            filterData = filterData + 
                taxonomyhiddenFields[0] + 
                "|" + 
                $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
                "|" + 
                $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
                ";";
            if(taxonomyhiddenFields[0] == "999")
            {
                filterDataText =  filterDataText+
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
                        ";";               
            }
            else
            {
                  filterDataText = filterDataText +
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[1]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[1]).val()+
                        ";";
            }          
        }   
        
        ChartFilterCallback.callback($("#<%= YAxis1DropDown.ClientID%>").val(), 
                YAxis2.value,
                filterData,
                filterDataText); 
    }
}

function ToggleIndicatorSelection()
{
    if($("#<%= YAxis2DropDown.ClientID%>").val()=="11")
    {  
        $("#divIndicators").show();
    }
    else
    {
        $("#divIndicators").hide();
    }  
}

function removeMacroeconomicSelection(box)
{
    var i;
    for(i=box.options.length-1;i>=0;i--)
    {
        if(box.options[i].value=="11")
        {
            box.remove(i);
        }
    }
}

function PlotChart()
{  
    $("#PlotChartMsg").hide();
    var xAxisSelected = $("#<%= hdnXAxis.ClientID%>").val();    
    var y1AxisSelected = $("#<%= hdnYAxis1.ClientID%>").val();
    var y2AxisSelected = $("#<%= hdnYAxis2.ClientID%>").val();
    //To maintain AxisCriteria
    var xAxisSelectedText = $("#<%= hdnXAxisText.ClientID%>").val();     
    var y1AxisSelectedText = $("#<%= hdnYAxis1Text.ClientID%>").val();   
    var y2AxisSelectedText= $("#<%= hdnYAxis2Text.ClientID%>").val();    
    
    var indictorsSelected = $("#<%= hdnTaxonomy11Selection.ClientID%>").val();
    var chartType = $("#<%= hiddenChartType.ClientID%>").val();  
    var list=$("#hdnControlsList").val();  
    var stringSplitArray = list.split(";");
    var filterData = "";
    var filterDataText = "";
    var j = 0;
    for(j = 0; j < stringSplitArray.length-1; j++)
    {  
        var taxonomyhiddenFields = stringSplitArray[j].split("|");
        var taxonomyHiddenFieldsSingleText = taxonomyhiddenFields[1].split("@");   
        var taxonomyHiddenFieldsMultipleText = taxonomyhiddenFields[2].split("@");
//        filterData = filterData + 
//            taxonomyhiddenFields[0] + 
//            "|" + 
//            $("#"+taxonomyhiddenFields[1]).val()+ 
//            "|" + 
//            $("#"+taxonomyhiddenFields[2]).val()+
//            ";";
     
         filterData = filterData + 
            taxonomyhiddenFields[0] + 
            "|" + 
            $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
            "|" + 
            $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
            ";";
            
            if(taxonomyhiddenFields[0] == "999")
            {
                filterDataText =  filterDataText+
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[0]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[0]).val()+
                        ";";                
            }
            else
            {
                  filterDataText = filterDataText +
                        taxonomyhiddenFields[3] + 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsSingleText[1]).val()+ 
                        "|" + 
                        $("#"+taxonomyHiddenFieldsMultipleText[1]).val()+
                        ";";
            }
              
    }   
    ChartsCallback.callback(xAxisSelected,
            y1AxisSelected,
            y2AxisSelected,
            filterData,
            chartType,
            indictorsSelected,
            xAxisSelectedText,
            y1AxisSelectedText,
            y2AxisSelectedText,
            filterDataText);
    //alert("callback complete");
    return false; 
}


</script>

<div >
<h2><div id="PlotChartMsg" class="PlotChartMessage">*Please click on PLOT CHART to update</div><asp:Label ID="ChartTitle" runat="server" Text="Dual Y-Axis Chart"></asp:Label></h2>
</div>
<div class="analyzeresults_col1">
    <div style="padding-bottom:20px;"> <%-- style="border-bottom: 1px #D9E1EC solid; padding-bottom:15px;"--%>
    <div id="xaxisDropdownDiv" runat="server" style="float:left;">  
        X Axis <asp:DropDownList ID="xAxisDropdown" runat="server" 
                  Width="110px" Font-Size="11px"></asp:DropDownList>
                  &nbsp;&nbsp;&nbsp;&nbsp;</div>
        <div id="yaxisDropdownDiv" runat="server" style="float:left;">          
        Y Axis1 <asp:DropDownList ID="YAxis1DropDown" runat="server" 
                  Width="110px" Font-Size="11px"></asp:DropDownList>            
                  &nbsp;&nbsp;&nbsp;&nbsp; </div>
        <div id="Div1" runat="server" style="float:left;">  
        Y Axis2 <asp:DropDownList ID="YAxis2DropDown" runat="server" 
                   Width="110px" Font-Size="11px"></asp:DropDownList>  
                   </div> 
    </div>
         
    <div>
         <dotnetCHARTING:Chart ID="TwoDChart" runat="server" 
            Visible="false">              
        </dotnetCHARTING:Chart>    
        
        <ComponentArt:CallBack ID="ChartsCallback" runat="server"
                PostState="true"
                OnCallback="Chart_Callback">
           <Content> 
            <asp:Label runat="server" ID="ErrorMessagelabel" ForeColor="red"></asp:Label>               
            <ComponentArt:Chart id="ThreeDChart" runat="server" 
                BackColor="White" 
                RenderingPrecision="0.1"            
                SaveImageOnDisk="true" 
                Visible="false">
            </ComponentArt:Chart>
            <asp:Image ID="ChartImg" runat="server" Visible="true" />
            <asp:Label id="ImageMapLabel" runat="server"/>
             <br />
            <div id="divChart" runat="server" 
                 style="overflow:auto; width:520px; height:150px; scrollbar-face-color: #BFC4D1;
                scrollbar-shadow-color: #FFFFFF;
                scrollbar-highlight-color: #FFFFFF;
                scrollbar-3dlight-color: #FFFFFF;
                scrollbar-darkshadow-color: #FFFFFF;
                scrollbar-track-color: #FFFFFF;
                scrollbar-arrow-color: #FFFFFF;">                    
                <asp:Image ID="LegendImg" runat="server" Visible="true" />            
                </div>
                        
            <asp:HiddenField ID="hdnTaxonomy11Selection" runat="server" />
            <asp:HiddenField ID="hdnImageurl" runat="server" Value=""/>
            <asp:HiddenField ID="hdnImageType" runat="server" Value=""/>
            <asp:HiddenField ID="hdnChartID" runat="server" /> 
            <asp:HiddenField ID="hdnChartCriteria" runat="server" Value=""/>
            <asp:HiddenField ID="hdnLegendUrl" runat="server" Value="" />  
            
           </Content>
           <LoadingPanelClientTemplate>
                <table class="loadingpanel" width="100%" style="height:300" cellspacing="0" cellpadding="0" border="0">
                  <tr>
                    <td align="center">
                    <table cellspacing="0" cellpadding="0" border="0">
                    <tr>
                      <td colspan="2">
                      <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="../assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                      </td>
                    </tr>
                    </table>
                    </td>
                  </tr>
                  </table>
            </LoadingPanelClientTemplate>
        </ComponentArt:CallBack> 
    </div>                   
</div>      
<div class="analyzeresults_col2" id="analyzeresults_filter">
   
    <h1>FILTER OPTIONS</h1>
    <div style="padding-bottom:15px">
         <div style="float:right;margin-bottom:4px;">   
                   <ComponentArt:ComboBox ID="ChartTypesDropDown" runat="server"
                                  KeyboardEnabled="false"
                                  AutoFilter="false"
                                  AutoHighlight="false"
                                  AutoComplete="false"
                                  CssClass="comboBox"                                            
                                  TextBoxCssClass="comboTextBox"
                                  DropDownCssClass="comboDropDown"
                                  ItemCssClass="comboItem"                      
                                  SelectedItemCssClass="comboItemHover"
                                  DropHoverImageUrl="../assets/images/drop_hover.gif"
                                  DropImageUrl="../assets/images/drop.gif"
                                  Width="123"
                                  DropDownHeight="120"
                                  DropDownWidth="120"
                                  Visible="true"
                                  >
                    <DropDownContent>
                         <ComponentArt:TreeView id="ChartTypesTreeView" Height="115" Width="120"
                                    DragAndDropEnabled="false"
                                    NodeEditingEnabled="false"
                                    KeyboardEnabled="true"
                                    CssClass="TreeView"
                                    NodeCssClass="TreeNode"                                                        
                                    NodeEditCssClass="NodeEdit"
                                    SelectedNodeCssClass="NodeSelected"
                                    LineImageWidth="19"
                                    LineImageHeight="16"
                                    DefaultImageWidth="16"
                                    DefaultImageHeight="16"
                                    ItemSpacing="0"
                                    NodeLabelPadding="3"
                                    ImagesBaseUrl="../assets/tvlines/"
                                    LineImagesFolderUrl="../assets/images/tvlines/"                                   
                                    EnableViewState="false"
                                    runat="server" 
                                    > 
                         <ClientEvents>
                           <NodeSelect EventHandler="ChartTypes_onNodeSelect" />
                         </ClientEvents>
                         </ComponentArt:TreeView>
                    </DropDownContent>
                  </ComponentArt:ComboBox>          
            </div>  
         <div style="float:left;">Chart&nbsp;Type: &nbsp;</div>
                                   
      </div>  
      
      <div id="divIndicators" style="padding-bottom:15px">        
              <div style="float:right;margin-bottom:4px;">
              <ComponentArt:ComboBox id="IndicatorFilterBox" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="120"
            DropDownHeight="220"
            DropDownWidth="217" >
          <DropdownContent>
              <ComponentArt:TreeView id="IndicatorTree" Height="220" Width="217"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"
                ItemSpacing="0"
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"                
                EnableViewState="false"                           
                runat="server" >
              <ClientEvents>
                <NodeSelect EventHandler="TaxonomyView11_onNodeSelect" />
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
        </ComponentArt:ComboBox>
              </div>
       <div style="float:left;">Indicators:
       </div>
       
       </div> 
      
      <ComponentArt:CallBack ID="ChartFilterCallback" runat="server"
        OnCallback="UpdateChartfilters_Callback"> 
        <Content>
            <GPMT:DynamicChartFilters ID="ChartFilters1" runat="server" />
        </Content>
        <LoadingPanelClientTemplate>
            <table class="loadingpanel" width="100%" style="height:150" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td align="center">
                <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td colspan="2">
                  <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="../assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                  </td>
                </tr>
                </table>
                </td>
              </tr>
              </table>
        </LoadingPanelClientTemplate>
        <ClientEvents>
            <CallbackComplete EventHandler="ChartFilterCallback_onCallbackComplete" />
        </ClientEvents>        
    </ComponentArt:CallBack> 
           
    <div class="button_right_chart" style="width:120px">
        <asp:LinkButton ID="PlotChart"  runat="server" 
            Text="Plot chart" 
            OnClientClick="javascript:PlotChart(); return false;">
        </asp:LinkButton>
    </div>    
    <br /><br />
      <h1>MY TOOLS</h1>
      <h2 class="tool_excel"><asp:LinkButton ID="lnkExcel" Text="Extract to Excel" runat="server" OnClick="lnkExcel_Click" ToolTip="extract chart data to excel"/></h2>
      <h2 class="tool_word"><asp:LinkButton ID="LinkButton1" Text="Extract to Word" runat="server" OnClick="lnkWord_Click" ToolTip="extract chart data to word"/></h2>
      <h2 class="tool_powerpoint"><asp:LinkButton ID="lnkPPT" Text="Extract to PowerPoint" runat="server" OnClick="lnkPPT_Click" ToolTip="extract chart data to power point"/></h2>               
<br /><br /><br /><br />
</div>
  
<asp:HiddenField ID="hiddenChartType" runat="server" />
<asp:HiddenField ID="hdnXAxis" runat="server" />
<asp:HiddenField ID="hdnYAxis1" runat="server" />
<asp:HiddenField ID="hdnYAxis2" runat="server" />
<asp:HiddenField ID="hdnXAxisText" runat="server" />
<asp:HiddenField ID="hdnYAxis1Text" runat="server" />
<asp:HiddenField ID="hdnYAxis2Text" runat="server" />
</asp:Content>
