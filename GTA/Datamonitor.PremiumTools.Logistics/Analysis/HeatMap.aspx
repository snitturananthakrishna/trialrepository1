<%@ Page Language="C#" 
         MasterPageFile="~/MasterPages/AnalysisMaster.Master" 
         AutoEventWireup="true" 
         CodeBehind="HeatMap.aspx.cs" 
         Inherits="Datamonitor.PremiumTools.Generic.Analysis.HeatMap"
         Theme="NormalTree"  %>
<%@ Register Src="../Controls/DynamicChartfilters.ascx" 
             TagName="DynamicChartfilters"
             TagPrefix="GMPT" %> 
<%@ Register Assembly="ComponentArt.Web.UI"  
             Namespace="ComponentArt.Web.UI" 
             TagPrefix="ComponentArt" %>          
<%@ Register Src="../Controls/ChartFilters.ascx"
             TagName="ChartFilters" 
             TagPrefix="GPMT" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">

<script type="text/javascript" language="javascript">

function ShowHideFilterOptions(){
  
  var selectedXAxis = 1;
      
  $("#filterContainer > div").show();    
 //$("#CharttypeContainer > div").show();
  
  $("div[id*='SingleFilterDiv']").show(); 
  $("div[id*='MultipleFilterDiv']").hide(); 
  //$("div[id*='MultipleFilterDiv']").show();        
  $("div[id$='SingleFilterDiv"+selectedXAxis+"']").hide(); 
  $("div[id$='MultipleFilterDiv"+selectedXAxis+"']").show();  
}

$(document).ready(function() {
    setTimeout("ShowHideFilterOptions()",500);
});

function ChartFilterCallback_onCallbackComplete(sender, eventArgs)
{
   ShowHideFilterOptions();   
}
function YAxis_onChange()
{
    var YAxisDropdown = document.getElementById('<%=MeasureDropDown.ClientID %>');
    ChartFilterCallback.callback(YAxisDropdown.value); 
    ShowHideFilterOptions();
}


</script> 
<div id="BackToChartWizardDiv" runat="server"  style="float:right;margin-top:-13px"><a href="chartwizard.aspx">Back to Chart Wizard</a></div>
<h2> <asp:Label ID="ChartTitle" runat="server" Text="Heat Map"></asp:Label></h2>   
 <div class="analyzeresults_col1">
    <div style="border-bottom: 1px #D9E1EC solid; padding-bottom:15px;"> 
        <asp:Label ID="Measurelbl" runat="server" Text="Measure"></asp:Label>
        <asp:DropDownList ID="MeasureDropDown" runat="server" Width="110px" Font-Size="11px" 
            AutoPostBack="true">
        </asp:DropDownList>&nbsp;&nbsp;
         Map Type <asp:DropDownList ID="MapTypeDropdown"  runat="server" Width="110px" Font-Size="11px">
                <asp:ListItem Value="World Map">World</asp:ListItem>
                <asp:ListItem Value="Europe Map">Europe</asp:ListItem>
                <asp:ListItem Value="North America Map">North America</asp:ListItem>
                <asp:ListItem Value="Central America Map">Central America</asp:ListItem>
                <asp:ListItem Value="South America Map">South America</asp:ListItem>
                <asp:ListItem Value="Asia Map">Asia</asp:ListItem>
                <%--<asp:ListItem Value="oceania">Oceania</asp:ListItem>--%>
                <asp:ListItem Value="Middle East Map">Middle East</asp:ListItem>
                <asp:ListItem Value="Africa Map">Africa</asp:ListItem>
         </asp:DropDownList>&nbsp;&nbsp;
         Plot by 
         <asp:RadioButton ID="rbPlotByValue" runat="server" Text="Value" GroupName="PlotBy" />
         <asp:RadioButton ID="rbPlotByRank" runat="server" Text="Rank" Checked="true" GroupName="PlotBy" />
         
    </div>              

<%-- <ComponentArt:CallBack ID="heatmapCallback" runat="server">
 <Content>
  <div id="Div1">   
    </div>
     <asp:Literal ID="WorldPopulationMap" runat="server"></asp:Literal>
 </Content>
 <LoadingPanelClientTemplate> 
     <table class="loadingpanel" width="100%" style="height:150" cellspacing="0" cellpadding="0" border="0">
          <tr>
            <td align="center">
            <table cellspacing="0" cellpadding="0" border="0">
            <tr>
              <td colspan="2">
              <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="../assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
              </td>
            </tr>
            </table>
            </td>
          </tr>
       </table>
   </LoadingPanelClientTemplate>
 </ComponentArt:CallBack>--%>
   <div id="mapidDiv">   
    </div>
     <asp:Literal ID="WorldPopulationMap" runat="server"></asp:Literal>
	   
</div>
   <div class="analyzeresults_col2" id="analyzeresults_filter"> 
     <h1>FILTER OPTIONS</h1>
    <div id="filterContainer">
   <%-- <ComponentArt:CallBack ID="ChartFilterCallback" runat="server" OnCallback="UpdateChartfilters_Callback">
       <Content>--%>
         <GMPT:DynamicChartfilters ID="ChartFilters1" runat="server" />
       <%--</Content>
        <LoadingPanelClientTemplate>
            <table class="loadingpanel" width="100%" style="height:150" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td align="center">
                <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td colspan="2">
                  <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="../assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                  </td>
                </tr>
                </table>
                </td>
              </tr>
              </table>
        </LoadingPanelClientTemplate>
        <ClientEvents>
            <CallbackComplete EventHandler="ChartFilterCallback_onCallbackComplete" />
        </ClientEvents>
    </ComponentArt:CallBack> --%>     
    </div> 
      
  <div class="button_right_chart" style="width:120px">
        <asp:LinkButton ID="PlotChart"  runat="server" 
            Text="Plot chart" 
            OnClick="PlotChart_Click">
        </asp:LinkButton>  <%--OnClientClick="javascript:PlotChart(); return false;"--%>      
   </div>
    
    <br /><br />
       <h1>MY TOOLS</h1>       
       <h2 class="tool_excel"><a href="javascript:SaveFusionMap('excel');" title="extract chart data to excel" id="lnkExcel">Export To Excel</a></h2>
       <h2 class="tool_word"><a href="#" onclick="javascript:SaveFusionMap('word');" title="extract chart data to word" id="lnkWord">Export To Word</a></h2>
       <h2 class="tool_powerpoint"><a href="javascript:SaveFusionMap('powerpoint');" title="extract chart data to power point" id="lnkPowerpoint">Export To PowerPoint</a></h2>
       
<br /><br /><br /><br />
</div>

<asp:HiddenField ID="hdnMeasure" runat="server" />
<asp:HiddenField id="hdnMap" runat="server" Value=""></asp:HiddenField> 
<asp:HiddenField ID="hdnXmlData" runat="server" />
<asp:HiddenField ID="hdnIndicatorText" runat="server" />
</asp:Content>
