<%@ Page Language="C#" 
         MasterPageFile="~/MasterPages/AnalysisMaster.Master" 
         AutoEventWireup="true" 
         CodeBehind="BubbleChart1.aspx.cs" 
         Inherits="Datamonitor.PremiumTools.Logistics.Analysis.BubbleChart1" 
         Theme="NormalTree"%>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<%@ Register Src="../Controls/DynamicChartfilters.ascx" TagName="DynamicChartfilters" TagPrefix="GMPT" %>
<%@ Register Assembly="dotnetCHARTING" 
        Namespace="dotnetCHARTING" 
        TagPrefix="dotnetCHARTING" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">
<style type="text/css"> 
.rightdiv
{
    float:right;
    margin-bottom:4px;
}
.leftdiv
{
    float:left;
    width:60px;
}
input[type=checkbox] 
{
	width:5px;
	margin-left:-65px;
	margin-right:-65px;
	padding:0px;
	text-align:left;
}

</style>
<script type="text/javascript" language="javascript">

function XAxisTree_onNodeSelect(sender, eventArgs)
{  
    $("#PlotChartMsg").show();
    XAxisCombo.set_text(eventArgs.get_node().get_value());
    XAxisCombo.collapse();
    document.getElementById('<%=hdnXAxis.ClientID %>').value = eventArgs.get_node().get_id();  
    document.getElementById('<%=hdnXAxisText.ClientID %>').value = eventArgs.get_node().get_value(); 
    document.getElementById('<%=hdnIndicatorText.ClientID %>').value = XAxisCombo.get_text()+"; "+ YAxisCombo.get_text()+";"+BubbleCombo.get_text();       
}

function YAxisTree_onNodeSelect(sender, eventArgs)
{    
    $("#PlotChartMsg").show();
    YAxisCombo.set_text(eventArgs.get_node().get_value());
    YAxisCombo.collapse();
    document.getElementById('<%=hdnYAxis.ClientID %>').value = eventArgs.get_node().get_id();  
    document.getElementById('<%=hdnYAxisText.ClientID %>').value = eventArgs.get_node().get_value();
    document.getElementById('<%=hdnIndicatorText.ClientID %>').value = XAxisCombo.get_text()+"; "+ YAxisCombo.get_text()+";"+BubbleCombo.get_text();         
}

function BubbleTree_onNodeSelect(sender, eventArgs)
{      
    $("#PlotChartMsg").show();
    BubbleCombo.set_text(eventArgs.get_node().get_value());
    BubbleCombo.collapse();
    document.getElementById('<%=hdnBubble.ClientID %>').value = eventArgs.get_node().get_id();
    document.getElementById('<%=hdnBubbleText.ClientID %>').value = eventArgs.get_node().get_text();
    document.getElementById('<%=hdnIndicatorText.ClientID %>').value = XAxisCombo.get_text()+"; "+ YAxisCombo.get_text()+";"+BubbleCombo.get_text();         
}

function Years_onChange()
{    
    $("#PlotChartMsg").show();
    var StartYearDropdown = document.getElementById('<%=StartYearDropDown.ClientID %>');
    var StartYearHdn = document.getElementById('<%=hdnStartYear.ClientID %>');
    StartYearHdn.value = StartYearDropdown.value;   
}

function CountryTree_onNodeCheckChange(sender,eventArgs)
{
   $("#PlotChartMsg").show();
   var hdnControl=document.getElementById('<%= hdnCountry.ClientID %>');    
   OnMultipleTreeCheckChange(eventArgs.get_node(),hdnControl,CountryCombo);
   document.getElementById('<%=hdnCountryText.ClientID %>').value = CountryCombo.get_text();
}

function PlotChart()
{
    $("#PlotChartMsg").hide();
    var xAxisIndicator = $("#<%= hdnXAxis.ClientID%>").val();   
    var yAxisIndicator= $("#<%= hdnYAxis.ClientID%>").val();  
    var bubbleIndicator = $("#<%= hdnBubble.ClientID%>").val();
     
    var SelectedXAxisText = $("#<%= hdnXAxisText.ClientID%>").val(); 
    var SelectedYAxisText = $("#<%= hdnYAxisText.ClientID%>").val();
    var SelectedBubbleText = $("#<%= hdnBubbleText.ClientID%>").val();        
    var CountrySelected = $("#<%= hdnCountry.ClientID%>").val();   
    var startYear=$("#<%= hdnStartYear.ClientID%>").val();
    
    ChartsCallback.callback(CountrySelected,
        xAxisIndicator,
        yAxisIndicator,
        bubbleIndicator,        
        startYear,
        SelectedXAxisText,
        SelectedYAxisText,
        SelectedBubbleText);
  
    return false;   
}

</script>


<div >
<h2><div id="PlotChartMsg" class="PlotChartMessage">*Please click on PLOT CHART to update</div><asp:Label ID="ChartTitle" runat="server" Text="Indicator Comparison"></asp:Label></h2>
</div>

<div class="analyzeresults_col1">
    <div style="border-bottom:solid 1px lightgrey;"> 
    <table> 
    <tr>
    <td>XAxis</td>    
    <td> 
         <ComponentArt:ComboBox id="XAxisCombo" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="203"
            DropDownHeight="220"
            DropDownWidth="200" >
          <DropdownContent>
              <ComponentArt:TreeView id="XAxisTree" Height="220" Width="200"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"
                ItemSpacing="0"
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"
                ShowLines="true"
                EnableViewState="false"                           
                runat="server" >
              <ClientEvents>                 
                   <NodeSelect EventHandler="XAxisTree_onNodeSelect" />
                   <%--<NodeCheckChange EventHandler="XAxisTree_onNodeCheckChange" />--%>
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
        </ComponentArt:ComboBox></td>    
    <td>&nbsp;&nbsp;YAxis</td>    
    <td> 
        <ComponentArt:ComboBox id="YAxisCombo" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="203"
            DropDownHeight="220"
            DropDownWidth="200" >
          <DropdownContent>
              <ComponentArt:TreeView id="YAxisTree" Height="220" Width="200"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"
                ItemSpacing="0"
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"
                ShowLines="true"
                EnableViewState="false"                           
                runat="server" >
              <ClientEvents>  
               <NodeSelect EventHandler="YAxisTree_onNodeSelect" />
               <%-- <NodeCheckChange EventHandler="YAxisTree_onNodeCheckChange" />--%>
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
        </ComponentArt:ComboBox></td>
    </tr>
    <tr>
    <td>Bubble</td>    
    <td> <ComponentArt:ComboBox id="BubbleCombo" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="203"
            DropDownHeight="220"
            DropDownWidth="200" >
          <DropdownContent>
              <ComponentArt:TreeView id="BubbleTree" Height="220" Width="200"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"
                ItemSpacing="0"
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"
                ShowLines="true"
                EnableViewState="false"                           
                runat="server" >
              <ClientEvents>
               <NodeSelect EventHandler="BubbleTree_onNodeSelect" />
              <%-- <NodeCheckChange EventHandler="BubbleTree_onNodeCheckChange" />--%>
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
        </ComponentArt:ComboBox></td>    
    <td>&nbsp;&nbsp;Country</td>    
    <td> 
         <ComponentArt:ComboBox id="CountryCombo" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="203"
            DropDownHeight="220"
            DropDownWidth="200" >
          <DropdownContent>
              <ComponentArt:TreeView id="CountryTree" Height="220" Width="200"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"
                ItemSpacing="0"
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"
               ShowLines="false"
                EnableViewState="false"                           
                runat="server" >
              <ClientEvents>
                <NodeCheckChange EventHandler="CountryTree_onNodeCheckChange" />
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
          
        </ComponentArt:ComboBox></td>
    <td><GMPT:CountryFilter id="CountryFilter1" runat="server"></GMPT:CountryFilter></td></tr>
    </table>                   
    </div>
    <br />
    <div>
     <dotnetCHARTING:Chart ID="TwoDChart" runat="server" DisableBrowserCache="false"
                GanttCompleteHatchStyle="zigZag" 
                CleanupPeriod="20"
                Visible="false">                                           
            </dotnetCHARTING:Chart> 
      
      <ComponentArt:CallBack ID="ChartsCallback" runat="server"
         PostState ="true"
         OnCallback="Chart_Callback">
         <Content>
            <asp:Image ID="ChartImg" runat="server" Visible="true" />
                 <asp:Label id="ImageMapLabel" runat="server"/>
                      <br />
                <div id="divChart" runat="server" 
                    style=" overflow:auto; width:520px; height:150px; scrollbar-face-color: #BFC4D1;
                    scrollbar-shadow-color: #FFFFFF;
                    scrollbar-highlight-color: #FFFFFF;
                    scrollbar-3dlight-color: #FFFFFF;
                    scrollbar-darkshadow-color: #FFFFFF;
                    scrollbar-track-color: #FFFFFF;
                    scrollbar-arrow-color: #FFFFFF;">                    
                    <asp:Image ID="LegendImg" runat="server" Visible="true" />            
                    </div>
                     <asp:HiddenField ID="hdnImageurl" runat="server" Value="" /> 
                     <asp:HiddenField ID="hdnLegendUrl" runat="server" Value="" /> 
         </Content>
         <LoadingPanelClientTemplate>
            <table class="loadingpanel" width="100%" style="height:300" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td align="center">
                <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td colspan="2">
                  <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="../assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                  </td>
                </tr>
                </table>
                </td>
              </tr>
              </table>
        </LoadingPanelClientTemplate>
         </ComponentArt:CallBack>
               
    </div>    
</div>

 <div class="analyzeresults_col2" id="analyzeresults_filter"> 
    <div id="filterContainer"> 
        <div id="StartYearDiv" runat="server">
            Year: &nbsp;<asp:DropDownList ID="StartYearDropDown" runat="server"                     
                    Width="70px" 
                    Font-Size="11px"></asp:DropDownList>            
            
        </div> 
        </div><br />    
    <div class="button_right_chart" style="width:120px">
        <asp:LinkButton ID="PlotChart"  runat="server" 
            Text="Plot chart" 
            OnClientClick="javascript:return PlotChart(); " ToolTip="Update Chart">
        </asp:LinkButton>
    </div>
    
    <br /><br />     
       <h1>MY TOOLS</h1>
       <h2 class="tool_excel"><asp:LinkButton ID="lnkExcel" Text="Extract to Excel" runat="server" OnClick="lnkExcel_Click" ToolTip="extract chart data to excel"/></h2>
       <h2 class="tool_word"><asp:LinkButton ID="lnkWord" Text="Extract to Word" runat="server" OnClick="lnkWord_Click"   ToolTip="extract chart data to word"/></h2>
       <h2 class="tool_powerpoint"><asp:LinkButton ID="lnkPPT" Text="Extract to PowerPoint" runat="server" OnClick="lnkPPT_Click"  ToolTip="extract chart data to power point"/></h2>               
<br /><br /><br /><br />

</div>  

<asp:HiddenField ID="hiddenChartType" runat="server" />
<asp:HiddenField ID="hdnXAxis" runat="server" />
<asp:HiddenField ID="hdnYAxis" runat="server" />
<asp:HiddenField ID="hdnBubble" runat="server" />
<asp:HiddenField ID="hdnIndicatorText" runat="server" />

<asp:HiddenField ID="hdnCountry" runat="server" />
<asp:HiddenField ID="hdnCountryText" runat="server" />
<asp:HiddenField ID="hdnIndicators" runat="server" />
<asp:HiddenField ID="hdnStartYear" runat="server" />
<asp:HiddenField ID="hdnEndYear" runat="server" />
<asp:HiddenField ID="hdnXAxisText" runat="server" />
<asp:HiddenField ID="hdnYAxisText" runat="server" />
<asp:HiddenField ID="hdnBubbleText" runat="server" />
</asp:Content>
