using System;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using System.Drawing;
using System.IO;
using Aspose.Cells;
using Aspose.Slides;
using Aspose.Words;
using CArt = ComponentArt.Web.UI;

namespace Datamonitor.PremiumTools.Generic.Analysis
{
    public partial class PieChart : AuthenticationBasePage
    {
        //PRIVATE MEMBERS
        private string AxisCriteria = string.Empty;         
        private bool IsSeriesVisible = true;
        private bool IsMeasureVisible = true;
        private bool IsPredefinedChart = false;
        
        // EVENT OVERRIDES
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack &&
                 !ChartFilterCallback.IsCallback &&
                 !ChartsCallback.IsCallback)
            {
                //Bind the axis selection dropdowns.
                BindAxisDropdownsData();
                BindChartTypeDropdownData();
                SelectDefaultchartType();

                xAxisDropdown.Attributes.Add("onchange", "javascript:XAxis_onChange();");
                MeasureDropDown.Attributes.Add("onchange", "javascript:YAxis_onChange(true);");
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Analysis";

            //Hide the 'back to chart wizard' link when loading predefined chart
            BackToChartWizardDiv.Visible = (Request.QueryString["id"] == null);

            if (!ChartFilterCallback.IsCallback && !ChartsCallback.IsCallback)
            {
                ChartFilters1.YAxis = hdnMeasure.Value.Length > 0 ?
                       Convert.ToString(System.Math.Abs(Convert.ToInt32(hdnMeasure.Value))) :
                       "";
                
                if (!IsPostBack)
                {
                    ChartFilters1.LoadData();
                }
            }        
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (!IsPostBack && !ChartsCallback.IsCallback)
            {
                PlotChart_Click(null, null);
            }
        }


        // PRIVATE METHODS (initial population)
        private void BindChartTypeDropdownData()
        {
            CArt.TreeViewNode newNode;

            //newNode = new CArt.TreeViewNode();
            //newNode.Text = "2D";
            //newNode.Value = "2D";
            //ChartTypesTreeView.Nodes.Add(newNode);

            newNode = new CArt.TreeViewNode();
            newNode.Text = "Pie";
            newNode.Value = "Pie";
            ChartTypesTreeView.Nodes.Add(newNode);

            newNode = new CArt.TreeViewNode();
            newNode.Text= "Donut";
            newNode.Value = "Donut";
            ChartTypesTreeView.Nodes.Add(newNode);
        }
       
        private void SelectDefaultchartType()
        {
            CArt.TreeViewNode LeafNode = null;
            foreach (CArt.TreeViewNode tnode in ChartTypesTreeView.Nodes)
            {
                if (tnode.Nodes.Count > 0)
                {
                    LeafNode = GetFirstLeafNode(tnode);
                    break;
                }
                else
                {
                    LeafNode = tnode;
                    break;
                }
            }
            if (LeafNode != null)
            {
                ChartTypesDropDown.Text = LeafNode.Text;
                hiddenChartType.Value = LeafNode.Value;
                ChartTypesTreeView.SelectedNode = LeafNode;
            }
        }
       
        private CArt.TreeViewNode GetFirstLeafNode(CArt.TreeViewNode parentNode)
        {
            foreach (CArt.TreeViewNode tnode in parentNode.Nodes)
            {
                if (tnode.Nodes.Count > 0)
                {
                    return GetFirstLeafNode(tnode);
                }
                else
                {
                    return tnode;
                }
            }
            return parentNode;
        }
       
        private void BindAxisDropdownsData()
        {
            XmlDocument document = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            string xPathExpression;
            string taxonomtypes;
            string includeYears;
            string from;

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                DataSet ds = SqlDataService.GetPredefinedChartMetaData(Convert.ToInt32(Request.QueryString["id"]));
                IsPredefinedChart = true;
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    string filterXml = ds.Tables[0].Rows[0]["Selections"].ToString();
                    document.LoadXml(filterXml);
                    IsSeriesVisible = Convert.ToBoolean(document.SelectSingleNode("//Series").Attributes["Visible"].Value);
                    IsMeasureVisible = Convert.ToBoolean(document.SelectSingleNode("//Measure").Attributes["Visible"].Value);
                    ChartTitle.Text = document.SelectSingleNode("//Title").InnerText;
                    
                }
            }


            if (IsSeriesVisible)
            {
                doc.Load(Server.MapPath(GlobalSettings.ChartFilterOptionsConfigPath));
                xPathExpression = "//Taxonomy[@type='XAxis']";
                XmlElement xaxisItem = (XmlElement)doc.SelectSingleNode(xPathExpression);
                //Get and bind data to X Axis drop down
                taxonomtypes = xaxisItem.Attributes["taxonomtypes"].Value;
                from = xaxisItem.Attributes["from"].Value;
                includeYears = xaxisItem.Attributes["includeYears"].Value;
                DataSet XAxisData = new DataSet();
                if (from.Equals("taxonomtypes"))
                {
                    XAxisData = AnalaysisData.GetAxisData(taxonomtypes, includeYears.Equals("true"));
                }
                else if (from.Equals("dataset"))
                {
                    XAxisData = SqlDataService.GetTaxonomyData(Convert.ToInt32(taxonomtypes));
                }

                xAxisDropdown.DataSource = XAxisData;
                xAxisDropdown.DataTextField = XAxisData.Tables[0].Columns["Name"].ToString();
                xAxisDropdown.DataValueField = XAxisData.Tables[0].Columns["ID"].ToString();
                xAxisDropdown.DataBind();

                try
                {
                    xAxisDropdown.SelectedValue = ConfigurationManager.AppSettings.Get("PieChartDefaultXAxisType");
                    hdnSeries.Value = ConfigurationManager.AppSettings.Get("PieChartDefaultXAxisType");
                    hdnSeriesText.Value = xAxisDropdown.SelectedItem.Text.ToString();
                }
                catch (Exception ex) { }

            }
            else
            {
                xaxisDropdownDiv.Visible = false;
                hdnSeries.Value = document.SelectSingleNode("//Series").Attributes["SelectedValue"].Value;
                hdnSeriesText.Value = document.SelectSingleNode("//Series").Attributes["SelectedText"].Value;
                xAxisDropdown.Items.Add(new ListItem(hdnSeriesText.Value, hdnSeries.Value));
                xAxisDropdown.SelectedValue = hdnSeries.Value;
            }

            if (!IsPredefinedChart)
            {
                //Get Y-Axis data.
                string SelectionsXML = GetFilterCriteriaFromSession();

                DataSet YAxisDataSet = SqlDataService.GetYAxisTaxonomyByCriteria(SelectionsXML, false);
                if (YAxisDataSet != null && YAxisDataSet.Tables.Count > 0)
                {
                    //Binding Y-Axis
                    if (GlobalSettings.ShowChartWizard &&
                        Request.QueryString["source"] != null &&
                        Request.QueryString["source"].ToString().Equals("wizard") &&
                        Session["SelectedMeasures"] != null &&
                        Session["SelectedMeasures"].ToString() != "")
                    {
                        //Filter measures if chart wizard selections are found
                        DataView SelectedMeasures = YAxisDataSet.Tables[0].DefaultView;
                        SelectedMeasures.RowFilter = "id in (" + Session["SelectedMeasures"].ToString() + ")";
                        DataTable SelectedMeasuresTable = SelectedMeasures.ToTable();

                        //Binding Y-Axis
                        MeasureDropDown.DataSource = SelectedMeasuresTable;
                        MeasureDropDown.DataTextField = SelectedMeasuresTable.Columns["displayName"].ToString();
                        MeasureDropDown.DataValueField = SelectedMeasuresTable.Columns["ID"].ToString();
                        MeasureDropDown.DataBind();
                    }
                    else
                    {
                        //Binding Y-Axis
                        MeasureDropDown.DataSource = YAxisDataSet;
                        MeasureDropDown.DataTextField = YAxisDataSet.Tables[0].Columns["displayName"].ToString();
                        MeasureDropDown.DataValueField = YAxisDataSet.Tables[0].Columns["ID"].ToString();
                        MeasureDropDown.DataBind();
                    }


                    try
                    {
                        MeasureDropDown.SelectedValue = GlobalSettings.DefaultYAxisType;
                        //hdnMeasure.Value = GlobalSettings.DefaultYAxisType;
                        hdnMeasure.Value = MeasureDropDown.SelectedValue;
                        hdnMeasureText.Value = MeasureDropDown.SelectedItem.Text.ToString();
                    }
                    catch (Exception ex) { }
                }
            }
            else
            {
                string MeasureDefaultValue = document.SelectSingleNode("//Measure").Attributes["SelectedValue"].Value;
                if (IsMeasureVisible)
                {
                    string MeasureValues = document.SelectSingleNode("//Measure").Attributes["Values"].Value;

                    string[] MeasureItems = MeasureValues.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < MeasureItems.Length; i++)
                    {
                        MeasureDropDown.Items.Add(new ListItem(MeasureItems[i].Split(new char[] { '|' })[1], MeasureItems[i].Split(new char[] { '|' })[0]));
                    }
                    try
                    {
                        MeasureDropDown.SelectedValue = MeasureDefaultValue;
                        hdnMeasure.Value = MeasureDefaultValue;
                        hdnMeasureText.Value = MeasureDropDown.SelectedItem.Text;
                    }
                    catch (Exception ex) { }
                    yaxisDropdownDiv.Visible = true;
                }
                else
                {
                    hdnMeasure.Value = MeasureDefaultValue;
                    hdnMeasureText.Value = document.SelectSingleNode("//Measure").Attributes["SelectedText"].Value;
                    yaxisDropdownDiv.Visible = false;
                }
            }
        }

        // METHODS (Build selection criteria into XML)
        private string GetDataFilterXML()
        {
            string SelectionsXML = string.Empty;
            string SelectionsTextXML = string.Empty;

            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {

                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {
                    string TaxonomySelectedValue = string.Empty;
                    string TaxonomySelectedText = string.Empty;
                    if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE") && !taxonomyRow["ID"].ToString().Equals("999"))
                    {
                        //Get the x axis, series taxonomies filter data from session
                        if (xAxisDropdown.SelectedValue.Equals(taxonomyRow["ID"].ToString()))
                        {
                            Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                            if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                            {
                                //Get all keys (ids) from dictionary
                                List<int> keys = new List<int>(selectedIDsList.Keys);
                                //convert int array to string array
                                string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                                TaxonomySelectedValue = string.Join(",", SelectedIDs);
                            }
                            //Get selected taxonomy id from user control's hidden field
                            if (ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection") != null)
                            {
                                TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection")).Value;
                                TaxonomySelectedValue = TaxonomySelectedValue.TrimEnd(new char[] { ',' });
                            }
                            if (ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText") != null)
                            {
                                TaxonomySelectedText = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText")).Value;
                                TaxonomySelectedText = TaxonomySelectedText.TrimEnd(new char[] { ',' });
                            }
                        }
                        else
                        {
                            //Get selected taxonomy id from user control's hidden field
                            if (ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection") != null)
                            {
                                TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection")).Value;
                            }
                            if (ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText") != null)
                            {
                                TaxonomySelectedText = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText")).Value;
                            }
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                                taxonomyRow["ID"].ToString(),
                                TaxonomySelectedValue,
                                taxonomyRow["ColumnRowID"].ToString());
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedText))
                        {
                            if (TaxonomySelectedText.Contains("'"))
                            {
                                TaxonomySelectedText = TaxonomySelectedText.Replace("'", "&apos;");
                            }

                            SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                               taxonomyRow["Name"].ToString(),
                               TaxonomySelectedText);
                        }
                    }
                    else if (taxonomyRow["ID"].ToString().Equals("999"))
                    {
                        if (xAxisDropdown.SelectedValue.Equals(taxonomyRow["ID"].ToString()))
                        {
                            //Year is selected as x-axis or series
                            //Get the years filter data from session
                            //int StartYear = CurrentSession.GetFromSession<int>("StartYear");
                            //int EndYear = CurrentSession.GetFromSession<int>("EndYear");

                            //TaxonomySelectedValue = StartYear.ToString();
                            //while (EndYear > StartYear)
                            //{
                            //    StartYear++;
                            //    TaxonomySelectedValue += "," + StartYear;
                            //}

                            //Get selected taxonomy id from user control's hidden field
                            if (ChartFilters1.FindControl("hdnMultipleTaxonomy999Selection") != null)
                            {
                                TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy999Selection")).Value;
                                TaxonomySelectedValue = TaxonomySelectedValue.TrimEnd(new char[] { ',' });
                            }
                        }
                        else
                        {
                            //Get selected taxonomy id from user control's hidden field
                            if (ChartFilters1.FindControl("hdnSingleTaxonomy999Selection") != null)
                            {
                                TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy999Selection")).Value;
                            }
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                             taxonomyRow["ID"].ToString(),
                             TaxonomySelectedValue,
                             taxonomyRow["ColumnRowID"].ToString());
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}'/>",
                               taxonomyRow["Name"].ToString(),
                               TaxonomySelectedValue);
                        }
                    }

                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
                SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);
                Session["SelectionsTextXML"] = SelectionsTextXML.ToString();
            }

            return SelectionsXML;
        }

        private string GetCallbackDataFilterXML(string series, string filterValues, string filterValuesText)
        {
            string SelectionsXML = string.Empty;
            string SelectionsTextXML = string.Empty;
            string[] taxonomyFilters = filterValues.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            string[] taxonomyFiltersText = filterValuesText.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < taxonomyFilters.Length; i++)
            {
                string taxonomyID = taxonomyFilters[i].Split('|')[0];
                string taxonomyName = taxonomyFiltersText[i].Split('|')[0];
                string taxonomyRowID = GetColumnRowIDById(taxonomyID);
                string taxonomyValues = taxonomyID.Equals(series) ?
                    taxonomyFilters[i].Split('|')[2] :
                    taxonomyFilters[i].Split('|')[1];

                string taxonomyValuesText = taxonomyID.Equals(series) ?
                 taxonomyFiltersText[i].Split('|')[2] :
                 taxonomyFiltersText[i].Split('|')[1];

                SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                                    taxonomyID,
                                    taxonomyValues.TrimEnd(new char[] { ',' }),
                                    taxonomyRowID);

                if (taxonomyID == "999")
                {
                    SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                                    taxonomyName,
                                    taxonomyValues.TrimEnd(new char[] { ',' }));
                }
                else
                {
                    string TaxonomySelectedText = taxonomyValuesText.TrimEnd(new char[] { ',' });
                    if (!string.IsNullOrEmpty(TaxonomySelectedText))
                    {
                        if (TaxonomySelectedText.Contains("'"))
                        {
                            TaxonomySelectedText = TaxonomySelectedText.Replace("'", "&apos;");
                        }

                        SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}'/>",
                             taxonomyName,
                             TaxonomySelectedText);
                    }
                }
            }

           
            SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);
            Session["SelectionsTextXML"] = SelectionsTextXML.ToString();
            return SelectionsXML;
        }
        
        // EVENT HANDLERS       
        protected void PlotChart_Click(object sender, EventArgs e)
        {
            DataSet ChartDataset = null;
            
            if (!string.IsNullOrEmpty(hdnSeries.Value) && !string.IsNullOrEmpty(hdnMeasure.Value))
            {
                AxisCriteria = string.Format("Series: {0}; Measure: {1}", hdnSeriesText.Value, hdnMeasureText.Value);

                //Get selection criteria   
                string SelectionsXML = GetDataFilterXML();

                string predefinedID = string.IsNullOrEmpty(Request.QueryString["id"]) ?
                    "" :
                    "Predefined Chart:" + Request.QueryString["id"].ToString();

                LogUsage("Charts - Pie", predefinedID, SelectionsXML);

                //Get chart data 
                ChartDataset = SqlDataService.GetPieChartData(Int32.Parse(hdnSeries.Value),
                    Int32.Parse(hdnMeasure.Value),
                    SelectionsXML); //Int32.Parse(xAxisDropdown.SelectedValue)
                ViewState["ChartDataset"] = ChartDataset;
            }

            //Plot chart
            if (ChartDataset!=null && ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {               
                string ChartOptions = hiddenChartType.Value;
                //string ChartType = "2D";                
                string ChartType = "Pie";                
               
                if (!string.IsNullOrEmpty(ChartOptions))
                {
                    ChartType = ChartOptions.Split('|')[0];             
                }

                if (ChartType == "3D")
                {
                    ComponentArt.Web.Visualization.Charting.Chart targetChart;
                    targetChart = ThreeDimensionalCharts.PlotPieChart(ChartDataset, ThreeDChart, "",AxisCriteria);
                    TwoDChart.Visible = false;
                    ThreeDChart.Visible = true;

                    //Save the chart in viewstate, used in extractions.
                    ViewState["3DChartImage"] = targetChart.ImageOutputDirectory.ToString();
                    //Save the Chart GUID in viewstate, used in extractions.
                    ViewState["ChartID"] = targetChart.CustomImageFileName.ToString();
                    ViewState["ChartType"] = "3D";

                    hdnImageurl.Value = targetChart.ImageOutputDirectory.ToString();
                    hdnChartID.Value = targetChart.CustomImageFileName.ToString();
                    hdnImageType.Value = "3D";
                }
                else 
                {
                    dotnetCHARTING.Chart targetChart;
                    ChartType = hiddenChartType.Value == "Pie" ? "Pie": "Donut";

                    targetChart = TwoDimensionalCharts.PlotPieChart(ChartDataset, TwoDChart, "",AxisCriteria,ChartType);
                    TwoDChart.Visible = false;
                    ThreeDChart.Visible = false;
                    TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                    if (!Directory.Exists(Server.MapPath("temp")))
                    {
                        Directory.CreateDirectory(Server.MapPath("temp"));
                    }

                    targetChart.FileManager.TempDirectory = "temp";

                    // Save the legend as a separate image file
                    targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.BottomMiddle;
                    if (targetChart.SeriesCollection.Count > 3)
                    {
                        targetChart.Height = Unit.Point(150 + ((targetChart.SeriesCollection.Count - 3) * 30));
                    }
                    LegendImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");
                    LegendImg.Visible = string.IsNullOrEmpty(LegendImg.ImageUrl.Trim()) ? false : true;
                    targetChart.Width = Unit.Point(520);
                    targetChart.Height = Unit.Point(300);
                    // Remove the legend from the chart and save the chart as a separate image file
                    targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.None;
                    ChartImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");
                    ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                    ImageMapLabel.Text = targetChart.ImageMapText;

                    //Save the chart in viewstate, used in extractions.
                    ViewState["2DChartImage"] = ChartImg.ImageUrl;
                    ViewState["ChartType"] = "2D";

                    hdnImageurl.Value = ChartImg.ImageUrl;
                    hdnImageType.Value = "2D";
                    hdnLegendUrl.Value = LegendImg.ImageUrl;
                }
            }
            //else
            //{
            //    ErrorMessagelabel.Text = "Data not available";
            //    TwoDChart.Visible = false;
            //    ThreeDChart.Visible = false;
            //    LegendImg.Visible = false;
            //}
        }                      

        // CALLBACK EVENT HANDLERS
        public void UpdateChartfilters_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            string YAxisValue = e.Parameters[0];
            string FilterData = e.Parameters[1];
            string FilterDataText = e.Parameters[2];

            ChartFilters1.YAxis = YAxisValue;
            ChartFilters1.LoadDataInCallback(FilterData, FilterDataText);
            ChartFilters1.RenderControl(e.Output);            
        }

        protected void Chart_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            ErrorMessagelabel.Text = "";

            AxisCriteria = "Series: " + e.Parameters[4].ToString()
                         + "; Measure: " + e.Parameters[5].ToString();
            //Get filter xml
            string FilterXML = GetCallbackDataFilterXML(e.Parameters[0].ToString(), 
                e.Parameters[2].ToString(),
                e.Parameters[6].ToString());

            //hdnCallbackChartCriteria.Value="
            DataSet ChartDataset = null;
            try
            {
                //Get chart data 
                ChartDataset = SqlDataService.GetPieChartData(Int32.Parse(e.Parameters[0].ToString()),
                    Int32.Parse(e.Parameters[1].ToString()),
                    FilterXML);
            }
            catch (Exception DataFetchException)
            {
                ChartDataset = null;
            }
            //Plot chart
            if (ChartDataset != null && ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                string ChartOptions = e.Parameters[3].ToString();
                string ChartType;

                ChartType = !string.IsNullOrEmpty(ChartOptions) ? ChartOptions.Split('|')[0] : "2D";

                if (ChartType == "3D")
                {
                    ComponentArt.Web.Visualization.Charting.Chart targetChart;
                    targetChart = ThreeDimensionalCharts.PlotPieChart(ChartDataset, ThreeDChart, "", AxisCriteria);

                    TwoDChart.Visible = false;
                    targetChart.Visible = true;
                    targetChart.RenderControl(e.Output);
                }
                else
                {
                    //ChartType = hiddenChartType.Value == "Pie" ? "Pie" : "Donut";
                    dotnetCHARTING.Chart targetChart;
                    targetChart = TwoDimensionalCharts.PlotPieChart(ChartDataset, TwoDChart, "", AxisCriteria,ChartType);

                    targetChart.Visible = false;
                    ThreeDChart.Visible = false;
                    TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                    if (!Directory.Exists(Server.MapPath("temp")))
                    {
                        Directory.CreateDirectory(Server.MapPath("temp"));
                    }

                    targetChart.FileManager.TempDirectory = "temp";
                    string strLegendPath = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");

                    targetChart.Width = Unit.Point(520);
                    targetChart.Height = Unit.Point(300);
                    // Remove the legend from the chart and save the chart as a separate image file
                    targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.None;
                    string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");

                    //string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap());

                    ChartImg.ImageUrl = ChartImg.ResolveUrl(strChartPath);
                    ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                    ImageMapLabel.Text = targetChart.ImageMapText;
                    ChartImg.RenderControl(e.Output);
                    hdnImageurl.Value = strChartPath;
                    hdnImageurl.RenderControl(e.Output);
                    hdnImageType.Value = ChartType;
                    hdnImageType.RenderControl(e.Output);
                    ImageMapLabel.RenderControl(e.Output);

                    LegendImg.ImageUrl = ChartImg.ResolveUrl(strLegendPath);
                    LegendImg.Visible = string.IsNullOrEmpty(LegendImg.ImageUrl.Trim()) ? false : true;
                    divChart.RenderControl(e.Output);
                    hdnLegendUrl.Value = strLegendPath;
                    hdnLegendUrl.RenderControl(e.Output);

                }
            }
            else
            {
                ErrorMessagelabel.Text = "Data not available";
            }
            ErrorMessagelabel.RenderControl(e.Output);
        }

        // EXPORT EVENT HANDLERS  
        protected void lnkWord_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;
            string chartTitle = hiddenChartType.Value == "Pie" ? "Pie Chart" : "Donut Chart";                    
            string SelectionsXML = GetDataFilterXML();

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetPieChartData(Int32.Parse(hdnSeries.Value),
                Int32.Parse(hdnMeasure.Value),
                SelectionsXML);

            LogUsage("Charts - Pie - Word download", "Pie Chart1"," ");

            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("Pie"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            else
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }         

            if (chartBitmap != null)
            {
                Document doc = ExtractionsHelper.ExportToWord(ChartDataset.Tables[0],
                                chartBitmap,
                                legendBitmap,
                                "PieChart",
                                WordTemplatePath,
                               chartTitle);
                doc.Save("OvumExtract.doc", SaveFormat.FormatDocument, Aspose.Words.SaveType.OpenInWord, Response);
            }
        }
      
        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;            
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;
            string chartTitle = hiddenChartType.Value == "Pie" ? "Pie Chart" : "Donut Chart";           
            string SelectionsXML = GetDataFilterXML();

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetPieChartData(Int32.Parse(hdnSeries.Value),
                Int32.Parse(hdnMeasure.Value),                
                SelectionsXML);

            LogUsage("Charts - Pie - Excel download", "Pie Chart1", " ");
            
            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("Pie"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            else
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }

            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            Workbook Excel;
            if (chartBitmap != null)
            {
                Excel = ExtractionsHelper.ExportToExcel(ChartDataset.Tables[0], 
                            chartBitmap, 
                            legendBitmap,
                            "PieChart",
                            ExcelTemplatePath, 
                            LogoPath,
                            chartTitle);
                Excel.Save("OvumExtract.xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);
            }
        }
       
        protected void lnkPPT_Click(object sender, EventArgs e)
        {
            string strExtractName = "OvumExtract";
            string chartImagePath = string.Empty;            
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;
            string chartTitle = hiddenChartType.Value == "Pie" ? "Pie Chart" : "Donut Chart";         
            string SelectionsXML = GetDataFilterXML();

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetPieChartData(Int32.Parse(hdnSeries.Value),
                Int32.Parse(hdnMeasure.Value),
                SelectionsXML);
            LogUsage("Charts - Pie - PPT download", "Pie Chart1", " ");

            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("Pie"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            else
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }

            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }
            if (chartBitmap != null)
            {
                Presentation presentation = ExtractionsHelper.ExportToPowerpoint(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "PieChart",
                    PowerpointTempaltePath,
                    chartTitle);
                Response.ContentType = "application/vnd.ms-powerpoint";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + strExtractName + ".ppt");
                presentation.Write(Response.OutputStream);
                Response.Flush();
            }
        }        
    }
}
