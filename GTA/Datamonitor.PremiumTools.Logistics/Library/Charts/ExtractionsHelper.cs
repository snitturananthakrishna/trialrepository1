using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Aspose.Slides;
using Aspose.Cells;
using Aspose.Words;
using System.Drawing;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Library
{
    public class ExtractionsHelper
    {
       #region EXCEL extractions

        /// <summary>
        /// Exports to Excel.
        /// </summary>
        /// <param name="ChartDatatable"></param>
        /// <param name="chartBitmap">chartBitmap</param>
        /// <param name="ChartType">the chart type</param>
        /// <param name="excelTemplatePath">excelTemplatePath</param>
        /// <param name="Header">Header</param>
        /// <returns></returns>
        public static Workbook ExportToExcelHeatmap(DataTable ChartDatatable, 
            Bitmap chartBitmap,   
            string ChartType,
            string excelTemplatePath,
            string dmLogoPath, 
            string Header)
        {
            Workbook excel = new Workbook();
            excel.Open(excelTemplatePath);
            DataTable FilterCriteriaDatatable = new DataTable();
            string SelectionsTextXML = string.Empty;
            Aspose.Cells.Cells exportCells = excel.Worksheets[0].Cells;

            #region SettingHeaderInformationOfsheet(i.e Logo,DateOfExtraction etc)

            excel.Worksheets[0].Pictures.Add(0, 0, dmLogoPath);

            excel.Worksheets[0].Cells["A7"].Style.Font.IsBold = true;
            excel.Worksheets[0].Cells["A7"].Style.Font.Size = 12;
            //excel.Worksheets[0].Cells["A4"].PutValue(Header);          
            if (HttpContext.Current.Session["IndicatorText"] != null)
            {
                string indicatorText = HttpContext.Current.Session["IndicatorText"].ToString();
                excel.Worksheets[0].Cells["A7"].PutValue(indicatorText);
            }

            #endregion           

            #region Putting Filter criteria in Excel sheet.

            int FilterTableRowNumber = 8;
            int FilterTableColumnNumber = 0;

            if (ChartType == "HeatMap")
            {
                if (HttpContext.Current.Session["SelectionsTextXML"] != null)
                {
                    SelectionsTextXML = HttpContext.Current.Session["SelectionsTextXML"].ToString();
                    SelectionsTextXML = SelectionsTextXML.Replace("&", "amp;");
                    DataSet FilterCriteriaDataset = new DataSet();
                    StringReader stringReader = new StringReader(SelectionsTextXML);
                    FilterCriteriaDataset.ReadXml(stringReader);
                    FilterCriteriaDatatable = FilterCriteriaDataset.Tables[0];   //LoadSelections();

                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].PutValue("Data Filtered by");
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].Style.Pattern = BackgroundType.Solid;
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].Style.Font.Color = excel.GetMatchingColor(Color.White);
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].Style.Font.Size = 8;
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].Style.Font.IsBold = true;
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber + 1].Style.Pattern = BackgroundType.Solid;
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber + 1].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));

                    if (FilterCriteriaDatatable.Rows.Count > 0)
                    {
                        int i=0;
                        for ( i = 1; i <= FilterCriteriaDatatable.Rows.Count; i++)
                        {
                            excel.Worksheets[0].Cells[i + FilterTableRowNumber, FilterTableColumnNumber].PutValue(FilterCriteriaDatatable.Rows[i - 1][0].ToString());
                            excel.Worksheets[0].Cells[i + FilterTableRowNumber, FilterTableColumnNumber + 1].PutValue(Microsoft.JScript.GlobalObject.unescape(FilterCriteriaDatatable.Rows[i - 1][1].ToString().Replace("amp;", "&")));
                        }
                        FilterTableRowNumber += i;
                    }
                }
            }


            #endregion            

            #region Putting Chart Image in ExcelSheet

            MemoryStream ms = new MemoryStream();

            chartBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            excel.Worksheets[0].Pictures.Add(FilterTableRowNumber+ 3, 0, ms);
            ms.Flush();

            #endregion                          

            

            #region putting chart data in excel

            DataTable chartTable = ChartDatatable.Copy();
            int chartTableRowNumber = FilterTableRowNumber+27;
            for (int j = 0; j < chartTable.Columns.Count; j++)
            {             
                if (chartTable.Columns[j].ColumnName.Contains("rowID"))                    
                {
                    ChartDatatable.Columns.Remove(ChartDatatable.Columns["rowID"].ColumnName.ToString());                     
                }
                if (chartTable.Columns[j].ColumnName.Contains("FusionID"))
                {
                    ChartDatatable.Columns.Remove(ChartDatatable.Columns["FusionID"].ColumnName.ToString());
                }
            }           

            if (ChartDatatable != null)
            {
                exportCells.ImportDataTable(ChartDatatable, true, chartTableRowNumber, 0, false);
            }

            #endregion

            #region Setting Fore Ground Colour

            for (int j = 0; j < ChartDatatable.Columns.Count; j++)
            {
                excel.Worksheets[0].Cells[chartTableRowNumber, j].Style.Pattern = BackgroundType.Solid;
                excel.Worksheets[0].Cells[chartTableRowNumber, j].Style.Font.Size = 8;
                excel.Worksheets[0].Cells[chartTableRowNumber, j].Style.Font.IsBold = true;
                excel.Worksheets[0].Cells[chartTableRowNumber, j].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                excel.Worksheets[0].Cells[chartTableRowNumber, j + 1].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                excel.Worksheets[0].Cells[chartTableRowNumber, j].Style.Font.Color = excel.GetMatchingColor(Color.White);
            }

            for (int i = 0; i < ChartDatatable.Rows.Count; i++)
            {
                for (int j = 0; j < ChartDatatable.Columns.Count; j++)
                {
                   // excel.Worksheets[0].Cells[i + 37, j].Style.Pattern = BackgroundType.Solid;
                    if (i % 2 == 0)
                    {
                        excel.Worksheets[0].Cells[i + chartTableRowNumber + 1, j].Style.ForegroundColor = excel.GetMatchingColor(Color.WhiteSmoke);
                        excel.Worksheets[0].Cells[i + chartTableRowNumber + 1, j].Style.Font.Size = 8;
                    }
                    else
                    {
                        excel.Worksheets[0].Cells[i + chartTableRowNumber + 1, j].Style.ForegroundColor = excel.GetMatchingColor(Color.White);
                        excel.Worksheets[0].Cells[i + chartTableRowNumber + 1, j].Style.Font.Size = 8;
                    }
                }
            }

            #endregion

            

            excel.Worksheets[0].Name = Header;
            //Make autofit the table columns
            excel.Worksheets[0].AutoFitColumn(0, chartTableRowNumber, chartTableRowNumber + ChartDatatable.Rows.Count);
            excel.Worksheets[0].AutoFitColumn(1, chartTableRowNumber, chartTableRowNumber + ChartDatatable.Rows.Count);
            excel.Worksheets[0].AutoFitColumn(2, chartTableRowNumber, chartTableRowNumber + ChartDatatable.Rows.Count);
            excel.Worksheets[0].AutoFitColumn(3, chartTableRowNumber, chartTableRowNumber + ChartDatatable.Rows.Count);
            excel.Worksheets[0].AutoFitColumn(4, chartTableRowNumber, chartTableRowNumber + ChartDatatable.Rows.Count);

            excel.Worksheets[0].Cells.SetColumnWidth(0, 60);
            excel.Worksheets.RemoveAt(1);
            try
            {
                excel.Worksheets["Title"].Move(0);

                excel.Worksheets[0].Cells[5, 1].PutValue("Date of Extraction" + " : " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
                excel.Worksheets[0].Cells[5, 1].Style.Font.Size = 8;
                excel.Worksheets.ActiveSheetIndex = 0;
            }
            catch
            {
                #region Putting Copyright Info In Excelsheet

                int copyRightInfoRowNumber = ChartDatatable.Rows.Count + chartTableRowNumber+10;
                WriteCopyRightInfo(copyRightInfoRowNumber, excel);

                #endregion
            }
            return excel;
        }       

        /// <summary>
        /// Exports to Excel
        /// </summary>
        /// <param name="ChartDatatable"></param>
        /// <param name="chartBitmap">chartBitmap</param>
        /// <param name="chartBitmap">legendBitmap</param>
        /// <param name="excelTemplatePath">excelTemplatePath</param>
        /// <param name="Header">Header</param>
        /// <returns></returns>
        public static Workbook ExportToExcel(DataTable ChartDatatable,
            Bitmap chartBitmap,
            Bitmap legendBitmap,
            string chartType,
            string excelTemplatePath,
            string FilePath,
            string Header) 
        {
            Workbook excel = new Workbook();
            excel.Open(excelTemplatePath);
            string SelectionsTextXML = string.Empty;
            //Get filter criteria datatable.
            DataTable FilterCriteriaDatatable = new DataTable(); //LoadSelections();  //FilterCriteriaDataset.Tables[0];   //LoadSelections();
            Aspose.Cells.Cells exportCells = excel.Worksheets[0].Cells;

            #region SettingHeaderInformationOfsheet(i.e Logo,DateOfExtraction etc)

            excel.Worksheets[0].Pictures.Add(0, 0, FilePath);

            excel.Worksheets[0].Cells["A7"].Style.Font.IsBold = true;
            excel.Worksheets[0].Cells["A7"].Style.Font.Size = 12;
            excel.Worksheets[0].Cells["A7"].PutValue(Header);           

            #endregion         

            #region Putting Filter criteria in Excel sheet.

            int FilterTableRowNumber = 8;
            int FilterTableColumnNumber = 0;

            if (chartType == "XYChart" ||
                chartType == "PieChart" ||
                chartType == "DualYAxisChart" ||
                chartType == "BubbleChart" ||
                chartType == "CountryComparisonChart" ||
                chartType == "IndicatorComparisonChart" ||
                chartType == "Custom Chart" ||
                chartType == "BasicBubbleChart")
            {
                if (HttpContext.Current.Session["SelectionsTextXML"] != null)
                {
                    SelectionsTextXML = HttpContext.Current.Session["SelectionsTextXML"].ToString();
                    SelectionsTextXML = SelectionsTextXML.Replace("&", "amp;");
                    DataSet FilterCriteriaDataset = new DataSet();
                    StringReader stringReader = new StringReader(SelectionsTextXML);
                    FilterCriteriaDataset.ReadXml(stringReader);
                    FilterCriteriaDatatable = FilterCriteriaDataset.Tables[0];   //LoadSelections();                    

                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].PutValue("Data Filtered by");
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].Style.Pattern = BackgroundType.Solid;
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].Style.Font.Color = excel.GetMatchingColor(Color.White);
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].Style.Font.Size = 8;
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber].Style.Font.IsBold = true;
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber + 1].Style.Pattern = BackgroundType.Solid;
                    excel.Worksheets[0].Cells[FilterTableRowNumber, FilterTableColumnNumber + 1].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));

                    if (FilterCriteriaDatatable.Rows.Count > 0)
                    {
                        for (int i = 1; i <= FilterCriteriaDatatable.Rows.Count; i++)
                        {
                            excel.Worksheets[0].Cells[i + FilterTableRowNumber, FilterTableColumnNumber].PutValue(FilterCriteriaDatatable.Rows[i - 1][0].ToString());
                            excel.Worksheets[0].Cells[i + FilterTableRowNumber, FilterTableColumnNumber + 1].PutValue(Microsoft.JScript.GlobalObject.unescape(Microsoft.JScript.GlobalObject.unescape(FilterCriteriaDatatable.Rows[i - 1][1].ToString().Replace("amp;", "&"))));
                        }
                    }
                    int k = 0;
                    for (k = FilterTableRowNumber; k < FilterCriteriaDatatable.Rows.Count + FilterTableRowNumber; k++)
                    {
                        for (int i = 0; i < FilterCriteriaDatatable.Columns.Count; i++)
                        {
                            excel.Worksheets[0].Cells[k, i].Style.Font.Size = 8;
                            excel.Worksheets[0].Cells[k, i + 1].Style.Font.Size = 8;
                        }
                    }
                    FilterTableRowNumber = k;
                }
            }

            #endregion            

            #region Putting Chart Image in ExcelSheet
                        
            MemoryStream ms = new MemoryStream();
            chartBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
            excel.Worksheets[0].Pictures.Add(FilterTableRowNumber+3, 0, ms);
            ms.Flush();

            #endregion

            

            #region putting Legend image in excel
            if (legendBitmap != null)
            {
                int ColNum = chartType == "Custom Chart" ? 4 : 3;
                int RowNum = FilterTableRowNumber+4;
                MemoryStream legendMs = new MemoryStream();
                legendBitmap.Save(legendMs, System.Drawing.Imaging.ImageFormat.Bmp);
                excel.Worksheets[0].Pictures.Add(RowNum, ColNum, legendMs);
                legendMs.Flush();
            }
            #endregion

            #region putting chart data in excel

            if (ChartDatatable != null)
            {
                exportCells.ImportDataTable(ChartDatatable, true, FilterTableRowNumber+24, 0, false);
            }

            #endregion

            #region Setting Fore Ground Colour

            int ChartDtRowNumber = FilterTableRowNumber + 24;
            for (int j = 0; j < ChartDatatable.Columns.Count; j++)
            {

                excel.Worksheets[0].Cells[ChartDtRowNumber, j].Style.Pattern = BackgroundType.Solid;                
                excel.Worksheets[0].Cells[ChartDtRowNumber, j].Style.Font.Size = 8;
                excel.Worksheets[0].Cells[ChartDtRowNumber, j].Style.Font.IsBold = true;
                excel.Worksheets[0].Cells[ChartDtRowNumber, j].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                excel.Worksheets[0].Cells[ChartDtRowNumber, j + 1].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                excel.Worksheets[0].Cells[ChartDtRowNumber, j].Style.Font.Color = excel.GetMatchingColor(Color.White);
            }

            //for (int i = 0; i < ChartDatatable.Rows.Count; i++)
            //{
            //    for (int j = 0; j < ChartDatatable.Columns.Count; j++)
            //    {
            //        //excel.Worksheets[0].Cells[i + 33, j].Style.Pattern = BackgroundType.Solid;
            //        if (i % 2 != 0)
            //        {
            //            excel.Worksheets[0].Cells[i + 37, j].Style.ForegroundColor = excel.GetMatchingColor(Color.WhiteSmoke);
            //            excel.Worksheets[0].Cells[i + 37, j].Style.Font.Size = 8;
            //        }
            //        else
            //        {
            //            excel.Worksheets[0].Cells[i + 37, j].Style.ForegroundColor = excel.GetMatchingColor(Color.White);                       
            //            excel.Worksheets[0].Cells[i + 37, j].Style.Font.Size = 8;
            //        }
            //    }
            //}

            #endregion            

            excel.Worksheets[0].Name = Header.Replace("/", " or ");
            //Make autofit the table columns
            excel.Worksheets[0].AutoFitColumn(0, ChartDtRowNumber, ChartDtRowNumber + ChartDatatable.Rows.Count);
            excel.Worksheets[0].AutoFitColumn(1, ChartDtRowNumber, ChartDtRowNumber + ChartDatatable.Rows.Count);
            excel.Worksheets[0].AutoFitColumn(2, ChartDtRowNumber, ChartDtRowNumber + ChartDatatable.Rows.Count);
            excel.Worksheets[0].AutoFitColumn(3, ChartDtRowNumber, ChartDtRowNumber + ChartDatatable.Rows.Count);
            excel.Worksheets[0].AutoFitColumn(4, ChartDtRowNumber, ChartDtRowNumber + ChartDatatable.Rows.Count);

            excel.Worksheets[0].Cells.SetColumnWidth(0, 60);
            excel.Worksheets.RemoveAt(1);
            try
            {
                excel.Worksheets["Title"].Move(0);

                excel.Worksheets[0].Cells[5, 1].PutValue("Date of Extraction" + " : " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
                excel.Worksheets[0].Cells[5, 1].Style.Font.Size = 8;
                excel.Worksheets.ActiveSheetIndex = 0;
            }
            catch
            {
                #region Putting Copyright Info In Excelsheet

                int copyRightInfoRowNumber = ChartDtRowNumber + ChartDatatable.Rows.Count+10;
                WriteCopyRightInfo(copyRightInfoRowNumber, excel);

                #endregion            
            }
            return excel;
        }

        /// <summary>
        /// Write copyright info in excel exports
        /// </summary>
        /// <param name="copyRightInfoRowNumber"></param>
        /// <param name="excel"></param>
        public static void WriteCopyRightInfo(int copyRightInfoRowNumber, Workbook excel)
        {
            #region Putting Copyright Info In Excelsheet
           
            excel.Worksheets[0].Cells[copyRightInfoRowNumber, 0].PutValue("Date of Extraction" + " : " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
            excel.Worksheets[0].Cells[copyRightInfoRowNumber, 0].Style.Font.Size = 8;
            excel.Worksheets[0].Cells[copyRightInfoRowNumber, 0].Style.Font.IsItalic = true;
          
            try
            {
                XmlTextReader XmlReader = new XmlTextReader(HttpContext.Current.Server.MapPath(GlobalSettings.GetCopyRightConfigPath));
                XmlDocument XmlInformation = new XmlDocument();
                XmlInformation.Load(XmlReader);
                XmlReader.Close();
                foreach (XmlNode Line in XmlInformation.GetElementsByTagName("line"))
                {
                    copyRightInfoRowNumber++;
                    //write the line text into current cell
                    excel.Worksheets[0].Cells[copyRightInfoRowNumber, 0].PutValue(Line.InnerText);
                    //text formating for the current cell                    
                    excel.Worksheets[0].Cells[copyRightInfoRowNumber, 0].Style.Font.Size = 8;
                    excel.Worksheets[0].Cells[copyRightInfoRowNumber, 0].Style.Font.IsItalic = true;
                }                
            }
            catch (Exception ex) { }

            #endregion
        }       

        /// <summary>
        /// Exports to excel.
        /// </summary>
        /// <param name="resultsTable">The results table.</param>
        /// <param name="logoPath">The logo path.</param>
        /// <param name="headingText">The heading text.</param>        
        /// <returns></returns>
        public static Workbook ExportToExcel(DataTable resultsTable,
            string excelTemplatePath,
            string logoPath,
            string headingText,
            string freezeValues,
            string autoFilterRange)
        {
            Workbook excel = new Workbook();
            excel.Open(excelTemplatePath);

            #region SettingHeaderInformationOfsheet(i.e Logo,DateOfExtraction etc)

            excel.Worksheets[0].Pictures.Add(0, 0, logoPath);

            excel.Worksheets[0].Cells["A7"].Style.Font.IsBold = true;
            excel.Worksheets[0].Cells["A7"].Style.Font.Size = 12;
            excel.Worksheets[0].Cells["A7"].PutValue(headingText);

            if (resultsTable.Rows.Count == GlobalSettings.ExcelDownloadMaxRows)
            {
                excel.Worksheets[0].Cells["A8"].Style.Font.IsItalic = true;
                excel.Worksheets[0].Cells["A8"].Style.Font.Size = 8;
                excel.Worksheets[0].Cells["A8"].PutValue("Only top " + GlobalSettings.ExcelDownloadMaxRows.ToString() + " results are extracted.");
            }
            #endregion

            //import data table to excel
            Aspose.Cells.Cells exportCells = excel.Worksheets[0].Cells;
            exportCells.ImportDataTable(resultsTable, true, 9, 0, false);
                        
            //default filters
            if (!string.IsNullOrEmpty(autoFilterRange))
            {
                excel.Worksheets[0].AutoFilter.Range = autoFilterRange;
            }

            //freezing rows & columns
            if (!string.IsNullOrEmpty(freezeValues))
            {
                int FreezingRowNum = Convert.ToInt32(freezeValues.Split('|')[0]);
                int FreezingColNum = Convert.ToInt32(freezeValues.Split('|')[1]);
                excel.Worksheets[0].FreezePanes(FreezingRowNum, FreezingColNum, FreezingRowNum, FreezingColNum);
            }

            #region Setting Fore Ground Colour

            for (int j = 0; j < resultsTable.Columns.Count; j++)
            {
                excel.Worksheets[0].Cells[9, j].Style.Pattern = BackgroundType.Solid;
                excel.Worksheets[0].Cells[9, j].Style.Font.Size = 9;
                excel.Worksheets[0].Cells[9, j].Style.Font.IsBold = true;
                excel.Worksheets[0].Cells[9, j].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                excel.Worksheets[0].Cells[9, j].Style.Font.Color = excel.GetMatchingColor(Color.White);
            }

            #endregion
                       
            //Set worksheet name
            excel.Worksheets[0].Name = headingText.Replace("Analyzer", "");

            //Variables used in cell formatting process
            Aspose.Cells.Cell cell;
            int StartRow=Convert.ToInt32(freezeValues.Split('|')[0]);
            int EndRow=StartRow+resultsTable.Rows.Count;

            //Changing column styles
            for (byte count = 0; count < resultsTable.Columns.Count; count++)
            {
                //Change the years and cagr column's styles
                if (count >= Convert.ToInt32(freezeValues.Split('|')[1]))
                {
                    //excel.Worksheets[0].Cells.Columns[count].Style.Number = 1;
                    excel.Worksheets[0].Cells.Columns[count].Style.HorizontalAlignment = TextAlignmentType.Right;

                    //Rewrite the cells data by converting its style - from string to number
                    for (int rw = StartRow; rw <= EndRow; rw++)
                    {
                        //Format the cell value.
                        cell = excel.Worksheets[0].Cells[rw, count];
                        if (!cell.StringValue.Contains("%"))
                        {
                            cell.PutValue(cell.StringValue, true);
                            cell.Style.Custom = "#,0.00";
                        }
                    }
                }

                //Make the table columns autofit
                excel.Worksheets[0].AutoFitColumn(count, 9, 9 + resultsTable.Rows.Count);                
            }

            //Check the width exceeds maxwidth or not
            if (GlobalSettings.ExcelFreezeColumnMaxWidth != 0)
            {
                for (byte count = 0; count < Convert.ToInt32(freezeValues.Split('|')[1]); count++)
                {
                    if (excel.Worksheets[0].Cells.GetColumnWidth(count) > GlobalSettings.ExcelFreezeColumnMaxWidth)
                    {
                        excel.Worksheets[0].Cells.SetColumnWidth(count, GlobalSettings.ExcelFreezeColumnMaxWidth);                       
                    }
                }
            }

            //set first column's width
            excel.Worksheets[0].Cells.SetColumnWidth(0, 25);
            try
            {
                excel.Worksheets["Title"].Move(0);
                
                excel.Worksheets[0].Cells[5, 1].PutValue("Date of Extraction" + " : " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
                excel.Worksheets[0].Cells[5, 1].Style.Font.Size = 8;
                excel.Worksheets.ActiveSheetIndex = 0;
            }
            catch
            {
                #region Putting Copyright Info In Excelsheet

                int CurrentRow = resultsTable.Rows.Count + 13;
                WriteCopyRightInfo(CurrentRow, excel);

                #endregion
            }
            return excel;
        }

        /// <summary>
        /// Exports to excel.
        /// </summary>        
        /// <param name="excel">The excel.</param>
        /// <param name="workSheetName">The work sheet name.</param>        
        /// <param name="resultsTable">The results table.</param>
        /// <returns></returns>
        public static Workbook ExportToExcel(Workbook excel,
            string excelTemplatePath,
            string workSheetName,
            DataTable resultsTable,
            string logoPath,
            string headingText)
        {
            //excel.Open(excelTemplatePath);            

            Worksheet Sheet = excel.Worksheets[1];
            Sheet.Name = workSheetName;

            #region SettingHeaderInformationOfsheet(i.e Logo,DateOfExtraction etc)

            excel.Worksheets[1].Pictures.Add(0, 0, logoPath);

            excel.Worksheets[1].Cells["A7"].Style.Font.IsBold = true;
            excel.Worksheets[1].Cells["A7"].Style.Font.Size = 12;
            excel.Worksheets[1].Cells["A7"].PutValue(headingText);

            if (resultsTable.Rows.Count == GlobalSettings.ExcelDownloadMaxRows)
            {
                excel.Worksheets[1].Cells["A8"].Style.Font.IsItalic = true;
                excel.Worksheets[1].Cells["A8"].Style.Font.Size = 8;
                excel.Worksheets[1].Cells["A8"].PutValue("Only top " + GlobalSettings.ExcelDownloadMaxRows.ToString() + " results are extracted.");
            }
            #endregion

            int StartingRow = 9;

            //import data table to excel
            Aspose.Cells.Cells exportCells = Sheet.Cells;
            exportCells.ImportDataTable(resultsTable, true, StartingRow, 0, false);

            //default filters
            excel.Worksheets[1].AutoFilter.Range = "A10:B10";

            //freezing rows & columns            
            excel.Worksheets[1].FreezePanes(10, 2, 10, 2);

            //Setting Fore Ground Colour

            for (int j = 0; j < resultsTable.Columns.Count; j++)
            {
                Sheet.Cells[StartingRow, j].Style.Pattern = BackgroundType.Solid;
                Sheet.Cells[StartingRow, j].Style.Font.Size = 9;
                Sheet.Cells[StartingRow, j].Style.Font.IsBold = true;
                Sheet.Cells[StartingRow, j].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                Sheet.Cells[StartingRow, j].Style.Font.Color = excel.GetMatchingColor(Color.White);
            }

            //Make autofit the table columns
            for (int count = 0; count < resultsTable.Columns.Count; count++)
            {
                Sheet.AutoFitColumn(count, StartingRow, StartingRow + resultsTable.Rows.Count);
            }
            //set first column's width
            excel.Worksheets[1].Cells.SetColumnWidth(0, 25);
            try
            {
                excel.Worksheets["Title"].Move(0);
                excel.Worksheets.ActiveSheetIndex = 0;
            }
            catch
            {
                
            }
            return excel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataTable LoadSelections()
        {
            //Get existing taxonomy types
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyDefaults();
            StringBuilder Selections = new StringBuilder();
            string TaxonomyTypeID = "";
            string TaxonomyType = "";

            DataTable FilterCriteria = new DataTable();
            FilterCriteria.Columns.Add("TaxonomyType", typeof(string));
            FilterCriteria.Columns.Add("TaxonomySelectionValue", typeof(string));

            foreach (DataRow Row in TaxonomyTypes.Tables[0].Rows)
            {
                TaxonomyTypeID = Row["ID"].ToString();
                TaxonomyType = Row["Name"].ToString();

                //Get current selections from sessionstate
                Dictionary<int, string> CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>(TaxonomyTypeID);
                if (CurrentSelections != null && CurrentSelections.Count > 0)
                {   // Loop over pairs with foreach
                    string SelectedTaxonomyValues = string.Empty;
                    foreach (KeyValuePair<int, string> TaxonomySelection in CurrentSelections)
                    {
                        SelectedTaxonomyValues = SelectedTaxonomyValues + TaxonomySelection.Value + ",";
                    }
                    SelectedTaxonomyValues = SelectedTaxonomyValues.Substring(0, SelectedTaxonomyValues.Length - 1);
                    DataRow dataRow = FilterCriteria.NewRow();
                    dataRow["TaxonomyType"] = TaxonomyType.ToString();
                    dataRow["TaxonomySelectionValue"] = SelectedTaxonomyValues.ToString();
                    FilterCriteria.Rows.Add(dataRow);
                }
            }
            return FilterCriteria;
        }        

        /// <summary>
        /// Adds the table to excel.
        /// </summary>
        /// <param name="excel">workbook</param>
        /// <param name="resultsTable">The source table</param>
        /// <param name="RowNumber">RowNumber</param>
        /// <param name="ColNumber">ColNumber</param>
        /// <param name="SheetName">SheetName</param>
        /// <returns></returns>
        public static Workbook ExportToExcel(Workbook excel,
             DataSet resultsDataset,
             int RowNumber,
             int ColNumber,
             string Country,
             int sheetNumber)
        {
            Aspose.Cells.Cells exportCells = excel.Worksheets[sheetNumber + 1].Cells; 
            byte columnNumber = Convert.ToByte(ColNumber);            
            excel.Worksheets[sheetNumber + 1].Cells[0, 0].PutValue(Country);

            if (resultsDataset.Tables.Count > 0)
            {
                if (sheetNumber == 0)
                {
                    exportCells.ImportDataTable(resultsDataset.Tables[0], false, RowNumber, columnNumber, false);                   
                }
                else
                {
                    exportCells.ImportDataTable(resultsDataset.Tables[0], false, RowNumber, columnNumber, false);
                }                
            }
            if (sheetNumber == 0)
            {
                GetFormulas(excel,0);
                //excel.Worksheets[sheetNumber + 1].Cells[16, 1].Formula = "='Operator revenue & capex'!K11";
            }
            if (sheetNumber == 1)
            {
                GetFormulas(excel, 1);
            }
            return excel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excel"></param>
        /// <param name="SheetNumber"></param>
        public static void GetFormulas(Workbook excel, int sheetNumber)
        {
            if (sheetNumber == 0)
            {
                //excel.Worksheets[0].Cells[rowNumber, j].Style.HorizontalAlignment = TextAlignmentType.Right;
                //For 17th cell:
                excel.Worksheets[sheetNumber + 1].Cells[10, 3].Formula = "=871743+139223";
                //For 17th cell:
                excel.Worksheets[sheetNumber + 1].Cells[16, 1].Formula = "='Operator revenue & capex'!K11";
                excel.Worksheets[sheetNumber + 1].Cells[16, 2].Formula = "='Operator revenue & capex'!L11";
                excel.Worksheets[sheetNumber + 1].Cells[16, 3].Formula = "='Operator revenue & capex'!M11";
                excel.Worksheets[sheetNumber + 1].Cells[16, 4].Formula = "='Operator revenue & capex'!N11";
                excel.Worksheets[sheetNumber + 1].Cells[16, 5].Formula = "='Operator revenue & capex'!O11";
                excel.Worksheets[sheetNumber + 1].Cells[16, 6].Formula = "='Operator revenue & capex'!P11";
                excel.Worksheets[sheetNumber + 1].Cells[16, 7].Formula = "='Operator revenue & capex'!Q11";
                excel.Worksheets[sheetNumber + 1].Cells[16, 8].Formula = "='Operator revenue & capex'!R11";

                //For 18th cell:
                excel.Worksheets[sheetNumber + 1].Cells[17, 1].Formula = "='Operator revenue & capex'!K24";
                excel.Worksheets[sheetNumber + 1].Cells[17, 2].Formula = "='Operator revenue & capex'!L24";
                excel.Worksheets[sheetNumber + 1].Cells[17, 3].Formula = "='Operator revenue & capex'!M24";
                excel.Worksheets[sheetNumber + 1].Cells[17, 4].Formula = "='Operator revenue & capex'!N24";
                excel.Worksheets[sheetNumber + 1].Cells[17, 5].Formula = "='Operator revenue & capex'!O24";
                excel.Worksheets[sheetNumber + 1].Cells[17, 6].Formula = "='Operator revenue & capex'!P24";
                excel.Worksheets[sheetNumber + 1].Cells[17, 7].Formula = "='Operator revenue & capex'!Q24";
                excel.Worksheets[sheetNumber + 1].Cells[17, 8].Formula = "='Operator revenue & capex'!R24";

                //For 19th cell:
                excel.Worksheets[sheetNumber + 1].Cells[18, 1].Formula = "=B18+B17";
                excel.Worksheets[sheetNumber + 1].Cells[18, 2].Formula = "=C18+C17";
                excel.Worksheets[sheetNumber + 1].Cells[18, 3].Formula = "=D18+D17";
                excel.Worksheets[sheetNumber + 1].Cells[18, 4].Formula = "=E18+E17";
                excel.Worksheets[sheetNumber + 1].Cells[18, 5].Formula = "=F18+F17";
                excel.Worksheets[sheetNumber + 1].Cells[18, 6].Formula = "=G18+G17";
                excel.Worksheets[sheetNumber + 1].Cells[18, 7].Formula = "=H18+H17";
                excel.Worksheets[sheetNumber + 1].Cells[18, 8].Formula = "=I18+I17";

                //For 23th cell:
                excel.Worksheets[sheetNumber + 1].Cells[22, 1].Formula = "='Operator revenue & capex'!K15";
                excel.Worksheets[sheetNumber + 1].Cells[22, 2].Formula = "='Operator revenue & capex'!L15";
                excel.Worksheets[sheetNumber + 1].Cells[22, 3].Formula = "='Operator revenue & capex'!M15";
                excel.Worksheets[sheetNumber + 1].Cells[22, 4].Formula = "='Operator revenue & capex'!N15";
                excel.Worksheets[sheetNumber + 1].Cells[22, 5].Formula = "='Operator revenue & capex'!O15";
                excel.Worksheets[sheetNumber + 1].Cells[22, 6].Formula = "='Operator revenue & capex'!P15";
                excel.Worksheets[sheetNumber + 1].Cells[22, 7].Formula = "='Operator revenue & capex'!Q15";
                excel.Worksheets[sheetNumber + 1].Cells[22, 8].Formula = "='Operator revenue & capex'!R15";

                //For 24th cell:
                excel.Worksheets[sheetNumber + 1].Cells[23, 1].Formula = "='Operator revenue & capex'!K29";
                excel.Worksheets[sheetNumber + 1].Cells[23, 2].Formula = "='Operator revenue & capex'!L29";
                excel.Worksheets[sheetNumber + 1].Cells[23, 3].Formula = "='Operator revenue & capex'!M29";
                excel.Worksheets[sheetNumber + 1].Cells[23, 4].Formula = "='Operator revenue & capex'!N29";
                excel.Worksheets[sheetNumber + 1].Cells[23, 5].Formula = "='Operator revenue & capex'!O29";
                excel.Worksheets[sheetNumber + 1].Cells[23, 6].Formula = "='Operator revenue & capex'!P29";
                excel.Worksheets[sheetNumber + 1].Cells[23, 7].Formula = "='Operator revenue & capex'!Q29";
                excel.Worksheets[sheetNumber + 1].Cells[23, 8].Formula = "='Operator revenue & capex'!R29";

                //For 25th cell:
                excel.Worksheets[sheetNumber + 1].Cells[24, 1].Formula = "=B24+B23";
                excel.Worksheets[sheetNumber + 1].Cells[24, 2].Formula = "=C24+C23";
                excel.Worksheets[sheetNumber + 1].Cells[24, 3].Formula = "=D24+D23";
                excel.Worksheets[sheetNumber + 1].Cells[24, 4].Formula = "=E24+E23";
                excel.Worksheets[sheetNumber + 1].Cells[24, 5].Formula = "=F24+F23";
                excel.Worksheets[sheetNumber + 1].Cells[24, 6].Formula = "=G24+G23";
                excel.Worksheets[sheetNumber + 1].Cells[24, 7].Formula = "=H24+H23";
                excel.Worksheets[sheetNumber + 1].Cells[24, 8].Formula = "=I24+I23";

                //For 27th cell:
                excel.Worksheets[sheetNumber + 1].Cells[26, 1].Formula = "='Mobile voice and data'!K7";
                excel.Worksheets[sheetNumber + 1].Cells[26, 2].Formula = "='Mobile voice and data'!L7";
                excel.Worksheets[sheetNumber + 1].Cells[26, 3].Formula = "='Mobile voice and data'!M7";
                excel.Worksheets[sheetNumber + 1].Cells[26, 4].Formula = "='Mobile voice and data'!N7";
                excel.Worksheets[sheetNumber + 1].Cells[26, 5].Formula = "='Mobile voice and data'!O7";
                excel.Worksheets[sheetNumber + 1].Cells[26, 6].Formula = "='Mobile voice and data'!P7";
                excel.Worksheets[sheetNumber + 1].Cells[26, 7].Formula = "='Mobile voice and data'!Q7";
                excel.Worksheets[sheetNumber + 1].Cells[26, 8].Formula = "='Mobile voice and data'!R7";

                //For 28th cell:
                excel.Worksheets[sheetNumber + 1].Cells[27, 1].Formula = "='Fixed voice services'!K27";
                excel.Worksheets[sheetNumber + 1].Cells[27, 2].Formula = "='Mobile voice and data'!L27";
                excel.Worksheets[sheetNumber + 1].Cells[27, 3].Formula = "='Mobile voice and data'!M27";
                excel.Worksheets[sheetNumber + 1].Cells[27, 4].Formula = "='Mobile voice and data'!N27";
                excel.Worksheets[sheetNumber + 1].Cells[27, 5].Formula = "='Mobile voice and data'!O27";
                excel.Worksheets[sheetNumber + 1].Cells[27, 6].Formula = "='Mobile voice and data'!P27";
                excel.Worksheets[sheetNumber + 1].Cells[27, 7].Formula = "='Mobile voice and data'!Q27";
                excel.Worksheets[sheetNumber + 1].Cells[27, 8].Formula = "='Mobile voice and data'!R27";

                //For 29th cell:
                excel.Worksheets[sheetNumber + 1].Cells[28, 1].Formula = "='Broadband services'!K7+'Broadband services'!K21+'Broadband services'!K76";
                excel.Worksheets[sheetNumber + 1].Cells[28, 2].Formula = "='Broadband services'!L7+'Broadband services'!L21+'Broadband services'!L76";
                excel.Worksheets[sheetNumber + 1].Cells[28, 3].Formula = "='Broadband services'!M7+'Broadband services'!M21+'Broadband services'!M76";
                excel.Worksheets[sheetNumber + 1].Cells[28, 4].Formula = "='Broadband services'!N7+'Broadband services'!N21+'Broadband services'!N76";
                excel.Worksheets[sheetNumber + 1].Cells[28, 5].Formula = "='Broadband services'!O7+'Broadband services'!O21+'Broadband services'!O76";
                excel.Worksheets[sheetNumber + 1].Cells[28, 6].Formula = "='Broadband services'!P7+'Broadband services'!P21+'Broadband services'!P76";
                excel.Worksheets[sheetNumber + 1].Cells[28, 7].Formula = "='Broadband services'!Q7+'Broadband services'!Q21+'Broadband services'!Q76";
                excel.Worksheets[sheetNumber + 1].Cells[28, 8].Formula = "='Broadband services'!R7+'Broadband services'!R21+'Broadband services'!R76";

                //For 31st cell:
                excel.Worksheets[sheetNumber + 1].Cells[30, 1].Formula = "='Mobile voice and data'!K9";
                excel.Worksheets[sheetNumber + 1].Cells[30, 2].Formula = "='Mobile voice and data'!L9";
                excel.Worksheets[sheetNumber + 1].Cells[30, 3].Formula = "='Mobile voice and data'!M9";
                excel.Worksheets[sheetNumber + 1].Cells[30, 4].Formula = "='Mobile voice and data'!N9";
                excel.Worksheets[sheetNumber + 1].Cells[30, 5].Formula = "='Mobile voice and data'!O9";
                excel.Worksheets[sheetNumber + 1].Cells[30, 6].Formula = "='Mobile voice and data'!P9";
                excel.Worksheets[sheetNumber + 1].Cells[30, 7].Formula = "='Mobile voice and data'!Q9";
                excel.Worksheets[sheetNumber + 1].Cells[30, 8].Formula = "='Mobile voice and data'!R9";

                //For 32nd cell:
                excel.Worksheets[sheetNumber + 1].Cells[31, 1].Formula = "='Fixed voice services'!K7";
                excel.Worksheets[sheetNumber + 1].Cells[31, 2].Formula = "='Fixed voice services'!L7";
                excel.Worksheets[sheetNumber + 1].Cells[31, 3].Formula = "='Fixed voice services'!M7";
                excel.Worksheets[sheetNumber + 1].Cells[31, 4].Formula = "='Fixed voice services'!N7";
                excel.Worksheets[sheetNumber + 1].Cells[31, 5].Formula = "='Fixed voice services'!O7";
                excel.Worksheets[sheetNumber + 1].Cells[31, 6].Formula = "='Fixed voice services'!P7";
                excel.Worksheets[sheetNumber + 1].Cells[31, 7].Formula = "='Fixed voice services'!Q7";
                excel.Worksheets[sheetNumber + 1].Cells[31, 8].Formula = "='Fixed voice services'!R7";

                //For 33rd cell:
                excel.Worksheets[sheetNumber + 1].Cells[32, 1].Formula = "='Broadband services'!K12";
                excel.Worksheets[sheetNumber + 1].Cells[32, 2].Formula = "='Broadband services'!L12";
                excel.Worksheets[sheetNumber + 1].Cells[32, 3].Formula = "='Broadband services'!M12";
                excel.Worksheets[sheetNumber + 1].Cells[32, 4].Formula = "='Broadband services'!N12";
                excel.Worksheets[sheetNumber + 1].Cells[32, 5].Formula = "='Broadband services'!O12";
                excel.Worksheets[sheetNumber + 1].Cells[32, 6].Formula = "='Broadband services'!P12";
                excel.Worksheets[sheetNumber + 1].Cells[32, 7].Formula = "='Broadband services'!Q12";
                excel.Worksheets[sheetNumber + 1].Cells[32, 8].Formula = "='Broadband services'!R12";

                //For 34th cell:
                excel.Worksheets[sheetNumber + 1].Cells[33, 1].Formula = "=SUM(B31:B33)";
                excel.Worksheets[sheetNumber + 1].Cells[33, 2].Formula = "=SUM(C31:C33)";
                excel.Worksheets[sheetNumber + 1].Cells[33, 3].Formula = "=SUM(D31:D33)";
                excel.Worksheets[sheetNumber + 1].Cells[33, 4].Formula = "=SUM(E31:E33)";
                excel.Worksheets[sheetNumber + 1].Cells[33, 5].Formula = "=SUM(F31:F33)";
                excel.Worksheets[sheetNumber + 1].Cells[33, 6].Formula = "=SUM(G31:G33)";
                excel.Worksheets[sheetNumber + 1].Cells[33, 7].Formula = "=SUM(H31:H33)";
                excel.Worksheets[sheetNumber + 1].Cells[33, 8].Formula = "=SUM(I31:I33)";

                //For 37th cell:
                excel.Worksheets[sheetNumber + 1].Cells[36, 1].Formula = "=B39+B46+B50+B51";
                excel.Worksheets[sheetNumber + 1].Cells[36, 2].Formula = "=C39+C46+C50+C51";
                excel.Worksheets[sheetNumber + 1].Cells[36, 3].Formula = "=D39+D46+D50+D51";
                excel.Worksheets[sheetNumber + 1].Cells[36, 4].Formula = "=E39+E46+E50+E51";
                excel.Worksheets[sheetNumber + 1].Cells[36, 5].Formula = "=F39+F46+F50+F51";
                excel.Worksheets[sheetNumber + 1].Cells[36, 6].Formula = "=G39+G46+G50+G51";
                excel.Worksheets[sheetNumber + 1].Cells[36, 7].Formula = "=H39+H46+H50+H51";
                excel.Worksheets[sheetNumber + 1].Cells[36, 8].Formula = "=I39+I46+I50+I51";

                //For 39th cell:
                excel.Worksheets[sheetNumber + 1].Cells[38, 1].Formula = "='Mobile voice and data'!K22";
                excel.Worksheets[sheetNumber + 1].Cells[38, 2].Formula = "='Mobile voice and data'!L22";
                excel.Worksheets[sheetNumber + 1].Cells[38, 3].Formula = "='Mobile voice and data'!M22";
                excel.Worksheets[sheetNumber + 1].Cells[38, 4].Formula = "='Mobile voice and data'!N22";
                excel.Worksheets[sheetNumber + 1].Cells[38, 5].Formula = "='Mobile voice and data'!O22";
                excel.Worksheets[sheetNumber + 1].Cells[38, 6].Formula = "='Mobile voice and data'!P22";
                excel.Worksheets[sheetNumber + 1].Cells[38, 7].Formula = "='Mobile voice and data'!Q22";
                excel.Worksheets[sheetNumber + 1].Cells[38, 8].Formula = "='Mobile voice and data'!R22";

                //For 40th cell:
                excel.Worksheets[sheetNumber + 1].Cells[39, 1].Formula = "='Mobile voice and data'!K23";
                excel.Worksheets[sheetNumber + 1].Cells[39, 2].Formula = "='Mobile voice and data'!L23";
                excel.Worksheets[sheetNumber + 1].Cells[39, 3].Formula = "='Mobile voice and data'!M23";
                excel.Worksheets[sheetNumber + 1].Cells[39, 4].Formula = "='Mobile voice and data'!N23";
                excel.Worksheets[sheetNumber + 1].Cells[39, 5].Formula = "='Mobile voice and data'!O23";
                excel.Worksheets[sheetNumber + 1].Cells[39, 6].Formula = "='Mobile voice and data'!P23";
                excel.Worksheets[sheetNumber + 1].Cells[39, 7].Formula = "='Mobile voice and data'!Q23";
                excel.Worksheets[sheetNumber + 1].Cells[39, 8].Formula = "='Mobile voice and data'!R23";

                //For 41st cell:
                excel.Worksheets[sheetNumber + 1].Cells[40, 1].Formula = "='Mobile voice and data'!K24";
                excel.Worksheets[sheetNumber + 1].Cells[40, 2].Formula = "='Mobile voice and data'!L24";
                excel.Worksheets[sheetNumber + 1].Cells[40, 3].Formula = "='Mobile voice and data'!M24";
                excel.Worksheets[sheetNumber + 1].Cells[40, 4].Formula = "='Mobile voice and data'!N24";
                excel.Worksheets[sheetNumber + 1].Cells[40, 5].Formula = "='Mobile voice and data'!O24";
                excel.Worksheets[sheetNumber + 1].Cells[40, 6].Formula = "='Mobile voice and data'!P24";
                excel.Worksheets[sheetNumber + 1].Cells[40, 7].Formula = "='Mobile voice and data'!Q24";
                excel.Worksheets[sheetNumber + 1].Cells[40, 8].Formula = "='Mobile voice and data'!R24";

                //For 42nd cell:
                excel.Worksheets[sheetNumber + 1].Cells[41, 1].Formula = "='Mobile voice and data'!K25";
                excel.Worksheets[sheetNumber + 1].Cells[41, 2].Formula = "='Mobile voice and data'!L25";
                excel.Worksheets[sheetNumber + 1].Cells[41, 3].Formula = "='Mobile voice and data'!M25";
                excel.Worksheets[sheetNumber + 1].Cells[41, 4].Formula = "='Mobile voice and data'!N25";
                excel.Worksheets[sheetNumber + 1].Cells[41, 5].Formula = "='Mobile voice and data'!O25";
                excel.Worksheets[sheetNumber + 1].Cells[41, 6].Formula = "='Mobile voice and data'!P25";
                excel.Worksheets[sheetNumber + 1].Cells[41, 7].Formula = "='Mobile voice and data'!Q25";
                excel.Worksheets[sheetNumber + 1].Cells[41, 8].Formula = "='Mobile voice and data'!R25";

                //For 43rd cell:
                excel.Worksheets[sheetNumber + 1].Cells[42, 1].Formula = "='Mobile voice and data'!K26";
                excel.Worksheets[sheetNumber + 1].Cells[42, 2].Formula = "='Mobile voice and data'!L26";
                excel.Worksheets[sheetNumber + 1].Cells[42, 3].Formula = "='Mobile voice and data'!M26";
                excel.Worksheets[sheetNumber + 1].Cells[42, 4].Formula = "='Mobile voice and data'!N26";
                excel.Worksheets[sheetNumber + 1].Cells[42, 5].Formula = "='Mobile voice and data'!O26";
                excel.Worksheets[sheetNumber + 1].Cells[42, 6].Formula = "='Mobile voice and data'!P26";
                excel.Worksheets[sheetNumber + 1].Cells[42, 7].Formula = "='Mobile voice and data'!Q26";
                excel.Worksheets[sheetNumber + 1].Cells[42, 8].Formula = "='Mobile voice and data'!R26";

                //For 44th cell:
                excel.Worksheets[sheetNumber + 1].Cells[43, 1].Formula = "='Mobile voice and data'!K27";
                excel.Worksheets[sheetNumber + 1].Cells[43, 2].Formula = "='Mobile voice and data'!L27";
                excel.Worksheets[sheetNumber + 1].Cells[43, 3].Formula = "='Mobile voice and data'!M27";
                excel.Worksheets[sheetNumber + 1].Cells[43, 4].Formula = "='Mobile voice and data'!N27";
                excel.Worksheets[sheetNumber + 1].Cells[43, 5].Formula = "='Mobile voice and data'!O27";
                excel.Worksheets[sheetNumber + 1].Cells[43, 6].Formula = "='Mobile voice and data'!P27";
                excel.Worksheets[sheetNumber + 1].Cells[43, 7].Formula = "='Mobile voice and data'!Q27";
                excel.Worksheets[sheetNumber + 1].Cells[43, 8].Formula = "='Mobile voice and data'!R27";

                //For 46th cell:
                excel.Worksheets[sheetNumber + 1].Cells[45, 1].Formula = "='Mobile voice and data'!K29";
                excel.Worksheets[sheetNumber + 1].Cells[45, 2].Formula = "='Mobile voice and data'!L29";
                excel.Worksheets[sheetNumber + 1].Cells[45, 3].Formula = "='Mobile voice and data'!M29";
                excel.Worksheets[sheetNumber + 1].Cells[45, 4].Formula = "='Mobile voice and data'!N29";
                excel.Worksheets[sheetNumber + 1].Cells[45, 5].Formula = "='Mobile voice and data'!O29";
                excel.Worksheets[sheetNumber + 1].Cells[45, 6].Formula = "='Mobile voice and data'!P29";
                excel.Worksheets[sheetNumber + 1].Cells[45, 7].Formula = "='Mobile voice and data'!Q29";
                excel.Worksheets[sheetNumber + 1].Cells[45, 8].Formula = "='Mobile voice and data'!R29";

                //For 47th cell:
                excel.Worksheets[sheetNumber + 1].Cells[46, 1].Formula = "='Mobile voice and data'!K30";
                excel.Worksheets[sheetNumber + 1].Cells[46, 2].Formula = "='Mobile voice and data'!L30";
                excel.Worksheets[sheetNumber + 1].Cells[46, 3].Formula = "='Mobile voice and data'!M30";
                excel.Worksheets[sheetNumber + 1].Cells[46, 4].Formula = "='Mobile voice and data'!N30";
                excel.Worksheets[sheetNumber + 1].Cells[46, 5].Formula = "='Mobile voice and data'!O30";
                excel.Worksheets[sheetNumber + 1].Cells[46, 6].Formula = "='Mobile voice and data'!P30";
                excel.Worksheets[sheetNumber + 1].Cells[46, 7].Formula = "='Mobile voice and data'!Q30";
                excel.Worksheets[sheetNumber + 1].Cells[46, 8].Formula = "='Mobile voice and data'!R30";

                //For 48th cell:
                excel.Worksheets[sheetNumber + 1].Cells[47, 1].Formula = "='Mobile voice and data'!K31";
                excel.Worksheets[sheetNumber + 1].Cells[47, 2].Formula = "='Mobile voice and data'!L31";
                excel.Worksheets[sheetNumber + 1].Cells[47, 3].Formula = "='Mobile voice and data'!M31";
                excel.Worksheets[sheetNumber + 1].Cells[47, 4].Formula = "='Mobile voice and data'!N31";
                excel.Worksheets[sheetNumber + 1].Cells[47, 5].Formula = "='Mobile voice and data'!O31";
                excel.Worksheets[sheetNumber + 1].Cells[47, 6].Formula = "='Mobile voice and data'!P31";
                excel.Worksheets[sheetNumber + 1].Cells[47, 7].Formula = "='Mobile voice and data'!Q31";
                excel.Worksheets[sheetNumber + 1].Cells[47, 8].Formula = "='Mobile voice and data'!R31";

                //For 49th cell:
                excel.Worksheets[sheetNumber + 1].Cells[48, 1].Formula = "='Mobile voice and data'!K32";
                excel.Worksheets[sheetNumber + 1].Cells[48, 2].Formula = "='Mobile voice and data'!L32";
                excel.Worksheets[sheetNumber + 1].Cells[48, 3].Formula = "='Mobile voice and data'!M32";
                excel.Worksheets[sheetNumber + 1].Cells[48, 4].Formula = "='Mobile voice and data'!N32";
                excel.Worksheets[sheetNumber + 1].Cells[48, 5].Formula = "='Mobile voice and data'!O32";
                excel.Worksheets[sheetNumber + 1].Cells[48, 6].Formula = "='Mobile voice and data'!P32";
                excel.Worksheets[sheetNumber + 1].Cells[48, 7].Formula = "='Mobile voice and data'!Q32";
                excel.Worksheets[sheetNumber + 1].Cells[48, 8].Formula = "='Mobile voice and data'!R32";

                //For 50th cell:
                excel.Worksheets[sheetNumber + 1].Cells[49, 1].Formula = "='Mobile voice and data'!K33";
                excel.Worksheets[sheetNumber + 1].Cells[49, 2].Formula = "='Mobile voice and data'!L33";
                excel.Worksheets[sheetNumber + 1].Cells[49, 3].Formula = "='Mobile voice and data'!M33";
                excel.Worksheets[sheetNumber + 1].Cells[49, 4].Formula = "='Mobile voice and data'!N33";
                excel.Worksheets[sheetNumber + 1].Cells[49, 5].Formula = "='Mobile voice and data'!O33";
                excel.Worksheets[sheetNumber + 1].Cells[49, 6].Formula = "='Mobile voice and data'!P33";
                excel.Worksheets[sheetNumber + 1].Cells[49, 7].Formula = "='Mobile voice and data'!Q33";
                excel.Worksheets[sheetNumber + 1].Cells[49, 8].Formula = "='Mobile voice and data'!R33";

                //For 51st cell:
                excel.Worksheets[sheetNumber + 1].Cells[50, 1].Formula = "='Mobile voice and data'!K34";
                excel.Worksheets[sheetNumber + 1].Cells[50, 2].Formula = "='Mobile voice and data'!L34";
                excel.Worksheets[sheetNumber + 1].Cells[50, 3].Formula = "='Mobile voice and data'!M34";
                excel.Worksheets[sheetNumber + 1].Cells[50, 4].Formula = "='Mobile voice and data'!N34";
                excel.Worksheets[sheetNumber + 1].Cells[50, 5].Formula = "='Mobile voice and data'!O34";
                excel.Worksheets[sheetNumber + 1].Cells[50, 6].Formula = "='Mobile voice and data'!P34";
                excel.Worksheets[sheetNumber + 1].Cells[50, 7].Formula = "='Mobile voice and data'!Q34";
                excel.Worksheets[sheetNumber + 1].Cells[50, 8].Formula = "='Mobile voice and data'!R34";

                //For 53rd cell:
                excel.Worksheets[sheetNumber + 1].Cells[52, 1].Formula = "=SUM(B54:B58)";
                excel.Worksheets[sheetNumber + 1].Cells[52, 2].Formula = "=SUM(C54:C58)";
                excel.Worksheets[sheetNumber + 1].Cells[52, 3].Formula = "=SUM(D54:D58)";
                excel.Worksheets[sheetNumber + 1].Cells[52, 4].Formula = "=SUM(E54:E58)";
                excel.Worksheets[sheetNumber + 1].Cells[52, 5].Formula = "=SUM(F54:F58)";
                excel.Worksheets[sheetNumber + 1].Cells[52, 6].Formula = "=SUM(G54:G58)";
                excel.Worksheets[sheetNumber + 1].Cells[52, 7].Formula = "=SUM(H54:H58)";
                excel.Worksheets[sheetNumber + 1].Cells[52, 8].Formula = "=SUM(I54:I58)";

                //For 54th cell:
                excel.Worksheets[sheetNumber + 1].Cells[53, 1].Formula = "='Broadband services'!K8+'Broadband services'!K52";
                excel.Worksheets[sheetNumber + 1].Cells[53, 2].Formula = "='Broadband services'!L8+'Broadband services'!L52";
                excel.Worksheets[sheetNumber + 1].Cells[53, 3].Formula = "='Broadband services'!M8+'Broadband services'!M52";
                excel.Worksheets[sheetNumber + 1].Cells[53, 4].Formula = "='Broadband services'!N8+'Broadband services'!N52";
                excel.Worksheets[sheetNumber + 1].Cells[53, 5].Formula = "='Broadband services'!O8+'Broadband services'!O52";
                excel.Worksheets[sheetNumber + 1].Cells[53, 6].Formula = "='Broadband services'!P8+'Broadband services'!P52";
                excel.Worksheets[sheetNumber + 1].Cells[53, 7].Formula = "='Broadband services'!Q8+'Broadband services'!Q52";
                excel.Worksheets[sheetNumber + 1].Cells[53, 8].Formula = "='Broadband services'!R8+'Broadband services'!R52";

                //For 55th cell:
                excel.Worksheets[sheetNumber + 1].Cells[54, 1].Formula = "='Broadband services'!K9+'Broadband services'!K58";
                excel.Worksheets[sheetNumber + 1].Cells[54, 2].Formula = "='Broadband services'!L9+'Broadband services'!L58";
                excel.Worksheets[sheetNumber + 1].Cells[54, 3].Formula = "='Broadband services'!M9+'Broadband services'!M58";
                excel.Worksheets[sheetNumber + 1].Cells[54, 4].Formula = "='Broadband services'!N9+'Broadband services'!N58";
                excel.Worksheets[sheetNumber + 1].Cells[54, 5].Formula = "='Broadband services'!O9+'Broadband services'!O58";
                excel.Worksheets[sheetNumber + 1].Cells[54, 6].Formula = "='Broadband services'!P9+'Broadband services'!P58";
                excel.Worksheets[sheetNumber + 1].Cells[54, 7].Formula = "='Broadband services'!Q9+'Broadband services'!Q58";
                excel.Worksheets[sheetNumber + 1].Cells[54, 8].Formula = "='Broadband services'!R9+'Broadband services'!R58";

                //For 56th cell:
                excel.Worksheets[sheetNumber + 1].Cells[55, 1].Formula = "='Broadband services'!K10+'Broadband services'!K59";
                excel.Worksheets[sheetNumber + 1].Cells[55, 2].Formula = "='Broadband services'!L10+'Broadband services'!L59";
                excel.Worksheets[sheetNumber + 1].Cells[55, 3].Formula = "='Broadband services'!M10+'Broadband services'!M59";
                excel.Worksheets[sheetNumber + 1].Cells[55, 4].Formula = "='Broadband services'!N10+'Broadband services'!N59";
                excel.Worksheets[sheetNumber + 1].Cells[55, 5].Formula = "='Broadband services'!O10+'Broadband services'!O59";
                excel.Worksheets[sheetNumber + 1].Cells[55, 6].Formula = "='Broadband services'!P10+'Broadband services'!P59";
                excel.Worksheets[sheetNumber + 1].Cells[55, 7].Formula = "='Broadband services'!Q10+'Broadband services'!Q59";
                excel.Worksheets[sheetNumber + 1].Cells[55, 8].Formula = "='Broadband services'!R10+'Broadband services'!R59";

                //For 57th cell:
                excel.Worksheets[sheetNumber + 1].Cells[56, 1].Formula = "='Broadband services'!K11+'Broadband services'!K57+'Broadband services'!K56+'Broadband services'!K60";
                excel.Worksheets[sheetNumber + 1].Cells[56, 2].Formula = "='Broadband services'!L11+'Broadband services'!L57+'Broadband services'!L56+'Broadband services'!L60";
                excel.Worksheets[sheetNumber + 1].Cells[56, 3].Formula = "='Broadband services'!M11+'Broadband services'!M57+'Broadband services'!M56+'Broadband services'!M60";
                excel.Worksheets[sheetNumber + 1].Cells[56, 4].Formula = "='Broadband services'!N11+'Broadband services'!N57+'Broadband services'!N56+'Broadband services'!N60";
                excel.Worksheets[sheetNumber + 1].Cells[56, 5].Formula = "='Broadband services'!O11+'Broadband services'!O57+'Broadband services'!O56+'Broadband services'!O60";
                excel.Worksheets[sheetNumber + 1].Cells[56, 6].Formula = "='Broadband services'!P11+'Broadband services'!P57+'Broadband services'!P56+'Broadband services'!P60";
                excel.Worksheets[sheetNumber + 1].Cells[56, 7].Formula = "='Broadband services'!Q11+'Broadband services'!Q57+'Broadband services'!Q56+'Broadband services'!Q60";
                excel.Worksheets[sheetNumber + 1].Cells[56, 8].Formula = "='Broadband services'!R11+'Broadband services'!R57+'Broadband services'!R56+'Broadband services'!R60";

                //For 58th cell:
                excel.Worksheets[sheetNumber + 1].Cells[57, 1].Formula = "='Broadband services'!K76";
                excel.Worksheets[sheetNumber + 1].Cells[57, 2].Formula = "='Broadband services'!L76";
                excel.Worksheets[sheetNumber + 1].Cells[57, 3].Formula = "='Broadband services'!M76";
                excel.Worksheets[sheetNumber + 1].Cells[57, 4].Formula = "='Broadband services'!N76";
                excel.Worksheets[sheetNumber + 1].Cells[57, 5].Formula = "='Broadband services'!O76";
                excel.Worksheets[sheetNumber + 1].Cells[57, 6].Formula = "='Broadband services'!P76";
                excel.Worksheets[sheetNumber + 1].Cells[57, 7].Formula = "='Broadband services'!Q76";
                excel.Worksheets[sheetNumber + 1].Cells[57, 8].Formula = "='Broadband services'!R76";

                //For 61st cell:
                excel.Worksheets[sheetNumber + 1].Cells[60, 1].Formula = "='Broadband services'!K69+'Broadband services'!K12";
                excel.Worksheets[sheetNumber + 1].Cells[60, 2].Formula = "='Broadband services'!L69+'Broadband services'!L12";
                excel.Worksheets[sheetNumber + 1].Cells[60, 3].Formula = "='Broadband services'!M69+'Broadband services'!M12";
                excel.Worksheets[sheetNumber + 1].Cells[60, 4].Formula = "='Broadband services'!N69+'Broadband services'!N12";
                excel.Worksheets[sheetNumber + 1].Cells[60, 5].Formula = "='Broadband services'!O69+'Broadband services'!O12";
                excel.Worksheets[sheetNumber + 1].Cells[60, 6].Formula = "='Broadband services'!P69+'Broadband services'!P12";
                excel.Worksheets[sheetNumber + 1].Cells[60, 7].Formula = "='Broadband services'!Q69+'Broadband services'!Q12";
                excel.Worksheets[sheetNumber + 1].Cells[60, 8].Formula = "='Broadband services'!R69+'Broadband services'!R12";

                //For 62nd cell:
                excel.Worksheets[sheetNumber + 1].Cells[61, 1].Formula = "='Consumer services'!K12";
                excel.Worksheets[sheetNumber + 1].Cells[61, 2].Formula = "='Consumer services'!L12";
                excel.Worksheets[sheetNumber + 1].Cells[61, 3].Formula = "='Consumer services'!M12";
                excel.Worksheets[sheetNumber + 1].Cells[61, 4].Formula = "='Consumer services'!N12";
                excel.Worksheets[sheetNumber + 1].Cells[61, 5].Formula = "='Consumer services'!O12";
                excel.Worksheets[sheetNumber + 1].Cells[61, 6].Formula = "='Consumer services'!P12";
                excel.Worksheets[sheetNumber + 1].Cells[61, 7].Formula = "='Consumer services'!Q12";

                //For 63rd cell:
                excel.Worksheets[sheetNumber + 1].Cells[62, 1].Formula = "='Consumer services'!K34";
                excel.Worksheets[sheetNumber + 1].Cells[62, 2].Formula = "='Consumer services'!L34";
                excel.Worksheets[sheetNumber + 1].Cells[62, 3].Formula = "='Consumer services'!M34";
                excel.Worksheets[sheetNumber + 1].Cells[62, 4].Formula = "='Consumer services'!N34";
                excel.Worksheets[sheetNumber + 1].Cells[62, 5].Formula = "='Consumer services'!O34";
                excel.Worksheets[sheetNumber + 1].Cells[62, 6].Formula = "='Consumer services'!P34";
                excel.Worksheets[sheetNumber + 1].Cells[62, 7].Formula = "='Consumer services'!Q34";

                //For 64th cell:
                excel.Worksheets[sheetNumber + 1].Cells[63, 1].Formula = "='Consumer services'!K57";
                excel.Worksheets[sheetNumber + 1].Cells[63, 2].Formula = "='Consumer services'!L57";
                excel.Worksheets[sheetNumber + 1].Cells[63, 3].Formula = "='Consumer services'!M57";
                excel.Worksheets[sheetNumber + 1].Cells[63, 4].Formula = "='Consumer services'!N57";
                excel.Worksheets[sheetNumber + 1].Cells[63, 5].Formula = "='Consumer services'!O57";
                excel.Worksheets[sheetNumber + 1].Cells[63, 6].Formula = "='Consumer services'!P57";
                excel.Worksheets[sheetNumber + 1].Cells[63, 7].Formula = "='Consumer services'!Q57";

                //For 65th cell:
                excel.Worksheets[sheetNumber + 1].Cells[64, 1].Formula = "='Consumer services'!K47";
                excel.Worksheets[sheetNumber + 1].Cells[64, 2].Formula = "='Consumer services'!L47";
                excel.Worksheets[sheetNumber + 1].Cells[64, 3].Formula = "='Consumer services'!M47";
                excel.Worksheets[sheetNumber + 1].Cells[64, 4].Formula = "='Consumer services'!N47";
                excel.Worksheets[sheetNumber + 1].Cells[64, 5].Formula = "='Consumer services'!O47";
                excel.Worksheets[sheetNumber + 1].Cells[64, 6].Formula = "='Consumer services'!P47";
                excel.Worksheets[sheetNumber + 1].Cells[64, 7].Formula = "='Consumer services'!Q47";

                //For 66th cell:
                excel.Worksheets[sheetNumber + 1].Cells[65, 1].Formula = "='Consumer services'!K48";
                excel.Worksheets[sheetNumber + 1].Cells[65, 2].Formula = "='Consumer services'!L48";
                excel.Worksheets[sheetNumber + 1].Cells[65, 3].Formula = "='Consumer services'!M48";
                excel.Worksheets[sheetNumber + 1].Cells[65, 4].Formula = "='Consumer services'!N48";
                excel.Worksheets[sheetNumber + 1].Cells[65, 5].Formula = "='Consumer services'!O48";
                excel.Worksheets[sheetNumber + 1].Cells[65, 6].Formula = "='Consumer services'!P48";
                excel.Worksheets[sheetNumber + 1].Cells[65, 7].Formula = "='Consumer services'!Q48";

                //For 67th cell:
                excel.Worksheets[sheetNumber + 1].Cells[66, 1].Formula = "='Consumer services'!K49";
                excel.Worksheets[sheetNumber + 1].Cells[66, 2].Formula = "='Consumer services'!L49";
                excel.Worksheets[sheetNumber + 1].Cells[66, 3].Formula = "='Consumer services'!M49";
                excel.Worksheets[sheetNumber + 1].Cells[66, 4].Formula = "='Consumer services'!N49";
                excel.Worksheets[sheetNumber + 1].Cells[66, 5].Formula = "='Consumer services'!O49";
                excel.Worksheets[sheetNumber + 1].Cells[66, 6].Formula = "='Consumer services'!P49";
                excel.Worksheets[sheetNumber + 1].Cells[66, 7].Formula = "='Consumer services'!Q49";

                //For 68th cell:
                excel.Worksheets[sheetNumber + 1].Cells[67, 1].Formula = "='Consumer services'!K50";
                excel.Worksheets[sheetNumber + 1].Cells[67, 2].Formula = "='Consumer services'!L50";
                excel.Worksheets[sheetNumber + 1].Cells[67, 3].Formula = "='Consumer services'!M50";
                excel.Worksheets[sheetNumber + 1].Cells[67, 4].Formula = "='Consumer services'!N50";
                excel.Worksheets[sheetNumber + 1].Cells[67, 5].Formula = "='Consumer services'!O50";
                excel.Worksheets[sheetNumber + 1].Cells[67, 6].Formula = "='Consumer services'!P50";
                excel.Worksheets[sheetNumber + 1].Cells[67, 7].Formula = "='Consumer services'!Q50";

                //For 69th cell:
                excel.Worksheets[sheetNumber + 1].Cells[68, 1].Formula = "='Consumer services'!K51";
                excel.Worksheets[sheetNumber + 1].Cells[68, 2].Formula = "='Consumer services'!L51";
                excel.Worksheets[sheetNumber + 1].Cells[68, 3].Formula = "='Consumer services'!M51";
                excel.Worksheets[sheetNumber + 1].Cells[68, 4].Formula = "='Consumer services'!N51";
                excel.Worksheets[sheetNumber + 1].Cells[68, 5].Formula = "='Consumer services'!O51";
                excel.Worksheets[sheetNumber + 1].Cells[68, 6].Formula = "='Consumer services'!P51";
                excel.Worksheets[sheetNumber + 1].Cells[68, 7].Formula = "='Consumer services'!Q51";

                //For 72nd cell:
                excel.Worksheets[sheetNumber + 1].Cells[71, 1].Formula = "='Broadband services'!K20";
                excel.Worksheets[sheetNumber + 1].Cells[71, 2].Formula = "='Broadband services'!L20";
                excel.Worksheets[sheetNumber + 1].Cells[71, 3].Formula = "='Broadband services'!M20";
                excel.Worksheets[sheetNumber + 1].Cells[71, 4].Formula = "='Broadband services'!N20";
                excel.Worksheets[sheetNumber + 1].Cells[71, 5].Formula = "='Broadband services'!O20";
                excel.Worksheets[sheetNumber + 1].Cells[71, 6].Formula = "='Broadband services'!P20";
                excel.Worksheets[sheetNumber + 1].Cells[71, 7].Formula = "='Broadband services'!Q20";

                //For 73rd cell:
                excel.Worksheets[sheetNumber + 1].Cells[72, 1].Formula = "='Enterprise services'!K7";
                excel.Worksheets[sheetNumber + 1].Cells[72, 2].Formula = "='Enterprise services'!L7";
                excel.Worksheets[sheetNumber + 1].Cells[72, 3].Formula = "='Enterprise services'!M7";
                excel.Worksheets[sheetNumber + 1].Cells[72, 4].Formula = "='Enterprise services'!N7";
                excel.Worksheets[sheetNumber + 1].Cells[72, 5].Formula = "='Enterprise services'!O7";
                excel.Worksheets[sheetNumber + 1].Cells[72, 6].Formula = "='Enterprise services'!P7";
                excel.Worksheets[sheetNumber + 1].Cells[72, 7].Formula = "='Enterprise services'!Q7";
                excel.Worksheets[sheetNumber + 1].Cells[72, 8].Formula = "='Enterprise services'!R7";

                //For 74th cell:
                excel.Worksheets[sheetNumber + 1].Cells[73, 1].Formula = "='Enterprise services'!L44";
                excel.Worksheets[sheetNumber + 1].Cells[73, 2].Formula = "='Enterprise services'!M44";
                excel.Worksheets[sheetNumber + 1].Cells[73, 3].Formula = "='Enterprise services'!N44";
                excel.Worksheets[sheetNumber + 1].Cells[73, 4].Formula = "='Enterprise services'!O44";
                excel.Worksheets[sheetNumber + 1].Cells[73, 5].Formula = "='Enterprise services'!P44";
                excel.Worksheets[sheetNumber + 1].Cells[73, 6].Formula = "='Enterprise services'!Q44";
                excel.Worksheets[sheetNumber + 1].Cells[73, 7].Formula = "='Enterprise services'!R44";

                //For 75th cell:
                excel.Worksheets[sheetNumber + 1].Cells[74, 1].Formula = "='Enterprise services'!K62";
                excel.Worksheets[sheetNumber + 1].Cells[74, 2].Formula = "='Enterprise services'!L62";
                excel.Worksheets[sheetNumber + 1].Cells[74, 3].Formula = "='Enterprise services'!M62";
                excel.Worksheets[sheetNumber + 1].Cells[74, 4].Formula = "='Enterprise services'!N62";
                excel.Worksheets[sheetNumber + 1].Cells[74, 5].Formula = "='Enterprise services'!O62";
                excel.Worksheets[sheetNumber + 1].Cells[74, 6].Formula = "='Enterprise services'!P62";
                excel.Worksheets[sheetNumber + 1].Cells[74, 7].Formula = "='Enterprise services'!Q62";

                //For 76th cell:
                excel.Worksheets[sheetNumber + 1].Cells[75, 1].Formula = "='Enterprise services'!K97";
                excel.Worksheets[sheetNumber + 1].Cells[75, 2].Formula = "='Enterprise services'!L97";
                excel.Worksheets[sheetNumber + 1].Cells[75, 3].Formula = "='Enterprise services'!M97";
                excel.Worksheets[sheetNumber + 1].Cells[75, 4].Formula = "='Enterprise services'!N97";
                excel.Worksheets[sheetNumber + 1].Cells[75, 5].Formula = "='Enterprise services'!O97";
                excel.Worksheets[sheetNumber + 1].Cells[75, 6].Formula = "='Enterprise services'!P97";
                excel.Worksheets[sheetNumber + 1].Cells[75, 7].Formula = "='Enterprise services'!Q97";

                //For 77th cell:
                excel.Worksheets[sheetNumber + 1].Cells[76, 1].Formula = "='Enterprise services'!K148";
                excel.Worksheets[sheetNumber + 1].Cells[76, 2].Formula = "='Enterprise services'!L148";
                excel.Worksheets[sheetNumber + 1].Cells[76, 3].Formula = "='Enterprise services'!M148";
                excel.Worksheets[sheetNumber + 1].Cells[76, 4].Formula = "='Enterprise services'!N148";
                excel.Worksheets[sheetNumber + 1].Cells[76, 5].Formula = "='Enterprise services'!O148";
                excel.Worksheets[sheetNumber + 1].Cells[76, 6].Formula = "='Enterprise services'!P148";
                excel.Worksheets[sheetNumber + 1].Cells[76, 7].Formula = "='Enterprise services'!Q148";

                //For 78th cell:
                excel.Worksheets[sheetNumber + 1].Cells[77, 1].Formula = "='Enterprise services'!K153";
                excel.Worksheets[sheetNumber + 1].Cells[77, 2].Formula = "='Enterprise services'!L153";
                excel.Worksheets[sheetNumber + 1].Cells[77, 3].Formula = "='Enterprise services'!M153";
                excel.Worksheets[sheetNumber + 1].Cells[77, 4].Formula = "='Enterprise services'!N153";
                excel.Worksheets[sheetNumber + 1].Cells[77, 5].Formula = "='Enterprise services'!O153";
                excel.Worksheets[sheetNumber + 1].Cells[77, 6].Formula = "='Enterprise services'!P153";
                excel.Worksheets[sheetNumber + 1].Cells[77, 7].Formula = "='Enterprise services'!Q153";

                //For 81st cell:
                excel.Worksheets[sheetNumber + 1].Cells[80, 1].Formula = "='Wholesale services'!K14";
                excel.Worksheets[sheetNumber + 1].Cells[80, 2].Formula = "='Wholesale services'!L14";
                excel.Worksheets[sheetNumber + 1].Cells[80, 3].Formula = "='Wholesale services'!M14";
                excel.Worksheets[sheetNumber + 1].Cells[80, 4].Formula = "='Wholesale services'!N14";
                excel.Worksheets[sheetNumber + 1].Cells[80, 5].Formula = "='Wholesale services'!O14";
                excel.Worksheets[sheetNumber + 1].Cells[80, 6].Formula = "='Wholesale services'!P14";
                excel.Worksheets[sheetNumber + 1].Cells[80, 7].Formula = "='Wholesale services'!Q14";
                excel.Worksheets[sheetNumber + 1].Cells[80, 8].Formula = "='Wholesale services'!R14";

                //For 82nd cell:
                excel.Worksheets[sheetNumber + 1].Cells[81, 1].Formula = "='Wholesale services'!K23";
                excel.Worksheets[sheetNumber + 1].Cells[81, 2].Formula = "='Wholesale services'!L23";
                excel.Worksheets[sheetNumber + 1].Cells[81, 3].Formula = "='Wholesale services'!M23";
                excel.Worksheets[sheetNumber + 1].Cells[81, 4].Formula = "='Wholesale services'!N23";
                excel.Worksheets[sheetNumber + 1].Cells[81, 5].Formula = "='Wholesale services'!O23";
                excel.Worksheets[sheetNumber + 1].Cells[81, 6].Formula = "='Wholesale services'!P23";
                excel.Worksheets[sheetNumber + 1].Cells[81, 7].Formula = "='Wholesale services'!Q23";
                excel.Worksheets[sheetNumber + 1].Cells[81, 8].Formula = "='Wholesale services'!R23";

                //For 83rd cell:
                excel.Worksheets[sheetNumber + 1].Cells[82, 1].Formula = "='Wholesale services'!K48";
                excel.Worksheets[sheetNumber + 1].Cells[82, 2].Formula = "='Wholesale services'!L48";
                excel.Worksheets[sheetNumber + 1].Cells[82, 3].Formula = "='Wholesale services'!M48";
                excel.Worksheets[sheetNumber + 1].Cells[82, 4].Formula = "='Wholesale services'!N48";
                excel.Worksheets[sheetNumber + 1].Cells[82, 5].Formula = "='Wholesale services'!O48";
                excel.Worksheets[sheetNumber + 1].Cells[82, 6].Formula = "='Wholesale services'!P48";
                excel.Worksheets[sheetNumber + 1].Cells[82, 7].Formula = "='Wholesale services'!Q48";
                excel.Worksheets[sheetNumber + 1].Cells[82, 8].Formula = "='Wholesale services'!R48";

                //For 84th cell:
                excel.Worksheets[sheetNumber + 1].Cells[83, 1].Formula = "='Wholesale services'!K78";
                excel.Worksheets[sheetNumber + 1].Cells[83, 2].Formula = "='Wholesale services'!L78";
                excel.Worksheets[sheetNumber + 1].Cells[83, 3].Formula = "='Wholesale services'!M78";
                excel.Worksheets[sheetNumber + 1].Cells[83, 4].Formula = "='Wholesale services'!N78";
                excel.Worksheets[sheetNumber + 1].Cells[83, 5].Formula = "='Wholesale services'!O78";
                excel.Worksheets[sheetNumber + 1].Cells[83, 6].Formula = "='Wholesale services'!P78";
                excel.Worksheets[sheetNumber + 1].Cells[83, 7].Formula = "='Wholesale services'!Q78";
                excel.Worksheets[sheetNumber + 1].Cells[83, 8].Formula = "='Wholesale services'!R78";

                //DATE
                excel.Worksheets[sheetNumber + 1].Cells[16, 11].Formula = "='Operator revenue & capex'!J11";
                excel.Worksheets[sheetNumber + 1].Cells[17, 11].Formula = "='Operator revenue & capex'!J18";
                excel.Worksheets[sheetNumber + 1].Cells[18, 11].Formula = "=L18";
                excel.Worksheets[sheetNumber + 1].Cells[22, 11].Formula = "='Operator revenue & capex'!J15";
                excel.Worksheets[sheetNumber + 1].Cells[23, 11].Formula = "='Operator revenue & capex'!J29";
                excel.Worksheets[sheetNumber + 1].Cells[24, 11].Formula = "=L24";                
                excel.Worksheets[sheetNumber + 1].Cells[26, 11].Formula = "='Mobile voice and data'!J7";
                excel.Worksheets[sheetNumber + 1].Cells[27, 11].Formula = "='Fixed voice services'!J27";                
                excel.Worksheets[sheetNumber + 1].Cells[28, 11].Formula = "='Broadband services'!J7";                
                excel.Worksheets[sheetNumber + 1].Cells[30, 11].Formula = "='Mobile voice and data'!J9";                
                excel.Worksheets[sheetNumber + 1].Cells[31, 11].Formula = "='Fixed voice services'!J9";
                excel.Worksheets[sheetNumber + 1].Cells[32, 11].Formula = "='Broadband services'!J12";                
                excel.Worksheets[sheetNumber + 1].Cells[33, 11].PutValue("multiple");
                excel.Worksheets[sheetNumber + 1].Cells[36, 11].Formula = "=L39";
                excel.Worksheets[sheetNumber + 1].Cells[38, 11].Formula = "='Mobile voice and data'!J22";
                excel.Worksheets[sheetNumber + 1].Cells[39, 11].Formula = "='Mobile voice and data'!J23";
                excel.Worksheets[sheetNumber + 1].Cells[40, 11].Formula = "='Mobile voice and data'!J24";
                excel.Worksheets[sheetNumber + 1].Cells[41, 11].Formula = "='Mobile voice and data'!J25";
                excel.Worksheets[sheetNumber + 1].Cells[42, 11].Formula = "='Mobile voice and data'!J26";
                excel.Worksheets[sheetNumber + 1].Cells[43, 11].Formula = "='Mobile voice and data'!J27";
                excel.Worksheets[sheetNumber + 1].Cells[46, 11].Formula = "='Mobile voice and data'!J30";
                excel.Worksheets[sheetNumber + 1].Cells[47, 11].Formula = "='Mobile voice and data'!J31";
                excel.Worksheets[sheetNumber + 1].Cells[48, 11].Formula = "='Mobile voice and data'!J32";
                excel.Worksheets[sheetNumber + 1].Cells[49, 11].Formula = "='Mobile voice and data'!J33";
                excel.Worksheets[sheetNumber + 1].Cells[50, 11].Formula = "='Mobile voice and data'!J34";
                excel.Worksheets[sheetNumber + 1].Cells[52, 11].PutValue("multiple");
                excel.Worksheets[sheetNumber + 1].Cells[53, 11].Formula = "='Broadband services'!J8";
                excel.Worksheets[sheetNumber + 1].Cells[54, 11].Formula = "='Broadband services'!J9";
                excel.Worksheets[sheetNumber + 1].Cells[55, 11].Formula = "='Broadband services'!J10";
                excel.Worksheets[sheetNumber + 1].Cells[56, 11].Formula = "='Broadband services'!J11";
                excel.Worksheets[sheetNumber + 1].Cells[57, 11].Formula = "='Broadband services'!J67";
                excel.Worksheets[sheetNumber + 1].Cells[60, 11].PutValue("multiple");
                excel.Worksheets[sheetNumber + 1].Cells[61, 11].Formula = "='Consumer services'!J12";
                excel.Worksheets[sheetNumber + 1].Cells[63, 11].Formula = "='Consumer services'!J34";
                excel.Worksheets[sheetNumber + 1].Cells[63, 11].Formula = "='Consumer services'!$J$57";
                excel.Worksheets[sheetNumber + 1].Cells[64, 11].Formula = "='Consumer services'!J47";
                excel.Worksheets[sheetNumber + 1].Cells[65, 11].Formula = "='Consumer services'!J48";
                excel.Worksheets[sheetNumber + 1].Cells[66, 11].Formula = "='Consumer services'!J49";
                excel.Worksheets[sheetNumber + 1].Cells[67, 11].Formula = "='Consumer services'!J50";
                excel.Worksheets[sheetNumber + 1].Cells[68, 11].Formula = "='Consumer services'!J51";
                excel.Worksheets[sheetNumber + 1].Cells[71, 11].Formula = "='Broadband services'!J20";
                excel.Worksheets[sheetNumber + 1].Cells[72, 11].Formula = "='Enterprise services'!J7";
                excel.Worksheets[sheetNumber + 1].Cells[73, 11].Formula = "='Enterprise services'!J44";
                excel.Worksheets[sheetNumber + 1].Cells[74, 11].Formula = "='Enterprise services'!J62";
                excel.Worksheets[sheetNumber + 1].Cells[75, 11].Formula = "='Enterprise services'!$J$97";
                excel.Worksheets[sheetNumber + 1].Cells[76, 11].Formula = "='Enterprise services'!J148";
                excel.Worksheets[sheetNumber + 1].Cells[77, 11].Formula = "='Enterprise services'!J153";
                excel.Worksheets[sheetNumber + 1].Cells[80, 11].Formula = "='Wholesale services'!J14";
                excel.Worksheets[sheetNumber + 1].Cells[81, 11].Formula = "='Wholesale services'!J23";
                excel.Worksheets[sheetNumber + 1].Cells[82, 11].Formula = "='Wholesale services'!J48";
                excel.Worksheets[sheetNumber + 1].Cells[83, 11].Formula = "='Wholesale services'!J78";

                for (int i = 0; i < 68; i++)
                {
                    excel.Worksheets[sheetNumber + 1].Cells[i + 16, 11].Style.HorizontalAlignment = TextAlignmentType.Right;
                }
                excel.Worksheets[sheetNumber + 1].Cells[79, 10].Style.HorizontalAlignment = TextAlignmentType.Right;
                excel.Worksheets[sheetNumber + 1].Cells[10, 10].PutValue("number");
                excel.Worksheets[sheetNumber + 1].Cells[16, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[17, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[18, 10].PutValue("'$ millions");

                excel.Worksheets[sheetNumber + 1].Cells[22, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[23, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[24, 10].PutValue("'$ millions");

                excel.Worksheets[sheetNumber + 1].Cells[26, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[27, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[28, 10].PutValue("'000s");

                excel.Worksheets[sheetNumber + 1].Cells[30, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[31, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[32, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[33, 10].PutValue("'$ millions");

                excel.Worksheets[sheetNumber + 1].Cells[36, 10].PutValue("'000s");

                excel.Worksheets[sheetNumber + 1].Cells[38, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[39, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[40, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[41, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[42, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[43, 10].PutValue("'000s");

                excel.Worksheets[sheetNumber + 1].Cells[45, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[46, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[47, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[48, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[49, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[50, 10].PutValue("'000s");

                excel.Worksheets[sheetNumber + 1].Cells[52, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[53, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[54, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[55, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[56, 10].PutValue("'000s");
                excel.Worksheets[sheetNumber + 1].Cells[57, 10].PutValue("'000s");
                
                excel.Worksheets[sheetNumber + 1].Cells[60, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[61, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[62, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[63, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[64, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[65, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[66, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[67, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[68, 10].PutValue("'$ millions");

                excel.Worksheets[sheetNumber + 1].Cells[71, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[72, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[73, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[74, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[75, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[76, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[77, 10].PutValue("'$ millions");

                excel.Worksheets[sheetNumber + 1].Cells[80, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[81, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[82, 10].PutValue("'$ millions");
                excel.Worksheets[sheetNumber + 1].Cells[83, 10].PutValue("'$ millions");
            }
            else
            {
                //For 6th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[5, 10].Formula = "=K11+K24";
                //excel.Worksheets[sheetNumber + 1].Cells[5, 11].Formula = "=L11+L24";
                //excel.Worksheets[sheetNumber + 1].Cells[5, 12].Formula = "=M11+M24";
                //excel.Worksheets[sheetNumber + 1].Cells[5, 13].Formula = "=N11+N24";
                //excel.Worksheets[sheetNumber + 1].Cells[5, 14].Formula = "=O11+O24";
                //excel.Worksheets[sheetNumber + 1].Cells[5, 15].Formula = "=P11+P24";
                //excel.Worksheets[sheetNumber + 1].Cells[5, 16].Formula = "=Q11+Q24";
                //excel.Worksheets[sheetNumber + 1].Cells[5, 17].Formula = "=R11+R24";

                //For 7th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[6, 10].Formula = "=K15+K29";
                //excel.Worksheets[sheetNumber + 1].Cells[6, 11].Formula = "=L15+L29";
                //excel.Worksheets[sheetNumber + 1].Cells[6, 12].Formula = "=M15+M29";
                //excel.Worksheets[sheetNumber + 1].Cells[6, 13].Formula = "=N15+N29";
                //excel.Worksheets[sheetNumber + 1].Cells[6, 14].Formula = "=O15+O29";
                //excel.Worksheets[sheetNumber + 1].Cells[6, 15].Formula = "=P15+P29";
                //excel.Worksheets[sheetNumber + 1].Cells[6, 16].Formula = "=Q15+Q29";
                //excel.Worksheets[sheetNumber + 1].Cells[6, 17].Formula = "=R15+R29";

                //For 8th cell:
                excel.Worksheets[sheetNumber + 1].Cells[7, 10].Formula = "=K7/K6";
                excel.Worksheets[sheetNumber + 1].Cells[7, 11].Formula = "=L7/L6";
                excel.Worksheets[sheetNumber + 1].Cells[7, 12].Formula = "=M7/M6";
                excel.Worksheets[sheetNumber + 1].Cells[7, 13].Formula = "=N7/N6";
                excel.Worksheets[sheetNumber + 1].Cells[7, 14].Formula = "=O7/O6";
                excel.Worksheets[sheetNumber + 1].Cells[7, 15].Formula = "=P7/P6";
                excel.Worksheets[sheetNumber + 1].Cells[7, 16].Formula = "=Q7/Q6";
                excel.Worksheets[sheetNumber + 1].Cells[7, 17].Formula = "=R7/R6";

                //For 11th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[10, 10].Formula = "=K12+K13";
                //excel.Worksheets[sheetNumber + 1].Cells[10, 11].Formula = "=L12+L13";
                //excel.Worksheets[sheetNumber + 1].Cells[10, 12].Formula = "=M12+M13";
                //excel.Worksheets[sheetNumber + 1].Cells[10, 13].Formula = "=N12+N13";
                //excel.Worksheets[sheetNumber + 1].Cells[10, 14].Formula = "=O12+O13";
                //excel.Worksheets[sheetNumber + 1].Cells[10, 15].Formula = "=P12+P13";
                //excel.Worksheets[sheetNumber + 1].Cells[10, 16].Formula = "=Q12+Q13";
                //excel.Worksheets[sheetNumber + 1].Cells[10, 17].Formula = "=R12+R13";

                //For 15th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[14, 10].Formula = "=K16+K17";
                //excel.Worksheets[sheetNumber + 1].Cells[14, 11].Formula = "=L16+L17";
                //excel.Worksheets[sheetNumber + 1].Cells[14, 12].Formula = "=M16+M17";
                //excel.Worksheets[sheetNumber + 1].Cells[14, 13].Formula = "=N16+N17";
                //excel.Worksheets[sheetNumber + 1].Cells[14, 14].Formula = "=O16+O17";
                //excel.Worksheets[sheetNumber + 1].Cells[14, 15].Formula = "=P16+P17";
                //excel.Worksheets[sheetNumber + 1].Cells[14, 16].Formula = "=Q16+Q17";
                //excel.Worksheets[sheetNumber + 1].Cells[14, 17].Formula = "=R16+R17";

                //For 19th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[18, 10].Formula = "=K15/K11";
                //excel.Worksheets[sheetNumber + 1].Cells[18, 11].Formula = "=L15/L11";
                //excel.Worksheets[sheetNumber + 1].Cells[18, 12].Formula = "=M15/M11";
                //excel.Worksheets[sheetNumber + 1].Cells[18, 13].Formula = "=N15/N11";
                //excel.Worksheets[sheetNumber + 1].Cells[18, 14].Formula = "=O15/O11";
                //excel.Worksheets[sheetNumber + 1].Cells[18, 15].Formula = "=P15/P11";
                //excel.Worksheets[sheetNumber + 1].Cells[18, 16].Formula = "=Q15/Q11";
                //excel.Worksheets[sheetNumber + 1].Cells[18, 17].Formula = "=R15/R11";

                //For 20th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[19, 10].Formula = "=K16/K12";
                //excel.Worksheets[sheetNumber + 1].Cells[19, 11].Formula = "=L16/L12";
                //excel.Worksheets[sheetNumber + 1].Cells[19, 12].Formula = "=M16/M12";
                //excel.Worksheets[sheetNumber + 1].Cells[19, 13].Formula = "=N16/N12";
                //excel.Worksheets[sheetNumber + 1].Cells[19, 14].Formula = "=O16/O12";
                //excel.Worksheets[sheetNumber + 1].Cells[19, 15].Formula = "=P16/P12";
                //excel.Worksheets[sheetNumber + 1].Cells[19, 16].Formula = "=Q16/Q12";
                //excel.Worksheets[sheetNumber + 1].Cells[19, 17].Formula = "=R16/R12";

                //For 21th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[20, 10].Formula = "=K17/K13";
                //excel.Worksheets[sheetNumber + 1].Cells[20, 11].Formula = "=L17/L13";
                //excel.Worksheets[sheetNumber + 1].Cells[20, 12].Formula = "=M17/M13";
                //excel.Worksheets[sheetNumber + 1].Cells[20, 13].Formula = "=N17/N13";
                //excel.Worksheets[sheetNumber + 1].Cells[20, 14].Formula = "=O17/O13";
                //excel.Worksheets[sheetNumber + 1].Cells[20, 15].Formula = "=P17/P13";
                //excel.Worksheets[sheetNumber + 1].Cells[20, 16].Formula = "=Q17/Q13";
                //excel.Worksheets[sheetNumber + 1].Cells[20, 17].Formula = "=R17/R13";

                //For 24th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[23, 10].Formula = "=K25+K26+K27";
                //excel.Worksheets[sheetNumber + 1].Cells[23, 11].Formula = "=L25+L26+L27";
                //excel.Worksheets[sheetNumber + 1].Cells[23, 12].Formula = "=M25+M26+M27";
                //excel.Worksheets[sheetNumber + 1].Cells[23, 13].Formula = "=N25+N26+N27";
                //excel.Worksheets[sheetNumber + 1].Cells[23, 14].Formula = "=O25+O26+O27";
                //excel.Worksheets[sheetNumber + 1].Cells[23, 15].Formula = "=P25+P26+P27";
                //excel.Worksheets[sheetNumber + 1].Cells[23, 16].Formula = "=Q25+Q26+Q27";
                //excel.Worksheets[sheetNumber + 1].Cells[23, 17].Formula = "=R25+R26+R27";

                //For 29th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[28, 10].Formula = "=K30+K31+K32";
                //excel.Worksheets[sheetNumber + 1].Cells[28, 11].Formula = "=L30+L31+L32";
                //excel.Worksheets[sheetNumber + 1].Cells[28, 12].Formula = "=M30+M31+M32";
                //excel.Worksheets[sheetNumber + 1].Cells[28, 13].Formula = "=N30+N31+N32";
                //excel.Worksheets[sheetNumber + 1].Cells[28, 14].Formula = "=O30+O31+O32";
                //excel.Worksheets[sheetNumber + 1].Cells[28, 15].Formula = "=P30+P31+P32";
                //excel.Worksheets[sheetNumber + 1].Cells[28, 16].Formula = "=Q30+Q31+Q32";
                //excel.Worksheets[sheetNumber + 1].Cells[28, 17].Formula = "=R30+R31+R32";

                //For 34th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[33, 10].Formula = "=K30/K26";
                //excel.Worksheets[sheetNumber + 1].Cells[33, 11].Formula = "=L30/L26";
                //excel.Worksheets[sheetNumber + 1].Cells[33, 12].Formula = "=M30/M26";
                //excel.Worksheets[sheetNumber + 1].Cells[33, 13].Formula = "=N30/N26";
                //excel.Worksheets[sheetNumber + 1].Cells[33, 14].Formula = "=O30/O26";
                //excel.Worksheets[sheetNumber + 1].Cells[33, 15].Formula = "=P30/P26";
                //excel.Worksheets[sheetNumber + 1].Cells[33, 16].Formula = "=Q30/Q26";
                //excel.Worksheets[sheetNumber + 1].Cells[33, 17].Formula = "=R30/R26";

                //For 35th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[34, 10].Formula = "=K31/K27";
                //excel.Worksheets[sheetNumber + 1].Cells[34, 11].Formula = "=L31/L27";
                //excel.Worksheets[sheetNumber + 1].Cells[34, 12].Formula = "=M31/M27";
                //excel.Worksheets[sheetNumber + 1].Cells[34, 13].Formula = "=N31/N27";
                //excel.Worksheets[sheetNumber + 1].Cells[34, 14].Formula = "=O31/O27";
                //excel.Worksheets[sheetNumber + 1].Cells[34, 15].Formula = "=P31/P27";
                //excel.Worksheets[sheetNumber + 1].Cells[34, 16].Formula = "=Q31/Q27";
                //excel.Worksheets[sheetNumber + 1].Cells[34, 17].Formula = "=R31/R27";

                //For 36th cell:
                //excel.Worksheets[sheetNumber + 1].Cells[35, 10].Formula = "=K32/K29";
                //excel.Worksheets[sheetNumber + 1].Cells[35, 11].Formula = "=L32/L29";
                //excel.Worksheets[sheetNumber + 1].Cells[35, 12].Formula = "=M32/M29";
                //excel.Worksheets[sheetNumber + 1].Cells[35, 13].Formula = "=N32/N29";
                //excel.Worksheets[sheetNumber + 1].Cells[35, 14].Formula = "=O32/O29";
                //excel.Worksheets[sheetNumber + 1].Cells[35, 15].Formula = "=P32/P29";
                //excel.Worksheets[sheetNumber + 1].Cells[35, 16].Formula = "=Q32/Q29";
                //excel.Worksheets[sheetNumber + 1].Cells[35, 17].Formula = "=R32/R29";

                excel.Worksheets[sheetNumber + 1].Cells[7, 9].Formula = "=J6";
                excel.Worksheets[sheetNumber + 1].Cells[18, 9].Formula = "=J17";
                excel.Worksheets[sheetNumber + 1].Cells[19, 9].Formula = "=J19";
                excel.Worksheets[sheetNumber + 1].Cells[20, 9].Formula = "=J20";
                excel.Worksheets[sheetNumber + 1].Cells[33, 9].Formula = "=J32";
                excel.Worksheets[sheetNumber + 1].Cells[34, 9].Formula = "=J34";
                excel.Worksheets[sheetNumber + 1].Cells[35, 9].Formula = "=J35";
            }
        }

        public static Stream GetImageBinary(int countryID, int rowID, int rowIndex)
        {
            try
            {
                DataSet ds = SqlDataService.GetCountryOverviewKSData(rowID, countryID);
                string values = string.Empty;

                for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                {
                    values = ds.Tables[0].Rows[0][i].ToString().Length > 0 ?
                        values + ds.Tables[0].Rows[0][i].ToString() + "," :
                        values + "0,";
                }
                values = values.Substring(0, values.Length - 1);

                ImageHelper img = new ImageHelper();

                byte[] bytes = rowIndex != -1 ? img.CreateImage("Bars", values, rowIndex) : img.CreateImage("Bars", values);

                return new MemoryStream(bytes);

            }
            catch
            {
                return null;
            }

        }

        public static Workbook ExportToExcelCountryOverviewData(string excelTemplatePath,
             string Header,
             string dmLogoPath,
             DataTable sectionIDsData,
             DataTable sectionsData)
        {
            Workbook excel = new Workbook();
            excel.Open(excelTemplatePath);
            DataTable countryOverview = new DataTable();
            DataView countryView = new DataView();
            Aspose.Cells.Cells exportCells = excel.Worksheets[0].Cells;
            int rowNumber = 9;

            excel.Worksheets[0].Name = "Country Overview";

            #region SettingHeaderInformationOfsheet

            excel.Worksheets[0].Pictures.Add(0, 0, dmLogoPath);

            excel.Worksheets[0].Cells["A7"].Style.Font.IsBold = true;
            excel.Worksheets[0].Cells["A7"].Style.Font.Size = 12;
            excel.Worksheets[0].Cells["A7"].PutValue(Header);

            #endregion

            #region KeyStatistics

            excel.Worksheets[0].Cells[rowNumber, 0].PutValue("Key Statistics");
            for (int i = 0; i < 2; i++)
            {
                excel.Worksheets[0].Cells[rowNumber, i].Style.Pattern = BackgroundType.Solid;
                excel.Worksheets[0].Cells[rowNumber, i].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.Color = excel.GetMatchingColor(Color.White);
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.Size = 8;
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.IsBold = true;
            }

            rowNumber += 1;
            if (HttpContext.Current.Session["KeyStatisticsData"] != null)
            {
                countryOverview = (DataTable)HttpContext.Current.Session["KeyStatisticsData"];
                DataTable keyStatsTable = new DataTable();
                keyStatsTable = countryOverview.Copy();

                for (int i = 0; i < countryOverview.Rows.Count; i++)
                {
                    if (countryOverview.Rows[i]["countryID"] != null && !string.IsNullOrEmpty(countryOverview.Rows[i]["countryID"].ToString()))
                    {
                        Stream strm = GetImageBinary(Convert.ToInt32(countryOverview.Rows[i]["countryID"].ToString()),
                              Convert.ToInt32(countryOverview.Rows[i]["itemID"].ToString()),
                              1);
                        excel.Worksheets[0].Pictures.Add(rowNumber + i, 1, strm);
                    }
                }

                keyStatsTable.Columns.Remove("rowID");
                keyStatsTable.Columns.Remove("itemID");
                keyStatsTable.Columns.Remove("countryID");

                if (keyStatsTable != null)
                {
                    exportCells.ImportDataTable(keyStatsTable, false, rowNumber, 0, false);
                }
            }
            #endregion

            rowNumber += countryOverview.Rows.Count + 1;

            #region Key Statistics Quartile Analysis

            excel.Worksheets[0].Cells[rowNumber, 0].PutValue("Key Statistics Quartile Analysis");
            for (int i = 0; i < 2; i++)
            {
                excel.Worksheets[0].Cells[rowNumber, i].Style.Pattern = BackgroundType.Solid;
                excel.Worksheets[0].Cells[rowNumber, i].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.Color = excel.GetMatchingColor(Color.White);
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.Size = 8;
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.IsBold = true;
            }

            rowNumber += 1;
            if (HttpContext.Current.Session["QuartileData"] != null)
            {
                countryOverview = (DataTable)HttpContext.Current.Session["QuartileData"];
                DataTable QuartileData = new DataTable();
                QuartileData = countryOverview.Copy();

                QuartileData.Columns.Remove("rowID");
                QuartileData.Columns.Remove("countryID");


                if (QuartileData != null)
                {
                    exportCells.ImportDataTable(QuartileData, false, rowNumber, 0, false);
                }
                for (int j = 0; j < QuartileData.Rows.Count; j++)
                {
                    excel.Worksheets[0].Cells[rowNumber + j, 1].Style.Font.IsBold = true;
                }
            }

            #endregion

            rowNumber += countryOverview.Rows.Count;

            #region SectionsData
            if (sectionsData != null)
            {
                for (int i = 0; i < sectionIDsData.Rows.Count; i++)
                {
                    int sectionid = Convert.ToInt16(sectionIDsData.Rows[i][0].ToString());
                    countryView = sectionsData.Copy().DefaultView;
                    countryView.RowFilter = "sectionid=" + sectionid;
                    DataTable SingleSectionData = new DataTable();
                    SingleSectionData = countryView.ToTable();
                    string SectionHeading = sectionIDsData.Rows[i]["Name"].ToString();

                    //Remove excess columns not to extract
                    string[] ColumnsToRemove = GlobalSettings.SectionColumnsToHideInExcel.Split("|".ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
                    foreach (string ColumnName in ColumnsToRemove)
                    {
                        SingleSectionData.Columns.Remove(ColumnName);
                    }
                    
                    rowNumber += 1;

                    excel.Worksheets[0].Cells[rowNumber, 0].PutValue(SectionHeading);
                    excel.Worksheets[0].Cells[rowNumber, 0].Style.Pattern = BackgroundType.Solid;
                    excel.Worksheets[0].Cells[rowNumber, 0].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                    excel.Worksheets[0].Cells[rowNumber, 0].Style.Font.Color = excel.GetMatchingColor(Color.White);
                    excel.Worksheets[0].Cells[rowNumber, 0].Style.Font.Size = 8;
                    excel.Worksheets[0].Cells[rowNumber, 0].Style.Font.IsBold = true;
                    rowNumber += 1;

                    if (SingleSectionData != null)
                    {
                        ExportToExcel(excel,
                            SingleSectionData,
                            rowNumber);

                        rowNumber += SingleSectionData.Rows.Count + 1;
                    }
                }
            }
            #endregion

            excel.Worksheets.RemoveAt(1);

            try
            {
                excel.Worksheets["Title"].Move(0);

                excel.Worksheets[0].Cells[5, 1].PutValue("Date of Extraction" + " : " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
                excel.Worksheets[0].Cells[5, 1].Style.Font.Size = 8;
                excel.Worksheets.ActiveSheetIndex = 0;
            }
            catch
            {
                #region Putting Copyright Info In Excelsheet

                //Putting CopyRight info
                int copyRightInfoRowNumber = rowNumber + 5;
                WriteCopyRightInfo(copyRightInfoRowNumber, excel);

                #endregion
            }

            return excel;
        }

        public static void ExportToExcel(Workbook excel,
               DataTable SingleSectionData,
               int rowNumber)
        {
            Aspose.Cells.Cells exportCells = excel.Worksheets[0].Cells;
            if (SingleSectionData != null && SingleSectionData.Rows.Count > 0)
            {
                exportCells.ImportDataTable(SingleSectionData, true, rowNumber, 0, false);
            }
            for (int i = 0; i < SingleSectionData.Columns.Count; i++)
            {
                excel.Worksheets[0].Cells[rowNumber - 1, i].Style.Pattern = BackgroundType.Solid;
                excel.Worksheets[0].Cells[rowNumber - 1, i].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                excel.Worksheets[0].Cells[rowNumber - 1, i].Style.Font.Color = excel.GetMatchingColor(Color.White);
                excel.Worksheets[0].Cells[rowNumber - 1, i].Style.Font.Size = 8;
                excel.Worksheets[0].Cells[rowNumber - 1, i].Style.Font.IsBold = true;

                excel.Worksheets[0].Cells[rowNumber, i].Style.Pattern = BackgroundType.Solid;
                excel.Worksheets[0].Cells[rowNumber, i].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(217, 225, 236));// 176, 196, 222));
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.Color = excel.GetMatchingColor(Color.Black);
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.Size = 8;
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.IsBold = true;
            }
            for (int j = 2; j < SingleSectionData.Columns.Count; j++)
            {
                excel.Worksheets[0].Cells[rowNumber, j].Style.HorizontalAlignment = TextAlignmentType.Right;
            }
        }

        public static Workbook ExportToExcelCountryComparisonData(string excelTemplatePath,
             string Header,
             string dmLogoPath,
             string selectedYear,
             DataTable ResultsData)
        {
            Workbook excel = new Workbook();
            excel.Open(excelTemplatePath);
            int rowNumber = 9;
            Aspose.Cells.Cells exportCells = excel.Worksheets[0].Cells;

            #region Heading
            excel.Worksheets[0].Pictures.Add(0, 0, dmLogoPath);
            excel.Worksheets[0].Cells["A7"].Style.Font.IsBold = true;
            excel.Worksheets[0].Cells["A7"].Style.Font.Size = 12;
            excel.Worksheets[0].Cells["A7"].PutValue(Header);
            #endregion
            excel.Worksheets[0].Name = Header;

            excel.Worksheets[0].Cells["A9"].PutValue("Year :");
            excel.Worksheets[0].Cells["B9"].PutValue(selectedYear);

            ResultsData.Columns.Remove("rowID");
            ResultsData.Columns.Remove("displayorder");
            if (ResultsData != null)
            {
                exportCells.ImportDataTable(ResultsData, true, rowNumber, 0, false);
            }

            for (int i = 0; i < ResultsData.Columns.Count; i++)
            {
                excel.Worksheets[0].Cells[rowNumber, i].Style.Pattern = BackgroundType.Solid;
                excel.Worksheets[0].Cells[rowNumber, i].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.Color = excel.GetMatchingColor(Color.White);
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.Size = 8;
                excel.Worksheets[0].Cells[rowNumber, i].Style.Font.IsBold = true;
            }
            for (int j = 1; j < ResultsData.Rows.Count; j++)
            {
                excel.Worksheets[0].Cells[rowNumber + j, 4].Style.HorizontalAlignment = TextAlignmentType.Right;
            }

            
            excel.Worksheets.RemoveAt(1);
            try
            {
                excel.Worksheets["Title"].Move(0);

                excel.Worksheets[0].Cells[5, 1].PutValue("Date of Extraction" + " : " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
                excel.Worksheets[0].Cells[5, 1].Style.Font.Size = 8;
                excel.Worksheets.ActiveSheetIndex = 0;
            }
            catch
            {
                #region Putting Copyright Info In Excelsheet

                int copyRightInfoRowNumber = ResultsData.Rows.Count + 13;
                WriteCopyRightInfo(copyRightInfoRowNumber, excel);

                #endregion
            }
            return excel;
        }

        /// <summary>
        /// Exports to excel trend chart.
        /// </summary>
        /// <param name="excelTemplatePath">The excel template path.</param>
        /// <param name="Header">The header.</param>
        /// <param name="dmLogoPath">The dm logo path.</param>
        /// <param name="countryOverviewBitmap">The country overview bitmap.</param>
        /// <param name="sheetName">Name of the sheet.</param>
        /// <param name="chartData">The chart data.</param>
        /// <returns></returns>
        public static Workbook ExportToExcelTrendChart(string excelTemplatePath,
                     string Header,
                     string dmLogoPath,
                     Bitmap countryOverviewBitmap,
                     string sheetName,
                     DataTable chartData)
        {
            Workbook excel = new Workbook();
            excel.Open(excelTemplatePath);
            excel.Worksheets[0].Name = sheetName;
            string SelectionsTextXML = string.Empty;
            //Get filter criteria datatable.
            DataTable FilterCriteriaDatatable = new DataTable();
            Aspose.Cells.Cells exportCells = excel.Worksheets[0].Cells;

            #region SettingHeaderInformationOfsheet(i.e Logo,DateOfExtraction etc)

            excel.Worksheets[0].Pictures.Add(0, 0, dmLogoPath);
            excel.Worksheets[0].Cells["A8"].Style.Font.IsBold = true;
            excel.Worksheets[0].Cells["A8"].Style.Font.Size = 12;
            excel.Worksheets[0].Cells["A8"].PutValue(Header);
            excel.Worksheets[0].AutoFitRow(7);

            #endregion          

            #region Putting Chart Image in ExcelSheet

            if (countryOverviewBitmap != null)
            {
                MemoryStream ms = new MemoryStream();
                countryOverviewBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                excel.Worksheets[0].Pictures.Add(9, 0, ms);
                ms.Flush();
            }

            #endregion

            #region Write the chart data

            if (chartData != null)
            {
                exportCells.ImportDataTable(chartData, true, 23, 0, false);

                for (int i = 0; i < chartData.Columns.Count; i++)
                {
                    excel.Worksheets[0].Cells[23, i].Style.Pattern = BackgroundType.Solid;
                    excel.Worksheets[0].Cells[23, i].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                    excel.Worksheets[0].Cells[23, i].Style.Font.Color = excel.GetMatchingColor(Color.White);
                    excel.Worksheets[0].Cells[23, i].Style.Font.Size = 8;
                    excel.Worksheets[0].Cells[23, i].Style.Font.IsBold = true;
                }
            }

            #endregion            

            excel.Worksheets.RemoveAt(1);
            try
            {
                excel.Worksheets["Title"].Move(0);

                excel.Worksheets[0].Cells[5, 1].PutValue("Date of Extraction" + " : " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
                excel.Worksheets[0].Cells[5, 1].Style.Font.Size = 8;
                excel.Worksheets.ActiveSheetIndex = 0;
            }
            catch
            {
                #region Putting Copyright Info In Excelsheet

                int copyRightInfoRowNumber = 28 + chartData.Rows.Count;
                WriteCopyRightInfo(copyRightInfoRowNumber, excel);

                #endregion
            }
            return excel;
        }

        /// <summary>
        /// Exports to excel trend chart.
        /// </summary>
        /// <param name="excelTemplatePath">The excel template path.</param>
        /// <param name="Header">The header.</param>
        /// <param name="dmLogoPath">The dm logo path.</param>
        /// <param name="countryOverviewBitmap">The country overview bitmap.</param>
        /// <param name="sheetName">Name of the sheet.</param>
        /// <param name="chartData">The chart data.</param>
        /// <returns></returns>
        public static Workbook ExportToExcelQuartileTrendChart(string excelTemplatePath,
                     string Header,
                     string dmLogoPath,                     
                     string sheetName,
                     string quartileYear,
                     string quartileUnits,
                     DataTable chartData)
        {
            Workbook excel = new Workbook();
            excel.Open(excelTemplatePath);
            excel.Worksheets[0].Name = sheetName;
            string SelectionsTextXML = string.Empty;
            //Get filter criteria datatable.
            DataTable FilterCriteriaDatatable = new DataTable();
            Aspose.Cells.Cells exportCells = excel.Worksheets[0].Cells;

            #region SettingHeaderInformationOfsheet(i.e Logo,DateOfExtraction etc)

            excel.Worksheets[0].Pictures.Add(0, 0, dmLogoPath);
            excel.Worksheets[0].Cells["A8"].Style.Font.IsBold = true;
            excel.Worksheets[0].Cells["A8"].Style.Font.Size = 12;
            excel.Worksheets[0].Cells["A8"].PutValue(Header);
            excel.Worksheets[0].AutoFitRow(7);

            #endregion           
           
            excel.Worksheets[0].Cells[4, 0].PutValue("Indicator :");
            excel.Worksheets[0].Cells[4, 0].Style.Font.IsBold = true;
            excel.Worksheets[0].Cells[4, 1].PutValue(quartileYear.ToString());
            excel.Worksheets[0].Cells[5, 0].PutValue("Units :");
            excel.Worksheets[0].Cells[5, 0].Style.Font.IsBold = true;
            excel.Worksheets[0].Cells[5, 1].PutValue(quartileUnits.ToString());
            

            #region Write the chart data

            if (chartData != null)
            {
                exportCells.ImportDataTable(chartData, true, 7, 0, false);

                for (int i = 0; i < chartData.Columns.Count; i++)
                {
                    excel.Worksheets[0].Cells[7, i].Style.Pattern = BackgroundType.Solid;
                    excel.Worksheets[0].Cells[7, i].Style.ForegroundColor = excel.GetMatchingColor(Color.FromArgb(0, 34, 82));
                    excel.Worksheets[0].Cells[7, i].Style.Font.Color = excel.GetMatchingColor(Color.White);
                    excel.Worksheets[0].Cells[7, i].Style.Font.Size = 8;
                    excel.Worksheets[0].Cells[7, i].Style.Font.IsBold = true;
                }
            }

            #endregion

            excel.Worksheets.RemoveAt(1);
            try
            {
                excel.Worksheets["Title"].Move(0);

                excel.Worksheets[0].Cells[5, 1].PutValue("Date of Extraction" + " : " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
                excel.Worksheets[0].Cells[5, 1].Style.Font.Size = 8;
                excel.Worksheets.ActiveSheetIndex = 0;
            }
            catch
            {
                #region Putting Copyright Info In Excelsheet

                int copyRightInfoRowNumber = 18 + chartData.Rows.Count;
                WriteCopyRightInfo(copyRightInfoRowNumber, excel);

                #endregion
            }
            return excel;
        }

        /// <summary>
        /// Exports the country profiler to excel.
        /// </summary>
        /// <param name="charts">The charts.</param>
        /// <param name="excelTemplatePath">The excel template path.</param>
        /// <param name="logoPath">The logo path.</param>
        /// <param name="pageTitle">The page title.</param>
        /// <returns></returns>
        public static Workbook ExportCountryProfilerToExcel(Dictionary<string, Dictionary<string, Bitmap>> charts,
                    string excelTemplatePath,
                    string logoPath,
                    string pageTitle)
        {
            Workbook excel = new Workbook();
            excel.Open(excelTemplatePath);
                                    
            Aspose.Cells.Cells exportCells = excel.Worksheets[0].Cells;

            #region SettingHeaderInformationOfsheet(i.e Logo,DateOfExtraction etc)

            excel.Worksheets[0].Pictures.Add(0, 0, logoPath);

            excel.Worksheets[0].Cells["A7"].Style.Font.IsBold = true;
            excel.Worksheets[0].Cells["A7"].Style.Font.Size = 12;
            excel.Worksheets[0].Cells["A7"].PutValue(pageTitle);

            #endregion

            #region Putting Chart Images in ExcelSheet

            excel.Worksheets.RemoveAt(1);

            int CopyrightInfoRowNumber=20;

            MemoryStream MemoryStreamData = new MemoryStream();
            int WorksheetCounter = 0;
            foreach (KeyValuePair<string, Dictionary<string, Bitmap>> Section in charts)
            {
                int RowNumber = 9;
                excel.Worksheets[WorksheetCounter].Name = Section.Key;
                excel.Worksheets[WorksheetCounter].Cells["A" + RowNumber.ToString()].PutValue(Section.Key);
                excel.Worksheets[WorksheetCounter].Cells["A" + RowNumber.ToString()].Style.Font.IsBold = true;
                excel.Worksheets[WorksheetCounter].Cells["A" + RowNumber.ToString()].Style.Font.Size = 10;
                RowNumber += 2;
                Dictionary<string, Bitmap> SectionCharts = Section.Value;
                int Counter = 0;
                foreach (KeyValuePair<string, Bitmap> SectionChart in SectionCharts)
                {   
                    excel.Worksheets[WorksheetCounter].Cells[RowNumber, Counter % 2].PutValue(SectionChart.Key);
                    excel.Worksheets[WorksheetCounter].Cells[RowNumber, Counter % 2].Style.Font.IsBold = true;
                    SectionChart.Value.Save(MemoryStreamData, System.Drawing.Imaging.ImageFormat.Bmp);
                    excel.Worksheets[WorksheetCounter].Pictures.Add(RowNumber + 2, Counter % 2, MemoryStreamData);
                    if (Counter % 2 == 1)
                    {
                        RowNumber += 20;
                    }
                    Counter++;
                    MemoryStreamData.Flush();
                }
                excel.Worksheets[WorksheetCounter].Cells.SetColumnWidth(0, 60);
                excel.Worksheets.Add();
                if (WorksheetCounter == 0)
                {
                    CopyrightInfoRowNumber = RowNumber;
                }
                WorksheetCounter++;

            }
            excel.Worksheets.RemoveAt(WorksheetCounter);

            #endregion                        
            
            WriteCopyRightInfo(CopyrightInfoRowNumber + 25, excel);

            return excel;
        }

        #endregion

       #region WORD extractions

        /// <summary> 
        /// Exports to Word.
        /// </summary>
        /// <param name="chartBitmap">chartBitmap</param>
        /// <param name="ChartType">the chart type</param>
        /// <param name="docTempPath">document Template Path</param>
        /// <param name="HeaderTxt"></param>
        /// <returns></returns>                

        public static Document ExportToWord(DataTable chartData,
            Bitmap chartBitmap,
            Bitmap legendBitmap,
            string chartType,
            string wordTemplatePath,
            string HeaderTxt)
        {
            string LegendImagePath = HttpContext.Current.Server.MapPath("~/Assets/Images/Maps/HeatMapLegend.bmp");

            Document document = new Document(wordTemplatePath);
            DocumentBuilder builder = new DocumentBuilder(document);
            
            builder.MoveToHeaderFooter(HeaderFooterType.HeaderEven);
            builder.Font.Size = 14;
            builder.Write(HeaderTxt);
            builder.MoveToHeaderFooter(HeaderFooterType.HeaderPrimary);
            builder.Font.Size = 14;
            builder.Write(HeaderTxt);
            builder.PageSetup.Orientation = Aspose.Words.Orientation.Landscape;

            double docUseableWidth = builder.PageSetup.PageWidth - builder.PageSetup.LeftMargin - builder.PageSetup.RightMargin;
            double docUsableHeight = builder.PageSetup.PageHeight - builder.PageSetup.TopMargin - builder.PageSetup.BottomMargin;

            builder.RowFormat.AllowAutoFit = false;
            builder.MoveToSection(0);
            builder.Font.Size = 14;
            builder.Write(HeaderTxt);
            builder.Writeln("\nDate of Extraction : " + DateTime.Now.ToLongDateString());
            builder.MoveToSection(1);
            builder.Font.Size = 14;

            builder.RowFormat.Height = 10;
            builder.Font.Size = 8;
            builder.Font.Bold = false;

            //builder.InsertBreak(BreakType.PageBreak);

            builder.Font.Bold = true;

            if (chartBitmap != null)
            {                
                //builder.InsertImage(chartBitmap, RelativeHorizontalPosition.Page, 15, RelativeVerticalPosition.Line, 45, 550, 270, WrapType.None, WrapSide.Both, false, null);
                //builder.InsertImage(chartBitmap, RelativeHorizontalPosition.Page, 100, RelativeVerticalPosition.Line, 30, 480, 300, WrapType.None, WrapSide.Both, false, null);
                //builder.InsertImage(legendBitmap, RelativeHorizontalPosition.Page, 100, RelativeVerticalPosition.Line, 350, 400, 50, WrapType.None, WrapSide.Both, false, null);
                builder.InsertImage(chartBitmap);
            }
            if (legendBitmap != null)
            {
                builder.InsertImage(legendBitmap);
            }
            //builder.InsertImage(chartBitmap, RelativeHorizontalPosition.Page, 150, RelativeVerticalPosition.Line, 60, 350, 250, WrapType.Square,WrapSide.Both,false,null);                              

            builder.InsertBreak(BreakType.LineBreak);
            double Column1Width = 100;
            double Column2Width = 400;

            #region Filter criteria Table
            if (chartType == "XYChart" ||
                chartType == "PieChart" ||
                chartType == "HeatMap" ||
                chartType == "CountryComparisonChart" ||
                chartType == "IndicatorComparisonChart" ||
                chartType == "CustomChart" ||
                chartType == "BasicBubbleChart")
            {
                if (HttpContext.Current.Session["SelectionsTextXML"] != null)
                {                    
                    string SelectionsTextXML = HttpContext.Current.Session["SelectionsTextXML"].ToString();
                    SelectionsTextXML = SelectionsTextXML.Replace("&", "amp;");
                    DataSet FilterCriteriaDataset = new DataSet();
                    StringReader stringReader = new StringReader(SelectionsTextXML);
                    FilterCriteriaDataset.ReadXml(stringReader);
                    DataTable FilterCriteriaDatatable = FilterCriteriaDataset.Tables[0];   //LoadSelections();

                    if (FilterCriteriaDatatable.Rows.Count > 0)
                    {
                        //start table
                        Aspose.Words.Table tbl = builder.StartTable();

                        //specify formating of cells
                        builder.CellFormat.Borders.LineStyle = Aspose.Words.LineStyle.Single;
                        builder.CellFormat.Borders.LineWidth = 1;
                        builder.CellFormat.Borders.Color = Color.Black;
                        builder.CellFormat.TopPadding = 3;

                        //header row
                        builder.Font.Bold = true;
                        builder.CellFormat.Shading.BackgroundPatternColor = System.Drawing.Color.FromArgb(0, 34, 82);
                        builder.Font.Color = Color.White;
                        builder.CellFormat.Width = Column1Width + Column2Width;
                        builder.InsertCell();
                        builder.Write("Data filtered by");
                        builder.EndRow();

                        //data rows
                        builder.Font.Bold = false;
                        builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                        builder.Font.Color = Color.Black;
                        for (int k = 0; k < FilterCriteriaDatatable.Rows.Count; k++)
                        {
                            builder.InsertCell();
                            builder.CellFormat.Width = Column1Width;
                            builder.Write(FilterCriteriaDatatable.Rows[k][0].ToString());

                            builder.InsertCell();
                            builder.CellFormat.Width = Column2Width;
                            builder.Write(Microsoft.JScript.GlobalObject.unescape(FilterCriteriaDatatable.Rows[k][1].ToString().Replace("amp;", "&")));

                            builder.EndRow();
                        }
                    }                    
                }
            }
           #endregion

            builder.InsertBreak(BreakType.LineBreak);

            #region ChartData
            if (chartData.Rows.Count > 0)
            {
                DataTable chartTable = chartData.Copy();
                for (int j = 0; j < chartTable.Columns.Count; j++)
                {
                    if (chartTable.Columns[j].ColumnName.Contains("rowID"))
                    {
                        chartData.Columns.Remove(chartData.Columns["rowID"].ColumnName.ToString());
                    }
                    if (chartTable.Columns[j].ColumnName.Contains("FusionID"))
                    {
                        chartData.Columns.Remove(chartData.Columns["FusionID"].ColumnName.ToString());
                    }
                }

                //start table
                Aspose.Words.Table tbl = builder.StartTable();
                //specify formating of cells
                builder.CellFormat.Borders.LineStyle = Aspose.Words.LineStyle.Single;
                builder.CellFormat.Borders.LineWidth = 1;
                builder.CellFormat.Borders.Color = Color.Black;
                builder.CellFormat.TopPadding = 3;

                //header row
                builder.Font.Bold = true;
                builder.CellFormat.Shading.BackgroundPatternColor = System.Drawing.Color.FromArgb(0, 34, 82);
                builder.Font.Color = Color.White;
                for (int i = 0; i < chartData.Columns.Count; i++)
                {
                    builder.InsertCell();
                    //builder.CellFormat.Width = (i == 0) ? Column1Width : Column2Width;
                    builder.CellFormat.Width = Column1Width;
                    builder.Write(chartData.Columns[i].ColumnName);
                }
                builder.EndRow();

                //data rows
                builder.Font.Bold = false;
                builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                builder.Font.Color = Color.Black;
                for (int k = 0; k < chartData.Rows.Count; k++)
                {
                    for (int j = 0; j < chartData.Columns.Count; j++)
                    {
                        builder.InsertCell();
                        //builder.CellFormat.Width = (j == 0) ? Column1Width : Column2Width;
                        builder.CellFormat.Width = Column1Width;
                        builder.Write(chartData.Rows[k][j].ToString());
                    }
                    builder.EndRow();
                }
            }

            #endregion

            return document;
        }

        public static Document ExportCountryOverviewDatatoWord(string wordTemplatePath,
               string HeaderTxt,
               string dmLogoPath,
               DataTable sectionIDsData,
               DataTable sectionsData)
        {
            Document document = new Document(wordTemplatePath);
            DocumentBuilder builder = new DocumentBuilder(document);
            DataView countryView = new DataView();

            builder.MoveToHeaderFooter(HeaderFooterType.HeaderEven);
            builder.Font.Size = 14;
            builder.Write(HeaderTxt);
            builder.MoveToHeaderFooter(HeaderFooterType.HeaderPrimary);
            builder.Font.Size = 14;
            builder.Write(HeaderTxt);
            builder.PageSetup.Orientation = Aspose.Words.Orientation.Landscape;

            double docUseableWidth = builder.PageSetup.PageWidth - builder.PageSetup.LeftMargin - builder.PageSetup.RightMargin;
            double docUsableHeight = builder.PageSetup.PageHeight - builder.PageSetup.TopMargin - builder.PageSetup.BottomMargin;

            builder.RowFormat.AllowAutoFit = false;
            builder.MoveToSection(0);
            builder.Font.Size = 14;
            builder.Write(HeaderTxt);
            builder.Writeln("\nDate of Extraction : " + DateTime.Now.ToLongDateString());
            builder.MoveToSection(1);
            builder.Font.Size = 14;

            builder.RowFormat.Height = 10;
            builder.Font.Size = 8;
            builder.Font.Name = "Arial";
            //builder.Font.Bold = false;

            //builder.InsertBreak(BreakType.PageBreak);
            builder.Font.Bold = true;

            #region KeyStatistics

            if (HttpContext.Current.Session["KeyStatisticsData"] != null)
            {
                Aspose.Words.Table tbl = builder.StartTable();               
                //Specify formatting of cells
                builder.CellFormat.Borders.LineStyle = Aspose.Words.LineStyle.Single;
                builder.CellFormat.Borders.LineWidth = 1;
                builder.CellFormat.Borders.Color = Color.Black;
               // builder.CellFormat.TopPadding = 2;
                //Header row
                builder.Font.Bold = true;
                builder.CellFormat.Shading.BackgroundPatternColor = System.Drawing.Color.FromArgb(0, 34, 82);
                builder.Font.Color = Color.White;
                builder.CellFormat.Width = 250;                
                builder.InsertCell();
                builder.Write("Key Statistics");
                builder.EndRow();

                //data rows
                builder.Font.Bold = false;
                builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                builder.Font.Color = Color.Black;
                DataTable countryOverview = new DataTable();               
                countryOverview = (DataTable)HttpContext.Current.Session["KeyStatisticsData"];
                DataTable keyStatsTable = new DataTable();
               
                for (int i = 0; i < countryOverview.Rows.Count; i++)
                {
                    if (countryOverview.Rows[i]["countryID"] != null && !string.IsNullOrEmpty(countryOverview.Rows[i]["countryID"].ToString()))
                    {
                        builder.InsertCell();
                        builder.CellFormat.Width = 150;
                        builder.Write(countryOverview.Rows[i]["Indicator"].ToString());

                        builder.InsertCell();
                        builder.CellFormat.Width = 100;
                        Stream strm = GetImageBinary(Convert.ToInt32(countryOverview.Rows[i]["countryID"].ToString()),
                              Convert.ToInt32(countryOverview.Rows[i]["itemID"].ToString()),
                                  1);
                        System.Drawing.Image keyStatImage = System.Drawing.Image.FromStream(strm);
                        builder.InsertImage(keyStatImage);
                        builder.EndRow();
                    }
                }
            }
            builder.EndTable();          
            
            #endregion

            builder.InsertBreak(BreakType.LineBreak);

            #region KeyStatistics Quartile Analysis

            if (HttpContext.Current.Session["QuartileData"] != null)
            {
                Aspose.Words.Table tbl = builder.StartTable();               
                //Specify formatting of cells
                builder.CellFormat.Borders.LineStyle = Aspose.Words.LineStyle.Single;
                builder.CellFormat.Borders.LineWidth = 1;
                builder.CellFormat.Borders.Color = Color.Black;
                builder.CellFormat.TopPadding = 2;
                //Header row
                builder.Font.Bold = true;
                builder.CellFormat.Shading.BackgroundPatternColor = System.Drawing.Color.FromArgb(0, 34, 82);
                builder.Font.Color = Color.White;
                builder.CellFormat.Width = 250;
                builder.InsertCell();
                builder.Write("Key Statistics Quartile Analysis");
                builder.EndRow();

                //data rows
                builder.Font.Bold = false;
                builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                builder.Font.Color = Color.Black;

                DataTable QuartileData = new DataTable();
                QuartileData = (DataTable)HttpContext.Current.Session["QuartileData"];

                for (int i = 0; i < QuartileData.Rows.Count; i++)
                {
                    builder.InsertCell();
                    builder.CellFormat.Width = 150;
                    builder.Write(QuartileData.Rows[i]["Indicator"].ToString());

                    builder.InsertCell();
                    builder.CellFormat.Width = 100;
                    builder.Write(QuartileData.Rows[i]["Rank"].ToString());
                    builder.EndRow();
                }
            }
            builder.EndTable();
           
            #endregion

            builder.InsertBreak(BreakType.PageBreak);

            #region Sections data
            if (sectionsData != null)
            {
                for (int i = 0; i < sectionIDsData.Rows.Count; i++)
                {
                    int sectionid = Convert.ToInt16(sectionIDsData.Rows[i][0].ToString());
                    countryView = sectionsData.Copy().DefaultView;
                    countryView.RowFilter = "sectionid=" + sectionid;
                    DataTable SingleSectionData = new DataTable();
                    SingleSectionData = countryView.ToTable();
                    string SectionHeading = sectionIDsData.Rows[i]["Name"].ToString();

                    Aspose.Words.Table tbl = builder.StartTable();
                    builder.RowFormat.Alignment = RowAlignment.Right;
                    //Specify formatting of cells
                    builder.CellFormat.Borders.LineStyle = Aspose.Words.LineStyle.Single;
                    builder.CellFormat.Borders.LineWidth = 1;
                    builder.CellFormat.Borders.Color = Color.Black;                   
                    //Header row
                    builder.Font.Bold = true;
                    builder.CellFormat.Shading.BackgroundPatternColor = System.Drawing.Color.FromArgb(0, 34, 82);
                    builder.Font.Color = Color.White;
                    builder.CellFormat.Width = 580;
                    builder.InsertCell();
                    builder.Write(SectionHeading);
                    builder.EndRow();

                    SingleSectionData.Columns.Remove("rowid");
                    SingleSectionData.Columns.Remove("itemid");
                    SingleSectionData.Columns.Remove("countryID");
                    SingleSectionData.Columns.Remove("sectionID");
                    SingleSectionData.Columns.Remove("IndentLevel");
                    SingleSectionData.Columns.Remove("Rounding");

                    if (SingleSectionData != null)
                    {
                        exportSingleSectionDatatoWord(builder, SingleSectionData);
                    }                   
                }
            }
            #endregion

            return document;         
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="singleSectionData"></param>
        /// <returns></returns>
        public static DocumentBuilder exportSingleSectionDatatoWord(DocumentBuilder builder,
              DataTable singleSectionData)
        {
            int cellWidth = 0;
            //Add Column names
            for (int j = 0; j < singleSectionData.Columns.Count; j++)
            {
                if (j == 0)
                    cellWidth = 150;
                else if (j == 1)
                    cellWidth = 45;
                else
                    cellWidth = 55;

                builder.Font.Bold = true ;
                builder.CellFormat.Shading.BackgroundPatternColor = Color.Lavender;
                builder.Font.Color = Color.Black;
                builder.InsertCell();
                builder.CellFormat.Width = cellWidth;       
                builder.Write(singleSectionData.Columns[j].ColumnName.ToString());

                if (j > 1)
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                else
                    builder.ParagraphFormat.Alignment = ParagraphAlignment.Left;
            }
            builder.EndRow();
            //Add data rows
            for (int i = 0; i < singleSectionData.Rows.Count;i++)
            {
                for (int j = 0; j < singleSectionData.Columns.Count; j++)
                {
                    if (j == 0)
                        cellWidth = 150;
                    else if (j == 1)
                        cellWidth = 45;
                    else
                        cellWidth = 55;                    
                    builder.Font.Bold = false;
                    //builder.ParagraphFormat.ClearFormatting();

                    if (i % 2 == 0)
                        builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                    else
                        builder.CellFormat.Shading.BackgroundPatternColor = Color.Lavender;
                    builder.Font.Color = Color.Black;

                    builder.InsertCell();    
                    builder.CellFormat.Width = cellWidth;  
                    builder.Write(singleSectionData.Rows[i][j].ToString());
                    if (j > 1)
                        builder.ParagraphFormat.Alignment = ParagraphAlignment.Right;
                    else
                        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left;
                    //builder.ParagraphFormat.ClearFormatting();                    
                }               
                builder.EndRow();
            }           
           
            builder.EndTable();
            builder.InsertBreak(BreakType.LineBreak);
            return builder;
        }

        /// <summary>
        /// Exports to Word.
        /// </summary>
        /// <param name="chartBitmap">chartBitmap</param>
        /// <param name="ChartType">the chart type</param>
        /// <param name="docTempPath">document Template Path</param>
        /// <param name="HeaderTxt"></param>
        /// <returns></returns>
        public static Document ExportToWord(DataTable chartData,
            Bitmap chartBitmap,
            string ChartType,
            string wordTemplatePath,
            string HeaderTxt)
        {
            Document document = new Document(wordTemplatePath);
            DocumentBuilder builder = new DocumentBuilder(document);

            builder.MoveToHeaderFooter(HeaderFooterType.HeaderEven);
            builder.Font.Size = 14;
            builder.Write(HeaderTxt);
            builder.MoveToHeaderFooter(HeaderFooterType.HeaderPrimary);
            builder.Font.Size = 14;
            builder.Write(HeaderTxt);
            builder.PageSetup.Orientation = Aspose.Words.Orientation.Landscape;

            double docUseableWidth = builder.PageSetup.PageWidth - builder.PageSetup.LeftMargin - builder.PageSetup.RightMargin;
            double docUsableHeight = builder.PageSetup.PageHeight - builder.PageSetup.TopMargin - builder.PageSetup.BottomMargin;

            builder.RowFormat.AllowAutoFit = false;
            builder.MoveToSection(0);
            builder.Font.Size = 14;
            builder.Write(HeaderTxt);
            builder.Writeln("\nDate of Extraction : " + DateTime.Now.ToLongDateString());
            builder.MoveToSection(1);
            builder.Font.Size = 14;

            builder.RowFormat.Height = 10;
            builder.Font.Size = 8;
            builder.Font.Bold = false;

            builder.InsertBreak(BreakType.PageBreak);

            builder.Font.Bold = true;

            if (chartBitmap != null)
            {
                //builder.InsertImage(chartBitmap, RelativeHorizontalPosition.Page, 15, RelativeVerticalPosition.Line, 45, 550, 270, WrapType.None, WrapSide.Both, false, null);
                //builder.InsertImage(chartBitmap, RelativeHorizontalPosition.Page, 100, RelativeVerticalPosition.Line, 30, 480, 300, WrapType.None, WrapSide.Both, false, null);
                builder.InsertImage(chartBitmap);
                //builder.InsertImage(chartBitmap, RelativeHorizontalPosition.Page, 150, RelativeVerticalPosition.Line, 60, 350, 250, WrapType.Square,WrapSide.Both,false,null);                              
            }
            double Column1Width = 100;
            
            builder.InsertBreak(BreakType.LineBreak);

            if (chartData.Rows.Count > 0)
            {
                //start table
                Aspose.Words.Table tbl = builder.StartTable();
                //specify formating of cells
                builder.CellFormat.Borders.LineStyle = Aspose.Words.LineStyle.Single;
                builder.CellFormat.Borders.LineWidth = 1;
                builder.CellFormat.Borders.Color = Color.Black;
                builder.CellFormat.TopPadding = 3;

                //header row
                builder.Font.Bold = true;
                builder.CellFormat.Shading.BackgroundPatternColor = System.Drawing.Color.FromArgb(0, 34, 82);
                builder.Font.Color = Color.White;
                for (int i = 0; i < chartData.Columns.Count; i++)
                {
                    builder.InsertCell();
                    builder.CellFormat.Width = Column1Width;
                    builder.Write(chartData.Columns[i].ColumnName);
                }
                builder.EndRow();

                //data rows
                builder.Font.Bold = false;
                builder.CellFormat.Shading.BackgroundPatternColor = Color.White;
                builder.Font.Color = Color.Black;
                for (int k = 0; k < chartData.Rows.Count; k++)
                {
                    for (int j = 0; j < chartData.Columns.Count; j++)
                    {
                        builder.InsertCell();
                        builder.CellFormat.Width = Column1Width;
                        builder.Write(chartData.Rows[k][j].ToString());
                    }
                    builder.EndRow();
                }
            }

            return document;
        }
#endregion

       #region PPT extractions

        /// <summary>
        /// Exports to powerpoint.
        /// </summary>
        /// <param name="chartBitmap">The chartBitmap.</param>
        /// <param name="chartType">the chart type</param>
        /// <param name="pptTempPath">The powerpoint template path.</param>
        /// <param name="HeaderTxt">The header</param>
        /// <returns></returns>
        public static Presentation ExportToPowerpoint(DataTable chartData,
            Bitmap chartBitmap,
            string chartType,
            string pptTemplatePath, 
            string HeaderTxt)
        {
            string LegendImagePath = HttpContext.Current.Server.MapPath("~/Assets/Images/Maps/HeatMapLegend.bmp");
            Presentation presentation = new Presentation(pptTemplatePath);

            Slide slide = presentation.GetSlideByPosition(2);
            int pictureId = presentation.Pictures.Add(new Aspose.Slides.Picture(presentation, chartBitmap));
            int LegendImageId = presentation.Pictures.Add(new Aspose.Slides.Picture(presentation, LegendImagePath));

            if (presentation.Slides[0].Placeholders[0] is TextHolder)
            {
                TextHolder txtHolder = (TextHolder)presentation.Slides[0].Placeholders[0];
                if (txtHolder.Text.ToLower() == "t2")
                {
                    txtHolder.Text = "";
                }
            }

            //Calculating picture width and height
            int pictureWidth = presentation.Pictures[pictureId - 1].Image.Width * 4;
            int pictureHeight = presentation.Pictures[pictureId - 1].Image.Height * 4;
            //Calculating slide width and height
            int slideWidth = slide.Parent.SlideSize.Width;
            int slideHeight = slide.Parent.SlideSize.Height;

            //Calculating the width and height of picture frame
            int pictureFrameWidth = Convert.ToInt32(slideWidth / 2 - pictureWidth / 2);
            int pictureFrameHeight = Convert.ToInt32(slideHeight / 2 - pictureHeight / 2);

            int useableHeght = presentation.Slides[presentation.Slides.Count - 1].Parent.SlideSize.Height - 1200;
            int slideUasbleWidth = presentation.Slides[presentation.Slides.Count - 1].Parent.SlideSize.Width - 1200;
            int left = 400;
            int top = 1000;

            //adding the header on top of the slide
            Aspose.Slides.Rectangle rectangle2 = slide.Shapes.AddRectangle(left, 400, slideUasbleWidth - 100, 500);
            rectangle2.LineFormat.ShowLines = false;
            rectangle2.AddTextFrame(HeaderTxt);
            rectangle2.TextFrame.WrapText = true;
            rectangle2.TextFrame.Paragraphs[0].Portions[0].FontIndex = 1;
            //Adding chart image            
            slide.Shapes.AddPictureFrame(pictureId, 400, pictureFrameHeight - 600, pictureWidth + pictureWidth / 2, pictureHeight + pictureHeight / 2);

            //adding filter criteria
            if (chartType == "HeatMap" )
            {
                if (HttpContext.Current.Session["SelectionsTextXML"] != null)
                {
                    string SelectionsTextXML = HttpContext.Current.Session["SelectionsTextXML"].ToString();
                    SelectionsTextXML = SelectionsTextXML.Replace("&", "amp;");
                    DataSet FilterCriteriaDataset = new DataSet();
                    StringReader stringReader = new StringReader(SelectionsTextXML);
                    FilterCriteriaDataset.ReadXml(stringReader);
                    DataTable FilterCriteriaDatatable = FilterCriteriaDataset.Tables[0];   //LoadSelections();

                    if (FilterCriteriaDatatable.Rows.Count > 0)
                    {
                        AddFilteringCriteria(slide, 4000, 800, 1400, 1500, FilterCriteriaDatatable);
                    }
                }
            }
            //Adding chart data
            slide = presentation.AddEmptySlide();           
            if (chartData.Rows.Count > 0)
            {
                DataTable chartTable = chartData.Copy();
                for (int j = 0; j < chartTable.Columns.Count; j++)
                {
                    if (chartTable.Columns[j].ColumnName.Contains("rowID"))
                    {
                        chartData.Columns.Remove(chartData.Columns["rowID"].ColumnName.ToString());
                    }
                    if (chartTable.Columns[j].ColumnName.Contains("FusionID"))
                    {
                        chartData.Columns.Remove(chartData.Columns["FusionID"].ColumnName.ToString());
                    }
                }

                AddChartData(HeaderTxt,presentation, slide, chartData, 0, 0, 0, slideUasbleWidth, left, top, useableHeght, 0, false);
            }  

            //Adding extracted date
            for (int i = 0; i < presentation.Slides[0].Shapes.Count; i++)
            {
                if (presentation.Slides[0].Shapes[i] is Aspose.Slides.Rectangle)
                {
                    Aspose.Slides.Rectangle txtHolder = (Aspose.Slides.Rectangle)presentation.Slides[0].Shapes[i];
                    if (txtHolder.TextFrame.Text == "Date")
                    {
                        txtHolder.TextFrame.Text = "Extracted on : " + DateTime.Today.ToLongDateString();
                        //txtHolder.Paragraphs[0].Portions[0].FontBold = true;
                        txtHolder.TextFrame.Paragraphs[0].Portions[0].FontColor = Color.Orange;
                        txtHolder.TextFrame.Paragraphs[0].HasBullet = 0;
                    }
                    else if (txtHolder.TextFrame.Text.ToLower() == "t1")
                    {
                        txtHolder.TextFrame.Text = HeaderTxt;
                        txtHolder.TextFrame.WrapText = true;
                        txtHolder.TextFrame.Paragraphs[0].Portions[0].FontIndex = 1;
                        txtHolder.TextFrame.Paragraphs[0].Portions[0].FontBold = false;
                        txtHolder.TextFrame.Paragraphs[0].Portions[0].FontHeight = 18;
                    }
                }
            }

            Slide slideLast = presentation.GetSlideByPosition(1);
            slideLast.SlidePosition = presentation.Slides.Count;

            return presentation;
        }

        /// <summary>
        /// Exports to powerpoint.
        /// </summary>
        /// <param name="chartBitmap">The chartBitmap</param>
        /// <param name="legendBitmap">The legendBitmap</param>
        /// <param name="chartType">the chart type</param>
        /// <param name="pptTempPath">The powerpoint template path.</param>
        /// <param name="HeaderTxt">The header</param>
        /// <returns></returns>       

        public static Presentation ExportToPowerpoint(DataTable chartData,
            Bitmap chartBitmap,
            Bitmap legendBitmap,
            string chartType,
            string pptTemplatePath,
            string HeaderTxt)
        {
            Presentation presentation = new Presentation(pptTemplatePath);
            int pictureId = 0;
            int LegendImageId = 0;

            Slide slide = presentation.GetSlideByPosition(2);
            pictureId = presentation.Pictures.Add(new Aspose.Slides.Picture(presentation, chartBitmap));            
            if (legendBitmap != null)
            {
                LegendImageId = presentation.Pictures.Add(new Aspose.Slides.Picture(presentation, legendBitmap));
            }

            //Calculating picture width and height
            int pictureWidth = presentation.Pictures[pictureId - 1].Image.Width * 4;
            int pictureHeight = presentation.Pictures[pictureId - 1].Image.Height * 4;
            //Calculating slide width and height
            int slideWidth = slide.Parent.SlideSize.Width;
            int slideHeight = slide.Parent.SlideSize.Height;            
            //Calculating the width and height of picture frame
            int pictureFrameWidth = Convert.ToInt32(slideWidth / 2 - pictureWidth / 2);
            int pictureFrameHeight = Convert.ToInt32(slideHeight / 2 - pictureHeight / 2);

            int slideUseableHeight = presentation.Slides[presentation.Slides.Count - 1].Parent.SlideSize.Height - 1200;
            int slideUseableWidth = presentation.Slides[presentation.Slides.Count - 1].Parent.SlideSize.Width - 1200;
            int left = 350;
            int top = 850;

            //adding the header on top of the slide
            Aspose.Slides.Rectangle rectangle2 = slide.Shapes.AddRectangle(left, 400, slideUseableWidth - 100, 500);
            rectangle2.LineFormat.ShowLines = false;
            rectangle2.AddTextFrame(HeaderTxt);
            rectangle2.TextFrame.WrapText = true;
            rectangle2.TextFrame.Paragraphs[0].Portions[0].FontIndex = 1;
            //Adding chart image
            slide.Shapes.AddPictureFrame(pictureId, 400, pictureFrameHeight - 600, pictureWidth + pictureWidth / 2, pictureHeight + pictureHeight / 2);
            //Adding Legend image      
            if (legendBitmap != null)
            {
                if (chartType == "XYChart" ||
                    chartType == "PieChart" ||
                    chartType == "CustomChart" ||
                    chartType == "CountryComparisonChart" ||
                    chartType == "IndicatorComparisonChart" )
                {
                    int legendWidth = presentation.Pictures[LegendImageId - 1].Image.Width * 4;
                    int legendHeight = presentation.Pictures[LegendImageId - 1].Image.Height * 4;

                    slide.Shapes.AddPictureFrame(LegendImageId, 600, pictureFrameHeight - 600 + pictureHeight + pictureHeight / 2, legendWidth + legendWidth / 2, legendHeight + legendHeight / 2);
                }
            }
            //adding filter criteria
            if (chartType == "XYChart" ||
                chartType == "PieChart" ||
                chartType == "CountryComparisonChart" ||
                chartType == "IndicatorComparisonChart" ||
                chartType == "CustomChart" ||                
                chartType == "BasicBubbleChart")
            {
                if (HttpContext.Current.Session["SelectionsTextXML"] != null)
                {
                    string SelectionsTextXML = HttpContext.Current.Session["SelectionsTextXML"].ToString();
                    SelectionsTextXML = SelectionsTextXML.Replace("&", "amp;");
                    DataSet FilterCriteriaDataset = new DataSet();
                    StringReader stringReader = new StringReader(SelectionsTextXML);
                    FilterCriteriaDataset.ReadXml(stringReader);
                    DataTable FilterCriteriaDatatable = FilterCriteriaDataset.Tables[0];   //LoadSelections();

                    if (FilterCriteriaDatatable.Rows.Count > 0)
                    {
                        AddFilteringCriteria(slide, 4000, 800, 1400, 1500, FilterCriteriaDatatable);
                    }
                }
            }
            slide = presentation.AddEmptySlide();
            slide = presentation.Slides[presentation.Slides.Count - 1];
            //Adding header to new slide
            Aspose.Slides.Rectangle rectangle = slide.Shapes.AddRectangle(400, 400, slideWidth - 100, 500);
            rectangle.LineFormat.ShowLines = false;
            rectangle.AddTextFrame(HeaderTxt);
            rectangle.TextFrame.WrapText = true;
            rectangle.TextFrame.Paragraphs[0].Portions[0].FontIndex = 1;
            
            //Adding chart data
            if (chartData.Rows.Count > 0)
            {
               AddChartData(HeaderTxt,presentation, slide, chartData, 0, 0, 0, slideUseableWidth, left, top, slideUseableHeight, 0, false);
            }           

            for (int i = 0; i < presentation.Slides[0].Shapes.Count; i++)
            {
                if (presentation.Slides[0].Shapes[i] is Aspose.Slides.Rectangle)
                {
                    Aspose.Slides.Rectangle txtHolder = (Aspose.Slides.Rectangle)presentation.Slides[0].Shapes[i];
                    if (txtHolder.TextFrame.Text == "Date")
                    {
                        txtHolder.TextFrame.Text = "Extracted on : " + DateTime.Today.ToLongDateString();
                        //txtHolder.Paragraphs[0].Portions[0].FontBold = true;
                        txtHolder.TextFrame.Paragraphs[0].Portions[0].FontColor = Color.Orange;
                        txtHolder.TextFrame.Paragraphs[0].HasBullet = 0;
                        txtHolder.TextFrame.Paragraphs[0].Portions[0].FontHeight = 16;
                    }
                    else if (txtHolder.TextFrame.Text.ToLower() == "t1")
                    {
                        txtHolder.TextFrame.Text = HeaderTxt;
                        txtHolder.TextFrame.WrapText = true;
                        txtHolder.TextFrame.Paragraphs[0].Portions[0].FontIndex = 1;
                        txtHolder.TextFrame.Paragraphs[0].Portions[0].FontBold = false;
                        txtHolder.TextFrame.Paragraphs[0].Portions[0].FontHeight = 18;
                    }
                    txtHolder.TextFrame.Paragraphs[0].Alignment = TextAlignment.Left;
                }
            }

            Slide slideLast = presentation.GetSlideByPosition(1);
            slideLast.SlidePosition = presentation.Slides.Count;

            return presentation;
        }

        /// <summary>
        /// Adds the table to slide.
        /// </summary>
        /// <param name="slide">The slide.</param>
        /// <param name="xPosition">The x position.</param>
        /// <param name="yPosition">The y position.</param>
        /// <param name="tableWidth">Width of the table.</param>
        /// <param name="tableHeight">Height of the table.</param>
        /// <param name="objSourceTable">The source table.</param>
        public static void AddFilteringCriteria(Slide slide, int xPosition, int yPosition, int tableWidth, int tableHeight, DataTable objFilter)
        {
            #region Add Header table
            Aspose.Slides.Table tblSlideHeader = slide.Shapes.AddTable(xPosition, yPosition, tableWidth, 200, 1, 1, 1, Color.Gray);

            #region Adding Header Row

            Aspose.Slides.Cell cell = tblSlideHeader.GetCell(0, 0);
            cell.FillFormat.Type = FillType.Solid;
            cell.FillFormat.ForeColor = Color.FromArgb(0, 34, 82);

            //setting the text in the cell
            string headerTxt = "Data filtered by";

            cell.TextFrame.Text = headerTxt;

            #region setting table cell properties

            cell.TextFrame.FitTextToShape();
            cell.TextFrame.WrapText = true;
            cell.TextFrame.Paragraphs[0].Portions[0].FontBold = true;
            cell.TextFrame.Paragraphs[0].Portions[0].FontColor = Color.White;
            cell.TextFrame.Paragraphs[0].Alignment = TextAlignment.Left;
            cell.TextFrame.Paragraphs[0].Portions[0].FontHeight = 9;
            cell.TextFrame.Paragraphs[0].HasBullet = 0;
            //cell.FillFormat.ForeColor = Color.White;
            cell.FillFormat.Type = FillType.Solid;

            #endregion

            #endregion

            #endregion

            #region Setting table parameters
            int columns = 1;
            int rows = objFilter.Rows.Count;
            double borderWidth = 1;

            #endregion

            #region Adding data Table
            //Adding a new table to the slide using specified table parameters
            Aspose.Slides.Table tblSlide = slide.Shapes.AddTable(xPosition, yPosition + 200, tableWidth, tableHeight, columns, rows, borderWidth, Color.Gray);

            //Setting the alternative text for the table that can help in future to find and identify this specific table
            tblSlide.AlternativeText = "myTable";
            #endregion

            #region Adding data rows

            if (objFilter.Rows.Count > 0)
            {
                for (int i = 0; i < objFilter.Rows.Count; i++)
                {
                    cell = tblSlide.GetCell(0, i);
                    //cell.FillFormat.ForeColor = Color.White;                  
                    Aspose.Slides.Paragraph objPara = new Aspose.Slides.Paragraph();
                    objPara.HasBullet = 0;
                    objPara.Alignment = TextAlignment.Left;

                    objPara.TextOffset = 0;
                    objPara.BulletOffset = 0;

                    objPara.BulletHeight = 1;
                    objPara.BulletFontIndex = 1;

                    Portion objPortion1 = new Portion();
                    Portion objPortion2 = new Portion();


                    objPortion1.FontBold = true;
                    objPortion1.FontHeight = 9;
                    objPortion1.Text = objFilter.Rows[i][0].ToString() + ": ";

                    objPortion2.FontBold = false;
                    objPortion2.FontHeight = 9;
                    objPortion2.Text = Microsoft.JScript.GlobalObject.unescape(objFilter.Rows[i][1].ToString().Replace("amp;", "&"));

                    objPara.Portions.Add(objPortion1);
                    objPara.Portions.Add(objPortion2);

                    cell.TextFrame.Paragraphs.Clear();
                    cell.TextFrame.Paragraphs.Add(objPara);
                }
            }
            #endregion
        }       

        public static void AddChartData(string HeaderTxt,Presentation presentation, Slide slide, DataTable chartData, int columnIndex, int rowIndex, int currentHeight, int slideWidth, int left, int top, int slideHeight, int expectedTableHeight, bool isToIncreaseNumber)
        {
            bool isRowHeightSet = false;                     

            #region Adding new slide to presentation by checking weather more rows will fit into the slide or not

            if ((currentHeight > slideHeight - 200 || rowIndex != 0) && (top > slideHeight - 200))
            {
                //adding new slide
                presentation.AddEmptySlide();
                //getting the newely added slide
                slide = presentation.Slides[presentation.Slides.Count - 1];
                //adding the header on top of the slide
                //#region Adding header to each slide
                Aspose.Slides.Rectangle rectangle = slide.Shapes.AddRectangle(400, 400, slideWidth - 100, 500);
                rectangle.LineFormat.ShowLines = false;
                rectangle.AddTextFrame(HeaderTxt);
                rectangle.TextFrame.WrapText = true;
                rectangle.TextFrame.Paragraphs[0].Portions[0].FontIndex = 1;
                //#endregion
                top = 850;
            }
            #endregion

            currentHeight = top;
            Aspose.Slides.Table dtSlide;

            //adding new table to slide
            dtSlide = slide.Shapes.AddTable(left, top, 600, 200, 1, 1);    //(left, top, 1000, 200, 1, 1);   //(  left, top, 850, 20, 1, 1);
            double tempWidth = 0;
            dtSlide.SetBorders(1.00, System.Drawing.Color.Black);
            int colNumber = 0;             

            if (columnIndex != 0)
                colNumber = columnIndex - chartData.Columns.Count;
            int cellNumber = 0;
            //Adding table columnnames
            for (int i = colNumber; i < chartData.Columns.Count; i++)
            {
                //add new column to table
                if (i != 0)
                    dtSlide.AddColumn();
                //adding new cell to the row
                Aspose.Slides.Cell cell = dtSlide.GetCell(i, 0);                
                cell.FillFormat.Type = FillType.Solid;   
                //if(i != 0)
                     cell.FillFormat.ForeColor = Color.FromArgb(0, 34, 82);
                cell.TextFrame.Text = chartData.Columns[i].ColumnName;

                #region setting cell properties
                cell.TextFrame.FitTextToShape();
                cell.TextFrame.Paragraphs[0].Portions[0].FontBold = false;
                cell.TextFrame.Paragraphs[0].Portions[0].FontColor = Color.White;
                cell.TextFrame.Paragraphs[0].Portions[0].FontHeight = 9;
                cell.TextFrame.Paragraphs[0].Alignment = TextAlignment.Left;
                cell.TextFrame.Paragraphs[0].HasBullet = 0;
                cell.FillFormat.Type = FillType.Solid;
                #endregion

                tempWidth = tempWidth + cell.Width;
                cellNumber++;
                if (tempWidth > slideWidth)
                    break;
            }

            if (currentHeight > slideHeight - 200)
            {
                dtSlide.Height = 0;
            }           
            //Color cellColor = Color.White;  
            string cellVal = string.Empty;
           // Color cellColor = Color.Pink;

            #region Export data Rows
            for (int i = rowIndex; i < chartData.Rows.Count; i++)
            {
                tempWidth = 0;
                cellNumber = 0;
                dtSlide.AddRow();

                #region when morethan One slide rows
                if (columnIndex != 0)
                {
                    for (int l = 0; l < chartData.Columns.Count; l++)
                    {
                        Aspose.Slides.Cell cell = dtSlide.GetCell(l, i - rowIndex + 1);
                        cell.FillFormat.Type = FillType.Solid;                       
                        if (chartData.Rows[i][l].ToString() != "")
                        {
                            cell.TextFrame.Paragraphs[0].Portions[0].FontBold = false;
                            cell.TextFrame.Paragraphs[0].Portions[0].FontHeight = 8;
                            cell.TextFrame.Paragraphs[0].HasBullet = 0;
                            //cell.FillFormat.ForeColor = Color.White;
                            //cellVal = chartData.Rows[i][l].ToString();                               
                        }
                        //cell properties when column not 0                             
                        //cell.FillFormat.ForeColor = cellColor;
                        //cell.TextFrame.Paragraphs[0].Portions[0].FontBold = false;
                        //cell.TextFrame.Paragraphs[0].Alignment = TextAlignment.Center;                      

                        #region Setting the cell properties
                        tempWidth = tempWidth + cell.Width;
                        cell.TextFrame.Text = chartData.Rows[i][l].ToString();
                        cell.TextFrame.Paragraphs[0].Portions[0].FontBold = false;
                        cell.TextFrame.Paragraphs[0].Alignment = TextAlignment.Left;
                        cell.TextFrame.FitTextToShape();
                        cell.TextFrame.Paragraphs[0].HasBullet = 0;
                        cell.TextFrame.Paragraphs[0].Portions[0].FontHeight = 8;
                        cell.FillFormat.ForeColor = Color.White;
                        #endregion
                    }
                }
                #endregion when morethen One slide rows

                //looping through the each cell
                for (int j = columnIndex; j < chartData.Columns.Count; j++)
                {
                    if (columnIndex == 0 && i < chartData.Rows.Count)
                    {
                        Aspose.Slides.Cell cell = dtSlide.GetCell(j, i + 1 - rowIndex);
                        tempWidth = tempWidth + cell.Width;
                        cell.TextFrame.FitTextToShape();
                        //currentHeight = dtSlide.Y + dtSlide.Height;
                        currentHeight = dtSlide.Height;
                        cell.TextFrame.Text = chartData.Rows[i][j].ToString();
                        //cell.FillFormat.ForeColor = Color.FromArgb(108, 128, 191);
                        cell.TextFrame.Paragraphs[0].Portions[0].FontBold = false;                       
                        cell.TextFrame.Paragraphs[0].Alignment = TextAlignment.Left;
                        cell.TextFrame.Paragraphs[0].Portions[0].FontColor = Color.Black;
                        cell.TextFrame.Paragraphs[0].Portions[0].FontHeight = 8;
                        cell.TextFrame.Paragraphs[0].HasBullet = 0;
                        cell.FillFormat.ForeColor = Color.White;
                    }
                    else
                    {
                        Aspose.Slides.Cell cell = dtSlide.GetCell(cellNumber + chartData.Columns.Count, i - rowIndex + 1);
                        tempWidth = tempWidth + cell.Width;

                        if (isRowHeightSet == false)
                        {
                            currentHeight = dtSlide.Y + dtSlide.Height;
                            isRowHeightSet = true;
                        }
                        cellNumber++;
                        if (tempWidth > slideWidth)
                        {
                            if (i == chartData.Rows.Count - 1)
                            {
                                isToIncreaseNumber = true;
                                columnIndex = j;

                            }
                            else if (i == chartData.Rows.Count - 1)
                            {
                                isToIncreaseNumber = false;
                                columnIndex = j;
                            }
                            break;
                        }
                        else if (tempWidth < slideWidth && (i == chartData.Rows.Count - 1))
                        {
                            isToIncreaseNumber = true;
                            columnIndex = j;
                        }
                    }
                }

                if (currentHeight > slideHeight - 200)
                {
                    if (i != chartData.Rows.Count - 1)
                    {
                        rowIndex = i + 1;
                        isToIncreaseNumber = false;
                    }
                    else
                    {
                        currentHeight = slideHeight + 200;
                        rowIndex = 0;
                        isToIncreaseNumber = true;
                    }

                    break;
                }
                else
                {
                    if (i == chartData.Rows.Count - 1)
                    {
                        currentHeight = slideHeight + 200;
                        rowIndex = 0;
                        isToIncreaseNumber = true;
                    }
                }
                if (i == chartData.Rows.Count - 1 && isToIncreaseNumber == false)
                {
                    currentHeight = slideHeight + 200;
                    rowIndex = 0;
                    isToIncreaseNumber = true;
                }
            }
            #endregion

            if ((columnIndex != 0 && columnIndex < (chartData.Columns.Count - 1)) || rowIndex != 0)
            {
                if (rowIndex == 0)
                    top = slideHeight + 100;
                else
                    top = dtSlide.Y + dtSlide.Height + 100;

                AddChartData(HeaderTxt,presentation, slide, chartData, columnIndex, rowIndex, currentHeight, slideWidth, left, top, slideHeight, dtSlide.Height, isToIncreaseNumber);
            }
        }

       #endregion
    }
}



