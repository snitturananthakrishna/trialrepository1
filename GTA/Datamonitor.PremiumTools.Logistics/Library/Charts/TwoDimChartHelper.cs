using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Drawing.Drawing2D;
using dotnetCHARTING;
using dotnetCHARTING.Libraries;
//using Aspose.Cells;
//using Aspose.Words;
//using Aspose.Slides;
//using System.Drawing.Imaging;
using ComponentArt.Web.UI;
using System.IO;
using System.Data.SqlClient;


namespace Datamonitor.PremiumTools.Generic.Library
{
    public class TwoDimensionalCharts
    {
        private string xAxisName = string.Empty;
        private string yAxisName = string.Empty;      

        static Color[] DMPalette = new Color[] { Color.FromArgb(0, 34, 82), 
            Color.FromArgb(108, 128, 191), 
            Color.FromArgb(165, 174, 204), 
            Color.FromArgb(210, 217, 229), 
            Color.FromArgb(216, 69, 25), 
            Color.FromArgb(255, 102, 0), 
            Color.FromArgb(255, 172, 112), 
            Color.FromArgb(250, 222, 199),
            Color.FromArgb(255, 153, 0), 
            Color.FromArgb(255, 204, 0) };
       
        /// <summary>
        /// Plots the XY chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>
        internal static Chart PlotXYChart(DataSet chartDataset, 
                                Chart chart, 
                                string chartSeriesType,
                                string yAxis1Text,
                                string AxisCriteria)
        {
            return PlotXYChart(chartDataset,
                                chart,
                                chartSeriesType,
                                yAxis1Text,
                                AxisCriteria,
                                500,
                                320);
        }

        /// <summary>
        /// Plots the XY chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>
        internal static Chart PlotXYChart(DataSet chartDataset,
                                Chart chart,
                                string chartSeriesType,
                                string yAxis1Text,
                                string AxisCriteria,
                                Unit width,
                                Unit height)
        {
            chart.Use3D = false;
            chart.Visible = true;
            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;
                chart.FileQuality = 100;
                DataEngine objDataEngine = new DataEngine();
                chart.SeriesCollection.Clear();

                objDataEngine.Data = chartDataset;
                dotnetCHARTING.SeriesCollection sc;

                //Set data fields
                objDataEngine.DataFields = "XAxis=" +
                    chartDataset.Tables[0].Columns[0].ColumnName +
                    ",YAxis=" + chartDataset.Tables[0].Columns[2].ColumnName +
                    ",splitBy=" + chartDataset.Tables[0].Columns[1].ColumnName;

                sc = objDataEngine.GetSeries();
                chart.SeriesCollection.Add(sc);
                chart.DataBind();

                string xAxisText = chartDataset.Tables[0].Columns[0].ColumnName;
                string seriesText = chartDataset.Tables[0].Columns[1].ColumnName;
                //Set axis label texts and styles
                chart.XAxis.Label.Text = xAxisText;
                chart.YAxis.Label.Text = yAxis1Text;
                chart.XAxis.DefaultTick.Label.Color = Color.Black;
                chart.XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.YAxis.DefaultTick.Label.Color = Color.Black;
                chart.YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.XAxis.Line.Color = Color.DarkGray;
                chart.XAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.YAxis.Line.Color = Color.DarkGray;
                chart.YAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.DefaultSeries.DefaultElement.Marker.Size = 4;
                if (chartSeriesType.Equals("stacked"))
                {
                    chart.YAxis.Scale = Scale.Stacked;
                    chartSeriesType = "7";
                } else if (chartSeriesType.Equals("stackedArea"))
                {
                    chart.YAxis.Scale = Scale.Stacked;
                    chartSeriesType = "4";
                }
                else if (chartSeriesType.Equals("Horizontal"))
                {                    
                    chartSeriesType = "7";
                    chart.XAxis.Label.Text = yAxis1Text;
                    chart.YAxis.Label.Text = xAxisText;
                    chart.YAxis.InvertScale = true;
                    chart.YAxis.SpacingPercentage = 40;
                }

                //Set chart series types
                for (int i = 0; i < chart.SeriesCollection.Count; i++)
                {
                    chart.SeriesCollection[i].Type = (SeriesType)Convert.ToInt32(chartSeriesType);
                }

                if (chartSeriesType == "4")
                {
                    chart.DefaultSeries.DefaultElement.Transparency = 20;
                    chart.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.None;
                }
                if (GlobalSettings.AllowAutoYaxisScaleRange)
                {
                    chart.YAxis.Scale = Scale.Range;
                }
                //Set chart styles
                chart.Palette = DMPalette;
                chart.Debug = false;
                chart.Height = height;
                chart.Width = width;
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.XAxis.SpacingPercentage = 30;
                chart.ChartArea.ClearColors();
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.ChartArea.Shadow.Color = Color.White;

                //Set legend properties and styles
                chart.LegendBox.Template = "%Icon%Name";
                SetStandardLegendStyles(chart);

                chart.DefaultElement.ToolTip = "%SeriesName";
                if (!string.IsNullOrEmpty(AxisCriteria))
                {
                    BuildChartArea(chart, AxisCriteria);
                }
            }

            return chart;
        }

        /// <summary>
        /// Sets the standard legend styles.
        /// </summary>
        /// <param name="objChart">The obj chart.</param>
        private static void SetStandardLegendStyles(dotnetCHARTING.Chart chart)
        {                
            chart.LegendBox.Background = new dotnetCHARTING.Background(System.Drawing.Color.White);
            chart.LegendBox.CornerBottomLeft = BoxCorner.Square;
            chart.LegendBox.CornerBottomRight = BoxCorner.Square;
            chart.LegendBox.CornerTopLeft = BoxCorner.Square;
            chart.LegendBox.CornerTopRight = BoxCorner.Square;
            chart.LegendBox.LabelStyle = new dotnetCHARTING.Label("", new System.Drawing.Font("Verdana", 7), Color.Black);
            chart.LegendBox.Line = new dotnetCHARTING.Line(Color.White);
            chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
            chart.LegendBox.Line.Color = Color.White;
            chart.LegendBox.Shadow.Color = Color.White;
            chart.LegendBox.InteriorLine.Color = Color.White;
        }

        /// <summary>
        /// Plots the Dual YAxis chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>
        internal static Chart PlotDualYAxisChart(DataSet chartDataset, 
                                   Chart chart, 
                                   string chartSeriesType,
                                   string yAxis1Text, 
                                   string yAxis2Text,
                                   string AxisCriteria) 
        {                      
            chart.Use3D = false;
            chart.Visible =true;            
            chart.Debug = false;
            //chart.FileManager.TempDirectory = HttpContext.Current.Server.MapPath("~/TempImages");
            chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;

            //DataView yAxisDataview = chartDataset.Tables[0].DefaultView;
            //yAxisDataview.RowFilter = "yType= Value";
            //DataTable yAxisDatatable = yAxisDataview.ToTable();

            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                chart.FileQuality = 100;
                DataEngine de = new DataEngine();
                DataEngine de1 = new DataEngine();

                SeriesCollection sc;
                SeriesCollection sc1 = new SeriesCollection();
                chart.SeriesCollection.Clear();
                de.Data = chartDataset;
                de.DataFields = "XAxis=" +
                     chartDataset.Tables[0].Columns[0].ColumnName +
                     ",YAxis=" + chartDataset.Tables[0].Columns[1].ColumnName;
                string xAxisText = chartDataset.Tables[0].Columns[0].ColumnName;
                sc = de.GetSeries();
               
                sc[0].XAxis = new dotnetCHARTING.Axis();
                sc[0].YAxis = new dotnetCHARTING.Axis();
                sc[0].XAxis.Label.Text = xAxisText;
                sc[0].YAxis.Label.Text = yAxis1Text;
                sc[0].Name = yAxis1Text;

                de1.Data = chartDataset;
                de1.DataFields = "XAxis=" +
                     chartDataset.Tables[0].Columns[0].ColumnName +
                     ",YAxis=" + chartDataset.Tables[0].Columns[2].ColumnName;
                //string yAxis2Text = chartDataset.Tables[0].Columns[2].ColumnName;
                sc1 = de1.GetSeries();

                for (int i = 0; i < sc1.Count; i++)
                {
                    sc.Add(sc1[i]);
                }
                for (int i = 1; i < sc.Count - 1; i++)
                {
                    sc[i].XAxis = sc[0].XAxis;
                    sc[i].YAxis = sc[0].YAxis;
                }

                if (sc.Count == 2)
                {
                    sc[1].XAxis = sc[0].XAxis;
                }
                chart.Type = dotnetCHARTING.ChartType.Combo;
                chart.SeriesCollection.Add(sc);                
                 sc[0].Type = SeriesType.Bar;

                 if (sc.Count == 2)
                 {
                     sc[1].YAxis = new dotnetCHARTING.Axis();
                     sc[1].YAxis.Orientation = dotnetCHARTING.Orientation.Right;
                     sc[1].YAxis.LabelRotate = true;
                     sc[1].Type = SeriesType.Line;
                     sc[1].Line.Width = 3;
                     sc[1].Name = yAxis2Text;
                     sc[1].YAxis.Label.Text = yAxis2Text;

                     sc[1].YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                     sc[1].XAxis.Line.Color = Color.DarkGray;
                     sc[1].XAxis.DefaultTick.Line.Color = Color.DarkGray;
                     sc[1].YAxis.Line.Color = Color.DarkGray;
                     sc[1].YAxis.DefaultTick.Line.Color = Color.DarkGray;
                 }
                chart.DataBind();

                sc[0].XAxis.DefaultTick.Label.Color = Color.Black;
                sc[0].XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                sc[0].YAxis.DefaultTick.Label.Color = Color.Black;
                sc[0].YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                sc[0].XAxis.Line.Color = Color.DarkGray;
                sc[0].XAxis.DefaultTick.Line.Color = Color.DarkGray;
                sc[0].YAxis.Line.Color = Color.DarkGray;
                sc[0].YAxis.DefaultTick.Line.Color = Color.DarkGray;
                sc[0].XAxis.SpacingPercentage = 30;
                if (GlobalSettings.AllowAutoYaxisScaleRange)
                {
                    sc[0].YAxis.Scale = Scale.Range;
                }
                
                //Set chart styles
                chart.Palette = DMPalette;
                chart.Debug = false;
                chart.Height = 300;
                chart.Width = 500;
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.DefaultAxis.ShowGrid = false;
                chart.DefaultAxis.AlternateGridBackground.Color = Color.White;                
                chart.ChartArea.ClearColors();
                chart.BackColor = Color.White;                
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.ChartArea.Shadow.Color = Color.White;
                
                BuildChartArea(chart, AxisCriteria);
            }
            chart.LegendBox.Template = "%Icon%Name";
            SetStandardLegendStyles(chart);
            return chart;
        }

        /// <summary>
        /// Plots the Pie chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>
        internal static Chart PlotPieChart(DataSet chartDataset, 
                                    Chart targetChart, 
                                    string chartSeriesType,
                                    string AxisCriteria,
                                    string chartType)
        {

            return PlotPieChart(chartDataset, targetChart, chartSeriesType, AxisCriteria, 500, 320,chartType,true);      
        }

        /// <summary>
        /// Plots the Pie chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>
        internal static Chart PlotPieChartProfiler(DataSet chartDataset,
                                    Chart targetChart,                                                                        
                                    Unit width,
                                    Unit height,
                                    string chartType)
        {            

            DataEngine objDataEngine = new DataEngine();
            targetChart.SeriesCollection.Clear();
            targetChart.Use3D = false;
            if (chartType == "Pie")
            {
                targetChart.Type = ChartType.Pies;
            }
            else
            {
                targetChart.Type = ChartType.Donuts;
            }
            targetChart.ShadingEffect = false;
            string units = chartDataset.Tables[0].Rows[0]["Units"].ToString();

            targetChart.DefaultAxis.ShowGrid = false;
            targetChart.DefaultChartArea.Background.Color = Color.White;
            targetChart.BackColor = Color.White;
            targetChart.DefaultAxis.AlternateGridBackground.Color = Color.White;

            targetChart.ChartArea.Line.Color = Color.LightGray;
            targetChart.ChartArea.Shadow.Color = Color.White;           

            objDataEngine.Data = chartDataset;
            objDataEngine.DataFields = "xAxis="
                + chartDataset.Tables[0].Columns[0].ColumnName
                + ", yAxis="
                + chartDataset.Tables[0].Columns[1].ColumnName;

            SeriesCollection sc = objDataEngine.GetSeries();

            targetChart.Debug = false;
            targetChart.LegendBox.Visible = true;
            targetChart.LegendBox.Template = "Icon%Name";
            targetChart.SeriesCollection.Add(sc);
            targetChart.Height = height;
            targetChart.Width = width;

            targetChart.PieLabelMode = PieLabelMode.Automatic;
            //targetChart.YAxis.FormatString = "n"; // "{0:0'%}";
            targetChart.PieLabelMode = PieLabelMode.Automatic;
            targetChart.DefaultSeries.DefaultElement.ShowValue = true;
            //targetChart.DefaultSeries.DefaultElement.SmartLabel.Line.Color = Color.Black;
           // targetChart.DefaultElement.SmartLabel.Color = Color.Black;
            targetChart.DefaultSeries.DefaultElement.SmartLabel.Line.EndCap = LineCap.Round;
            targetChart.DefaultSeries.DefaultElement.SmartLabel.Text = "%YPercentOfTotal"; 
            //targetChart.DefaultSeries.DefaultElement.SmartLabel.Text = "%YValue " + units + " (%YPercentOfTotal)";
            targetChart.DefaultSeries.DefaultElement.SmartLabel.Font = new System.Drawing.Font("verdana",9, FontStyle.Bold);
                        
            targetChart.Palette = DMPalette;
            targetChart.DataBind();
            SetStandardLegendStyles(targetChart);
            targetChart.DefaultElement.ToolTip = "%Name";           

            return targetChart;
        }

        internal static Chart PlotPieChart(DataSet chartDataset,
                                    Chart targetChart,
                                    string chartSeriesType,
                                    string AxisCriteria,
                                    Unit width,
                                    Unit height,
                                    string chartType,
                                    bool showLabel)
        {
            //targetChart.FileManager.TempDirectory = HttpContext.Current.Server.MapPath("~/TempImages");

            DataEngine objDataEngine = new DataEngine();
            targetChart.SeriesCollection.Clear();
            targetChart.Use3D = false;
            if (chartType == "Pie")
            {
                targetChart.Type = ChartType.Pies;
            }
            else
            {
                targetChart.Type = ChartType.Donuts;
            }
            targetChart.ShadingEffect = false;
            string units = chartDataset.Tables[0].Rows[0]["Units"].ToString();

            targetChart.DefaultAxis.ShowGrid = false;
            targetChart.DefaultChartArea.Background.Color = Color.White;
            targetChart.BackColor = Color.White;
            targetChart.DefaultAxis.AlternateGridBackground.Color = Color.White;

            targetChart.ChartArea.Line.Color = Color.White;
            targetChart.ChartArea.Shadow.Color = Color.White;

            objDataEngine.Data = chartDataset;
            objDataEngine.DataFields = "xAxis="
                + chartDataset.Tables[0].Columns[0].ColumnName
                + ", yAxis="
                + chartDataset.Tables[0].Columns[1].ColumnName;

            SeriesCollection sc = objDataEngine.GetSeries();

            targetChart.Debug = false;
            targetChart.LegendBox.Visible = true;
            targetChart.LegendBox.Template = "Icon%Name";
            targetChart.SeriesCollection.Add(sc);
            targetChart.Height = height;
            targetChart.Width = width;

            targetChart.PieLabelMode = PieLabelMode.Automatic;
            //targetChart.YAxis.FormatString = "n"; // "{0:0'%}";
            targetChart.PieLabelMode = PieLabelMode.Outside;
            targetChart.DefaultSeries.DefaultElement.ShowValue = showLabel;
            targetChart.DefaultSeries.DefaultElement.SmartLabel.Line.Color = Color.Black;
            targetChart.DefaultElement.SmartLabel.Color = Color.Black;
            targetChart.DefaultSeries.DefaultElement.SmartLabel.Line.EndCap = LineCap.Round;
            //targetChart.DefaultSeries.DefaultElement.SmartLabel.Text = "%YValue " + units + " ("+ "%YPercentOfTotal"+")"; 
            targetChart.DefaultSeries.DefaultElement.SmartLabel.Text = "%YValue (<%YPercentOfTotal,###,##0.#>)";
            targetChart.DefaultSeries.DefaultElement.SmartLabel.Font = new System.Drawing.Font("arial", 9, FontStyle.Regular);

            targetChart.Palette = DMPalette;
            targetChart.DataBind();
            SetStandardLegendStyles(targetChart);
            targetChart.DefaultElement.ToolTip = "%Name";
            //Adding Total value to the pie chart                     
            Series sum = sc.Calculate("", Calculation.Sum);
            double s = 0;
            foreach (Element e in sum.Elements)
            {
                if (e.YValue != double.NaN)
                {
                    s += e.YValue;
                }
            }
            string annotationText = string.Format("{0:#,0.#}", s);
            Annotation totalVal = new Annotation();
            totalVal.Position = new Point(0, 0); //(350, 150);
            totalVal.ClearColors();
            totalVal.Label.Text = "Total Value: " + annotationText + " " + units;
            // By default the annotation will create a rectangle for the text. In order to place the text on one line we'll 
            // set the dynamic size property to false.     
            totalVal.DynamicSize = false;
            targetChart.Annotations.Add(totalVal);

            if (!string.IsNullOrEmpty(AxisCriteria))
            {
                BuildChartArea(targetChart, AxisCriteria);
            }

            return targetChart;
        }       

         /// <summary>
        /// Plots the Bubble chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>
        internal static Chart PlotBubbleChart(DataSet chartDataset, 
                                    Chart targetChart, 
                                    string chartSeriesType,
                                    string xAxisText,
                                    string yAxis1Text,
                                    string yAxis2Text,
                                    string AxisCriteria)
        {
            targetChart.FileManager.ImageFormat = ImageFormat.Png;
            targetChart.FileQuality = 100;
            DataTable chartDataTable = new DataTable();
            chartDataTable = chartDataset.Tables[0];   

            if (chartDataset.Tables[0].Rows.Count > 0)
            {
                //BuildChartArea(targetChart, AxisCriteria);
                targetChart.Height = 300;
                targetChart.Width = 500;

                targetChart.SeriesCollection.Clear();                                
                targetChart.TitleBox.ClearColors();                
                targetChart.TitleBox.Label.Color = Color.Gray;                               
                
                #region Bind bubble chart data
                targetChart.SeriesCollection.Add(GetSeriesCollection(chartDataset, xAxisText, yAxis1Text, yAxis2Text));             
                targetChart.DefaultSeries.Type = SeriesType.Bubble;
                targetChart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center;              
                targetChart.DefaultSeries.Palette = DMPalette;
                targetChart.DefaultElement.ShowValue = true;
                targetChart.DefaultElement.Transparency = 40;              
                targetChart.DefaultElement.SmartLabel.Color = Color.Black;                      
                
                // Set the Shading
                targetChart.ShadingEffectMode = ShadingEffectMode.None;
                targetChart.Use3D = true;

                // Set the label alignment.
                targetChart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center;

                #region Set chart properties              
                targetChart.XAxis.DefaultTick.Label.Color = Color.Black;
                targetChart.XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                targetChart.XAxis.Label.Text = xAxisText;               
                targetChart.DefaultElement.SmartLabel.Font = new System.Drawing.Font("verdana", 7);

                targetChart.YAxis.Label.Text = yAxis1Text;
                targetChart.YAxis.DefaultTick.Label.Color = Color.Black;
                targetChart.YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);

                targetChart.XAxis.Line.Color = Color.DarkGray;
                targetChart.XAxis.DefaultTick.Line.Color = Color.Gray;
                targetChart.YAxis.Line.Color = Color.DarkGray;
                targetChart.YAxis.DefaultTick.Line.Color = Color.Gray;
                
                #endregion

                targetChart.ChartArea.ClearColors();
                targetChart.DefaultChartArea.Background.Color = Color.White;
                targetChart.ChartArea.Line.Color = Color.LightGray;
                targetChart.ChartArea.Shadow.Color = Color.White;
                targetChart.Debug = true;
                targetChart.DataBind();

                #endregion              

                //targetChart.DefaultAxis.ShowGrid = false;
                targetChart.DefaultAxis.AlternateGridBackground.Color = Color.White;               
                //BuildChartArea(targetChart, AxisCriteria);
            }        
            return targetChart;            
        }      

        /// <summary>
        /// Method to get series of bubble chart.
        /// </summary>
        /// <param name="chartDataset">dataset</param>
        /// <param name="xAxisText">xAxisText</param>
        /// <param name="yAxis1Text">yAxis1Text</param>
        /// <param name="yAxis2Text">yAxis2Text</param>
        /// <returns>Series Collection</returns>
        public static SeriesCollection GetSeriesCollection(DataSet chartDataset,
                                            string xAxisText,
                                            string yAxis1Text,
                                            string yAxis2Text)
        {
            SeriesCollection SC = new SeriesCollection();
            string xAxisElement = xAxisText;
            string yAxisElement = yAxis1Text;
            string bubbleName = yAxis2Text;

            if (chartDataset.Tables.Count > 0)
            {
                Series s = new Series();

                foreach (DataRow dRow in chartDataset.Tables[0].Rows)
                {
                    Element e = new Element();
                    string bubbleUnits = dRow["BubbleUnits"].ToString().TrimEnd(new char[] {' '});
                    
                    if (dRow["xAxis"] != DBNull.Value)
                    {
                        e.XValue = Convert.ToDouble(dRow["xAxis"]);
                        if (dRow["yAxis"] != DBNull.Value)
                        {
                            e.YValue = Convert.ToDouble(dRow["yAxis"]);
                        }
                        if (dRow["Bubble"] != DBNull.Value)
                        {
                            e.SmartLabel.Text = dRow["Country"].ToString();
                            e.ToolTip = dRow["Bubble"].ToString() +
                                        "(" + bubbleUnits + ")";
                            string bubbleValue = string.Format("{0:#,0.#}", dRow["Bubble"].ToString());
                            //e.BubbleSize  = Convert.ToDouble(dRow["Bubble"]);                                                                                    
                            e.BubbleSize = Convert.ToDouble(bubbleValue);                                                                                    
                        }
                        s.Elements.Add(e);
                    }
                }
                SC.Add(s);
            }
            return SC;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chartDataset"></param>
        /// <param name="chart"></param>
        /// <param name="chartSeriesType"></param>
        /// <param name="yAxis1Text"></param>
        /// <param name="yAxis2Text"></param>
        /// <returns></returns>
        internal static Chart PlotIndicatorComparisonChart(DataSet chartDataset,
              Chart chart,
              string chartSeriesType,
              string yAxis1Text,
              string yAxis2Text,
              string axisCriteria)
        {
            chart.Use3D = false;
            chart.Visible = true;
            chart.Debug = false;

            chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;
            chart.DefaultSeries.DefaultElement.ToolTip = "%XValue";
            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                chart.FileQuality = 100;
                DataEngine de = new DataEngine();
                DataEngine de1 = new DataEngine();

                SeriesCollection sc;
                SeriesCollection sc1 = new SeriesCollection();
                chart.SeriesCollection.Clear();
                de.Data = chartDataset;
                de.DataFields = "XAxis=" +
                    chartDataset.Tables[0].Columns[0].ColumnName +
                    ",YAxis=" + chartDataset.Tables[0].Columns[2].ColumnName +
                    ",splitBy=" + chartDataset.Tables[0].Columns[1].ColumnName;

                sc = de.GetSeries();
                if (sc.Count == 0)
                {
                    return chart;
                }
                sc[0].XAxis = new dotnetCHARTING.Axis();
                sc[0].YAxis = new dotnetCHARTING.Axis();
                sc[0].XAxis.Label.Text = chartDataset.Tables[0].Columns[0].ColumnName;
                sc[0].YAxis.Label.Text = yAxis1Text;    //chartDataset.Tables[0].Columns[2].ColumnName;
                sc[0].DefaultElement.Marker.Size = 4;
                chart.Type = dotnetCHARTING.ChartType.Combo;
                chart.SeriesCollection.Add(sc);                
                sc[0].XAxis = sc[0].XAxis;
                sc[0].YAxis = sc[0].YAxis;
                sc[0].Type = (SeriesType)Convert.ToInt32(chartSeriesType);
                if (chartSeriesType == "4")
                {
                    sc[0].DefaultElement.Transparency = 20;
                    sc[0].DefaultElement.Marker.Type = ElementMarkerType.None;
                }
                
                if (sc.Count == 2)
                {
                    sc[1].XAxis = sc[0].XAxis;
                    sc[1].YAxis = new dotnetCHARTING.Axis();
                    sc[1].YAxis.Orientation = dotnetCHARTING.Orientation.Right;
                    sc[1].YAxis.LabelRotate = true;
                    sc[1].Type = (SeriesType)Convert.ToInt32(chartSeriesType);
                    sc[1].YAxis.Label.Text = yAxis2Text;
                    sc[1].DefaultElement.Marker.Size = 4;
                    if (chartSeriesType == "4")
                    {
                        sc[1].DefaultElement.Transparency = 20;
                        sc[1].DefaultElement.Marker.Type = ElementMarkerType.None;
                    }
                }
               
                chart.DataBind();

                sc[0].XAxis.DefaultTick.Label.Color = Color.Black;
                sc[0].XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                sc[0].YAxis.DefaultTick.Label.Color = Color.Black;
                sc[0].YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                sc[0].XAxis.Line.Color = Color.DarkGray;
                sc[0].XAxis.DefaultTick.Line.Color = Color.DarkGray;
                sc[0].YAxis.Line.Color = Color.DarkGray;
                sc[0].YAxis.DefaultTick.Line.Color = Color.DarkGray;
                sc[0].XAxis.SpacingPercentage = 30;
                if (sc.Count == 2)
                {
                    sc[1].YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                    sc[1].XAxis.Line.Color = Color.DarkGray;
                    sc[1].XAxis.DefaultTick.Line.Color = Color.DarkGray;
                    sc[1].YAxis.Line.Color = Color.DarkGray;
                    sc[1].YAxis.DefaultTick.Line.Color = Color.DarkGray;
                }
                if (GlobalSettings.AllowAutoYaxisScaleRange)
                {
                    chart.YAxis.Scale = Scale.Range;
                }
                //Set chart styles
                chart.Palette = DMPalette;
                chart.Debug = false;
                chart.Height = 300;
                chart.Width = 500;
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.DefaultAxis.ShowGrid = false;
                chart.DefaultAxis.AlternateGridBackground.Color = Color.White;
                chart.ChartArea.ClearColors();
                chart.BackColor = Color.White;
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.ChartArea.Shadow.Color = Color.White;

                chart.LegendBox.Template = "%Icon%Name";
                SetStandardLegendStyles(chart);
                chart.DefaultElement.ToolTip = "%SeriesName";
                BuildChartArea(chart, axisCriteria);              
            }
            
            return chart;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chartDataset"></param>
        /// <param name="chart"></param>
        /// <param name="chartSeriesType"></param>
        /// <param name="yAxis1Text"></param>
        /// <param name="yAxis2Text"></param>
        /// <param name="axisCriteria"></param>
        /// <param name="Yaxis1SeriesTypes"></param>
        /// <param name="Yaxis2SeriesTypes"></param>
        /// <returns></returns>
        internal static Chart PlotMultiSeriesIndicatorComparisonChart(DataSet chartDataset,
              Chart chart,
              string chartSeriesType,
              string yAxis1Text,
              string yAxis2Text,
              string axisCriteria,
              string Yaxis1SeriesTypes,
            string Yaxis2SeriesTypes)
        {
            chart.Use3D = false;
            chart.Visible = true;
            chart.Debug = false;

            chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;
            chart.DefaultSeries.DefaultElement.ToolTip = "%XValue";
            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                chart.FileQuality = 100;
                DataEngine de = new DataEngine();
                DataEngine de1 = new DataEngine();

                SeriesCollection sc;
                SeriesCollection sc1 = new SeriesCollection();
                chart.SeriesCollection.Clear();
                de.Data = chartDataset;
                de.DataFields = "XAxis=" +
                    chartDataset.Tables[0].Columns[0].ColumnName +
                    ",YAxis=" + chartDataset.Tables[0].Columns[2].ColumnName +
                    ",splitBy=" + chartDataset.Tables[0].Columns[1].ColumnName;

                sc = de.GetSeries();
                chart.SeriesCollection.Add(sc);
                if (sc.Count == 0)
                {
                    return chart;
                }
                Axis Xaxis1 = new Axis();
                Axis YAxis1 = new Axis();
                Axis YAxis2 = new Axis();

                string[] YAxis1Options = Yaxis1SeriesTypes.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                string[] YAxis1ChartTypes = YAxis1Options.Length > 0 ? YAxis1Options[0].Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries) : null;
                string[] YAxis1seriesNames = YAxis1Options.Length > 1 ? YAxis1Options[1].Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries) : null;

                for (int i = 0; i < sc.Count; i++)
                {
                    for (int j = 0; j < YAxis1seriesNames.Length; j++)
                    {
                        if (YAxis1seriesNames[j].Equals(sc[i].Name))
                        {
                            sc[i].Type = (dotnetCHARTING.SeriesType)Convert.ToInt32(YAxis1ChartTypes[j]);
                            sc[i].XAxis = Xaxis1;
                            sc[i].YAxis = YAxis1;
                            sc[i].XAxis.Label.Text = chartDataset.Tables[0].Columns[0].ColumnName;
                            sc[i].YAxis.Label.Text = yAxis1Text;   
                            sc[i].DefaultElement.Marker.Size = 4;
                            if (YAxis1ChartTypes[j] == "4")
                            {
                                sc[i].DefaultElement.Transparency = 20;
                                sc[i].DefaultElement.Marker.Type = ElementMarkerType.None;
                            }
                            sc[i].XAxis.DefaultTick.Label.Color = Color.Black;
                            sc[i].XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                            sc[i].YAxis.DefaultTick.Label.Color = Color.Black;
                            sc[i].YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                            sc[i].XAxis.Line.Color = Color.DarkGray;
                            sc[i].XAxis.DefaultTick.Line.Color = Color.DarkGray;
                            sc[i].YAxis.Line.Color = Color.DarkGray;
                            sc[i].YAxis.DefaultTick.Line.Color = Color.DarkGray;
                            sc[i].XAxis.SpacingPercentage = 30;
                            break;
                        }
                    }
                }
                string[] YAxis2Options = Yaxis2SeriesTypes.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                string[] YAxis2ChartTypes = YAxis2Options.Length > 0 ? YAxis2Options[0].Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries) : null;
                string[] YAxis2seriesNames = YAxis2Options.Length > 1 ? YAxis2Options[1].Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries) : null;

                for (int i = 0; i < sc.Count; i++)
                {
                    for (int j = 0; j < YAxis2seriesNames.Length; j++)
                    {
                        if (YAxis2seriesNames[j].Equals(sc[i].Name))
                        {
                            sc[i].Type = (dotnetCHARTING.SeriesType)Convert.ToInt32(YAxis2ChartTypes[j]);
                            sc[i].XAxis = Xaxis1;
                            sc[i].YAxis = YAxis2;
                            sc[i].XAxis.Label.Text = chartDataset.Tables[0].Columns[0].ColumnName;
                            sc[i].YAxis.Label.Text = yAxis2Text;
                            sc[i].YAxis.Orientation = dotnetCHARTING.Orientation.Right;
                            sc[i].YAxis.LabelRotate = true;
                            sc[i].DefaultElement.Marker.Size = 4;
                            if (YAxis2ChartTypes[j] == "4")
                            {
                                sc[i].DefaultElement.Transparency = 20;
                                sc[i].DefaultElement.Marker.Type = ElementMarkerType.None;
                            }
                       
                            sc[i].YAxis.DefaultTick.Label.Color = Color.Black;
                            sc[i].YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);                           
                            sc[i].YAxis.Line.Color = Color.DarkGray;
                            sc[i].YAxis.DefaultTick.Line.Color = Color.DarkGray;                            
                            break;
                        }
                    }
                }
                            

                chart.DataBind();
                if (GlobalSettings.AllowAutoYaxisScaleRange)
                {
                    chart.YAxis.Scale = Scale.Range;
                }
                
                //Set chart styles
                chart.Palette = DMPalette;
                chart.Debug = false;
                chart.Height = 300;
                chart.Width = 500;
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.DefaultAxis.ShowGrid = false;
                chart.DefaultAxis.AlternateGridBackground.Color = Color.White;
                chart.ChartArea.ClearColors();
                chart.BackColor = Color.White;
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.ChartArea.Shadow.Color = Color.White;

                chart.LegendBox.Template = "%Icon%Name";
                SetStandardLegendStyles(chart);
                chart.DefaultElement.ToolTip = "%SeriesName";
                BuildChartArea(chart, axisCriteria);
            }

            return chart;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="chartDataset"></param>
        /// <param name="chart"></param>
        /// <param name="chartSeriesType"></param>
        /// <param name="yAxis1Text"></param>
        /// <returns></returns>
        internal static Chart PlotCustomChart(DataSet chartDataset,
            Chart chart,
            string chartSeriesType,
            string yAxis1Text)
        {
            chart.Use3D = false;
            chart.Visible = true;
            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;
                chart.FileQuality = 100;
                DataEngine objDataEngine = new DataEngine();
                chart.SeriesCollection.Clear();

                objDataEngine.Data = chartDataset;
                dotnetCHARTING.SeriesCollection sc;

                //Set data fields
                objDataEngine.DataFields = "XAxis=" +
                    chartDataset.Tables[0].Columns[1].ColumnName +
                    ",YAxis=" + chartDataset.Tables[0].Columns[2].ColumnName +
                    ",splitBy=" + chartDataset.Tables[0].Columns[0].ColumnName;

                sc = objDataEngine.GetSeries();
                chart.SeriesCollection.Add(sc);
                chart.DataBind();

                //Set axis label texts and styles
                chart.XAxis.Label.Text = chartDataset.Tables[0].Columns[1].ColumnName;
                chart.YAxis.Label.Text = yAxis1Text;     //chartDataset.Tables[0].Columns[2].ColumnName;
                chart.XAxis.DefaultTick.Label.Color = Color.Black;
                chart.XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.YAxis.DefaultTick.Label.Color = Color.Black;
                chart.YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.XAxis.Line.Color = Color.DarkGray;
                chart.XAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.YAxis.Line.Color = Color.DarkGray;
                chart.YAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.DefaultSeries.DefaultElement.Marker.Size = 4;
                if (chartSeriesType.Equals("stacked"))
                {
                    chart.YAxis.Scale = Scale.Stacked;
                    chartSeriesType = "7";
                }

                //Set chart series types
                for (int i = 0; i < chart.SeriesCollection.Count; i++)
                {
                    chart.SeriesCollection[i].Type = (SeriesType)Convert.ToInt32(chartSeriesType);
                }

                if (chartSeriesType == "4")
                {
                    chart.DefaultSeries.DefaultElement.Transparency = 20;
                }
                if (GlobalSettings.AllowAutoYaxisScaleRange)
                {
                    chart.YAxis.Scale = Scale.Range;
                }
                //Set chart styles
                chart.Palette = DMPalette;
                chart.Debug = false;
                chart.Height = 300;
                
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.XAxis.SpacingPercentage = 30;
                chart.ChartArea.ClearColors();
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.ChartArea.Shadow.Color = Color.White;

                //Set legend properties and styles
                
                chart.LegendBox.Template = "%Icon%Name";
                SetStandardLegendStyles(chart);
                chart.DefaultElement.ToolTip = "%SeriesName";
                chart.LegendBox.Position = LegendBoxPosition.Middle;
            }

            return chart;
        }

        internal static void BuildChartArea(dotnetCHARTING.Chart chart, string AxisCriteria)
        {
            chart.NoDataLabel.Text = "";
            chart.ChartArea.GridPosition = new Point(0, 0);
            //Adding ChartArea
            ChartArea chartArea = new ChartArea();
            chart.ExtraChartAreas.Add(chartArea);
            chart.ChartAreaSpacing = 0;
            chartArea.ClearColors();
            chartArea.HeightPercentage = 12;
            chartArea.Label.Text = AxisCriteria;
            chartArea.Label.Alignment = StringAlignment.Near;
            chartArea.GridPosition = new Point(0, 1);
            chart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Custom;
            //chart.TitleBox.ClearColors();
            //chart.TitleBox.Label.Text = AxisCriteria.ToString();
            //chart.TitleBox.Label.Color = Color.Gray;
            
        }

        internal static Chart AddCopyrightText(Chart chart, int position)
        {  
            if (GlobalSettings.ShowCopyrightTextOnChart)
            {
                if (chart.ExtraChartAreas.Count > 0)
                {
                    chart.ExtraChartAreas[0].HeightPercentage = 14;
                    chart.ExtraChartAreas[0].Label.Text = chart.ExtraChartAreas[0].Label.Text + "\n" + GlobalSettings.CopyrightText;
                }
                else
                {
                    BuildChartArea(chart, GlobalSettings.CopyrightText);
                    chart.ExtraChartAreas[0].HeightPercentage = 17;
                }
            }
            return chart;
        }       
    }
}
