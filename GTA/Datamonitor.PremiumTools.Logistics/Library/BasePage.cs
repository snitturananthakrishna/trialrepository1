using System;
using System.Data;
using System.Text;
using System.Web;
using System.Xml;
using System.Collections.Generic;
using System.Web.UI;
using System.Configuration;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Library
{   
    /// <summary>
    /// The Base page for all pages in the apllication
    /// </summary>
    public class BasePage : Page
    {
        #region Private fields
        private string _pageTitle;
        #endregion

        #region Protected properties
        /// <summary>
        /// Property used to set the custom title of the page. By default the page title will be the site's title e.g. "Datamonitor".
        /// If you wish to append to this title then set this property and the page title will become "{Site title} - {CustomPageTitle}".
        /// </summary>
        protected string CustomPageTitle
        {
            get { return this._pageTitle; }
            set { this._pageTitle = value; }
        }

        /// <summary>
        /// Gets the current page title, taking into account the current value of
        /// the CustomPageTitle property.
        /// </summary>
        protected string PageTitle
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(GlobalSettings.SiteTitle);
                if (!string.IsNullOrEmpty(this.CustomPageTitle))
                {
                    sb.Append(" - ");
                    sb.Append(this.CustomPageTitle);
                }
                return sb.ToString();
            }
        }

        #endregion

        #region Protected virtual methods
        protected override void OnPreRender(EventArgs e)
        {
            if (this.Page.Header != null)
            {
                // Set Custom Page Title
                this.Title = this.PageTitle;
            }
        }
        #endregion

        /// <summary>
        /// Gets the filter criteria from session.
        /// </summary>
        /// <returns></returns>
        protected static string GetFilterCriteriaWithForcedTaxonomies()
        {
            string SelectionsXML = string.Empty;
            string UniqueSelectedTaxonomyIds = GetUniqueSelectedTaxonomyIdsFromSession();

            //Get forced taxonomies
            Dictionary<string, string> ForcedDependencies = Datamonitor.PremiumTools.Generic.Library.SqlDataAccess.SqlDataService.GetForcedDependencies(UniqueSelectedTaxonomyIds);
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyTypes();
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
                {
                    Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                    if (selectedIDsList != null && selectedIDsList.Count > 0)
                    {                           
                        //convert int array to string array
                        string SelectedTaxonomyIDs = "";
                        string SelectedParentTaxonomyIDs = "";
                        foreach (KeyValuePair<int, string> selectedID in selectedIDsList)
                        {
                            if (selectedID.Value.Contains("(All)"))
                            {
                                SelectedParentTaxonomyIDs += selectedID.Key.ToString() + ",";
                            }
                            else
                            {
                                SelectedTaxonomyIDs += selectedID.Key.ToString() + ",";
                            }
                        }
                       
                        //Include forced dependency ids if exist
                        if(ForcedDependencies.ContainsKey(taxonomyRow["ID"].ToString()))
                        {
                            SelectedTaxonomyIDs += ForcedDependencies[taxonomyRow["ID"].ToString()];
                        }
                        //Build selections xml
                        SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' parentSelectionIDs='{2}' columnID='{3}' />",
                            taxonomyRow["ID"].ToString(),
                            SelectedTaxonomyIDs.TrimEnd(','),
                            SelectedParentTaxonomyIDs.TrimEnd(','),
                            taxonomyRow["ColumnRowID"].ToString()); 
                    }
                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);              
            }
            return SelectionsXML;
        }

        /// <summary>
        /// Gets the unique selected taxonomy ids from session.
        /// </summary>
        /// <returns></returns>
         protected static string GetUniqueSelectedTaxonomyIdsFromSession()
        {
            string UniqueSelectedTaxonomyIds = string.Empty;
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyTypes();
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
                {
                    Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                    if (selectedIDsList != null && selectedIDsList.Count > 0)
                    {
                        //Get all keys (ids) from dictionary
                        List<int> keys = new List<int>(selectedIDsList.Keys);
                        //convert int array to string array                        
                        string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                        //Build Unique Selected Taxonomy Ids
                        UniqueSelectedTaxonomyIds = UniqueSelectedTaxonomyIds.Length > 0 ?
                            UniqueSelectedTaxonomyIds + "," + string.Join(",", SelectedIDs) :
                            string.Join(",", SelectedIDs);
                    }
                }
            }
            return UniqueSelectedTaxonomyIds;
        }

        /// <summary>
        /// Gets the filter criteria from session.
        /// </summary>
        /// <returns></returns>
        protected static string GetFilterCriteriaFromSession()
        {
            string SelectionsXML = string.Empty;           
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyTypes();
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
                {
                    Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                    if (selectedIDsList != null && selectedIDsList.Count > 0)
                    {
                        //convert int array to string array
                        string SelectedTaxonomyIDs = "";
                        string SelectedParentTaxonomyIDs = "";
                        foreach (KeyValuePair<int, string> selectedID in selectedIDsList)
                        {
                            if (selectedID.Value.Contains("(All)"))
                            {
                                SelectedParentTaxonomyIDs += selectedID.Key.ToString() + ",";
                            }
                            else
                            {
                                SelectedTaxonomyIDs += selectedID.Key.ToString() + ",";
                            }
                        }
                        //Build selections xml
                        SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' parentSelectionIDs='{2}' columnID='{3}' />",
                            taxonomyRow["ID"].ToString(),
                            SelectedTaxonomyIDs.TrimEnd(','),
                            SelectedParentTaxonomyIDs.TrimEnd(','),
                            taxonomyRow["ColumnRowID"].ToString()); 
                      
                    }
                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }
            return SelectionsXML;
        }        

        /// <summary>
        /// Gets the selected years fromsession.
        /// </summary>
        /// <returns></returns>
        protected static string GetSelectedYearsFromsession()
        {
            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
            int EndYear = CurrentSession.GetFromSession<int>("EndYear");

            string TaxonomySelectedValue = "[Y" + StartYear.ToString() + "]";
            while (EndYear > StartYear)
            {
                StartYear++;
                TaxonomySelectedValue += ", [Y" + StartYear + "]";
            }
            return TaxonomySelectedValue;
        }

        /// <summary>
        /// Gets the column ID by name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected static string GetColumnIDByName(string name)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath(GlobalSettings.TaxonomyTypesConfigPath));
            string xPathExpression = "//TaxonomyType[@ColumnName='" + name + "']";
            XmlElement viewItem = (XmlElement)doc.SelectSingleNode(xPathExpression);
            string ID;
            ID = viewItem != null ? viewItem.Attributes["ID"].Value : string.Empty;

            return ID;

        }

        /// <summary>
        /// Gets thecolumn row ID by name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        protected static string GetColumnRowIDByName(string name)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath(GlobalSettings.TaxonomyTypesConfigPath));
            string xPathExpression = "//TaxonomyType[@ColumnName='" + name + "']";
            XmlElement viewItem = (XmlElement)doc.SelectSingleNode(xPathExpression);
            string ID;
            ID = viewItem != null ? viewItem.Attributes["ColumnRowID"].Value : string.Empty;

            return ID;

        }

        protected static string GetColumnRowIDById(string id)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath(GlobalSettings.TaxonomyTypesConfigPath));
            string xPathExpression = "//TaxonomyType[@ID='" + id + "']";
            XmlElement viewItem = (XmlElement)doc.SelectSingleNode(xPathExpression);
            string ID;
            ID = viewItem != null ? viewItem.Attributes["ColumnRowID"].Value : string.Empty;

            return ID;

        }

        /// <summary>
        /// Gets the predefined view's selectable Ids.
        /// </summary>
        /// <param name="viewID">The view ID.</param>
        /// <returns></returns>
        protected static string GetPredefinedViewSelectableIDs(string viewID)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(HttpContext.Current.Server.MapPath(GlobalSettings.PredefinedViewsConfigPath));
            string xPathExpression = "//view[@id='" + viewID + "']";
            XmlElement viewItem = (XmlElement)doc.SelectSingleNode(xPathExpression);
            string SelectableTaxonomyTypes;
            SelectableTaxonomyTypes = viewItem != null ? viewItem.Attributes["selectableTaxonomies"].Value : string.Empty;


            return SelectableTaxonomyTypes;
        }

        /// <summary>
        /// Gets the column row ids by taxonomy ids.
        /// </summary>
        /// <param name="taxonomyIds">The taxonomy ids.</param>
        /// <returns></returns>
        protected static List<string> GetColumnRowIdsbyTaxonomyIds(string taxonomyIds)
        {
            List<string> Ids = new List<string>(taxonomyIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            List<string> ColumnrowIDs = new List<string>();
            foreach (string ID in Ids)
            {
                ColumnrowIDs.Add(GetColumnRowIDById(ID));
            }
            return ColumnrowIDs;
        }

        /// <summary>
        /// Gets the filter criteria from session for savedsearches.
        /// </summary>
        /// <returns></returns>
        public static string GetFilterCriteriaWithForcedTaxonomiesForSavedSearches()
        {
            string SavedSearchSelectionsXML = string.Empty;

            string UniqueSelectedTaxonomyIds = GetUniqueSelectedTaxonomyIdsFromSession();

            //Get forced taxonomies
            Dictionary<string, string> ForcedDependencies = Datamonitor.PremiumTools.Generic.Library.SqlDataAccess.SqlDataService.GetForcedDependencies(UniqueSelectedTaxonomyIds);
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyTypes();
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
                {
                    Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                    if (selectedIDsList != null && selectedIDsList.Count > 0)
                    {
                        //Get all keys (ids) from dictionary
                        //List<int> keys = new List<int>(selectedIDsList.Keys);                        
                        ////convert int array to string array
                        //string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });                        
                        //string SelectedTaxonomyIDs = string.Join(",", SelectedIDs);

                        //List<string> values = new List<string>(selectedIDsList.Values);
                        //string[] SelectedTaxonomyIDsText = Array.ConvertAll<string, string>(values.ToArray(), delegate(string value) { return value.ToString(); });
                        //string SelectedTaxonomyTexts = string.Join(",", SelectedTaxonomyIDsText);
                        
                        //Get all keys (ids) from dictionary
                        string SelectedTaxonomyIDs = "";
                        string SelectedParentTaxonomyIDs = "";
                        string SelectedTaxonomyTexts = "";
                        string SelectedParentTaxonomyTexts = "";

                        foreach (KeyValuePair<int, string> selectedID in selectedIDsList)
                        {
                            if (selectedID.Value.Contains("(All)"))
                            {
                                SelectedParentTaxonomyIDs += selectedID.Key.ToString() + ",";
                                SelectedParentTaxonomyTexts += selectedID.Value.ToString() + "|";
                            }
                            else
                            {
                                SelectedTaxonomyIDs += selectedID.Key.ToString() + ",";
                                SelectedTaxonomyTexts += selectedID.Value.ToString() + "|";
                            }
                        }

                        //Include forced dependency ids if exist
                        if (ForcedDependencies.ContainsKey(taxonomyRow["ID"].ToString()))
                        {
                            SelectedTaxonomyIDs += "," + ForcedDependencies[taxonomyRow["ID"].ToString()];
                        }                      
                        //Build selections xml for savedsearches
                        SavedSearchSelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' selectIDsText='{3}' parentSelectionIDs='{4}' parentSelectIDsText='{5}'/>",
                            taxonomyRow["ID"].ToString(),
                            SelectedTaxonomyIDs.TrimEnd(','),
                            taxonomyRow["ColumnRowID"].ToString(),
                            SelectedTaxonomyTexts.TrimEnd('|'),
                            SelectedParentTaxonomyIDs.TrimEnd(','),
                            SelectedParentTaxonomyTexts.TrimEnd('|'));

                    }
                }                
                SavedSearchSelectionsXML = string.Format("<selections>{0}</selections>", SavedSearchSelectionsXML);
            }
            return SavedSearchSelectionsXML;
        }        
    }
}
