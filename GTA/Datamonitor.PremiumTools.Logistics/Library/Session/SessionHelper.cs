using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Datamonitor.PremiumTools.Generic.Library
{
    /// <summary>
    /// Wrapper class to access session data
    /// </summary>
    public static class CurrentSession
    {

        /// <summary>
        /// Gets from session.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static T GetFromSession<T>(string key)
        {
            object obj;

            if (HttpContext.Current.Session != null)
            {
                obj = HttpContext.Current.Session[key];
                if (obj == null)
                {
                    return default(T);
                }
            }
            else
            {
                return default(T);
            }
            return (T)obj;
        }

        /// <summary>
        /// Sets in the session.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public  static void SetInSession<T>(string key, T value)
        {
            if (value == null)
            {
                HttpContext.Current.Session.Remove(key);
            }
            else
            {
                HttpContext.Current.Session[key] = value;
            }
        }

        /// <summary>
        /// Clears each taxonomy type's selections in session
        /// </summary>
        public static void ClearSelectionsFromSession()
        {
            //Get existing taxonomy types
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyLinks();
            //Remove each taxonomy type's selections from session
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                foreach (DataRow TaxonomyType in TaxonomyTypes.Tables[0].Rows)
                {
                    if (TaxonomyType["FullID"].ToString() == TaxonomyType["ID"].ToString())
                    {
                        SetInSession<Dictionary<int, string>>(TaxonomyType["FullID"].ToString(), null);
                    }
                    else
                    {
                        SetInSession<Dictionary<int, string>>(TaxonomyType["ID"].ToString(), null);
                        SetInSession<Dictionary<int, string>>(TaxonomyType["FullID"].ToString(), null);
                    }
                }
            }
            SetInSession<string>("PredefinedViewID", null);
            SetInSession<Dictionary<int, string>>("Groups", null);
            SetInSession<Dictionary<int, string>>("AutoComplete", null);
        }

    }
}
