using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using System.Text;

namespace Datamonitor.PremiumTools.Generic.Library
{
    public class FusionMaps
    {
        /// <summary>
        /// encodes the dataURL before it's served to FusionMaps
        /// If you have parameters in your dataURL, you'll necessarily need to encode it
        /// </summary>
        /// <param name="dataURL">dataURL to be fed to Map</param>
        /// <param name="noCacheStr">Whether to add aditional string to URL to disable caching of data</param>
        /// <returns>Encoded dataURL, ready to be consumed by FusionMaps</returns>
        public static string EncodeDataURL(string dataURL, bool noCacheStr)
        {
            string result = dataURL;
            if (noCacheStr)
            {
                result += (dataURL.IndexOf("?") != -1) ? "&" : "?";
                //Replace : in time with _, as FusionMaps cannot handle : in URLs
                result += "FCCurrTime=" + DateTime.Now.ToString().Replace(":", "_");
            }

            return System.Web.HttpUtility.UrlEncode(result);
        }

        /// <summary>
        /// Generate html code for rendering Map
        /// This function assumes that you've already included the FusionMaps JavaScript class in your page
        /// </summary>
        /// <param name="MapSWF">SWF File Name (and Path) of the Map which you intend to plot</param>
        /// <param name="strURL">If you intend to use dataURL method for this Map, pass the URL as this parameter. Else, set it to "" (in case of dataXML method)</param>
        /// <param name="strXML">If you intend to use dataXML method for this Map, pass the XML data as this parameter. Else, set it to "" (in case of dataURL method)</param>
        /// <param name="MapId">Id for the Map, using which it will be recognized in the HTML page. Each Map on the page needs to have a unique Id.</param>
        /// <param name="MapWidth">Intended width for the Map (in pixels)</param>
        /// <param name="MapHeight">Intended height for the Map (in pixels)</param>
        /// <param name="debugMode">Whether to start the Map in debug mode</param>
        /// <param name="registerWithJS">Whether to ask Map to register itself with JavaScript</param>
        /// /// <param name="transparent">Whether transparent Map (true / false)</param>
        /// <returns>JavaScript + HTML code required to embed a Map</returns>
        private static string RenderMapALL(string MapSWF, string strURL, string strXML, string MapId, string MapWidth, string MapHeight, bool debugMode, bool registerWithJS, bool transparent)
        {

            StringBuilder builder = new StringBuilder();

            //builder.AppendFormat("<!-- START Script Block for Map {0} -->" + Environment.NewLine, MapId);
            //builder.AppendFormat("<div id='{0}Div' >" + Environment.NewLine, MapId);
            //builder.Append("Map." + Environment.NewLine);
            //builder.Append("</div>" + Environment.NewLine);
            builder.Append("<script type=\"text/javascript\">" + Environment.NewLine);
            builder.AppendFormat("var Map_{0} = new FusionMaps(\"{1}\", \"{0}\", \"{2}\", \"{3}\", \"{4}\", \"{5}\");" + Environment.NewLine, MapId, MapSWF, MapWidth, MapHeight, boolToNum(debugMode), boolToNum(registerWithJS));
            if (strXML.Length == 0)
            {
                builder.AppendFormat("Map_{0}.setDataURL(\"{1}\");" + Environment.NewLine, MapId, strURL);
            }
            else
            {
                builder.AppendFormat("Map_{0}.setDataXML(\"{1}\");" + Environment.NewLine, MapId, strXML);
            }

            if (transparent == true)
            {
                builder.AppendFormat("Map_{0}.setTransparent({1});" + Environment.NewLine, MapId, "true");
            }

            builder.AppendFormat("Map_{0}.render(\"{1}Div\");" + Environment.NewLine, MapId, MapId);
            builder.Append("</script>" + Environment.NewLine);
            //builder.AppendFormat("<!-- END Script Block for Map {0} -->" + Environment.NewLine, MapId);
            return builder.ToString();
        }

        /// <summary>
        /// Generate html code for rendering Map
        /// This function assumes that you've already included the FusionMaps JavaScript class in your page
        /// </summary>
        /// <param name="MapSWF">SWF File Name (and Path) of the Map which you intend to plot</param>
        /// <param name="strURL">If you intend to use dataURL method for this Map, pass the URL as this parameter. Else, set it to "" (in case of dataXML method)</param>
        /// <param name="strXML">If you intend to use dataXML method for this Map, pass the XML data as this parameter. Else, set it to "" (in case of dataURL method)</param>
        /// <param name="MapId">Id for the Map, using which it will be recognized in the HTML page. Each Map on the page needs to have a unique Id.</param>
        /// <param name="MapWidth">Intended width for the Map (in pixels)</param>
        /// <param name="MapHeight">Intended height for the Map (in pixels)</param>
        /// <param name="debugMode">Whether to start the Map in debug mode</param>
        /// <param name="registerWithJS">Whether to ask Map to register itself with JavaScript</param>
        /// <param name="transparent">Whether transparent Map (true / false)</param>
        /// <returns>JavaScript + HTML code required to embed a Map</returns>
        public static string RenderMap(string MapSWF, string strURL, string strXML, string MapId, string MapWidth, string MapHeight, bool debugMode, bool registerWithJS, bool transparent)
        {
            return RenderMapALL(MapSWF, strURL, strXML, MapId, MapWidth, MapHeight, debugMode, registerWithJS, transparent);
        }

        ///// <summary>
        ///// Generate html code for rendering Map
        ///// This function assumes that you've already included the FusionMaps JavaScript class in your page
        ///// </summary>
        ///// <param name="MapSWF">SWF File Name (and Path) of the Map which you intend to plot</param>
        ///// <param name="strURL">If you intend to use dataURL method for this Map, pass the URL as this parameter. Else, set it to "" (in case of dataXML method)</param>
        ///// <param name="strXML">If you intend to use dataXML method for this Map, pass the XML data as this parameter. Else, set it to "" (in case of dataURL method)</param>
        ///// <param name="MapId">Id for the Map, using which it will be recognized in the HTML page. Each Map on the page needs to have a unique Id.</param>
        ///// <param name="MapWidth">Intended width for the Map (in pixels)</param>
        ///// <param name="MapHeight">Intended height for the Map (in pixels)</param>
        ///// <param name="debugMode">Whether to start the Map in debug mode</param>
        ///// <param name="registerWithJS">Whether to ask Map to register itself with JavaScript</param>
        ///// <returns>JavaScript + HTML code required to embed a Map</returns>
        //public static string RenderMap(string MapSWF, string strURL, string strXML, string MapId, string MapWidth, string MapHeight, bool debugMode, bool registerWithJS)
        //{
        //    return RenderMap(MapSWF, strURL, strXML, MapId, MapWidth, MapHeight, debugMode, registerWithJS, false);
        //}        

        /// <summary>
        /// Transform the meaning of boolean value in integer value
        /// </summary>
        /// <param name="value">true/false value to be transformed</param>
        /// <returns>1 if the value is true, 0 if the value is false</returns>
        private static int boolToNum(bool value)
        {
            return value ? 1 : 0;
        }
    }
}


