using System;
using System.Data;
using System.Web;
using System.Collections.Generic;
using System.Web.Configuration;

namespace Datamonitor.PremiumTools.Generic.Library
{
    /// <summary>
    /// GlobalSettings class is used to "cache" settings that are used globally and frequently
    /// and are stored in the applications settings and connectionstrings section of the web.config file.
    /// This way the web.config file does not have to be read from disk each time a "Site Wide Setting' needs to be accessed. 
    /// When changes are made to the web.config file, the appdomain will be reset so the static constructor will be run when 
    /// the application restarts and the new values will be retrieved. 
    /// </summary>
    public static class GlobalSettings
    {
        /// <summary>
        /// Gets the taxonomy types.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetTaxonomyTypes()
        {
            DataSet TaxonomyTypes = new DataSet();
            //Read valid taxonomy types information from config file
            TaxonomyTypes.ReadXml(HttpContext.Current.Server.MapPath(TaxonomyTypesConfigPath));
            
            return TaxonomyTypes;
        }


        /// <summary>
        /// Gets the taxonomy links.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetTaxonomyLinks()
        {
            DataSet TaxonomyTypes = new DataSet();            
            TaxonomyTypes.ReadXml(HttpContext.Current.Server.MapPath(TaxonomyLinksConfigPath));            
            return TaxonomyTypes;
        }

        /// <summary>
        /// Gets the taxonomy defaults.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetTaxonomyDefaults()
        {
            DataSet TaxonomyDefaults = new DataSet();
            //Read valid taxonomy types information from config file
            TaxonomyDefaults.ReadXml(HttpContext.Current.Server.MapPath(TaxonomyDefaultsConfigPath));

            return TaxonomyDefaults;
        }

        /// <summary>
        /// Gets the predefined views config path.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetPredefinedViews()
        {
            DataSet PredefinedViews = new DataSet();
            //Read valid taxonomy types information from config file
            PredefinedViews.ReadXml(HttpContext.Current.Server.MapPath(PredefinedViewsConfigPath));

            return PredefinedViews;
        }

        /// <summary>
        /// Gets the taxonomy types config path.
        /// </summary>
        /// <value>The taxonomy types config path.</value>
        public static string TaxonomyTypesConfigPath
        {
            get { return WebConfigurationManager.AppSettings.Get("TaxonomyTypesConfigPath").ToString(); }
        }

        public static string TaxonomyLinksConfigPath
        {
            get { return WebConfigurationManager.AppSettings.Get("TaxonomyLinksConfigPath").ToString(); }
        }

        /// <summary>
        /// Gets the taxonomy defaults config path.
        /// </summary>
        /// <value>The taxonomy defaults config path.</value>
        public static string TaxonomyDefaultsConfigPath
        {
            get { return WebConfigurationManager.AppSettings.Get("TaxonomyDefaultsConfigPath").ToString(); }
        }

        /// <summary>
        /// Gets the chart filter options config path.
        /// </summary>
        /// <value>The chart filter options config path.</value>
        public static string ChartFilterOptionsConfigPath
        {
            get { return WebConfigurationManager.AppSettings.Get("ChartFilterOptionsConfigPath").ToString(); }
        }

        /// <summary>
        /// Gets the display text filters config path.
        /// </summary>
        /// <value>The display text filters config path.</value>
        public static string DisplayTextFiltersConfigPath
        {
            get { return WebConfigurationManager.AppSettings.Get("DisplayTextFiltersConfigPath").ToString(); }
        }

        /// <summary>
        /// Gets the predefined views config path.
        /// </summary>
        /// <value>The predefined views config path.</value>
        public static string PredefinedViewsConfigPath
        {
            get { return WebConfigurationManager.AppSettings.Get("PredefinedViewsConfigPath").ToString(); }
        }

        /// <summary>
        /// Gets the default taxonomy type id of the X axis.
        /// </summary>
        /// <value>The default taxonomy type id of X axis.</value>
        public static string DefaultXAxisType
        {
            get { return WebConfigurationManager.AppSettings.Get("DefaultXAxisType").ToString(); }
        }

        /// <summary>
        /// Gets the default taxonomy id of the Y axis.
        /// </summary>
        /// <value>The default taxonomy id  of the Y axis.</value>
        public static string DefaultYAxisType
        {
            get { return WebConfigurationManager.AppSettings.Get("DefaultYAxisType").ToString(); }
        }

        /// <summary>
        /// Gets the default taxonomy type id of the series.
        /// </summary>
        /// <value>The default taxonomy type id of the series.</value>
        public static string DefaultSeriesType
        {
            get { return WebConfigurationManager.AppSettings.Get("DefaultSeriesType").ToString(); }
        }

        /// <summary>
        /// Gets the default start year
        /// </summary>
        /// <value>The start year default.</value>
        public static string StartYearDefault
        {
            get { return WebConfigurationManager.AppSettings.Get("StartYearDefault").ToString(); }
        }

        /// <summary>
        /// Gets the default end year
        /// </summary>
        /// <value>The end year default.</value>
        public static string EndYearDefault
        {
            get { return WebConfigurationManager.AppSettings.Get("EndYearDefault").ToString(); }
        }

        /// <summary>
        /// Gets a value indicating whether the taxonomy data is cached.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this taxonomy data is cached; otherwise, <c>false</c>.
        /// </value>
        public static bool IsTaxonomyDataCached
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("CacheTaxonomydata").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets the columns to hide.
        /// </summary>
        /// <value>The columns to hide.</value>
        public static string ColumnsToHide
        {
            get { return WebConfigurationManager.AppSettings.Get("ColumnsToHide").ToString().ToLower(); }
        }

        /// <summary>
        /// Gets the columns to hide intially.
        /// </summary>
        /// <value>The columns to hide intially.</value>
        public static string ColumnsToHideIntially
        {
            get { return WebConfigurationManager.AppSettings.Get("ColumnsToHideIntially").ToString().ToLower(); }
        }

        /// <summary>
        /// Gets the columns to ignore.
        /// </summary>
        /// <value>The columns to ignore.</value>
        public static string ColumnsToIgnore
        {
            get { return WebConfigurationManager.AppSettings.Get("ColumnsToIgnore").ToString().ToLower(); }
        }       

        /// <summary>
        /// Gets the valid years.
        /// </summary>
        /// <value>The valid years.</value>
        public static string[] ValidYears
        {
            get
            {
                List<string> Years = new List<string>();

                int StartYear = Convert.ToInt32(WebConfigurationManager.AppSettings.Get("StartYearDefault"));
                int EndYear = Convert.ToInt32(WebConfigurationManager.AppSettings.Get("EndYearDefault"));

                while (EndYear >= StartYear)
                {
                    Years.Add(StartYear.ToString());
                    StartYear++;
                }

                return Years.ToArray();
            }
        }
        
        /// <summary>
        /// Gets the column ids.
        /// </summary>
        /// <value>The column ids.</value>
        public static Dictionary<string,string> ColumnIds
        {
            get
            {
                 DataSet TaxonomyTypes = GetTaxonomyTypes();
                 Dictionary<string, string> columnIDs = new Dictionary<string,string>();
                 if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
                 {                     
                     foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
                     {
                         columnIDs.Add(taxonomyRow["ColumnRowID"].ToString(), taxonomyRow["ColumnName"].ToString());
                     }
                 }

                 return columnIDs;
            }
        }

        /// <summary>
        /// Gets the copy right information.
        /// </summary>
        /// <returns></returns>        
        public static string GetCopyRightConfigPath
        {
            get { return WebConfigurationManager.AppSettings.Get("CopyRightConfigPath").ToString(); }
        }

        /// <summary>
        /// Gets the width of the get default grid column.
        /// </summary>
        /// <value>The width of the get default grid column.</value>
        public static int GetDefaultGridColumnWidth
        {
            get { return Convert.ToInt32(WebConfigurationManager.AppSettings.Get("DefaultGridColumnwidth")); }
        }

        /// <summary>
        /// Gets the name for extraction file.
        /// </summary>
        /// <returns></returns>        
        public static string GetExtractName
        {
            get { return WebConfigurationManager.AppSettings.Get("ExtractName").ToString(); }
        }

        /// <summary>
        /// Gets the site title.
        /// </summary>
        /// <value>The site title.</value>
        public static string SiteTitle
        {
            get { return WebConfigurationManager.AppSettings.Get("Title").ToString(); }
        }



        /// <summary>
        /// Gets the get extract logo path for DM.
        /// </summary>
        /// <value>The get extract logo path for DM.</value>
        public static string GetExtractLogoPathForDM
        {
            get { return WebConfigurationManager.AppSettings.Get("ExtractLogoPathForDM").ToString(); }
        }

        /// <summary>
        /// Gets the extract logo path for ovum.
        /// </summary>
        /// <value>The extract logo path for ovum.</value>
        public static string ExtractLogoPathForOvum
        {
            get { return WebConfigurationManager.AppSettings.Get("ExtractLogoPathForOvum").ToString(); }
        }

        /// <summary>
        /// Gets the options to be displayed in view result's display text column filter types.
        /// </summary>
        /// <returns></returns>        
        public static string GetDisplayTextFilterTypes
        {
            get { return WebConfigurationManager.AppSettings.Get("DisplayTextFilterTypes").ToString(); }
        }

        /// <summary>
        /// Gets the mandatory taxonomy types list.
        /// </summary>
        /// <returns></returns>        
        public static string GetMandatoryTaxonomyInSearch
        {
            get { return WebConfigurationManager.AppSettings.Get("MandatoryTaxonomyInSearch").ToString(); }
        }

        /// <summary>
        /// Gets the macro economic data minimum year.
        /// </summary>
        /// <returns></returns>        
        public static string GetMacroEconomicDataMinYear
        {
            get { return WebConfigurationManager.AppSettings.Get("MacroEconomicDataMinyear").ToString(); }
        }

        /// <summary>
        /// Gets the macro economic data maximum year.
        /// </summary>
        /// <returns></returns>        
        public static string GetMacroEconomicDataMaxYear
        {
            get { return WebConfigurationManager.AppSettings.Get("MacroEconomicDataMaxYear").ToString(); }
        }

        /// <summary>
        /// Gets a value indicating whether the results grid is of client mode or callback.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this tresults grid is of client mode; otherwise, callback mode<c>false</c>.
        /// </value>
        public static bool EnableManulaPagingInGrid2
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("EnableManulaPagingInGrid2").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets a value indicating whether [include macroeconomic data].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [include macroeconomic data]; otherwise, <c>false</c>.
        /// </value>
        public static bool IncludeMacroeconomicData
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("IncludeMacroeconomicData").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets the default currencyid.
        /// </summary>
        /// <returns></returns>        
        public static string DefaultCurrencyID
        {
            get { return WebConfigurationManager.AppSettings.Get("DefaultCurrencyID").ToString(); }
        }

        /// <summary>
        /// Gets a value indicating whether [to show results view switch links].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [to show results view switch links]; otherwise, <c>false</c>.
        /// </value>
        public static bool ShowAllResultViews
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowAllResultViews").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets the notes link.
        /// </summary>
        /// <value>The notes link.</value>
        public static string NotesLink
        {
            get { return WebConfigurationManager.AppSettings.Get("NotesLink").ToString(); }
        }

        public static bool ShowNotesLink
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether this site is SSO enabled.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this site is SSO enabled; otherwise, <c>false</c>.
        /// </value>
        public static bool IsSSOEnabled
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("SSOEnabled").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets a value indicating whether [show copyright text on chart].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [show copyright text on chart]; otherwise, <c>false</c>.
        /// </value>
        public static bool ShowCopyrightTextOnChart
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowCopyrightTextOnChart").ToString().ToLower()); }
        }


        /// <summary>
        /// Gets the default results page.
        /// </summary>
        /// <returns></returns>        
        public static string DefaultResultsPage
        {
            get { return WebConfigurationManager.AppSettings.Get("DefaultResultsPage").ToString(); }
        }


        /// <summary>
        /// Gets the start year select value
        /// </summary>
        /// <value>The start year select value.</value>
        public static string StartYearSelectValue
        {
            get { return WebConfigurationManager.AppSettings.Get("StartYearSelectValue").ToString(); }
        }

        /// <summary>
        /// Gets the end year select value
        /// </summary>
        /// <value>The end year select value.</value>
        public static string EndYearSelectValue
        {
            get { return WebConfigurationManager.AppSettings.Get("EndYearSelectValue").ToString(); }
        }

        /// <summary>
        /// Gets the maximum rows to be downloaded to excel in  view results
        /// </summary>
        /// <value>The maximum rows to be downloaded to excel.</value>
        public static int ExcelDownloadMaxRows
        {
            get { return Convert.ToInt32(WebConfigurationManager.AppSettings.Get("ExcelDownloadMaxRows").ToString()); }
        }

        /// <summary>
        /// Gets a value indicating whether taxonomy definition is shown in results page or not.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if taxonomy definition is shown in results page; otherwise, <c>false</c>.
        /// </value>
        public static bool ShowTaxonomyDefinitionInResults
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowTaxonomyDefinitionInResults").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets a value indicating whether taxonomy definition is shown in search page or not.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if taxonomy definition is shown in search page; otherwise, <c>false</c>.
        /// </value>
        public static bool ShowTaxonomyDefinitionInSearch
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowTaxonomyDefinitionInSearch").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets the trend chart type.
        /// </summary>
        /// <value>The trend chart type.</value>
        public static string TrendChartType
        {
            get { return WebConfigurationManager.AppSettings.Get("TrendChartType").ToString(); }
        }

        /// <summary>
        /// Gets the trend chart absolute data chart type.
        /// </summary>
        /// <value>The trend chart absolute data chart type.</value>
        public static string TrendChartAbsoluteDataChartType
        {
            get { return WebConfigurationManager.AppSettings.Get("TrendChartAbsoluteDataChartType").ToString(); }
        }

        /// <summary>
        /// Gets the trend chart growth data chart type.
        /// </summary>
        /// <value>The trend chart growth data chart type.</value>
        public static string TrendChartGrowthDataChartType
        {
            get { return WebConfigurationManager.AppSettings.Get("TrendChartGrowthDataChartType").ToString(); }
        }

        /// <summary>
        /// Gets the KC redirect URL.
        /// </summary>
        /// <value>The KC redirect URL.</value>
        public static string KCRedirectUrl
        {
            get { return WebConfigurationManager.AppSettings.Get("KCRedirectUrl"); }
        }

        /// <summary>
        /// Gets the ovum KC redirect URL.
        /// </summary>
        /// <value>The ovum KC redirect URL.</value>
        public static string OvumKCRedirectUrl
        {
            get { return WebConfigurationManager.AppSettings.Get("OvumKCRedirectUrl"); }
        }

        public static string SMTPServer
        {
            get { return WebConfigurationManager.AppSettings.Get("SMTPServer"); }
        }

        /// <summary>
        /// Gets the methodology link.
        /// </summary>
        /// <value>The methodology link.</value>
        public static string MethodologyLink
        {
            get { return WebConfigurationManager.AppSettings.Get("MethodologyLink").ToString(); }
        }

        /// <summary>
        /// Gets a value indicating whether absolute values should allowed in results or not.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if absolute values is allowed; otherwise, <c>false</c>.
        /// </value>
        public static bool AllowAbsoluteValues
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("AllowAbsoluteValues").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets a value indicating whether index to 100 values should allowed in results or not.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if index to 100 is allowed; otherwise, <c>false</c>.
        /// </value>
        public static bool AllowIndexTo100
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("AllowIndexTo100").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets a value indicating whether [allow per capita].
        /// </summary>
        /// <value><c>true</c> if [allow per capita]; otherwise, <c>false</c>.</value>
        public static bool AllowPerCapita
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("AllowPerCapita")); }
        }

        /// <summary>
        /// Gets a value indicating whether growth rates allowed in results or not.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if growth rates is allowed; otherwise, <c>false</c>.
        /// </value>
        public static bool AllowGrowthRates
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("AllowGrowthRates").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets a value indicating whether Measure is displayed in heatmap or not.
        /// </summary>
        public static bool IncludeMeasureInHeatMap
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("IncludeMeasureInHeatMap").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets the alternate results page link.
        /// </summary>
        /// <value>The alternate results page link.</value>
        public static string AlternateResultsPage
        {
            get { return WebConfigurationManager.AppSettings.Get("AlternateResultsPage").ToString(); }
        }

        /// <summary>
        /// Gets the excel freezed values for all columns.
        /// </summary>
        /// <value>The excel freezed values for all columns.</value>
        public static string ExcelFreezedValuesForAllColumns
        {
            get { return WebConfigurationManager.AppSettings.Get("ExcelFreezedValuesForAllColumns").ToString(); }
        }

        /// <summary>
        /// Gets the excel freezed values for display text.
        /// </summary>
        /// <value>The excel freezed values for display text.</value>
        public static string ExcelFreezedValuesForDisplayText
        {
            get { return WebConfigurationManager.AppSettings.Get("ExcelFreezedValuesForDisplayText").ToString(); }
        }

        /// <summary>
        /// Gets the excel auto filter range for all columns.
        /// </summary>
        /// <value>The excel auto filter range for all columns.</value>
        public static string ExcelAutoFilterRangeForAllColumns
        {
            get { return WebConfigurationManager.AppSettings.Get("ExcelAutoFilterRangeForAllColumns").ToString(); }
        }

        /// <summary>
        /// Gets the excel auto filter range for display text.
        /// </summary>
        /// <value>The excel auto filter range for display text.</value>
        public static string ExcelAutoFilterRangeForDisplayText
        {
            get { return WebConfigurationManager.AppSettings.Get("ExcelAutoFilterRangeForDisplayText").ToString(); }
        }

        /// <summary>
        /// Gets a value indicating whether country comparison can be shown or not.
        /// </summary>
        public static bool ShowCountryComparison
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowCountryComparison").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets a value indicating whether country overview can be shown or not.
        /// </summary>
        public static bool ShowCountryOverview
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowCountryOverview").ToString().ToLower()); }
        }

        public static bool ShowQuickSearchPage
        {
            get
            {
                if(string.IsNullOrEmpty(WebConfigurationManager.AppSettings.Get("ShowQuickSearchPage")))
                {
                    return false;
                }
                return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowQuickSearchPage").ToString().ToLower());
            }
        }

        /// <summary>
        /// Gets the country comparison maximum limit
        /// </summary>
        /// <value>The maximum limit of countries to compare.</value>
        public static int CountryComparisonMaxLimit
        {
            get { return Convert.ToInt32(WebConfigurationManager.AppSettings.Get("CountryComparisonMaxLimit").ToString()); }
        }

        /// <summary>
        /// Gets the name of the additional details link column.
        /// </summary>
        /// <value>The name of the additional details link column.</value>
        public static string AdditionalDetailsLinkColumnName
        {
            get { return WebConfigurationManager.AppSettings.Get("AdditionalDetailsLinkColumnName"); }
        }

        /// <summary>
        /// Gets the copyright text.
        /// </summary>
        /// <value>The copyright text.</value>
        public static string CopyrightText
        {
            get { return WebConfigurationManager.AppSettings.Get("CopyrightText"); }
        }

        /// <summary>
        /// Gets a value indicating whether [allow summation].
        /// </summary>
        /// <value><c>true</c> if [allow summation]; otherwise, <c>false</c>.</value>
        public static bool AllowSummation
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("AllowSummation").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets the search page.
        /// </summary>
        /// <value>The search page.</value>
        public static string SearchPage
        {
            get { return WebConfigurationManager.AppSettings.Get("SearchPage"); }
        }

        /// <summary>
        /// Gets the country browser default country ID.
        /// </summary>
        /// <value>The country browser default country ID.</value>
        public static string CountryBrowserDefaultCountryID
        {
            get { return WebConfigurationManager.AppSettings.Get("CountryBrowserDefaultCountryID").ToString(); }
        }

        /// <summary>
        /// Gets the country overview quartle popup year.
        /// </summary>
        /// <value>The country overview quartle popup year.</value>
        public static int CountryOverviewQuartlePopupYear
        {
            get { return Convert.ToInt32(WebConfigurationManager.AppSettings.Get("CountryOverviewQuartlePopupYear").ToString()); }
        }


        /// <summary>
        /// Gets the mandatory taxonomy type Ids.
        /// </summary>
        /// <value>The mandatory taxonomy type Ids.</value>
        public static string MandatoryTaxonomyTypeIDs
        {
            get { return WebConfigurationManager.AppSettings.Get("MandatoryTaxonomyTypeIDs"); }
        }

        /// <summary>
        /// Gets the default taxonomy type row number.
        /// </summary>
        /// <value>The default taxonomy type row number.</value>
        public static int DefaultTaxonomyTypeRowNumber
        {
            get 
            { 
                string StringRowNumber=WebConfigurationManager.AppSettings.Get("DefaultTaxonomyTypeRowNumber");
                int RowNumber = 0;

                if (StringRowNumber.Trim().Length > 0)
                {
                    Int32.TryParse(StringRowNumber, out RowNumber);
                }
                return RowNumber;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [format values in grid].
        /// </summary>
        /// <value><c>true</c> if [format values in grid]; otherwise, <c>false</c>.</value>
        public static bool FormatValuesInGrid
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("FormatValuesInGrid").ToLower()); }
        }


        /// <summary>
        /// Gets the country comparison default year.
        /// </summary>
        /// <value>The country comparison default year.</value>
        public static string CountryComparisonDefaultYear
        {
            get { return WebConfigurationManager.AppSettings.Get("CountryComparisonDefaultYear"); }
        }

        /// <summary>
        /// Gets a value indicating whether [show taxonomy definition in results grid].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [show taxonomy definition in results grid]; otherwise, <c>false</c>.
        /// </value>
        public static bool ShowTaxonomyDefinitionInResultsGrid
        {
            get { return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowTaxonomyDefinitionInResultsGrid").ToString().ToLower()); }
        }

        /// <summary>
        /// Gets the default type of the selection taxonomy.
        /// </summary>
        /// <value>The default type of the selection taxonomy.</value>
        public static string DefaultSelectionTaxonomyType
        {
            get { return WebConfigurationManager.AppSettings.Get("DefaultSelectionTaxonomyType"); }
        }

        /// <summary>
        /// Gets the default selection.
        /// </summary>
        /// <value>The default selection.</value>
        public static string DefaultSelection
        {
            get { return WebConfigurationManager.AppSettings.Get("DefaultSelection"); }
        }

        /// <summary>
        /// Gets the width of the excel freeze column max.
        /// </summary>
        /// <value>The width of the excel freeze column max.</value>
        public static int ExcelFreezeColumnMaxWidth
        {
            get
            {
                if (WebConfigurationManager.AppSettings.Get("ExcelFeezeColumnMaxWidth").Length>0)
                {
                    return Convert.ToInt32(WebConfigurationManager.AppSettings.Get("ExcelFeezeColumnMaxWidth"));
                }
                else
                {
                    return 0;
                }
            }
        }


        /// <summary>
        /// Gets the country browser notes.
        /// </summary>
        /// <value>The country browser notes.</value>
        public static string CountryBrowserNotes
        {
            get { return WebConfigurationManager.AppSettings.Get("CountryBrowserNotes"); }
        }

        /// <summary>
        /// Gets a value indicating whether [allow auto yaxis scale range].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [allow auto yaxis scale range]; otherwise, <c>false</c>.
        /// </value>
        public static bool AllowAutoYaxisScaleRange
        {
            get
            {
                if (WebConfigurationManager.AppSettings.Get("AllowAutoYaxisScaleRange") != null)
                {
                    return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("AllowAutoYaxisScaleRange").ToLower());
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [show display options in country comparison].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [show display options in country comparison]; otherwise, <c>false</c>.
        /// </value>
        public static bool ShowDisplayOptionsInCountryComparison
        {
            get
            {
                if (WebConfigurationManager.AppSettings.Get("ShowDisplayOptionsInCountryComparison") != null)
                {
                    return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowDisplayOptionsInCountryComparison").ToLower());
                }
                return false;
            }
        }

        /// <summary>
        /// Gets the excel template path.
        /// </summary>
        /// <value>The excel template path.</value>
        public static string ExcelTemplatePath
        {
            get { return WebConfigurationManager.AppSettings.Get("ExcelTemplatePath"); }
        }

        /// <summary>
        /// Gets the word template path.
        /// </summary>
        /// <value>The word template path.</value>
        public static string WordTemplatePath
        {
            get { return WebConfigurationManager.AppSettings.Get("WordTemplatePath"); }
        }

        /// <summary>
        /// Gets the powerpoint tempalte path.
        /// </summary>
        /// <value>The powerpoint tempalte path.</value>
        public static string PowerpointTempaltePath
        {
            get { return WebConfigurationManager.AppSettings.Get("PowerpointTempaltePath"); }
        }

        /// <summary>
        /// Gets a value indicating whether [show chart wizard].
        /// </summary>
        /// <value><c>true</c> if [show chart wizard]; otherwise, <c>false</c>.</value>
        public static bool ShowChartWizard
        {
            get
            {
                if (WebConfigurationManager.AppSettings.Get("ShowChartWizard") != null)
                {
                    return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowChartWizard").ToLower());
                }
                return false;
            }
        }


        /// <summary>
        /// Gets the quick search taxonomy links.
        /// </summary>
        /// <returns></returns>
        public static DataSet GetQuickSearchTaxonomyLinks()
        {
            DataSet QuickSearchTaxonomyTypes = new DataSet();
            QuickSearchTaxonomyTypes.ReadXml(HttpContext.Current.Server.MapPath(QuickSearchTaxonomyLinksConfigPath));
            return QuickSearchTaxonomyTypes;
        }

        /// <summary>
        /// Gets the quick search taxonomy links config path.
        /// </summary>
        /// <value>The quick search taxonomy links config path.</value>
        public static string QuickSearchTaxonomyLinksConfigPath
        {
            get { return WebConfigurationManager.AppSettings.Get("QuickSearchTaxonomyLinksConfigPath").ToString(); }
        }

        /// <summary>
        /// Gets the quick search page.
        /// </summary>
        /// <value>The quick search page.</value>
        public static string QuickSearchPage
        {
            get { return WebConfigurationManager.AppSettings.Get("QuickSearchPage").ToString(); }
        }

        /// <summary>
        /// Gets the default search page.
        /// </summary>
        /// <value>The default search page.</value>
        public static string DefaultSearchPage
        {
            get 
            {
                if (WebConfigurationManager.AppSettings.Get("DefaultSearchPage").ToString() == "quicksearch")
                {
                    return QuickSearchPage;
                }
                else
                {
                    return SearchPage;
                }
            }
        }


        /// <summary>
        /// Gets a value indicating whether [allow local currency conversion].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [allow local currency conversion]; otherwise, <c>false</c>.
        /// </value>
        public static bool AllowLocalCurrencyConversion
        {
            get
            {
                if (WebConfigurationManager.AppSettings.Get("AllowLocalCurrencyConversion") != null)
                {
                    return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("AllowLocalCurrencyConversion").ToLower());
                }
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [show ffy column in excel].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [show ffy column in excel]; otherwise, <c>false</c>.
        /// </value>
        public static bool ShowFfyColumnInExcel
        {
            get
            {
                if (WebConfigurationManager.AppSettings.Get("ShowFfyColumnInExcel") != null)
                {
                    return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowFfyColumnInExcel").ToLower());
                }
                return false;
            }
        }

        /// <summary>
        /// Gets the section columns to hide in excel.
        /// </summary>
        /// <value>The section columns to hide in excel.</value>
        public static string SectionColumnsToHideInExcel
        {
            get { return WebConfigurationManager.AppSettings.Get("SectionColumnsToHideInExcel"); }
        }

        /// <summary>
        /// Gets the column no to start formating.
        /// </summary>
        /// <value>The column no to start formating.</value>
        public static int ColumnNoToStartFormating
        {
            get
            {
                if (WebConfigurationManager.AppSettings.Get("ColumnNoToStartFormating").Length > 0)
                {
                    return Convert.ToInt32(WebConfigurationManager.AppSettings.Get("ColumnNoToStartFormating"));
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets the currency exchange rate year.
        /// </summary>
        /// <value>The currency exchange rate year.</value>
        public static int CurrencyExchangeRateYear
        {
            get
            {
                if (WebConfigurationManager.AppSettings.Get("CurrencyExchangeRateYear").Length > 0)
                {
                    return Convert.ToInt32(WebConfigurationManager.AppSettings.Get("CurrencyExchangeRateYear"));
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether [show rank column in sections grid].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [show rank column in sections grid]; otherwise, <c>false</c>.
        /// </value>
        public static bool ShowRankColumnInSectionsGrid
        {
            get
            {
                if (WebConfigurationManager.AppSettings.Get("ShowRankColumnInSectionsGrid") != null)
                {
                    return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowRankColumnInSectionsGrid").ToLower());
                }
                return false;
            }
        }

        /// <summary>
        /// Gets the dependent taxonomy source.
        /// </summary>
        /// <value>The dependent taxonomy source.</value>
        public static string DependentTaxonomySource
        {
            get { return WebConfigurationManager.AppSettings.Get("DependentTaxonomySource"); }
        }

        /// <summary>
        /// Gets the dependent taxonomy types.
        /// </summary>
        /// <value>The dependent taxonomy types.</value>
        public static string DependentTaxonomyTypes
        {
            get { return WebConfigurationManager.AppSettings.Get("DependentTaxonomyTypes"); }
        }

        /// <summary>
        /// Gets a value indicating whether [show country filter link].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [show country filter link]; otherwise, <c>false</c>.
        /// </value>
        public static bool ShowCountryFilterLink
        {
            get
            {
                if (WebConfigurationManager.AppSettings.Get("ShowCountryFilterLink") != null)
                {
                    return Convert.ToBoolean(WebConfigurationManager.AppSettings.Get("ShowCountryFilterLink").ToLower());
                }
                return false;
            }
        }


        /// <summary>
        /// Gets the no of decimals to grid values.
        /// </summary>
        /// <value>The no of decimals to grid values.</value>
        public static int NoOfDecimalsToGridValues
        {
            get
            {
                if (!string.IsNullOrEmpty(WebConfigurationManager.AppSettings.Get("NoOfDecimalsToGridValues")))
                {
                    return Convert.ToInt32(WebConfigurationManager.AppSettings.Get("NoOfDecimalsToGridValues"));
                }
                else
                {
                    return 0;
                }
            }
        }
    }

    
}
