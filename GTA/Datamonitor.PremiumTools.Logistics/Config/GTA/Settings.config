<appSettings>
  <add key="Title" value="Global Telecoms Analyzer" />
  <add key="LandingPage" value="~/results/countryoverview.aspx"/>
  
  <!--Search Page settings -->
  <add key="MandatoryTaxonomyInSearch" value ="Geography,Indicator"/>
  <add key="MandatoryTaxonomyTypeIDs" value ="1,2"/>
  <add key="CacheTaxonomydata" value="false" />
  <add key="Years" value="2008,2009,2010,2011,2012,2013"/>  
  <add key="StartYearDefault" value="2006"/>
  <add key="EndYearDefault" value="2016"/>
  <add key="StartYearSelectValue" value="2006"/>
  <add key="EndYearSelectValue" value="2016"/>
  <add key="IncludeMacroeconomicData" value="true" />
  <add key="MacroEconomicDataMinYear" value="1990"/>
  <add key="MacroEconomicDataMaxYear" value="2020"/>
  <add key="ShowTaxonomyDefinitionInSearch" value="true"/>
  <add key="AllowSummation" value="true"/>
  <add key="SearchPage" value="search1.aspx"/>
  <add key="DefaultTaxonomyTypeRowNumber" value="0"/>
  <add key ="ShowCopyrightTextOnChart" value="true"/>
  <add key ="CopyrightText" value="Source - Ovum"/>
  <add key ="CountryBrowserNotes" value="Click here to access our complete coverage of telecom datasets using multi-select options across categories, countries and year ranges."/>
  <add key ="DefaultSelectionTaxonomyType" value="2"/>
  <add key ="DefaultSelection" value="|2~3221~Total fixed voice line services(All)|2A~3221~Total fixed voice line services(All)|2~3222~Mobile sevices(All)|2I~3222~Mobile sevices(All)|2~3223~Mobile Connections(All)|2I~3223~Mobile Connections(All)|2~3224~Mobile devices(All)|2I~3224~Mobile devices(All)|2~3225~Consumer Services(All)|2J~3225~Consumer Services(All)|2~3226~Enterprise Services(All)|2K~3226~Enterprise Services(All)|2~3227~Wholesale services(All)|2L~3227~Wholesale services(All)|2~3209~Internet access(All)|2F~3209~Internet access(All)|2~3210~Fixed Network(All)|2N~3210~Fixed Network(All)|2~3211~Mobile Network(All)|2O~3211~Mobile Network(All)|2~3214~WAN Components(All)|2P~3214~WAN Components(All)|2~3215~Datacom Components(All)|2Q~3215~Datacom Components(All)|2~3216~FTTX Systems Components(All)|2R~3216~FTTX Systems Components(All)|2~3217~RF cable TV Components(All)|2S~3217~RF cable TV Components(All)|"/>
  <add key="QuickSearchPage" value=""/>
  <add key="DefaultSearchPage" value="search1"/>
	
  <!--Results page settings-->
  <add key="DefaultGridColumnwidth" value="60"/>
  <add key="ShowSourceColumninGrid" value="false"/>  
  <add key="ColumnsToHide" value="|flag|rowid|ffy|comments|conversionid|displayorder|additionaldetailsavailable|definition|taxonomyid|measureid|"/> <!--add in lower case --> 
  <add key="ColumnsToHideIntially" value="|basin_area|field_welltype|indicatortext|company|operator1|"/><!-- in lower case -->  
  <add key="ColumnsToIgnore" value="|disc|on|operator|block|welldepth|productionsystem|rownumber|"/>
  <add key="SelectableTaxonomiesInGrid1" value="1,2,4" />
  <add key="DefaultPageSizeForGrid" value ="5"/>
  <add key="DefaultCurrencyID" value ="3201"/>
  <add key="DefaultResultsPage" value ="results/view2.aspx"/>
  <add key="AlternateResultsPage" value ="results/view3.aspx"/>
  <add key="ShowAllResultViews" value ="false"/>
  <add key="EnableManulaPagingInGrid2" value ="false"/>
  <add key="NotesLink" value="~/config/gta/glossary.pdf"/>
  <add key="MethodologyLink" value="~/config/gta/methodology.htm"/>
  <add key="ExcelDownloadMaxRows" value="1000"/>
  <add key="ShowTaxonomyDefinitionInResults" value="false"/>
  <add key="TrendChartType" value="both"/><!--both/absoluteonly/growthonly-->
  <add key="TrendChartAbsoluteDataChartType" value="auto"/><!--auto/line/bar/area-->
  <add key="TrendChartGrowthDataChartType" value="auto"/><!--auto/line/bar/area-->
  <add key="AllowAbsoluteValues" value="true"/>
  <add key="AllowIndexTo100" value="true"/>
  <add key="AllowGrowthRates" value="false"/>
  <add key="AllowPerCapita" value="false"/>
  <add key="ExcelFreezedValuesForAllColumns" value="10|5"/>
  <add key="ExcelFreezedValuesForDisplayText" value="10|3"/>
  <add key="ExcelAutoFilterRangeForAllColumns" value="A10:E10"/>
  <add key="ExcelAutoFilterRangeForDisplayText" value="A10:C10"/>
  <add key="ShowCountryComparison" value="true"/>
  <add key="ShowCountryOverview" value="true"/>
  <add key="CountryComparisonMaxLimit" value="10"/>
  <add key="AdditionalDetailsLinkColumnName" value=""/>
  <add key="IncludeMeasureinCountryComparisonGrid" value="true"/>
  <add key="FormatValuesInGrid" value="true"/>
  <add key="CountryComparisonDefaultYear" value="2010"/>
  <add key="ShowTaxonomyDefinitionInResultsGrid" value="true"/>
  <add key="ExcelFeezeColumnMaxWidth" value="25"/>
  <add key="AllowAutoYaxisScaleRange" value="true"/>
  <add key="ShowDisplayOptionsInCountryComparison" value="true"/>

	<!-- Country Snapshot page settings-->
  <add key="CountryBrowserDefaultCountryID" value="888"/>
  <add key="CountryOverviewQuartlePopupYear" value="2010"/>
  <add key="SectionColumnsToHideInExcel" value="|rowid|itemid|countryid|sectionid|indentlevel|rounding|indicatorid|definition|"/>
  <add key="ColumnNoToStartFormating" value="5"/>
  <add key="ShowRankColumnInSectionsGrid" value="false"/>
	
  <!-- Analysis(chart) page settings-->
  <add key="DefaultXAxisType" value="999"/>
  <add key="DefaultYAxisType" value="1095"/>
  <add key="DefaultSeriesType" value="1"/>
  <add key="PieChartDefaultXAxisType" value="1"/>  
  <add key="PieChartDefaultSeriesType" value="999"/>  

  <!--Error page settings-->
  <add key="ErrorMailSender" value="gtaerror@datamonitor.com"/>
  <add key="ErrorMailSubject" value="An error occured in GTA Premium tool"/>
  <add key="ErrorLogPath" value="D:\NDBSite\Errors\"/>

  <!--Configuration file paths-->
  <add key="TaxonomyTypesConfigPath" value ="~\Config\GTA\Taxonomy.xml"/>
  <add key="TaxonomyLinksConfigPath" value ="~\Config\GTA\TaxonomyLinks.xml"/>
  <add key="TaxonomyDefaultsConfigPath" value ="~\Config\GTA\TaxonomyDefaults.xml"/>
  <add key="ChartFilterOptionsConfigPath" value ="~\Config\GTA\XYChartFilters.xml"/>
  <add key="DisplayTextFiltersConfigPath" value ="~\Config\GTA\DisplayTextFilters.xml"/>
  <add key="PredefinedViewsConfigPath" value ="~\Config\GTA\PredefinedViews.xml"/>

  <!-- Extraction templates paths -->
  <add key="ExcelTemplatePath" value="~/Templates/ExtractTableExcel_ovum.xls" />
  <add key="WordTemplatePath" value="~/Templates/ExtractFigureWord_ovum.doc" />
  <add key="PowerpointTempaltePath" value="~/Templates/ExtractFigurePowerPoint_ovum.ppt" />
  <add key="CountryOverviewExcelConfigPath" value ="~\Config\GTA\CO.xls"/>
  
  <!-- HeatMap settings-->	
  <add key ="IncludeMeasureInHeatmap" value="true"/>

</appSettings>

