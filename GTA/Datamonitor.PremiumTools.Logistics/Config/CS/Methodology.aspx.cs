using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ComponentArt.Web.UI;
using System.Xml;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Controls;

namespace Datamonitor.PremiumTools.Logistics.Config.CS
{
    public partial class Methodology : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tvForAll.Nodes.Clear();

                LoadData();
            }
        }

        /// <summary>
        /// Gets the treeview data and binds.
        /// </summary>        
        /// <param name="taxonomyTypeID">The taxonomy type id.</param>
        public void LoadData()
        {
            tvForAll.Nodes.Clear();
            DataTable TaxonomyTable;
            DataSet TaxonomyList;
            
            //Get taxonomy


            TaxonomyList = SqlDataService.GetIndicatorMethodologyLinks();
            
            tvForAll.ShowLines = true;


            TaxonomyTable = TaxonomyList.Tables.Count > 0 ? TaxonomyList.Tables[0] : null;

            if (TaxonomyList.Tables[0].Select("parentTaxonomyID is not null").Length > 0)
            {
                TaxonomyList.Relations.Add("NodeRelation", TaxonomyList.Tables[0].Columns["ID"], TaxonomyList.Tables[0].Columns["parentTaxonomyID"]);
            }


            

            foreach (DataRow dbRow in TaxonomyTable.Rows)
            {
                if (dbRow.IsNull("parentTaxonomyID"))
                {
                    ComponentArt.Web.UI.TreeViewNode ParentNode = CreateNode(dbRow["ID"].ToString(),
                        dbRow["Name"].ToString(),
                        dbRow["section"].ToString() + "/" + dbRow["displayName"].ToString(),
                        Convert.ToBoolean(dbRow["isLeafNode"].ToString()),
                        Convert.ToBoolean(dbRow["expandNode"].ToString()),
                        "",
                        Convert.ToBoolean(dbRow["ShowCheckBox"].ToString()));


                    int NodeID = Convert.ToInt32(dbRow["ID"]);


                    bool HasMethodology = dbRow["flag"] != null && dbRow["flag"].ToString().ToLower().Equals("true");
                    ParentNode.Expanded = true;
                    ParentNode.ClientTemplateId = HasMethodology ? "MT" : "NT";
                    PopulateSubTree(dbRow, ParentNode);
                    tvForAll.Nodes.Add(ParentNode);
                }
            }

            if (tvForAll.Nodes.Count == 1)
            {
                //Expand root level node
                tvForAll.Nodes[0].Expanded = true;
            }
            
            //if (Source == "quicksearch")
            //{
            //    tvForAll.ShowLines = false;
            //}
        }

        /// <summary>
        /// Populates the sub tree.
        /// </summary>
        /// <param name="dbRow">The data row.</param>
        /// <param name="node">The node.</param>
        private void PopulateSubTree(DataRow dbRow,
            TreeViewNode node)
        {
            bool CanExpandParentnode = false;
            foreach (DataRow childRow in dbRow.GetChildRows("NodeRelation"))
            {
                //ComponentArt.Web.UI.TreeViewNode childNode = CreateNode(childRow["Text"].ToString(), childRow["ImageUrl"].ToString(), true);
                ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(childRow["ID"].ToString(),
                        childRow["Name"].ToString(),
                        childRow["section"].ToString()+"/"+childRow["displayName"].ToString(),
                        Convert.ToBoolean(childRow["isLeafNode"].ToString()),
                        Convert.ToBoolean(childRow["expandNode"].ToString()),
                        "",
                        Convert.ToBoolean(childRow["ShowCheckBox"].ToString()));

                bool HasMethodology = childRow["flag"] != null && childRow["flag"].ToString().ToLower().Equals("true");

                int NodeID = Convert.ToInt32(childRow["ID"]);

                NodeID = (NodeID < 0) ? NodeID * -1 : NodeID;

                newNode.ClientTemplateId = HasMethodology  ? "MT" : "NT";
                newNode.Checked = node.Checked;
                newNode.Expanded = false;

                //populate sub level tree if child nodes exists                
                PopulateSubTree(childRow, newNode);
                //add current node to parent node
                node.Nodes.Add(newNode);
            }            
        }

        /// <summary>
        /// Creates the node.
        /// </summary>
        /// <param name="nodeID">The id.</param>
        /// <param name="nodeText">The text.</param>
        /// <param name="nodeValue">The value.</param>
        /// <param name="isLeafNode"></param>
        /// <param name="expanded">if set to <c>true</c> [expanded].</param>
        /// <param name="imageUrl">The imageurl.</param>
        /// <param name="showCheckBox"></param>
        /// <returns></returns>
        private ComponentArt.Web.UI.TreeViewNode CreateNode(string nodeID,
            string nodeText,
            string nodeValue,
            bool isLeafNode,
            bool expanded,
            string imageUrl,
            bool showCheckBox)
        {
            ComponentArt.Web.UI.TreeViewNode Node = new ComponentArt.Web.UI.TreeViewNode();
            Node.Text = nodeText;
            Node.ImageUrl = imageUrl;
            Node.Expanded = expanded;
            Node.Value = nodeValue;
            Node.ID = nodeID;
            Node.ShowCheckBox = false;
            //if (!isLeafNode)
            //{
            //    Node.ContentCallbackUrl = "TaxonomySubtreeXml.aspx?id=" + nodeID;
            //}
            return Node;
        }
    }
}
